track_bogus = 0
track_intro_music = 1
track_son_of_dathomir = 2
track_main_theme = 3
track_a_nostalgia_from_far_far_away = 4
track_the_road_to_war = 5
track_a_hope_for_rebellion = 6
track_march_of_brothers = 7
track_alliance_strikes_back = 8
track_battle_loppak_slusk = 9
track_cantina_rag = 10
track_bonus_battle_track = 11
track_bonus_opening_battle = 12
track_chase_ship = 13
track_desert_training = 14
track_final_battle = 15
track_listen_up = 16
track_march_site = 17
track_onderon_battle = 18
track_overture_music = 19
track_sloppiness_battle = 20
track_surprise_on_endor = 21
track_teaser_music = 22
track_walker_gambit = 23
track_yoda_doesnt_need_a_lightsaber = 24


