snd_click = 0
snd_tutorial_1 = 1
snd_tutorial_2 = 2
snd_gong = 3
snd_quest_taken = 4
snd_quest_completed = 5
snd_quest_succeeded = 6
snd_quest_concluded = 7
snd_quest_failed = 8
snd_quest_cancelled = 9
snd_rain = 10
snd_money_received = 11
snd_money_paid = 12
snd_sword_clash_1 = 13
snd_sword_clash_2 = 14
snd_sword_clash_3 = 15
snd_lightsaber_swing = 16
snd_sword_swing = 17
snd_footstep_grass = 18
snd_footstep_wood = 19
snd_footstep_water = 20
snd_footstep_horse = 21
snd_footstep_horse_1b = 22
snd_footstep_horse_1f = 23
snd_footstep_horse_2b = 24
snd_footstep_horse_2f = 25
snd_footstep_horse_3b = 26
snd_footstep_horse_3f = 27
snd_footstep_horse_4b = 28
snd_footstep_horse_4f = 29
snd_footstep_horse_5b = 30
snd_footstep_horse_5f = 31
snd_jump_begin = 32
snd_jump_end = 33
snd_jump_begin_water = 34
snd_jump_end_water = 35
snd_horse_jump_begin = 36
snd_horse_jump_end = 37
snd_horse_jump_begin_water = 38
snd_horse_jump_end_water = 39
snd_release_bow = 40
snd_release_crossbow = 41
snd_throw_javelin = 42
snd_throw_axe = 43
snd_throw_knife = 44
snd_throw_stone = 45
snd_reload_crossbow = 46
snd_reload_crossbow_continue = 47
snd_pull_bow = 48
snd_pull_arrow = 49
snd_arrow_pass_by = 50
snd_bolt_pass_by = 51
snd_javelin_pass_by = 52
snd_stone_pass_by = 53
snd_axe_pass_by = 54
snd_knife_pass_by = 55
snd_bullet_pass_by = 56
snd_incoming_arrow_hit_ground = 57
snd_incoming_bolt_hit_ground = 58
snd_incoming_javelin_hit_ground = 59
snd_incoming_stone_hit_ground = 60
snd_incoming_axe_hit_ground = 61
snd_incoming_knife_hit_ground = 62
snd_incoming_bullet_hit_ground = 63
snd_outgoing_arrow_hit_ground = 64
snd_outgoing_bolt_hit_ground = 65
snd_outgoing_javelin_hit_ground = 66
snd_outgoing_stone_hit_ground = 67
snd_outgoing_axe_hit_ground = 68
snd_outgoing_knife_hit_ground = 69
snd_outgoing_bullet_hit_ground = 70
snd_draw_sword = 71
snd_put_back_sword = 72
snd_draw_greatsword = 73
snd_put_back_greatsword = 74
snd_draw_axe = 75
snd_put_back_axe = 76
snd_draw_greataxe = 77
snd_put_back_greataxe = 78
snd_draw_spear = 79
snd_put_back_spear = 80
snd_draw_crossbow = 81
snd_put_back_crossbow = 82
snd_draw_revolver = 83
snd_put_back_revolver = 84
snd_draw_dagger = 85
snd_put_back_dagger = 86
snd_draw_bow = 87
snd_put_back_bow = 88
snd_draw_shield = 89
snd_put_back_shield = 90
snd_draw_other = 91
snd_put_back_other = 92
snd_body_fall_small = 93
snd_body_fall_big = 94
snd_horse_body_fall_begin = 95
snd_horse_body_fall_end = 96
snd_hit_wood_wood = 97
snd_hit_metal_metal = 98
snd_hit_wood_metal = 99
snd_shield_hit_wood_wood = 100
snd_shield_hit_metal_metal = 101
snd_shield_hit_wood_metal = 102
snd_shield_hit_metal_wood = 103
snd_shield_broken = 104
snd_man_hit = 105
snd_man_die = 106
snd_woman_hit = 107
snd_woman_die = 108
snd_woman_yell = 109
snd_hide = 110
snd_unhide = 111
snd_neigh = 112
snd_gallop = 113
snd_battle = 114
snd_arrow_hit_body = 115
snd_metal_hit_low_armor_low_damage = 116
snd_metal_hit_low_armor_high_damage = 117
snd_metal_hit_high_armor_low_damage = 118
snd_metal_hit_high_armor_high_damage = 119
snd_wooden_hit_low_armor_low_damage = 120
snd_wooden_hit_low_armor_high_damage = 121
snd_wooden_hit_high_armor_low_damage = 122
snd_wooden_hit_high_armor_high_damage = 123
snd_mp_arrow_hit_target = 124
snd_blunt_hit = 125
snd_player_hit_by_arrow = 126
snd_pistol_shot = 127
snd_man_grunt = 128
snd_man_breath_hard = 129
snd_man_stun = 130
snd_man_grunt_long = 131
snd_man_yell = 132
snd_man_warcry = 133
snd_sneak_town_halt = 134
snd_horse_walk = 135
snd_horse_trot = 136
snd_horse_canter = 137
snd_horse_gallop = 138
snd_horse_breath = 139
snd_horse_snort = 140
snd_horse_low_whinny = 141
snd_block_fist = 142
snd_man_hit_blunt_weak = 143
snd_man_hit_blunt_strong = 144
snd_man_hit_pierce_weak = 145
snd_man_hit_pierce_strong = 146
snd_man_hit_cut_weak = 147
snd_man_hit_cut_strong = 148
snd_man_victory = 149
snd_fire_loop = 150
snd_torch_loop = 151
snd_dummy_hit = 152
snd_dummy_destroyed = 153
snd_gourd_destroyed = 154
snd_cow_moo = 155
snd_cow_slaughter = 156
snd_distant_dog_bark = 157
snd_distant_owl = 158
snd_distant_chicken = 159
snd_distant_carpenter = 160
snd_distant_blacksmith = 161
snd_tutorial_fail = 162
snd_your_flag_taken = 163
snd_enemy_flag_taken = 164
snd_flag_returned = 165
snd_team_scored_a_point = 166
snd_enemy_scored_a_point = 167
snd_force_lightning = 168
snd_lightsaber_block_laser = 169
snd_lightsaber_takeout = 170
snd_lightsaber_putaway = 171
snd_lightsaber_idleloop = 172
snd_blaster_shot = 173
snd_blaster_shot_cis = 174
snd_blaster_shot_e5 = 175
snd_blaster_shot_se14 = 176
snd_blaster_shot_se34 = 177
snd_blaster_shot_wrist = 178
snd_blaster_shot_bowcaster = 179
snd_blaster_shot_westarm5 = 180
snd_blaster_shot_dc15 = 181
snd_blaster_shot_dc15s = 182
snd_blaster_shot_dc17 = 183
snd_blaster_shot_dc17pistol = 184
snd_blaster_shot_heavyblaster = 185
snd_blaster_shot_ee3 = 186
snd_Cycler_Rifle = 187
snd_minigun = 188
snd_snipersound = 189
snd_blaster_shot_a280 = 190
snd_blaster_shot_dnsr = 191
snd_blaster_shot_dh17p = 192
snd_blaster_shot_dh17r = 193
snd_blaster_shot_dlt19 = 194
snd_blaster_shot_dlt19x = 195
snd_blaster_shot_e11 = 196
snd_blaster_shot_ld1 = 197
snd_blaster_shot_rv10 = 198
snd_blaster_shot_rt97c = 199
snd_blaster_shot_sforc = 200
snd_blaster_shot_sg4 = 201
snd_blaster_shot_t21 = 202
snd_rocket_launcher_shot = 203
snd_jetpack_start = 204
snd_jetpack_loop = 205
snd_jetpack_end = 206
snd_explode1 = 207
snd_force_healing = 208
snd_keyboard_type = 209
snd_droid_die = 210
snd_droid_hit = 211
snd_droid_grunt = 212
snd_droid_yell = 213
snd_droid_victory = 214
snd_droid_stun = 215
snd_wc_clone = 216
snd_wc_clone_sniper = 217
snd_wc_clone_arc = 218
snd_wc_b1 = 219
snd_wc_b2 = 220
snd_wc_magna = 221
snd_wc_geo = 222
snd_wc_stormtrooper = 223
snd_wc_stormtrooper_officer = 224
snd_wc_rebel = 225
snd_wc_rebel_officer = 226
snd_wc_sith_trooper = 227
snd_wc_sith_officer = 228
snd_wc_tor_trooper = 229
snd_wc_tor_officer = 230
snd_wc_black_sun = 231
snd_wc_jedi = 232
snd_wc_sith = 233
snd_magnaguard_die = 234
snd_magnaguard_hit = 235
snd_magnaguard_grunt = 236
snd_magnaguard_yell = 237
snd_magnaguard_victory = 238
snd_magnaguard_grunt_long = 239
snd_magnaguard_victory = 240
snd_magnaguard_stun = 241
snd_win_tune_republic = 242
snd_win_tune_separatists = 243
snd_lose_tune_republic = 244
snd_lose_tune_separatists = 245
snd_force_offence = 246
snd_beep = 247
snd_magnaguard_swing = 248
snd_gun_sounds_begin = 249
snd_shotgun_sound = 250
snd_sonic_blaster = 251
snd_westar_twin_sound = 252
snd_sonic_blaster = 253
snd_westar_twin_sound = 254
snd_twin_blaster_sound_shot = 255
snd_twin_blaster_boba_shot = 256
snd_gun_sounds_end = 257
snd_ocean_ambiance_distant = 258
snd_plain_ambiance_day = 259
snd_plain_ambiance_night = 260
snd_plain_birds = 261
snd_desert_ambiance = 262
snd_thunder_close = 263
snd_thunder_medium = 264
snd_thunder_far = 265


