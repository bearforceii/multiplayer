from module_constants import *
from ID_factions import *
from header_items import  *
from header_operations import *
from header_triggers import *
#_Sebastian_ begin
from ID_animations import *
from ID_sounds import *
#_Sebastian_ end

#_Sebastian_
# sound and particles are now triggered client side
# additional light saber sounds are now fired server side and automatically synced to the clients

#region Basic stuff

####################################################################################################################
#  Each item record contains the following fields:
#  1) Item id: used for referencing items in other files.
#     The prefix itm_ is automatically added before each item id.
#  2) Item name. Name of item as it'll appear in inventory window
#  3) List of meshes.  Each mesh record is a tuple containing the following fields:
#    3.1) Mesh name.
#    3.2) Modifier bits that this mesh matches.
#     Note that the first mesh record is the default.
#  4) Item flags. See header_items.py for a list of available flags.
#  5) Item capabilities. Used for which animations this item is used with. See header_items.py for a list of available flags.
#  6) Item value.
#  7) Item stats: Bitwise-or of various stats about the item such as:
#      weight, abundance, difficulty, head_armor, body_armor,leg_armor, etc...
#  8) Modifier bits: Modifiers that can be applied to this item.
#  9) [Optional] Triggers: List of simple triggers to be associated with the item.
#  10) [Optional] Factions: List of factions that item can be found as merchandise.
####################################################################################################################

# Some constants for ease of use.
imodbits_none = 0
imodbits_horse_basic = imodbit_swaybacked|imodbit_lame|imodbit_spirited|imodbit_heavy|imodbit_stubborn
imodbits_cloth  = imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick | imodbit_hardened
imodbits_armor  = imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_plate  = imodbit_cracked | imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced
imodbits_shield  = imodbit_cracked | imodbit_battered |imodbit_thick | imodbit_reinforced
imodbits_sword   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered
imodbits_sword_high   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered|imodbit_masterwork
imodbits_axe   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_mace   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_pick   = imodbit_rusty | imodbit_chipped | imodbit_balanced | imodbit_heavy
imodbits_bow = imodbit_cracked | imodbit_bent | imodbit_strong |imodbit_masterwork
imodbits_crossbow = imodbit_cracked | imodbit_bent | imodbit_masterwork
imodbits_missile   = imodbit_bent | imodbit_large_bag
imodbits_thrown   = imodbit_bent | imodbit_heavy| imodbit_balanced| imodbit_large_bag
imodbits_thrown_minus_heavy = imodbit_bent | imodbit_balanced| imodbit_large_bag

imodbits_horse_good = imodbit_spirited|imodbit_heavy
imodbits_good   = imodbit_sturdy | imodbit_thick | imodbit_hardened | imodbit_reinforced
imodbits_bad    = imodbit_rusty | imodbit_chipped | imodbit_tattered | imodbit_ragged | imodbit_cracked | imodbit_bent

#_Sebastian_ begin
light_missile_hit = (ti_on_missile_hit,
	[
		#hit types
    # 0 = world
    # 1 = agent
    # 2 = dynamic prop
    # 3 = world
    # 4 = mission object
    # 8 = friend
    # 9 = neutral agent
    # 10 = under water
		
	  # (store_trigger_param_1, ":agent_id"),
	  (store_trigger_param_2, ":collision_type"),
		
		(neg|multiplayer_is_dedicated_server),#only client side
		(try_begin),
			(this_or_next|eq, ":collision_type", 0),
			(eq, ":collision_type", 3),
			(particle_system_burst_no_sync, "psys_bullet_hit", pos1, 2),
		(else_try),
			(this_or_next|eq, ":collision_type", 2),
			(eq, ":collision_type", 4),
			(particle_system_burst_no_sync, "psys_bullet_hit_objects", pos1, 2),
		(try_end),
	])
#_Sebastian_ end

# Replace winged mace/spiked mace with: Flanged mace / Knobbed mace?
# Fauchard (majowski glaive) 
#endregion

items = [
#region Native items
# item_name, mesh_name, item_properties, item_capabilities, slot_no, cost, bonus_flags, weapon_flags, scale, view_dir, pos_offset
 ["no_item","INVALID ITEM", [("invalid_item",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],

 ["tutorial_spear", "Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 0 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(0 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],
 ["tutorial_club", "Club", [("club",0)], itp_type_one_handed_wpn| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 0 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
 ["tutorial_battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(108)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["tutorial_arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(0,pierce)|max_ammo(20),imodbits_missile],
 ["tutorial_bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|abundance(90)|weapon_length(55)|thrust_damage(0,pierce)|max_ammo(18),imodbits_missile],
 ["tutorial_short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 0 , weight(1)|difficulty(0)|spd_rtng(98) | shoot_speed(49) | thrust_damage(12 ,  pierce  ),imodbits_bow ],
 ["tutorial_crossbow", "Crossbow", [("crossbow",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0 , weight(3)|difficulty(0)|spd_rtng(42)|  shoot_speed(68) | thrust_damage(32,pierce)|max_ammo(1),imodbits_crossbow ],
 ["tutorial_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|difficulty(0)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_missile ],
 ["tutorial_saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 0,abundance(90)|body_armor(3)|difficulty(0)|horse_speed(40)|horse_maneuver(38)|horse_charge(8),imodbits_horse_basic],
 ["tutorial_shield", "Kite Shield", [("shield_kite_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(150),imodbits_shield ],
 ["tutorial_staff_no_attack","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_parry_polearm|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(0,blunt) | thrust_damage(0,blunt),imodbits_none],
 ["tutorial_staff","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(16,blunt) | thrust_damage(16,blunt),imodbits_none],
 ["tutorial_sword", "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(18 , cut) | thrust_damage(15 ,  pierce),imodbits_sword ],
 ["tutorial_axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(4)|difficulty(0)|spd_rtng(91) | weapon_length(108)|swing_damage(19 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

 ["tutorial_dagger","Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(40)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],


 ["horse_meat","Horse Meat", [("raw_meat",0)], itp_type_goods|itp_consumable|itp_food, 0, 12,weight(40)|food_quality(30)|max_ammo(40),imodbits_none],
# Items before this point are hardwired and their order should not be changed!
 ["practice_sword","Practice Sword", [("practice_sword",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_wooden_parry|itp_wooden_attack, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(22,blunt)|thrust_damage(20,blunt),imodbits_none],
 ["heavy_practice_sword","Heavy Practice Sword", [("heavy_practicesword",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_greatsword,
    21, weight(6.25)|spd_rtng(94)|weapon_length(128)|swing_damage(30,blunt)|thrust_damage(24,blunt),imodbits_none],
 ["practice_dagger","Practice Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_no_parry|itp_wooden_attack, itc_dagger|itcf_carry_dagger_front_left, 2,weight(0.5)|spd_rtng(110)|weapon_length(47)|swing_damage(16,blunt)|thrust_damage(14,blunt),imodbits_none],
 ["practice_axe", "Practice Axe", [("hatchet",0)], itp_type_one_handed_wpn| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 24 , weight(2) | spd_rtng(95) | weapon_length(75) | swing_damage(24, blunt) | thrust_damage(0, pierce), imodbits_axe],
 ["arena_axe", "Axe", [("arena_axe",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 137 , weight(1.5)|spd_rtng(100) | weapon_length(69)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["arena_sword", "Sword", [("arena_sword_one_handed",0),("sword_medieval_b_scabbard", ixmesh_carry),], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 243 , weight(1.5)|spd_rtng(99) | weapon_length(95)|swing_damage(22 , blunt) | thrust_damage(20 ,  blunt),imodbits_sword_high ],
 ["arena_sword_two_handed",  "Two Handed Sword", [("arena_sword_two_handed",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 670 , weight(2.75)|spd_rtng(93) | weapon_length(110)|swing_damage(30 , blunt) | thrust_damage(24 ,  blunt),imodbits_sword_high ],
 ["arena_lance",         "Lance", [("arena_lance",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
 90 , weight(2.5)|spd_rtng(96) | weapon_length(150)|swing_damage(20 , blunt) | thrust_damage(25 ,  blunt),imodbits_polearm ],
 ["practice_staff","Practice Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(2.5)|spd_rtng(103) | weapon_length(118)|swing_damage(18,blunt) | thrust_damage(18,blunt),imodbits_none],
 ["practice_lance","Practice Lance", [("joust_of_peace",0)], itp_couchable|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_greatlance, 18,weight(4.25)|spd_rtng(58)|weapon_length(240)|swing_damage(0,blunt)|thrust_damage(15,blunt),imodbits_none],
 ["practice_shield","Practice Shield", [("shield_round_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 20,weight(3.5)|body_armor(1)|hit_points(200)|spd_rtng(100)|shield_width(50),imodbits_none],
 ["practice_bow","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
##                                                     ("hunting_bow",0)],                  itp_type_bow|itp_two_handed|itp_primary|itp_attach_left_hand, itcf_shoot_bow, 4,weight(1.5)|spd_rtng(90)|shoot_speed(40)|thrust_damage(19,blunt),imodbits_none],
 ["practice_crossbow", "Practice Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0, weight(3)|spd_rtng(42)| shoot_speed(68) | thrust_damage(32,blunt)|max_ammo(1),imodbits_crossbow],
 ["practice_javelin", "Practice Javelins", [("javelin",0),("javelins_quiver_new", ixmesh_carry)], itp_type_thrown |itp_primary|itp_next_item_as_melee,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 0, weight(5) | spd_rtng(91) | shoot_speed(28) | thrust_damage(27, blunt) | max_ammo(50) | weapon_length(75), imodbits_thrown],
 ["practice_javelin_melee", "practice_javelin_melee", [("javelin",0)], itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry , itc_staff, 0, weight(1)|difficulty(0)|spd_rtng(91) |swing_damage(12, blunt)| thrust_damage(14,  blunt)|weapon_length(75),imodbits_polearm ],
 ["practice_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(10)|weapon_length(0),imodbits_thrown ],
 ["practice_throwing_daggers_100_amount", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(100)|weapon_length(0),imodbits_thrown ],
# ["cheap_shirt","Cheap Shirt", [("shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 4,weight(1.25)|body_armor(3),imodbits_none],
 ["practice_horse","Practice Horse", [("saddle_horse",0)], itp_type_horse, 0, 37,body_armor(10)|horse_speed(40)|horse_maneuver(37)|horse_charge(14),imodbits_none],
 ["practice_arrows","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_missile],
## ["practice_arrows","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo)], itp_type_arrows, 0, 31,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_none],
 ["practice_bolts","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(49),imodbits_missile],
 ["practice_arrows_10_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(10),imodbits_missile],
 ["practice_arrows_100_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(100),imodbits_missile],
 ["practice_bolts_9_amount","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(9),imodbits_missile],
 ["practice_boots", "Practice Boots", [("boot_nomad_a",0)], itp_type_foot_armor |itp_civilian  | itp_attach_armature,0, 11 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10), imodbits_cloth ],
 ["red_tourney_armor","Red Tourney Armor", [("tourn_armor_a",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["blue_tourney_armor","Blue Tourney Armor", [("mail_shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["green_tourney_armor","Green Tourney Armor", [("leather_vest",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["gold_tourney_armor","Gold Tourney Armor", [("padded_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["red_tourney_helmet","Red Tourney Helmet",[("flattop_helmet",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["blue_tourney_helmet","Blue Tourney Helmet",[("segmented_helm",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["green_tourney_helmet","Green Tourney Helmet",[("hood_c",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["gold_tourney_helmet","Gold Tourney Helmet",[("hood_a",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],

["arena_shield_red", "Shield", [("arena_shield_red",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_blue", "Shield", [("arena_shield_blue",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_green", "Shield", [("arena_shield_green",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_yellow", "Shield", [("arena_shield_yellow",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],

["arena_armor_white", "Arena Armor White", [("arena_armorW_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_red", "Arena Armor Red", [("arena_armorR_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_blue", "Arena Armor Blue", [("arena_armorB_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_green", "Arena Armor Green", [("arena_armorG_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_yellow", "Arena Armor Yellow", [("arena_armorY_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_tunic_white", "Arena Tunic White ", [("arena_tunicW_new",0)], itp_type_body_armor |itp_covers_legs ,0, 47 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_red", "Arena Tunic Red", [("arena_tunicR_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ], 
["arena_tunic_blue", "Arena Tunic Blue", [("arena_tunicB_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ], 
["arena_tunic_green", "Arena Tunic Green", [("arena_tunicG_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_yellow", "Arena Tunic Yellow", [("arena_tunicY_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
#headwear
["arena_helmet_red", "Arena Helmet Red", [("arena_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_blue", "Arena Helmet Blue", [("arena_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_green", "Arena Helmet Green", [("arena_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_yellow", "Arena Helmet Yellow", [("arena_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_white", "Steppe Helmet White", [("steppe_helmetW",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_red", "Steppe Helmet Red", [("steppe_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_blue", "Steppe Helmet Blue", [("steppe_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_green", "Steppe Helmet Green", [("steppe_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_yellow", "Steppe Helmet Yellow", [("steppe_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["tourney_helm_white", "Tourney Helm White", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_red", "Tourney Helm Red", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_blue", "Tourney Helm Blue", [("tourney_helmB",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_green", "Tourney Helm Green", [("tourney_helmG",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_yellow", "Tourney Helm Yellow", [("tourney_helmY",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_red", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_blue", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_green", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_yellow", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],

# A treatise on The Method of Mechanical Theorems Archimedes
 
#This book must be at the beginning of readable books
 ["book_tactics","De Re Militari", [("book_a",0)], itp_type_book, 0, 4000,weight(2)|abundance(100),imodbits_none],
 ["book_persuasion","Rhetorica ad Herennium", [("book_b",0)], itp_type_book, 0, 5000,weight(2)|abundance(100),imodbits_none],
 ["book_leadership","The Life of Alixenus the Great", [("book_d",0)], itp_type_book, 0, 4200,weight(2)|abundance(100),imodbits_none],
 ["book_intelligence","Essays on Logic", [("book_e",0)], itp_type_book, 0, 2900,weight(2)|abundance(100),imodbits_none],
 ["book_trade","A Treatise on the Value of Things", [("book_f",0)], itp_type_book, 0, 3100,weight(2)|abundance(100),imodbits_none],
 ["book_weapon_mastery", "On the Art of Fighting with Swords", [("book_d",0)], itp_type_book, 0, 4200,weight(2)|abundance(100),imodbits_none],
 ["book_engineering","Method of Mechanical Theorems", [("book_open",0)], itp_type_book, 0, 4000,weight(2)|abundance(100),imodbits_none],

#Reference books
#This book must be at the beginning of reference books
 ["book_wound_treatment_reference","The Book of Healing", [("book_c",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],
 ["book_training_reference","Manual of Arms", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],
 ["book_surgery_reference","The Great Book of Surgery", [("book_c",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],

 #other trade goods (first one is spice)
 ["spice","Spice", [("spice_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 880,weight(40)|abundance(25)|max_ammo(50),imodbits_none],
 ["salt","Salt", [("salt_sack",0)], itp_merchandise|itp_type_goods, 0, 255,weight(50)|abundance(120),imodbits_none],


 #["flour","Flour", [("salt_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 40,weight(50)|abundance(100)|food_quality(45)|max_ammo(50),imodbits_none],

 ["oil","Oil", [("oil",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 450,weight(50)|abundance(60)|max_ammo(50),imodbits_none],

 ["pottery","Pottery", [("jug",0)], itp_merchandise|itp_type_goods, 0, 100,weight(50)|abundance(90),imodbits_none],

 ["raw_flax","Flax Bundle", [("raw_flax",0)], itp_merchandise|itp_type_goods, 0, 150,weight(40)|abundance(90),imodbits_none],
 ["linen","Linen", [("linen",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],

 ["wool","Wool", [("wool_sack",0)], itp_merchandise|itp_type_goods, 0, 130,weight(40)|abundance(90),imodbits_none],
 ["wool_cloth","Wool Cloth", [("wool_cloth",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],

 ["raw_silk","Raw Silk", [("raw_silk_bundle",0)], itp_merchandise|itp_type_goods, 0, 600,weight(30)|abundance(90),imodbits_none],
 ["raw_dyes","Dyes", [("dyes",0)], itp_merchandise|itp_type_goods, 0, 200,weight(10)|abundance(90),imodbits_none],
 ["velvet","Velvet", [("velvet",0)], itp_merchandise|itp_type_goods, 0, 1025,weight(40)|abundance(30),imodbits_none],

 ["iron","Iron", [("iron",0)], itp_merchandise|itp_type_goods, 0,264,weight(60)|abundance(60),imodbits_none],
 ["tools","Tools", [("iron_hammer",0)], itp_merchandise|itp_type_goods, 0, 410,weight(50)|abundance(90),imodbits_none],

 ["raw_leather","Hides", [("leatherwork_inventory",0)], itp_merchandise|itp_type_goods, 0, 120,weight(40)|abundance(90),imodbits_none],
 ["leatherwork","Leatherwork", [("leatherwork_frame",0)], itp_merchandise|itp_type_goods, 0, 220,weight(40)|abundance(90),imodbits_none],
 
 ["raw_date_fruit","Date Fruit", [("date_inventory",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 120,weight(40)|food_quality(10)|max_ammo(10),imodbits_none],
 ["furs","Furs", [("fur_pack",0)], itp_merchandise|itp_type_goods, 0, 391,weight(40)|abundance(90),imodbits_none],

 ["wine","Wine", [("amphora_slim",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 220,weight(30)|abundance(60)|max_ammo(50),imodbits_none],
 ["ale","Ale", [("ale_barrel",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 120,weight(30)|abundance(70)|max_ammo(50),imodbits_none],

# ["dry_bread", "wheat_sack", itp_type_goods|itp_consumable, 0, slt_none,view_goods,95,weight(2),max_ammo(50),imodbits_none],
#foods (first one is smoked_fish)
 ["smoked_fish","Smoked Fish", [("smoked_fish",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(110)|food_quality(50)|max_ammo(50),imodbits_none],
 ["cheese","Cheese", [("cheese_b",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 ["honey","Honey", [("honey_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 220,weight(5)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 ["sausages","Sausages", [("sausages",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(10)|abundance(110)|food_quality(40)|max_ammo(40),imodbits_none],
 ["cabbages","Cabbages", [("cabbage",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 30,weight(15)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["dried_meat","Dried Meat", [("smoked_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
 ["apples","Fruit", [("apple_basket",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 44,weight(20)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["raw_grapes","Grapes", [("grapes_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 75,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x2 for wine
 ["raw_olives","Olives", [("olive_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 100,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x3 for oil
 ["grain","Grain", [("wheat_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 30,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],

 ["cattle_meat","Beef", [("raw_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 80,weight(20)|abundance(100)|food_quality(80)|max_ammo(50),imodbits_none],
 ["bread","Bread", [("bread_a",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 50,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["chicken","Chicken", [("chicken",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 95,weight(10)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["pork","Pork", [("pork",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
 ["butter","Butter", [("butter_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 150,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 

 #Would like to remove flour altogether and reduce chicken, pork and butter (perishables) to non-trade items. Apples could perhaps become a generic "fruit", also representing dried fruit and grapes
 # Armagan: changed order so that it'll be easier to remove them from trade goods if necessary.
#************************************************************************************************
# ITEMS before this point are hardcoded into item_codes.h and their order should not be changed!
#************************************************************************************************

# Quest Items

 ["siege_supply","Supplies", [("ale_barrel",0)], itp_type_goods, 0, 96,weight(40)|abundance(70),imodbits_none],
 ["quest_wine","Wine", [("amphora_slim",0)], itp_type_goods, 0, 46,weight(40)|abundance(60)|max_ammo(50),imodbits_none],
 ["quest_ale","Ale", [("ale_barrel",0)], itp_type_goods, 0, 31,weight(40)|abundance(70)|max_ammo(50),imodbits_none],

 
# Tutorial Items

["dummy_horse","Dummy Horse", [("nothing",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(45)|hit_points(150)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(50)|horse_charge(28)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3,fac_kingdom_2]],
["dummy_armour", "Dummy Armour", [("nothing",0)], itp_type_body_armor|itp_covers_legs|itp_merchandise   ,0, 352 , weight(5)|abundance(100)|head_armor(0)|body_armor(26)|leg_armor(7)|difficulty(0) ,imodbits_armor ],
["dummy_weapon", "Dummy Sword", [("nothing",0)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 147 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(94)|swing_damage(28 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ] ,


#endregion

################DO NOT ADD ITEMS BEFORE THIS LINE/MARKO/#######################
#region Sw items
#sw new items

#region jedi stuff
["jedicloth1", "Robe", [("jedicloth1",0)], itp_merchandise| itp_type_body_armor |itp_civilian ,0,
 0 , weight(12)|abundance(100)|head_armor(29)|body_armor(50)|leg_armor(32)|difficulty(0) ,imodbits_cloth,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_jedicloth", ":agent_no")])]],
["armoured_robes", "Battlearmour", [("JediArmoredRobes",0)], itp_merchandise| itp_type_body_armor |itp_civilian ,0,
 500 , weight(18)|abundance(100)|head_armor(31)|body_armor(55)|leg_armor(36)|difficulty(0) ,imodbits_cloth,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_armoured_robes", ":agent_no")])]], 
["simple_robes", "Robe", [("JediRobes",0)], itp_merchandise| itp_type_body_armor |itp_civilian ,0,
 500 , weight(12)|abundance(100)|head_armor(29)|body_armor(50)|leg_armor(32)|difficulty(0) ,imodbits_cloth,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_armoured_robes", ":agent_no")])]],  
["padawan_robes", "Robe", [("PadawanRobes",0)], itp_merchandise| itp_type_body_armor |itp_civilian ,0,
 0 , weight(10)|abundance(100)|head_armor(12)|body_armor(30)|leg_armor(17)|difficulty(0) ,imodbits_cloth,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_padawan_robes", ":agent_no")])]],  
["sith_hood_black", "Black Hood", [("sith_hood",0),("sith_hood_icon", ixmesh_inventory)], itp_type_head_armor|itp_merchandise| itp_attach_armature   ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["simple_hood", "Hood", [("JediArmoredHood",0),("sith_hood_icon", ixmesh_inventory)], itp_type_head_armor|itp_merchandise|itp_covers_hair|itp_attach_armature   ,0, 100 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0) ,imodbits_cloth,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_armoured_robes", ":agent_no")])]],

#wooden = laser
["lightsaber_2_ai", "Green Lightsaber", [("lightsaber_green_ai",0)], itp_wooden_attack|itp_wooden_parry|itp_no_pick_up_from_ground|itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_lightsaber_twohanded,
 500 , weight(6)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(70 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["lightsaber_3_ai", "Red Lightsaber", [("lightsaber_red_ai",0)], itp_wooden_attack|itp_no_pick_up_from_ground|itp_wooden_parry|itp_type_two_handed_wpn| itp_two_handed|itp_primary, itc_lightsaber_twohanded,
 500 , weight(6)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(70 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_  
#["lightsaber_ai", "Red Lightsaber", [("lightsaber_sleek",0)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itcf_carry_sword_left_hip|itp_two_handed|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded,
# 500 , weight(3)|difficulty(8)|spd_rtng(93) | weapon_length(99)|swing_damage(70 , cut) | thrust_damage(64 ,  pierce),imodbits_sword_high,
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["lightsaber_sleek", "Sleekphantom's Legendary Lightsaber", [("lightsaber_sleek",0)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itcf_carry_sword_left_hip|itp_two_handed|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded,
# 500 , weight(3)|difficulty(8)|spd_rtng(93) | weapon_length(99)|swing_damage(100 , cut) | thrust_damage(70 ,  pierce),imodbits_sword_high,
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_red", "Red Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn|itp_two_handed|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_red_off", "Red Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt2_red", "Red Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_onehanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
   [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt2_red_off", "Red Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt3_red", "Red Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_can_penetrate_shield|itp_bonus_against_shield| itp_primary|itp_next_item_as_melee, itc_lightsaber_standard|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt3_red_off", "Red Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt_throw1_red_thrown", "Red Lightsaber Throwable", [("hilt2",0),("lightsaber_red_thrown", ixmesh_flying_ammo)], itp_type_thrown |itp_wooden_attack|itp_wooden_parry|itp_primary|itp_next_item_as_melee|itp_ignore_friction|itp_no_pick_up_from_ground,itcf_throw_stone|itc_parry_onehanded, 900, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(60,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy, 
[(ti_on_missile_hit,[(multiplayer_is_server),(store_trigger_param_1, ":agent"),
					 (call_script, "script_lightsaber_throw_trigger", "itm_hilt_throw1_red_thrown", ":agent", "spr_sw_lightsaber_blade_red")])]],
["hilt_throw1_red", "Red Lightsaber Throwable", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary, itc_lightsaber_onehanded,
 900 , weight(3)|difficulty(8)|spd_rtng(83) | weapon_length(99)|swing_damage(100 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high ],
 
["hilt4_red", "Red Double-Bladed Lightsaber", [("hilt4",0)], itp_wooden_attack|itp_wooden_parry|itp_type_one_handed_wpn|itp_primary|itp_next_item_as_melee|itp_can_penetrate_shield|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced, itc_staff|itcf_carry_sword_left_hip,
 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(140)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_axe , 
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt4_red_off", "Red Double-Bladed Lightsaber", [("hilt4",0)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  blunt),imodbits_sword_high ],  
 
 
["hilt1_blue", "Blue Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itp_two_handed|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_blue_off", "Blue Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt2_blue", "Blue Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_onehanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt2_blue_off", "Blue Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt3_blue", "Blue Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_can_penetrate_shield|itp_bonus_against_shield| itp_primary|itp_next_item_as_melee, itc_lightsaber_standard|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt3_blue_off", "Blue Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt_throw1_blue_thrown", "Blue Lightsaber Throwable", [("hilt2",0),("lightsaber_blue_thrown", ixmesh_flying_ammo)], itp_type_thrown |itp_wooden_attack|itp_wooden_parry|itp_primary|itp_next_item_as_melee|itp_ignore_friction|itp_no_pick_up_from_ground,itcf_throw_stone|itc_parry_onehanded, 900, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(60,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy, 
[(ti_on_missile_hit,[(multiplayer_is_server),(store_trigger_param_1, ":agent"),
					 (call_script, "script_lightsaber_throw_trigger", "itm_hilt_throw1_blue_thrown", ":agent", "spr_sw_lightsaber_blade_blue")])]],
["hilt_throw1_blue", "Blue Lightsaber Throwable", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary, itc_lightsaber_onehanded,
 900 , weight(3)|difficulty(8)|spd_rtng(83) | weapon_length(99)|swing_damage(100 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high ],
#["hilt4_blue", "Blue Double-Bladed Lightsaber", [("hilt4",0)], itp_wooden_attack|itp_wooden_parry|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced, itc_staff,
# 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(140)|swing_damage(70 , cut) | thrust_damage(60 ,  blunt),imodbits_axe , 
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["hilt4_blue", "Blue Double-Bladed Lightsaber", [("hilt4",0)], itp_type_polearm|itcf_carry_sword_left_hip|itp_primary, 0,
# 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  blunt),imodbits_sword_high ],

["fpike_red", "Red Lightsaber Pike", [("force_pike",0)], itp_type_polearm|itp_wooden_attack|itp_no_blur|itp_wooden_parry|itp_next_item_as_melee|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_staff|itcf_carry_spear,
 500 , weight(6)|difficulty(8)|spd_rtng(84) | weapon_length(140)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_axe,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["fpike_red_off", "Red Lightsaber Pike", [("force_pike",0)], itp_type_polearm|itp_primary, itcf_carry_spear,
 500 , weight(6)|difficulty(8)|spd_rtng(84) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_axe],  
["fpike_blue", "Blue Lightsaber Pike", [("force_pike",0)], itp_type_polearm|itp_wooden_attack|itp_no_blur|itp_wooden_parry|itp_next_item_as_melee|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_bonus_against_shield|itp_wooden_parry,itc_staff|itcf_carry_spear,
 500 , weight(6)|difficulty(8)|spd_rtng(84) | weapon_length(140)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_axe,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["fpike_blue_off", "Blue Lightsaber Pike", [("force_pike",0)], itp_type_polearm|itp_primary, itcf_carry_spear,
 500 , weight(6)|difficulty(8)|spd_rtng(84) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_axe],   
 
["hilt1_green", "Green Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itp_two_handed|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_green_off", "Green Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt2_green", "Green Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_onehanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt2_green_off", "Green Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt3_green", "Green Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_can_penetrate_shield|itp_bonus_against_shield| itp_primary|itp_next_item_as_melee, itc_lightsaber_standard|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt3_green_off", "Green Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt_throw1_green_thrown", "Green Lightsaber Throwable", [("hilt2",0),("lightsaber_green_thrown", ixmesh_flying_ammo)], itp_type_thrown |itp_wooden_attack|itp_wooden_parry|itp_primary|itp_next_item_as_melee|itp_ignore_friction|itp_no_pick_up_from_ground,itcf_throw_stone|itc_parry_onehanded, 900, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(60,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy, 
[(ti_on_missile_hit,[(multiplayer_is_server),(store_trigger_param_1, ":agent"),
					 (call_script, "script_lightsaber_throw_trigger", "itm_hilt_throw1_green_thrown", ":agent", "spr_sw_lightsaber_blade_green")])]],
["hilt_throw1_green", "Green Lightsaber Throwable", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary, itc_lightsaber_onehanded,
 900 , weight(3)|difficulty(8)|spd_rtng(83) | weapon_length(99)|swing_damage(100 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high ],
#["hilt4_green", "Green Double-Bladed Lightsaber", [("hilt4",0)], itp_wooden_attack|itp_wooden_parry|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced, itc_staff,
# 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(140)|swing_damage(70 , cut) | thrust_damage(60 ,  blunt),imodbits_axe , 
# [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["hilt4_green_off", "Green Double-Bladed Lightsaber", [("hilt4",0)], itp_type_polearm|itcf_carry_sword_left_hip|itp_primary, 0,
# 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  blunt),imodbits_sword_high ],   

["hilt1_purple", "Purple Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itp_two_handed|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_purple_off", "Purple Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt2_purple", "Purple Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_onehanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt2_purple_off", "Purple Lightsaber", [("hilt2",0),("hilt2_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt3_purple", "Purple Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm| itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_standard|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt3_purple_off", "Purple Lightsaber", [("hilt3",0),("hilt3_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
["hilt_throw1_purple_thrown", "Purple Lightsaber Throwable", [("hilt2",0),("lightsaber_purple_thrown", ixmesh_flying_ammo)], itp_type_thrown |itp_wooden_attack|itp_wooden_parry|itp_primary|itp_next_item_as_melee|itp_ignore_friction|itp_no_pick_up_from_ground,itcf_throw_stone|itc_parry_onehanded, 900, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(60,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy, 
[(ti_on_missile_hit,[(multiplayer_is_server),(store_trigger_param_1, ":agent"),
					 (call_script, "script_lightsaber_throw_trigger", "itm_hilt_throw1_green_thrown", ":agent", "spr_sw_lightsaber_blade_purple")])]],
["hilt_throw1_purple", "Purple Lightsaber Throwable", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary, itc_lightsaber_onehanded,
 900 , weight(3)|difficulty(8)|spd_rtng(83) | weapon_length(99)|swing_damage(100 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high ],
#["hilt4_purple", "Purple Double-Bladed Lightsaber", [("hilt4",0)], itp_wooden_attack|itp_wooden_parry|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced, itc_staff,
# 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(140)|swing_damage(70 , cut) | thrust_damage(60 ,  blunt),imodbits_axe , 
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["hilt4_purple_off", "Purple Double-Bladed Lightsaber", [("hilt4",0)], itp_type_polearm|itcf_carry_sword_left_hip|itp_primary, 0,
# 700 , weight(4)|difficulty(9)|spd_rtng(83) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  blunt),imodbits_sword_high ], 

["hilt1_yellow", "Yellow Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itp_two_handed|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_yellow_off", "Yellow Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
#["hilt2_yellow", "Yellow Lightsaber", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary|itp_next_item_as_melee, itc_lightsaber_onehanded,
# 500 , weight(3)|difficulty(8)|spd_rtng(86) | weapon_length(99)|swing_damage(60 , cut) | thrust_damage(51 ,  pierce),imodbits_sword_high,
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["hilt2_yellow_off", "Yellow Lightsaber", [("hilt2",0)], itp_type_polearm|itcf_carry_sword_left_hip|itp_primary, 0,
# 500 , weight(3)|difficulty(8)|spd_rtng(86) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
#["hilt3_yellow", "Yellow Lightsaber", [("hilt3",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm| itp_primary|itp_next_item_as_melee, itc_lightsaber_standard,
# 500 , weight(3)|difficulty(8)|spd_rtng(86) | weapon_length(99)|swing_damage(60 , cut) | thrust_damage(51 ,  pierce),imodbits_sword_high,
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["hilt3_yellow_off", "Yellow Lightsaber", [("hilt3",0)], itp_type_polearm|itcf_carry_sword_left_hip|itp_primary, 0,
# 500 , weight(3)|difficulty(8)|spd_rtng(86) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],
#["hilt_throw1_green_thrown", "Yellow Lightsaber Throwable", [("hilt2",0),("lightsaber_green_thrown", ixmesh_flying_ammo)], itp_type_thrown |itp_wooden_attack|itp_wooden_parry|itp_primary|itp_next_item_as_melee|itp_ignore_friction|itp_no_pick_up_from_ground,itcf_throw_stone|itc_parry_onehanded, 900, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(60,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy, 
#[(ti_on_missile_hit,[(multiplayer_is_server),(store_trigger_param_1, ":agent"),
#					 (call_script, "script_lightsaber_throw_trigger", "itm_hilt_throw1_green_thrown", ":agent", "spr_sw_lightsaber_blade_green")])]],
#["hilt_throw1_green", "Yellow Lightsaber Throwable", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary, itc_lightsaber_onehanded,
# 900 , weight(3)|difficulty(8)|spd_rtng(83) | weapon_length(99)|swing_damage(60 , cut) | thrust_damage(51 ,  pierce),imodbits_sword_high ],  

["hilt1_orange", "Orange Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_two_handed_wpn| itp_two_handed|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary|itp_next_item_as_melee, itc_lightsaber_twohanded|itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(220 , cut) | thrust_damage(186 ,  pierce),imodbits_sword_high,
  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
["hilt1_orange_off", "Orange Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_type_polearm|itp_primary, itcf_carry_sword_left_hip,
 500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(0)|swing_damage(0 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high],

["lightsaber_blade_red_1",  "_", [("lightsaber_blade_red_1",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],
["lightsaber_blade_red_2",  "_", [("lightsaber_blade_red_4",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],
["lightsaber_blade_red_3",  "_", [("lightsaber_blade_red_5",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],
["lightsaber_blade_blue_1",  "_", [("lightsaber_blade_blue_1",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["lightsaber_blade_blue_3",  "_", [("lightsaber_blade_blue_5",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
#["lightsaber_blade_blue_2",  "_", [("lightsaber_blade_blue_4",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["lightsaber_blade_green_1",  "_", [("lightsaber_blade_green_1",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
#["lightsaber_blade_green_2",  "_", [("lightsaber_blade_green_4",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["lightsaber_blade_purple_1",  "_", [("lightsaber_blade_purple_1",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
#["lightsaber_blade_purple_2",  "_", [("lightsaber_blade_purple_4",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["lightsaber_blade_yellow_1",  "_", [("lightsaber_blade_yellow_1",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["lightsaber_blade_orange_1",  "_", [("lightsaber_blade_orange_1",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
#["lightsaber_blade_red_double",  "_", [("lightsaber_blade_red_4",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(30)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],

["hilt_1_red_thrown", "Red Lightsaber", [("hilt1",0),("lightsaber_red_thrown", ixmesh_flying_ammo)], itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,blunt)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy],
["hilt_2_red_thrown", "Red Lightsaber", [("hilt2",0),("lightsaber_red_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_3_red_thrown", "Red Lightsaber", [("hilt3",0),("lightsaber_red_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_1_blue_thrown", "Blue Lightsaber", [("hilt1",0),("lightsaber_blue_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_2_blue_thrown", "Blue Lightsaber", [("hilt2",0),("lightsaber_blue_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_3_blue_thrown", "Blue Lightsaber", [("hilt3",0),("lightsaber_blue_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_1_green_thrown", "Green Lightsaber", [("hilt1",0),("lightsaber_green_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_2_green_thrown", "Green Lightsaber", [("hilt2",0),("lightsaber_green_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["hilt_3_green_thrown", "Green Lightsaber", [("hilt3",0),("lightsaber_green_thrown", ixmesh_flying_ammo)], itp_no_pick_up_from_ground|itp_type_thrown |itp_primary,itcf_throw_axe, 0, weight(3)|difficulty(0)|spd_rtng(99) | shoot_speed(18) | thrust_damage(0,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],

["lightsaber_no_blade",  "_", [("nothing",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],

#["hilt1_blue_dual", "Dual Blue Lightsaber", [("hilt1",0),("hilt1_carry",ixmesh_carry)], itp_wooden_attack|itp_wooden_parry|itp_type_one_handed_wpn| itp_two_handed|itp_primary|itp_next_item_as_melee, itc_staff|itcf_carry_sword_left_hip,
#500 , weight(3)|difficulty(8)|spd_rtng(89) | weapon_length(99)|swing_damage(100 , cut) | thrust_damage(85 ,  pierce),imodbits_sword_high,
#  [(ti_on_weapon_attack, [(store_trigger_param_1, ":agent"),(agent_play_sound, ":agent", "snd_lightsaber_swing")])]],#_Sebastian_
#["hilt1_blue_offhand", "Blue Lightsaber Offhand", [("hilt1_offhand",0)], itp_type_hand_armor|itp_merchandise|itp_primary, itcf_carry_buckler_left, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_sword_high],
#endregion

#region Electrostaffs
["electrostaff",         "Electrostaff", [("electrostaff",0)], itp_wooden_attack|itp_no_pick_up_from_ground|itp_wooden_parry|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced,itc_staff|itcf_carry_spear,
 673 , weight(4.5)|difficulty(10)|spd_rtng(84) | weapon_length(172)|swing_damage(90 , cut) | thrust_damage(48,  pierce),imodbits_axe,
 [
 #(ti_on_init_item, [
 #(neg|multiplayer_is_dedicated_server),#_Sebastian_ only client side
 #(set_position_delta,0,148,0),
 #(particle_system_add_new, "psys_electrostaff"),
 #(particle_system_emit, "psys_electrostaff", 9000000),
 #(set_position_delta,0,-109,0),
 #(particle_system_add_new, "psys_electrostaff"),
 #(particle_system_emit, "psys_electrostaff", 9000000)]),
 (ti_on_weapon_attack, [
 # (multiplayer_is_server),#_Sebastian_ trigger is only fired server side anyway
 (store_trigger_param_1, ":agent"),
 (agent_play_sound, ":agent", "snd_magnaguard_swing")])]],#_Sebastian_
					 
["electrostaff_alt",         "Electrostaff", [("electrostaff",0)], itp_wooden_attack|itp_wooden_parry|itp_no_pick_up_from_ground|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced,itc_nodachi|itcf_carry_spear,
 673 , weight(4.5)|difficulty(10)|spd_rtng(84) | weapon_length(172)|swing_damage(90 , cut) | thrust_damage(0 ,  cut),imodbits_axe,
 [#(ti_on_init_item, [
 #(neg|multiplayer_is_dedicated_server),#_Sebastian_ only client side
 #(set_position_delta,0,148,0),
 #(particle_system_add_new, "psys_electrostaff"),
 #(particle_system_emit, "psys_electrostaff", 9000000),
 #(set_position_delta,0,-109,0),
 #(particle_system_add_new, "psys_electrostaff"),
 #(particle_system_emit, "psys_electrostaff", 9000000)]),
 (ti_on_weapon_attack, [
 # (multiplayer_is_server),#_Sebastian_ trigger is only fired server side anyway
 (store_trigger_param_1, ":agent"),
 (agent_play_sound, ":agent", "snd_magnaguard_swing")])]],#_Sebastian_

#endregion      

#region Jetpacks and personal shield 
["clone_jetpack_blast",  "_", [("clone_jetpack_blast",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],
["jetpack_blast",  "_", [("jetpack_blast",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],
["manda_jetpack_blast",  "_", [("manda_jetpack_blast",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],
["droid_jetpack_blast",  "_", [("droid_jetpack_blast",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],

["personal_shield",  "_", [("personal_shield",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth],

#_Sebastian_ disable
# ["blaster_damage_very_low", "_", [("e5_blaster",0)],  itp_type_musket |itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
# 0 , spd_rtng(160) | shoot_speed(160) | thrust_damage(10 ,pierce)|accuracy(87),imodbits_none],
# ["blaster_damage_low", "_", [("e5_blaster",0)],  itp_type_musket |itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
# 0 , spd_rtng(160) | shoot_speed(160) | thrust_damage(17 ,pierce)|accuracy(87),imodbits_none],
# ["blaster_damage_medium", "_", [("e5_blaster",0)],  itp_type_musket |itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
# 0 , spd_rtng(160) | shoot_speed(160) | thrust_damage(24 ,pierce)|accuracy(87),imodbits_none],
# ["blaster_damage_high", "_", [("e5_blaster",0)],  itp_type_musket |itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
# 0 , spd_rtng(160) | shoot_speed(160) | thrust_damage(32 ,pierce)|accuracy(87),imodbits_none],
# ["blaster_damage_very_high", "_", [("e5_blaster",0)],  itp_type_musket |itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
# 0 , spd_rtng(160) | shoot_speed(160) | thrust_damage(38 ,pierce)|accuracy(87),imodbits_none],
#endregion

#region Armors
["droid_armor",  "Droid Armour", [("battledroid_body",0)], itp_type_body_armor|itp_covers_head |itp_covers_hands ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_battledroid", ":agent_no")])]],
["droid_armor_hardened",  "Hardened Droid Armour", [("battledroid_body_hardened",0)], itp_type_body_armor|itp_covers_head |itp_covers_hands ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_droid_armor_hardened", ":agent_no")])]],
["droid_boots",  "Droid Feet", [("invalid_item",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["droid_helmet", "Droid Head", [("battledroid_head",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_battledroid", ":agent_no")])]],
["droid_helmet_hardened", "Hardened Droid Head", [("battledroid_head_hardened",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_droid_armor_hardened", ":agent_no")])]],
["droid_gloves","Droid Hands", [("invalid_item",0)], itp_merchandise|itp_type_hand_armor,0, 0, weight(0.5)|abundance(100)|body_armor(0)|difficulty(0),imodbits_armor],
["droid_no_head", "Droid Head", [("invalid_item",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
#["droid_helmet_arm", "Droid Arm", [("tactical_arm",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_battledroid", ":agent_no")])]],


["bx_droid_armor",  "BX Droid Armour", [("battledroid_body_bx",0)], itp_type_body_armor|itp_covers_head |itp_covers_hands ,0, 0 , weight(10)|abundance(100)|head_armor(15)|body_armor(36)|leg_armor(17)|difficulty(0) ,imodbits_armor ],
["hk_series",  "HK Droid Armour", [("hk_series",0)], itp_type_body_armor|itp_covers_head|itp_covers_hands ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["hk_series_helmet", "HK Droid Head", [("hk_series_helmet",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["bx_droid_helmet", "Droid Head", [("battledroid_head_bx",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],

["droid_armor_rocket",  "Droid Armour", [("battledroid_body_rocket",0)], itp_type_body_armor  |itp_covers_hands ,0, 0 , weight(30)|abundance(100)|head_armor(5)|body_armor(10)|leg_armor(4)|difficulty(0) ,imodbits_armor ],
["droid_helmet_rocket", "Droid Head", [("battledroid_head_rocket",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["droid_armor_bx_rocket",  "BX Droid Armour", [("battledroid_body_bx_rocket",0)], itp_type_body_armor  |itp_covers_hands ,0, 0 , weight(10)|abundance(100)|head_armor(5)|body_armor(10)|leg_armor(4)|difficulty(0) ,imodbits_armor ],
["droid_armor_commander_rocket",  "Commander Droid Armour", [("battledroid_body_bx_rocket",0)], itp_type_body_armor  |itp_covers_hands ,0, 300 , weight(10)|abundance(100)|head_armor(5)|body_armor(10)|leg_armor(4)|difficulty(0) ,imodbits_armor ],

["super_droid_head", "Super Battle Droid Head", [("nothing",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(16) ,imodbits_plate ],
["super_droid_body",  "Super Battle Droid Body", [("super_battle_droid",0)], itp_type_body_armor|itp_covers_head  |itp_covers_hands ,0, 400 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["super_droid_body_2",  "Super Battle Droid Body", [("B2-Battledroid",0)], itp_type_body_armor|itp_covers_head  |itp_covers_hands ,0, 400 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["super_droid_body_enhanced",  "Enhanced Super Battle Droid Body", [("B2-Battledroid_enhanced",0)], itp_type_body_armor|itp_covers_head  |itp_covers_hands ,0, 700 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
#["super_droid_hands","Super Battle Droid Hands", [("magnaguard_handL",0)], itp_merchandise|itp_type_hand_armor,0, 0, weight(0.5)|abundance(100)|body_armor(5)|difficulty(16),imodbits_armor], #Increased difficulty to prevent 0.35 usage

["magnaguard_helmet", "MagnaGuard Head", [("magnaguard_head_plain",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["magnaguard_body",  "MagnaGuard Body", [("magnaguard_body_plain",0)], itp_type_body_armor  |itp_covers_hands ,0, 1000 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["magnaguard_hands","MagnaGuard Hands", [("magnaguard_handL",0)], itp_merchandise|itp_type_hand_armor,0, 0, weight(0.5)|abundance(100)|body_armor(0)|difficulty(0),imodbits_armor],

["magnaguard_helmet_cloak", "MagnaGuard Head", [("magnaguard_head2",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["magnaguard_body_cloak",  "MagnaGuard Cloak", [("magnaguard_body_cloak",0)], itp_type_body_armor  |itp_covers_hands ,0, 800 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["magnaguard_hands_cloak","MagnaGuard Hands", [("magnaguard_handL",0)], itp_merchandise|itp_type_hand_armor,0, 0, weight(0.5)|abundance(100)|body_armor(0)|difficulty(0),imodbits_armor],

["hands_nothing","Invisible Hands", [("nothing",0)], itp_merchandise|itp_type_hand_armor,0, 0, weight(0.5)|abundance(100)|body_armor(5)|difficulty(0),imodbits_armor],
["boots_nothing", "Invisible Boots", [("nothing",0)], itp_merchandise| itp_type_foot_armor|itp_attach_armature,0,
 174 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],

["clone_armor",  "ARC Trooper Armour", [("ArcTrooper",0)], itp_type_body_armor|itp_replaces_helm|itp_replaces_shoes,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2", ":agent_no")])]],
#["clone_armor_2",  "ARC Trooper Armour(Double pauldron)", [("ArcTrooper2",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(10)|difficulty(0) ,imodbits_armor,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper", ":agent_no")])]], 
#["clone_armor_2_camo",  "ARC Trooper Armour(Double pauldrons, Wood Camo)", [("ArcTrooper2_camo",0)], itp_type_body_armor ,0, 200 , weight(18)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(10)|difficulty(0) ,imodbits_armor,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_camo", ":agent_no")])]],  
#["clone_armor_2_desert",  "ARC Trooper Armour(Double pauldrons, Desert Camo)", [("ArcTrooper2_desert",0)], itp_type_body_armor ,0, 200 , weight(18)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(10)|difficulty(0) ,imodbits_armor,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_desert", ":agent_no")])]],   

["clone_armor_camo",  "ARC Trooper Armour(Wood Camo)", [("ArcTrooper_camo",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_camo", ":agent_no")])]], 

["clone_armor_desert",  "ARC Trooper Armour(Desert Camo)", [("ArcTrooper_desert",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_desert", ":agent_no")])]],  

["clone_armor_plain",  "Clone Armour", [("ArcTrooper_plain",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper", ":agent_no")])]],

["clone_armor_plain_camo",  "Clone Armour(Wood Camo)", [("ArcTrooper_plain_camo",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_camo", ":agent_no")])]], 

["clone_armor_plain_desert",  "Clone Armour(Desert Camo)", [("ArcTrooper_plain_desert",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_desert", ":agent_no")])]],  
#["clone_armor_jetpack",  "Clone Armour", [("ArcTrooper_jetpack",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper", ":agent_no")])]],

["clone_armor_plain_jetpack",  "Clone Armour", [("ArcTrooper_plain_jetpack",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper", ":agent_no")])]],
 ["clone_armor_plain_jetpack_camo",  "Clone Armour(Camo)", [("ArcTrooper_plain_jetpack_camo",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_camo", ":agent_no")])]],
 ["clone_armor_plain_jetpack_desert",  "Clone Armour(Desert)", [("ArcTrooper_plain_jetpack_desert",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_desert", ":agent_no")])]],
["clone_helmet", "Clone Helm", [("ArcTrooperHelm",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm", ":agent_no")])]],

["clone_helmet_arc", "ARC Helm", [("arc_clone_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_helm", ":agent_no")])]],

["clone_helmet_arc_desert", "ARC Helm(Desert)", [("arc_clone_helmet_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_helm_desert", ":agent_no")])]],

["clone_helmet_arc_camo", "ARC Helm(Camo)", [("arc_clone_helmet_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_helm_camo", ":agent_no")])]], 

["clone_helmet_camo", "Clone Helm(Wood Camo)", [("ArcTrooperHelmCamo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm_camo", ":agent_no")])]],

["clone_helmet_desert", "Clone Helm(Desert Camo)", [("ArcTrooperHelmDesert",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm_desert", ":agent_no")])]],

["clonecommando_armor",  "Commando Armour", [("clonecommando_armor",0)], itp_type_body_armor ,0, 300 , weight(10)|abundance(100)|head_armor(15)|body_armor(36)|leg_armor(17)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_commandoarmour", ":agent_no")])]],

["clone_helmet_commander", "Clone Commander Helm", [("ArcTrooperHelm_commander",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm", ":agent_no")])]],
["clone_helmet_commander_camo", "Clone Commander Helm(Camo)", [("ArcTrooperHelm_commander_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm_camo", ":agent_no")])]],
["clone_helmet_commander_desert", "Clone Commander Helm(Desert)", [("ArcTrooperHelm_commander_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm_desert", ":agent_no")])]],
#["clone_phase_ii_armour",  "Clone Armour", [("p2_clone_armour",0)], itp_type_body_armor|itp_covers_head ,0, 300 , weight(25)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(12)|difficulty(0) ,imodbits_armor],
#["arc_phase_ii_armour",  "ARC Armour", [("p2_arc_armour",0)], itp_type_body_armor|itp_covers_head ,0, 300 , weight(25)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(12)|difficulty(0) ,imodbits_armor],

["clone_cold_armor",  "Clone Cold Armour", [("CloneSnowAssaultTrooperSuit",0)], itp_type_body_armor|itp_covers_head ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_CloneSnowAssaultTrooperSuit", ":agent_no")])]],
["clone_cold_helmet", "Clone Cold Helm", [("CloneSnowAssaultTrooperHelmet_pos",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_CloneSnowAssaultTrooperHelmet_pos", ":agent_no")])]],

["clone_arf_helmet", "Advanced Recon Force Helm", [("arftrooper_helm",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arf_helmet", ":agent_no")])]],
["clone_arf_helmet_camo", "Advanced Recon Force Helm(Camo)", [("arftrooper_helm_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arf_helmet_camo", ":agent_no")])]],
["clone_arf_helmet_desert", "Advanced Recon Force Helm(Desert)", [("arftrooper_helm_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arf_helmet_desert", ":agent_no")])]], 

["clone_head", "Clone Head", [("cloneface_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0.1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["clone_head_scar", "Clone Head(Scarred)", [("cloneface_scar_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0.1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],

["clone_scout_helmet", "Clone Scout Helm", [("ScoutTrooper_fin",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["clone_scout_helmet_camo", "Clone Scout Helm(Camo)", [("ScoutTrooper_fin_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["clone_scout_helmet_desert", "Clone Scout Helm(Desert)", [("ScoutTrooper_fin_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],

["clonecommando_helmet", "Commando Helm", [("clonecommando_helm",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_commandohelmet", ":agent_no")])]],

["clone_paratrooper_helmet", "Clone Paratrooper Helm", [("h_rep_sniper_blue",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_paratrooper_helm", ":agent_no")])]],
["clone_paratrooper_helmet_camo", "Clone Paratrooper Helm(Camo)", [("h_rep_sniper_blue_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_paratrooper_helm_camo", ":agent_no")])]],
["clone_paratrooper_helmet_desert", "Clone Paratrooper Helm(Desert)", [("h_rep_sniper_blue_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_paratrooper_helm_desert", ":agent_no")])]],

["clone_commander_helmet", "Clone Commander Helm", [("arc_clone_helmet_commander",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_helm", ":agent_no")])]],
["clone_commander_helmet_camo", "Clone Commander Helm(Camo)", [("arc_clone_helmet_commander_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_helm_camo", ":agent_no")])]],
["clone_commander_helmet_desert", "Clone Commander Helm(Desert)", [("arc_clone_helmet_commander_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper2_helm_desert", ":agent_no")])]],

["clone_sharpshooter_helmet", "Clone Sharpshooter Helm", [("sharpshooter_helm",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm", ":agent_no")])]],
["clone_sharpshooter_helmet_camo", "Clone Sharpshooter Helm(Camo)", [("sharpshooter_helm_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm_camo", ":agent_no")])]],
["clone_sharpshooter_helmet_desert", "Clone Sharpshooter Helm(Desert)", [("sharpshooter_helm_desert",0)], itp_type_head_armor | itp_covers_head  ,0, 200 , weight(2.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_arctrooper_helm_desert", ":agent_no")])]],

["galactic_marine_armour",  "Galactic Marine Armour", [("galacticmarine_armour",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["galactic_marine_helmet", "Galactic Marine Helmet", [("galacticmarine_helmet",0)], itp_type_head_armor | itp_covers_head| itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["wookiee_armour",  "Wookiee Warrior", [("wookiee",0)], itp_type_body_armor|itp_replaces_helm|itp_replaces_shoes|itp_covers_head|itp_covers_hands,0, 0 , weight(16)|abundance(100)|head_armor(26)|body_armor(40)|leg_armor(28)|difficulty(22) ,imodbits_armor],
["wookiee_armour_2",  "Wookiee Chieftain", [("wookiee_body_2",0)], itp_type_body_armor|itp_covers_head|itp_covers_hands,0, 0 , weight(18)|abundance(100)|head_armor(29)|body_armor(50)|leg_armor(32)|difficulty(22) ,imodbits_armor],
["geonosian_armour",  "Geonosian Warrior", [("geonosian_plain",0)], itp_type_body_armor|itp_covers_head|itp_covers_hands,0, 0 , weight(18)|abundance(100)|head_armor(26)|body_armor(55)|leg_armor(35)|difficulty(22) ,imodbits_armor],
["geonosian_armour_wings",  "Geonosian Warrior", [("geonosian",0)], itp_type_body_armor|itp_covers_head|itp_covers_hands,0, 500 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(22) ,imodbits_armor],

["ewok_armour",  "Ewok Warrior", [("ewok",0)], itp_type_body_armor|itp_replaces_helm|itp_replaces_shoes|itp_covers_head|itp_covers_hands,0, 0 , weight(10)|abundance(100)|head_armor(15)|body_armor(28)|leg_armor(10)|difficulty(22) ,imodbits_armor],

["snowtrooper_armour",  "Snowtrooper Armour", [("snowtrooper_armour",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["stormtrooper_armour",  "Stormtrooper Armour", [("stormarmor_v5",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["stormtrooper_armour_camo",  "Stormtrooper Armour(Camo)", [("stormarmor_v5_camo",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["stormtrooper_armour_jetpack",  "Stormtrooper Armour", [("stormarmor_v5_jetpack",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor],
["stormtrooper_armour_jetpack_camo",  "Stormtrooper Armour", [("stormarmor_v5_jetpack_camo",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor],
["stormtrooper_armour_officer",  "Stormtrooper Officer Armour", [("stormarmor_v4",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_stormarmour", ":agent_no")])]],
["stormtrooper_armour_officer_camo",  "Stormtrooper Officer Armour(Camo)", [("stormarmor_v4_camo",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_stormarmour_camo", ":agent_no")])]],
["stormtrooper_helmet_clean", "Stormtrooper Helm", [("storm_helmet_clean",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["stormtrooper_helmet_clean_camo", "Stormtrooper Helm(Camo)", [("storm_helmet_clean_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],

["scouttrooper_armour",  "Scout Trooper Armour", [("scouttrooperarmor",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor], 
["scouttrooper_helmet", "Scout Trooper Helm", [("scouttrooperhelmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],



["rebel_armour",  "Rebel Armour", [("rebel_troop",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_rebel_armour", ":agent_no")])]],
["rebel_armour_jetpack",  "Rebel Armour", [("rebel_troop_jetpack",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_rebel_armour", ":agent_no")])]], 
["rebel_armour_officer",  "Rebel Officer Armour", [("rebel_officer",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_rebel_officer_armour", ":agent_no")])]],
["rebel_helmet", "Rebel Helm", [("rebel_troop_helmet",0)], itp_type_head_armor,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_rebel_helmet", ":agent_no")])]],
["rebel_helmet_officer", "Rebel Officer Helm", [("rebel_officer_helmet",0)], itp_type_head_armor,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_rebel_helmet_officer", ":agent_no")])]], 
["rebel_helmet_forest", "Rebel Forest Helm", [("rebel_helmet_forest",0)], itp_type_head_armor|itp_covers_hair,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["rebel_helmet_snow", "Rebel Snow Helm", [("rebel_snow_helm",0)], itp_type_head_armor|itp_covers_hair,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],

["rebel_snow_armour",  "Rebel Snow Armour", [("rebel_snow_armor",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor], 

["rebel_trooper_vest",  "Rebel Heavy Armour", [("rebel_trooper_vest",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_rebel_heavy_armour", ":agent_no")])]],
["rebel_trooper_helmet", "Rebel Heavy Helm", [("rebel_trooper_helmet",0)], itp_type_head_armor,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],

  
["head_chagrian", "Chagrian Head", [("chagrian",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0) ,imodbits_plate],
["head_keldorian", "Kel Dorian Head", [("keldorian",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_massassi", "Massassi Head", [("massassi",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_feeorin", "Feeorin Head", [("feeorin",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_devaronian", "Devaronian Head", [("devaronian_sith",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_nikto", "Nikto Head", [("kasajjin_nikto",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_weequay", "Weequay Head", [("weequay",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_togruta_1", "Togruta Head", [("togruta_lekku",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_togruta_2", "Togruta Head", [("togruta_lekkub",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_rodian_1", "Rodian Head", [("rodian_a",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_rodian_2", "Rodian Head", [("rodian_b",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_twilek_1", "Twi'lek Head", [("twilek_a",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_twilek_2", "Twi'lek Head", [("twilek_b",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_mon_calamari", "Mon Calamari Head", [("mon_cal_head",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_zabrak", "Zabrak Head", [("zabrak",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],


["head_chagrian_r", "Chagrian Head", [("chagrian",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_keldorian_r", "Kel Dorian Head", [("keldorian",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_feeorin_r", "Feeorin Head", [("feeorin",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_devaronian_r", "Devaronian Head", [("devaronian_sith",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_nikto_r", "Nikto Head", [("kasajjin_nikto",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_weequay_r", "Weequay Head", [("weequay",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_togruta_1_r", "Togruta Head", [("togruta_lekku",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_togruta_2_r", "Togruta Head", [("togruta_lekkub",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_rodian_1_r", "Rodian Head", [("rodian_a",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_rodian_2_r", "Rodian Head", [("rodian_b",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_twilek_1_r", "Twi'lek Head", [("twilek_a",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_twilek_2_r", "Twi'lek Head", [("twilek_b",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_mon_calamari_r", "Mon Calamari Head", [("mon_cal_head",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_zabrak_r", "Zabrak Head", [("zabrak",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_nautolan_r", "Nautolan Head", [("nautolan_head1",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["head_nautolan", "Nautolan Head", [("nautolan_head1",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["def_gloves","Leather Gloves", [("nothing",0)], itp_merchandise|itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(4),imodbits_cloth], ##SUPER IMPORTANT

["mandalorian_helmet", "Mandalorian Helm", [("mandalorian_head",0),("mandalorian_head_icon", ixmesh_inventory)], itp_type_head_armor | itp_covers_head |itp_attach_armature  ,0, 421 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_mandalorian_head", ":agent_no")])]],
["mandalorian_helmet_jetpack", "Mandalorian Helm", [("mandalorian_jetpack_head",0)], itp_type_head_armor | itp_covers_head |itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_mandalorian_head", ":agent_no")])]],
["mandalorian_no_helmet_jetpack", "Jetpack", [("mandalorian_jetpack",0)], itp_type_head_armor | itp_doesnt_cover_hair |itp_attach_armature  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_mandalorian_head", ":agent_no")])]],
["mandalorian_armor",  "Mandalorian Armour", [("mandalorian_body",0)], itp_type_body_armor,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_mandalorian_body", ":agent_no")])]],
 #endregion

["sith_jetpack",  "Sith Jetpack", [("sith_jetpack",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_Sith_trooper_body", ":agent_no")])]],
["Sith_trooper_body",  "Sith Trooper Armour", [("Sith_trooper_body",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_Sith_trooper_body", ":agent_no")])]],
["sith_trooper_helmet", "Sith Trooper Helmet", [("sith_trooper_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_sith_trooper_helmet", ":agent_no")])]],
["sith_acolyte",  "Sith Acolyte Armour", [("sith_acolyte",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(23)|body_armor(45)|leg_armor(25)|difficulty(0) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_sith_acolyte", ":agent_no")])]],
["sith_acolyte_hood",  "Sith Acolyte Hood", [("sith_acolyte_hood",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_sith_acolyte", ":agent_no")])]],
["sith_acolyte2",  "Sith Acolyte Armour", [("sith_acolyte2",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(29)|body_armor(50)|leg_armor(32)|difficulty(0) ,imodbits_armor], 
["sith_acolyte3",  "Sith Acolyte Armour", [("sith_acolyte3",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(29)|body_armor(50)|leg_armor(32)|difficulty(0) ,imodbits_armor], 
#["sith_acolyte_hood", "Sith Acolyte Hood", [("sith_acolyte_hood",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
#["sith_acolyte2_hood", "Sith Acolyte Hood v2", [("sith_acolyte2_hood",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
#["sith_acolyte3_hood", "Sith Acolyte Hood v3", [("sith_acolyte3_hood",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
#["sith_acolyte_hood_maskless", "Sith Acolyte Hood v4", [("sith_acolyte_hood_maskless",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],

["havoc_armor",  "Republic Trooper Armor", [("havoc_armor",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_havoc_armor", ":agent_no")])]], 
["havoc_helmet", "Republic Trooper Helmet", [("havoc_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_havoc_helmet", ":agent_no")])]], 
 ["havoc_armor_camo",  "Republic Trooper Armor(Camo)", [("havoc_armor_camo",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_havoc_armor_camo", ":agent_no")])]], 
["havoc_helmet_camo", "Republic Trooper Helmet", [("havoc_helmet_camo",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_havoc_helmet_camo", ":agent_no")])]], 
["jedi_armor",  "Jedi Armour", [("jedi_armor",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(29)|body_armor(50)|leg_armor(32)|difficulty(0) ,imodbits_armor,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_jedi_armor", ":agent_no")])]],
["jedi_armor_hood", "Jedi Hood", [("jedi_armor_hood",0)], itp_type_head_armor | itp_covers_hair  ,0, 0 , weight(0)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["havoc_armor_jet",  "Republic Jetpack", [("havoc_armor_jet",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_havoc_armor", ":agent_no")])]],  
["havoc_armor_jet_camo",  "Republic Jetpack(Camo)", [("havoc_armor_jet_camo",0)], itp_type_body_armor ,0, 0 , weight(11)|abundance(100)|head_armor(13)|body_armor(17)|leg_armor(7)|difficulty(0) ,imodbits_armor,
  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(call_script, "script_colorize_armor", "tableau_havoc_armor_camo", ":agent_no")])]],   
  

["experimental_jedi",  "Jedi Armour", [("experimental_jedi",0)], itp_type_body_armor ,0, 0 , weight(18)|abundance(100)|head_armor(0)|body_armor(25)|leg_armor(10)|difficulty(0) ,imodbits_armor],

["senate_commando_armor",  "Senate Commando Armour", [("senate_commando_armor",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["senate_commando_armor_helmet", "Senate Commando Armour Helmet", [("senate_commando_armor_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
["senate_commando_armor_leader",  "Senate Commando Leader Armour", [("senate_commando_armor_leader",0)], itp_type_body_armor ,0, 0 , weight(25)|abundance(100)|head_armor(15)|body_armor(23)|leg_armor(10)|difficulty(0) ,imodbits_armor],
["senate_commando_armor_leader_helmet", "Senate Commando Leader Armour Helmet", [("senate_commando_armor_leader_helmet",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
 
["nothing", "Nothing", [("nothing",0)], itp_type_head_armor | itp_covers_head  ,0, 0 , weight(2)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate],
 
 #region Items
#_Sebastian_ begin
# swing damge = max blast damage
# hit points = damage radius in cm
["missile_launcher_ammo", "Grenade Pack", [("nothing",0),("missile_launcher",ixmesh_flying_ammo)], itp_type_bolts|itp_merchandise|itp_bonus_against_shield|itp_can_penetrate_shield|itp_default_ammo, 0, 0,weight(3)|weapon_length(3)|thrust_damage(0,pierce)|swing_damage(245, blunt)|hit_points(450)|max_ammo(3),imodbits_missile,
[(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 2, 1),
	(call_script, "script_spawn_scene_prop", "spr_sw_grenade_tmp"),#_Sebastian_
    (scene_prop_set_slot, reg0, slot_prop_timer, 1),
    (scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["smoke_grenade", "Smoke Grenade", [("smoke_grenade",0),("smoke_grenade_carry",ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee , itcf_carry_bowcase_left|itcf_show_holster_when_drawn|itcf_throw_stone, 400 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(20) | thrust_damage(0, blunt)|max_ammo(2)|weapon_length(8),imodbit_large_bag,
[(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 4, 1),
	(call_script, "script_spawn_scene_prop", "spr_sw_smoke_grenade_tmp"),
	(scene_prop_set_slot, reg0, slot_prop_timer, 0),
	(scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["signal_nade_red", "Red Signal Flare", [("smoke_grenade_red",0),("signal_grenade_red_carry",ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee , itcf_carry_bowcase_left|itcf_show_holster_when_drawn|itcf_throw_stone, 200 , weight(4)|difficulty(0)|spd_rtng(115) | shoot_speed(30) | thrust_damage(0, blunt)|max_ammo(4)|weapon_length(8),imodbit_large_bag,
[(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 4, 1),
	(call_script, "script_spawn_scene_prop", "spr_sw_signal_nade_red_tmp"),
	(scene_prop_set_slot, reg0, slot_prop_timer, 0),
	(scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["signal_nade_green", "Green Signal Flare", [("smoke_grenade_green",0),("signal_grenade_green_carry",ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee , itcf_carry_bowcase_left|itcf_show_holster_when_drawn|itcf_throw_stone, 200 , weight(4)|difficulty(0)|spd_rtng(115) | shoot_speed(30) | thrust_damage(0, blunt)|max_ammo(4)|weapon_length(8),imodbit_large_bag,
[(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 4, 1),
	(call_script, "script_spawn_scene_prop", "spr_sw_signal_nade_green_tmp"),
	(scene_prop_set_slot, reg0, slot_prop_timer, 0),
	(scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["signal_nade_blue", "Blue Signal Flare", [("smoke_grenade_blue",0),("signal_grenade_blue_carry",ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee , itcf_carry_bowcase_left|itcf_show_holster_when_drawn|itcf_throw_stone, 200 , weight(4)|difficulty(0)|spd_rtng(115) | shoot_speed(30) | thrust_damage(0, blunt)|max_ammo(4)|weapon_length(8),imodbit_large_bag,
[(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 4, 1),
	(call_script, "script_spawn_scene_prop", "spr_sw_signal_nade_blue_tmp"),
	(scene_prop_set_slot, reg0, slot_prop_timer, 0),
	(scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["frag_grenade", "Thermal Detonator", [("thermaldetonator",0),("thermaldetonator_carry",ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_can_penetrate_shield|itp_bonus_against_shield|itp_primary ,itcf_carry_bowcase_left|itcf_show_holster_when_drawn|itcf_throw_stone, 650, weight(4)|difficulty(0)|spd_rtng(80)|shoot_speed(20)|thrust_damage(0, blunt)|swing_damage(245, blunt)|hit_points(450)|max_ammo(2)|weapon_length(8),imodbit_large_bag,
[

	(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 2, 1),
	(call_script, "script_spawn_scene_prop", "spr_sw_grenade_tmp"),
	(scene_prop_set_slot, reg0, slot_prop_timer, 1),
 	(scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["det_pack", "Detonation Pack", [("det_pack",0)], itp_type_thrown |itp_can_penetrate_shield|itp_bonus_against_shield|itp_merchandise|itp_primary ,itcf_throw_stone, 500, weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(6) | thrust_damage(0, blunt)|swing_damage(245, blunt)|hit_points(400)|max_ammo(2)|weapon_length(8),imodbit_large_bag,
[	
	(ti_on_init_scene_prop,
    [
      (store_trigger_param_1, ":instance_no"),
      (scene_prop_set_hit_points, ":instance_no", 2000),
    ]),
	
	(ti_on_scene_prop_hit,
    [
      (play_sound, "snd_dummy_hit"),
      (particle_system_burst, "psys_dummy_smoke", pos1, 3),
      (particle_system_burst, "psys_dummy_straw", pos1, 10),      
    ]),
	
	(ti_on_missile_hit,[
	(multiplayer_is_server),
	(store_trigger_param_1, ":agent"),
	(position_set_z_to_ground_level, pos1),
	(position_move_z, pos1, 16, 1),
	(position_rotate_x, pos1, 45),
	(call_script, "script_spawn_scene_prop", "spr_sw_det_pack"),
	(scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["det_pack_fire", "Detonation Pack Trigger", [("hilt2",0)], itp_wooden_attack|itp_wooden_parry|itp_type_polearm|itp_merchandise|itp_primary, itcf_overswing_onehanded, 100, weight(3)|difficulty(8)|spd_rtng(120) | weapon_length(0)|swing_damage(0, cut) | thrust_damage(0, pierce),imodbits_sword_high,
[(ti_on_weapon_attack,[
	(store_trigger_param_1, ":agent"),
	(try_for_prop_instances, ":instance", "spr_sw_det_pack", somt_temporary_object),
		(scene_prop_slot_eq, ":instance", slot_prop_belongs_to_agent, ":agent"),
		(prop_instance_get_position, pos1, ":instance"),
		(call_script, "script_effect_at_position", itm_det_pack, ":agent"),
		(call_script, "script_remove_scene_prop", ":instance"),
	(try_end),
])]],

["snowball", "Snowball", [("snowball",0),("snowball_carry",ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee , itcf_carry_bowcase_left|itcf_show_holster_when_drawn|itcf_throw_stone, 
400 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(20) | thrust_damage(600, blunt)|max_ammo(60)|accuracy(200)|body_armor(1)|weapon_length(8),imodbit_large_bag ],
#_Sebastian_ end

#Melee Weapons
["knuckle_vibro_blade", "Knuckle Plate Vibro Blade", [("knuckle_vibro_blade",0)], itp_wooden_attack|itp_no_blur|itp_no_parry|itp_type_polearm|itp_merchandise|itp_primary, itc_cleaver|itcf_carry_sword_left_hip,
 100 , weight(3)|difficulty(8)|spd_rtng(80) | weapon_length(10)|swing_damage(79 , cut) | thrust_damage(54 ,  pierce),imodbits_sword_high ],
["viber_sword",         "Vibro Sword", [("vibrosword_new",0)], itp_type_one_handed_wpn|itp_no_blur|itp_merchandise|itp_primary|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 
 250, weight(1.5)|difficulty(0)|spd_rtng(77) | weapon_length(65)|swing_damage(88 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
["viber_evb",         "Emergency Vibro Blade", [("evb",0),("evb_carry",ixmesh_carry)], itp_type_one_handed_wpn|itp_no_blur|itp_merchandise|itp_primary|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 
 300, weight(1.5)|difficulty(0)|spd_rtng(77) | weapon_length(65)|swing_damage(88 , cut) | thrust_damage(88 ,  pierce),imodbits_sword ], 
["viber_blade",         "Vibro Blade", [("sw_vibroblade",0)], itp_type_one_handed_wpn|itp_no_blur|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,  
 400 , weight(3.8)|difficulty(8)|spd_rtng(70) | weapon_length(105)|swing_damage(115 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
["ryyk_blade",         "Ryyk Blade", [("spadabearforce",0)], itp_type_two_handed_wpn|itp_no_blur|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 420 , weight(2.75)|difficulty(8)|spd_rtng(81) | weapon_length(105)|swing_damage(104 , cut) | thrust_damage(104 ,  pierce),imodbits_sword_high ], 
["viber_knife",         "Vibro Knife", [("vibro_knife",0)], itp_type_one_handed_wpn|itp_no_parry|itp_no_blur|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_sword_left_hip, 
 200 , weight(0.5)|difficulty(0)|spd_rtng(90) | weapon_length(40)|swing_damage(90 , cut) | thrust_damage(81 ,  pierce),imodbits_sword ],
["mandalorian_staff",         "Mandalorian Staff", [("sw_mandalorian_staff",0)], itp_wooden_attack|itp_no_blur|itp_wooden_parry|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced,itc_staff|itcf_carry_spear,
 400 , weight(4)|difficulty(9)|spd_rtng(77) | weapon_length(172)|swing_damage(104 , cut) | thrust_damage(88 ,  pierce),imodbits_axe ],
["geonosian_spear",         "Geonosian Spear", [("geospear",0)], itp_wooden_attack|itp_wooden_parry|itp_no_blur|itp_type_one_handed_wpn|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_merchandise|itp_unbalanced,itc_staff|itcf_carry_spear,
 400 , weight(4)|difficulty(9)|spd_rtng(75) | weapon_length(175)|swing_damage(99 , cut) | thrust_damage(95 ,  pierce),imodbits_axe ], 
["vibrosword_3", "Vibro Sword", [("vibro_sword3",0)], itp_type_one_handed_wpn|itp_merchandise|itp_no_blur|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,  
 250 , weight(1.5)|difficulty(8)|spd_rtng(77) | weapon_length(65)|swing_damage(88 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
["medic_pack", "Medic Pack", [("medkit",0)], itp_type_one_handed_wpn|itp_primary|itp_no_parry, itc_dagger|itp_no_blur|itcf_carry_bowcase_left,
 120, weight(1)|spd_rtng(95)|weapon_length(40)|swing_damage(0,cut), imodbits_none],
# ["shield_pack", "Shield Repair Kit", [("medkit",0)], itp_type_one_handed_wpn|itp_primary|itp_no_parry, itc_dagger|itp_no_blur|itcf_carry_bowcase_left,
 # 120, weight(1)|spd_rtng(95)|weapon_length(20)|swing_damage(0,cut), imodbits_none],
["vibrosword_1", "Vibro Sword", [("vibro_sword1",0)], itp_type_one_handed_wpn|itp_merchandise|itp_no_blur|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,  
 200 , weight(3)|difficulty(8)|spd_rtng(77) | weapon_length(65)|swing_damage(101 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ], 
["sword2", "Vibro Sword", [("sword2",0)], itp_type_one_handed_wpn|itp_merchandise|itp_no_blur|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,  
 200 , weight(3)|difficulty(8)|spd_rtng(77) | weapon_length(65)|swing_damage(101 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ], 
#["candy_cane",         "Candy Cane", [("candy_cane",0)], itp_type_two_handed_wpn|itp_no_blur|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
# 420 , weight(2.75)|difficulty(8)|spd_rtng(86) | weapon_length(105)|swing_damage(58 , cut) | thrust_damage(44 ,  pierce),imodbits_sword_high ], 

#["binoculars", "Binoculars", [("binoculars",0),("binoculars_carry",ixmesh_carry)], itp_type_one_handed_wpn|itp_primary|itp_no_parry, itc_dagger|itcf_carry_bowcase_left, 100 , weight(1)| weapon_length(1), imodbits_none], #original binoc
["binoculars", "Binoculars", [("binoculars",0),("binoculars_carry",ixmesh_carry)], itp_type_one_handed_wpn|itp_wooden_parry|itp_primary, itcf_carry_bowcase_left,  100, weight(0)|difficulty(0)|spd_rtng(93) | weapon_length(102)|swing_damage(30 , cut) | thrust_damage(30 ,  pierce),imodbits_sword_high],
#endregion

#region AI stuff
#AI only:
["e5_blaster_ai", "E-5 Blaster Rifle", [("e5_blaster",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(165) | thrust_damage(30 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_blaster_shot_e5),imodbits_none],
["se34_blaster_ai", "SE-34 Blaster", [("se_34",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(34 ,pierce)|max_ammo(500)|accuracy(38)|abundance(snd_blaster_shot_se34),imodbits_none],
["se14_blaster_ai", "SE-14 Blaster", [("se_14r",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(32 ,pierce)|max_ammo(500)|accuracy(35)|abundance(snd_blaster_shot_se14),imodbits_none],
["ee3_blaster_ai", "EE-3 Blaster", [("ee3",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(32 ,pierce)|max_ammo(500)|accuracy(35)|abundance(snd_blaster_shot_ee3),imodbits_none], 
["sonic_blaster_ai", "Sonic Blaster", [("sonicblaster",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(60 ,pierce)|max_ammo(500)|accuracy(145)|abundance(snd_sonic_blaster),imodbits_none],  
["bowcaster_blaster_ai", "EE-3 Blaster", [("bowcaster_wookie",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(60 ,pierce)|max_ammo(500)|accuracy(145)|abundance(snd_blaster_shot_bowcaster),imodbits_none],   
["wrist_blaster_ai", "Wrist Blaster", [("nothing",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(32 ,pierce)|max_ammo(500)|accuracy(65)|abundance(snd_blaster_shot_wrist),imodbits_none],
["e5s_blaster_ai", "E-5s Sniper", [("e_5s",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(190) | thrust_damage(43 ,pierce)|max_ammo(500)|accuracy(33)|abundance(snd_snipersound),imodbits_none], 
["e5_blaster_melee_ai", "E-5 Blaster Rifle", [("e5_blaster",0)], itp_type_two_handed_wpn|itp_no_pick_up_from_ground|itp_primary|itp_wooden_parry, itc_nodachi, 0, weight(3.5)|difficulty(0)|spd_rtng(75) |swing_damage(20, blunt)| thrust_damage(22,  blunt)|weapon_length(55),imodbits_none],
["blast_shield_ai", "Blast Shield", [("droid_shield",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
 277 , weight(3.5)|hit_points(1500)|body_armor(2)|spd_rtng(80)|shield_width(50),imodbits_shield ],
["dc15_blaster_ai", "DC-15 Blaster Rifle", [("dc15_blaster",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(165) | thrust_damage(30 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["dawnsorrow_rifle_ai", "Dawnsorrow Blaster Rifle", [("dawnsorrow_rifle",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(165) | thrust_damage(30 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_blaster_shot_dnsr),imodbits_none], 
["dh17_rifle_ai", "DH-17 Blaster Rifle", [("dh_17r",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(165) | thrust_damage(30 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_blaster_shot_dh17r),imodbits_none],  
#["dc15_blaster_melee_ai", "DC-15 Blaster Rifle", [("dc15_blaster",0)], itp_type_two_handed_wpn|itp_no_pick_up_from_ground|itp_primary|itp_wooden_parry, itc_nodachi, 0, weight(3.5)|difficulty(0)|spd_rtng(75) |swing_damage(20, blunt)| thrust_damage(15,  blunt)|weapon_length(72),imodbits_none],				
["dc15x_sniper_ai", "DC-15x Sniper Rifle", [("DC-15",0)], itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(5) | shoot_speed(203) | thrust_damage(39 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_snipersound),imodbits_none], 
["droid_sniper_ai", "E-5s Sniper Rifle", [("e_5s",0)], itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(5) | shoot_speed(203) | thrust_damage(39 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_snipersound),imodbits_none],  

["senate_commando_pike_ai_aim", "Senate Commando Pike", [("senate_commando_pike",0)], itp_no_pick_up_from_ground|itp_type_musket|itp_merchandise|itp_next_item_as_melee|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(5) | shoot_speed(165) | thrust_damage(26 ,pierce)|max_ammo(500)|accuracy(99)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["senate_commando_pike_ai", "Senate Commando Pike", [("senate_commando_pike",0)], itp_type_polearm|itp_wooden_attack|itp_no_blur|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry,itc_staff|itcf_carry_spear, 
 0, weight(13)|difficulty(8)|spd_rtng(80) | weapon_length(140)|swing_damage(45 , cut) | thrust_damage(65 ,  pierce),imodbits_polearm], 
 
["e11_blaster_ai", "E-11 Blaster", [("e_11",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(34 ,pierce)|max_ammo(500)|accuracy(38)|abundance(snd_blaster_shot_e11),imodbits_none],
["a280_blaster_ai", "A280 Blaster", [("a280",0)],  itp_no_pick_up_from_ground|itp_type_musket|itp_next_item_as_melee|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 
 0 , weight(3.5)|difficulty(0)|spd_rtng(254) | shoot_speed(100) | thrust_damage(34 ,pierce)|max_ammo(500)|accuracy(38)|abundance(snd_blaster_shot_a280),imodbits_none],
 
["blaster_republic_ai", "DC-15 Blaster Rifle", [("dc15_blaster",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itcf_shoot_crossbow, 
0 , weight(3.5)|difficulty(0)|spd_rtng(37) | shoot_speed(70) | thrust_damage(63 ,pierce)|max_ammo(1),abundance(snd_blaster_shot_dc15s),imodbits_none], 
 #Mark_end
 #Player only:
#_Sebastian_ new item tulpe

#itp_unbalanced makes the gun stationary while aiming

#damage_reference-, ranged_type- and damage- slots are not needed at all

#shot_speed... only up to 204 #IMPORTANT

#spd_rtng = ready to fire time in 1/100 sec, up to 255
#max_ammo = ammo consumption per missile, up to 1023
#accuracy = dispersion in 1/10 degree, up to 255
#hitpoints = frequency in 1/1000 sec, up to 65535
#food_quality = missiles per shot, up to 255
#body_armor = spray factor in in 1/10 degree, up to 255
#weapon_length = animation, up to 1023
#abundance = sound, up to 1023

#endregion

#region Shotty weapons

###############################################################
["player_guns_begin", "0", [("invalid_item",0)], 0, 0, 0, 0, 0],#_Sebastian_ IMPORTANT !!!
###############################################################

["vibro_sword_blaster_combo", "Sword and Blaster", [("vibro_sword2",0)], itp_type_musket |itp_merchandise|itp_primary|itp_next_item_as_melee, 0, 310, weight(1.5)|difficulty(0)|spd_rtng(38) | shoot_speed(185) | thrust_damage(24 ,pierce)|max_ammo(10)|accuracy(80),imodbits_none],
["vibro_sword_blaster_combo_melee",         "Sword and Blaster", [("vibro_sword2",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 0, weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(55)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_polearm],
["kyd21_offhand","KYD-21 Blaster Pistol", [("kyd21_blaster_offhandL",0)], itp_merchandise|itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],

["senate_commando_pike_aim", "Senate Commando Pike", [("senate_commando_pike",0)], itp_type_musket |itp_merchandise|itp_primary|itp_next_item_as_melee|itp_two_handed, itcf_carry_quiver_back, 
 750, weight(13)|difficulty(0)|spd_rtng(1)|shoot_speed(135)|thrust_damage(66,pierce)|max_ammo(1)|accuracy(7)|hit_points(1000)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["senate_commando_pike", "Senate Commando Pike", [("senate_commando_pike",0)], itp_type_polearm|itp_wooden_attack|itp_no_blur|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry,itc_staff|itcf_carry_spear, 
 750, weight(13)|difficulty(8)|spd_rtng(77) | weapon_length(140)|swing_damage(81 , cut) | thrust_damage(117 ,  pierce),imodbits_polearm],
 
#["tactical_arm_aim", "[PH]Droid Arm", [("nothing",0)], itp_type_musket |itp_merchandise|itp_primary|itp_next_item_as_melee|itp_two_handed, itcf_carry_quiver_back, 
# 200, weight(7)|difficulty(0)|spd_rtng(1)|shoot_speed(135)|thrust_damage(35,pierce)|max_ammo(1)|accuracy(7)|hit_points(1000)|food_quality(2)|body_armor(1)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_blaster_shot_dc15s),imodbits_none],
#["tactical_arm", "[PH]Droid Melee Arm", [("nothing",0)], itp_type_one_handed_wpn|itp_type_shield|itp_no_blur|itp_merchandise|itp_primary|itp_primary, itc_scimitar, 
# 200, weight(7)|difficulty(0)|spd_rtng(77) | weapon_length(65)|swing_damage(49 , cut) | thrust_damage(49 ,  pierce),imodbits_sword ],
 
#["bowcaster", "Bowcaster", [("bowcaster_wookie",0)],  itp_type_musket |itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee ,0, 
# 320 , weight(4.4)|difficulty(21)|spd_rtng(160) | shoot_speed(220) | thrust_damage(29 ,pierce)|max_ammo(10)|accuracy(87),imodbits_none],
#["bowcaster_aim", "Bowcaster", [("bowcaster_wookie",0)],  itp_type_musket |itp_merchandise|itp_primary|itp_two_handed ,0, 
# 320 , weight(4.4)|difficulty(21)|spd_rtng(20) | shoot_speed(220) | thrust_damage(29 ,pierce)|max_ammo(10)|accuracy(120),imodbits_none],

##SMG##
["dc17m_blaster", "DC-17m Blaster SMG",  [("dc17m",0),("dc17m_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(30)|hit_points(110)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc17),imodbits_none],
["dc17m_blaster_aim", "DC-17m Blaster SMG", [("dc17m",0),("dc17m_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(21)|hit_points(200)|food_quality(1)|body_armor(9)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc17),imodbits_none], 

["se_14_blaster", "SE-14 Blaster SMG", [("se_14r",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(30)|hit_points(110)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_se14),imodbits_none],
["se_14_blaster_aim", "SE-14 Blaster SMG", [("se_14r",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(21)|hit_points(200)|food_quality(1)|body_armor(9)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_se14),imodbits_none], 

["sg_4_blaster", "SG-4 Blaster SMG", [("sg_4",0),("sg_4_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(30)|hit_points(110)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sg4),imodbits_none],
["sg_4_blaster_aim", "SG-4 Blaster SMG", [("sg_4",0),("sg_4_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(21)|hit_points(200)|food_quality(1)|body_armor(9)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sg4),imodbits_none], 

["sfor_c_blaster", "SFOR Blaster SMG", [("sfor_c",0),("sfor_c_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(30)|hit_points(110)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sforc),imodbits_none],
["sfor_c_blaster_aim", "SFOR Carbine SMG", [("sfor_c",0),("sfor_c_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(21)|hit_points(200)|food_quality(1)|body_armor(9)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sforc),imodbits_none], 

["orepublic_smg", "Republic SMG", [("orepublic_smg",0),("orepublic_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(30)|hit_points(110)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sforc),imodbits_none],
["orepublic_smg_aim", "Republic SMG", [("orepublic_smg",0),("orepublic_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(21)|hit_points(200)|food_quality(1)|body_armor(9)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sforc),imodbits_none], 
 
["sempire_smg", "Sith SMG", [("sempire_smg",0),("sempire_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(30)|hit_points(110)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sforc),imodbits_none],
["sempire_smg_aim", "Sith SMG", [("sempire_smg",0),("sempire_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(90)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(21)|hit_points(200)|food_quality(1)|body_armor(9)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sforc),imodbits_none], 
##SMG##
##Carbines##
["dc15s_blaster", "DC-15s Blaster Carbine", [("dc15s_blaster",0),("dc15s_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(27)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["dc15s_blaster_aim", "DC-15s Blaster Carbine", [("dc15s_blaster",0),("dc15s_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(17)|hit_points(150)|food_quality(1)|body_armor(8)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none], 

["e5_blaster", "E-5 Blaster Carbine", [("e5_blaster",0),("e5_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(27)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_e5),imodbits_none],
["e5_blaster_aim", "E-5 Blaster Carbine", [("e5_blaster",0),("e5_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(17)|hit_points(150)|food_quality(1)|body_armor(8)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_e5),imodbits_none], 

["e11_blaster", "E-11 Blaster Carbine", [("e_11",0),("e_11_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(27)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_e11),imodbits_none],
["e11_blaster_aim", "E-11 Blaster Carbine", [("e_11",0),("e_11_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(17)|hit_points(150)|food_quality(1)|body_armor(8)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_e11),imodbits_none], 
 
["e11_blaster_tor", "E-11 Blaster Carbine", [("e_11",0),("e_11_tor_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(27)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_e11),imodbits_none],
["e11_blaster_tor_aim", "E-11 Blaster Carbine", [("e_11",0),("e_11_tor_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(17)|hit_points(150)|food_quality(1)|body_armor(8)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_e11),imodbits_none], 
 
 
["a280_blaster", "A280 Blaster Carbine", [("a280",0),("a280_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(27)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_a280),imodbits_none],
["a280_blaster_aim", "A280 Blaster Carbine", [("a280",0),("a280_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(0)|shoot_speed(100)|thrust_damage(48,pierce)|max_ammo(1)|accuracy(17)|hit_points(150)|food_quality(1)|body_armor(8)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_a280),imodbits_none], 
##Carbines##
##Light Rifles##
["westar_m5_blaster", "Westar M5 Light Rifle", [("westar_m5",0),("westar_m5_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(33)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_westarm5),imodbits_none],
["westar_m5_blaster_aim", "Westar M5 Light Rifle", [("westar_m5",0),("westar_m5_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(15)|hit_points(260)|food_quality(1)|body_armor(5)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_westarm5),imodbits_none], 

["ee3_blaster", "EE-3 Light Rifle", [("ee3",0),("ee3_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(33)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["ee3_blaster_aim", "EE-3 Light Rifle", [("ee3",0),("ee3_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(15)|hit_points(260)|food_quality(1)|body_armor(5)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 

["dlt_20a_blaster", "DLT-20AC Light Rifle", [("dlt_20ac",0),("dlt_20ac_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(33)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_westarm5),imodbits_none],
["dlt_20a_blaster_aim", "DLT-20AC Light Rifle", [("dlt_20ac",0),("dlt_20ac_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(15)|hit_points(260)|food_quality(1)|body_armor(5)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_westarm5),imodbits_none], 

["tc22_blaster", "TC-22 Light Rifle", [("tc22",0),("tc22_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(33)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["tc22_blaster_aim", "TC-22 Light Rifle", [("tc22",0),("tc22_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(15)|hit_points(260)|food_quality(1)|body_armor(5)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 

["orepublic_lightrifle", "Republic Light Rifle", [("orepublic_lightrifle",0),("orepublic_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(33)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["orepublic_lightrifle_aim", "Republic Light Rifle", [("orepublic_lightrifle",0),("orepublic_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(15)|hit_points(260)|food_quality(1)|body_armor(5)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 
		
["sempire_lightrifle", "Sith Light Rifle", [("sempire_lightrifle",0),("sempire_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(33)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["sempire_lightrifle_aim", "Sith Light Rifle", [("sempire_lightrifle",0),("sempire_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(15)|hit_points(260)|food_quality(1)|body_armor(5)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 
		##DLT-20A - NO MODEL YET
		##T-22 - NO MODEL YET
##Light Rifles##
##Rifles##
["dc15_blaster", "DC-15A Blaster Rifle", [("dc15_blaster",0),("dc15_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["dc15_blaster_aim", "DC-15A Blaster Rifle", [("dc15_blaster",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none], 

["carbine2", "DC-15A Blaster Rifle", [("dc15_blaster",0),("dc15_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["carbine2_aim", "DC-15A Blaster Rifle", [("dc15_blaster",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none],  
 
["se_34_blaster", "SE-34 Blaster Rifle", [("se_34",0),("se_34_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_se34),imodbits_none],
["se_34_blaster_aim", "SE-34 Blaster Rifle", [("se_34",0),("se_34_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_se34),imodbits_none], 
 
["dnsr_blaster", "Dawnsorrow Blaster Rifle", [("dawnsorrow_rifle",0),("dawnsorrow_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dnsr),imodbits_none],
["dnsr_blaster_aim", "Dawnsorrow Blaster Rifle", [("dawnsorrow_rifle",0),("dawnsorrow_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dnsr),imodbits_none], 

["dh_17r_blaster", "DH-17 Blaster Rifle", [("dh_17r",0),("dh_17r_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dh17r),imodbits_none],
["dh_17r_blaster_aim", "DH-17 Blaster Rifle", [("dh_17r",0),("dh_17r_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dh17r),imodbits_none], 

["sempire_rifle", "Sith Blaster Rifle", [("sempire_rifle",0),("sempire_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dh17r),imodbits_none],
["sempire_rifle_aim", "Sith Blaster Rifle", [("sempire_rifle",0),("sempire_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dh17r),imodbits_none], 

["orepublic_rifle", "Republic Blaster Rifle", [("orepublic_rifle",0),("orepublic_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(38)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dh17r),imodbits_none],
["orepublic_rifle_aim", "Republic Blaster Rifle", [("orepublic_rifle",0),("orepublic_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(130)|thrust_damage(62,pierce)|max_ammo(1)|accuracy(8)|hit_points(320)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dh17r),imodbits_none],  

#["carbine2", "Stable Blaster Rifle", [("carbine2",0),("carbine2_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 #600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(140)|thrust_damage(17,pierce)|max_ammo(1)|accuracy(20)|hit_points(150)|food_quality(1)|body_armor(15)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
#["dc15_blaster_aim", "Stable Blaster Rifle", [("carbine2",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 #600, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(140)|thrust_damage(17,pierce)|max_ammo(1)|accuracy(5)|hit_points(180)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none],  
 ##Rifles##

##Snipers
["dc15x_sniper", "DC-15x Sniper Rifle", [("DC-15",0),("DC-15_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(50)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_snipersound),imodbits_none],
["dc15x_sniper_aim", "DC-15x Sniper Rifle", [("DC-15",0),("DC-15_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(2)|hit_points(1650)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_snipersound),imodbits_none], 

["e_5s_sniper", "E-5s Sniper Rifle", [("e_5s",0),("e_5s_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(50)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_snipersound),imodbits_none],
["e_5s_sniper_aim", "E-5s Sniper Rifle", [("e_5s",0),("e_5s_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(2)|hit_points(1650)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_snipersound),imodbits_none], 

["dlt_19x_sniper", "DLT-19X Sniper Rifle", [("dlt_19x",0),("dlt_19x_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(50)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dlt19x),imodbits_none],
["dlt_19x_sniper_aim", "DLT-19X  Sniper Rifle", [("dlt_19x",0),("dlt_19x_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(2)|hit_points(1650)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dlt19x),imodbits_none], 

["ld1_sniper", "LD-1 Sniper Rifle", [("ld_1",0),("ld_1_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(50)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ld1),imodbits_none],
["ld1_sniper_aim", "LD-1 Sniper Rifle", [("ld_1",0),("ld_1_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(2)|hit_points(1650)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ld1),imodbits_none], 

["orepublic_sniper", "Republic Sniper Rifle", [("orepublic_sniper",0),("orepublic_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(50)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ld1),imodbits_none],
["orepublic_sniper_aim", "Republic Sniper Rifle", [("orepublic_sniper",0),("orepublic_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(2)|hit_points(1650)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ld1),imodbits_none], 

["sempire_sniper", "Sith Sniper Rifle", [("sempire_sniper",0),("sempire_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(50)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ld1),imodbits_none],
["sempire_sniper_aim", "Sith Sniper Rifle", [("sempire_sniper",0),("sempire_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(40)|difficulty(0)|spd_rtng(0)|shoot_speed(200)|thrust_damage(110,pierce)|max_ammo(1)|accuracy(2)|hit_points(1650)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ld1),imodbits_none], 
 
["cyc_rifle", "Cycler Rifle", [("Cyc_rifle",0),("Cyc_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 700, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(150)|thrust_damage(97,pierce)|max_ammo(1)|accuracy(43)|hit_points(450)|food_quality(1)|body_armor(43)|weapon_length(anim_rifle_aim_std)|abundance(snd_Cycler_Rifle),imodbits_none],
["cyc_rifle_aim", "Cycler Rifle", [("Cyc_rifle",0),("Cyc_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 700, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(150)|thrust_damage(97,pierce)|max_ammo(1)|accuracy(4)|hit_points(1425)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_Cycler_Rifle),imodbits_none], 
##Snipers## 

##LMGs##
["z7_rotary_blaster", "Z-6 Rotary Blaster", [("z7_rotary",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_rotary)|abundance(snd_minigun),imodbits_none],
["z7_rotary_blaster_aim", "Z-6 Rotary Blaster", [("z7_rotary",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_rotary)|abundance(snd_minigun),imodbits_none],

["wrist_blaster1", "Light Machine Blaster", [("nothing",0), ("icon_lmg", ixmesh_inventory)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, 0,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_blaster_shot_wrist),imodbits_none],
["wrist_blaster1_aim", "Light Machine Blaster", [("nothing",0)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, 0,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_blaster_shot_wrist),imodbits_none],

["dlt_19_blaster", "DLT-19X LMG", [("dlt_19",0),("dlt_19_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dlt19),imodbits_none],
["dlt_19_blaster_aim", "DLT-19X LMG", [("dlt_19",0),("dlt_19_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dlt19),imodbits_none],

["rt_97c_blaster", "RT-97C LMG", [("rt_97c",0),("rt_97c_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_rt97c),imodbits_none],
["rt_97c_blaster_aim", "RT-97C LMG", [("rt_97c",0),("rt_97c_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(40)|difficulty(0)|spd_rtng(5)|shoot_speed(100)|thrust_damage(30,pierce)|max_ammo(1)|accuracy(40)|hit_points(78)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_rt97c),imodbits_none],
##LMGs##

##HMGs##
["wrist_blaster2", "Heavy Machine Blaster", [("nothing",0), ("icon_hmg", ixmesh_inventory)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, 0,
 900, weight(150)|difficulty(0)|spd_rtng(15)|shoot_speed(100)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(19)|hit_points(55)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_blaster_shot_wrist),imodbits_none],
["wrist_blaster2_aim", "Heavy Machine Blaster", [("nothing",0)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, 0,
 900, weight(150)|difficulty(0)|spd_rtng(15)|shoot_speed(100)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(19)|hit_points(55)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_blaster_shot_wrist),imodbits_none],

["heavy_mg", "Heavy Machine Blaster", [("heavy_mg",0),("heavy_mg_carry",ixmesh_carry)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(150)|difficulty(0)|spd_rtng(15)|shoot_speed(100)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(19)|hit_points(55)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_heavy)|abundance(snd_blaster_shot_wrist),imodbits_none],
["heavy_mg_aim", "Heavy Machine Blaster", [("heavy_mg",0),("heavy_mg_carry",ixmesh_carry)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(150)|difficulty(0)|spd_rtng(15)|shoot_speed(100)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(19)|hit_points(55)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_heavy)|abundance(snd_blaster_shot_wrist),imodbits_none], 

#["mortar", "Mortar", [("mortar",0),("mortar_carry",ixmesh_carry)], itp_type_musket|itp_unbalanced|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
# 900 , weight(9)|difficulty(0)|spd_rtng(4)|shoot_speed(43)|thrust_damage(0,pierce)|max_ammo(110)|accuracy(30)|hit_points(3000)|food_quality(1)|body_armor(40)|weapon_length(anim_rifle_aim_std)|abundance(snd_rocket_launcher_shot),imodbits_none],
##HMGs##

##Pistols##
["dc_17_pistol", "DC-17 Pistol", [("dc17",0)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_blaster_shot_dc17pistol),imodbits_none], 

["westar_34_pistol", "Westar 34 Pistol", [("westar_34",0)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_twin_blaster_sound_shot),imodbits_none], 

["de10_blaster_pistol", "DE-10 Pistol", [("de_10_blaster",0)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_blaster_shot_dc17pistol),imodbits_none], 
 
["kyd21_blaster_pistol", "KYD-21 Pistol", [("kyd21_blaster",0)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_blaster_shot_dc17pistol),imodbits_none], 

["dh17_blaster_pistol", "DH-17 Pistol", [("dh_17p",0),("dh17p_holster",ixmesh_carry)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right|itcf_show_holster_when_drawn, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_blaster_shot_dh17p),imodbits_none], 
 
["se14_blaster_pistol", "SE-14r Pistol", [("se_14p",0)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_blaster_shot_se14),imodbits_none], 

["wrist_blaster", "Wrist Pistol", [("nothing",0), ("icon_lmg", ixmesh_inventory)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_blaster_shot_se14),imodbits_none],

["scout_blaster_pistol", "Scout Blaster Pistol", [("scout_trooper_pistol",0)], itp_type_musket|itp_two_handed|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 120, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(13)|hit_points(250)|food_quality(1)|body_armor(4)|weapon_length(anim_rifle_aim_pistol)|abundance(snd_blaster_shot_se14),imodbits_none], 

 ##Pistols##
##Special Weapons##
["westar_34_twin", "Westar 34 Twin Pistols", [("westar_34",0),("westar_34_twin_inventory", ixmesh_inventory)], itp_type_musket|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 450, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(38)|hit_points(400)|food_quality(2)|body_armor(8)|weapon_length(anim_rifle_aim_twin_pistol)|abundance(snd_westar_twin_sound),imodbits_none], 
["westar_34_twin_no_sound", "Westar 34 Twin Pistols", [("westar_34",0),("westar_34_twin_inventory", ixmesh_inventory)], itp_type_musket|itp_merchandise|itp_primary, 0, 
 450, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(38)|hit_points(400)|food_quality(2)|body_armor(8)|weapon_length(anim_rifle_aim_pistol),imodbits_none], 
["westar_34_twin_offhand", "Westar 34 Twin Pistols", [("westar_34_offhandL",0)], itp_type_hand_armor|itp_merchandise|itp_primary, itcf_carry_buckler_left, 90, weight(0.25)|body_armor(2)|difficulty(0),imodbits_cloth],

["dc17_twin", "DC-17 Twin Pistols", [("dc17",0),("westar_34_twin_inventory", ixmesh_inventory)], itp_type_musket|itp_merchandise|itp_primary, itcf_carry_revolver_right, 
 450, weight(1)|difficulty(0)|spd_rtng(0)|shoot_speed(65)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(38)|hit_points(400)|food_quality(2)|body_armor(8)|weapon_length(anim_rifle_aim_twin_pistol)|abundance(snd_blaster_shot_dc17pistol),imodbits_none], 
["dc17_twin_offhand", "DC 17 Twin Pistols", [("dc17_offhandL",0)], itp_type_hand_armor|itp_merchandise|itp_primary, itcf_carry_buckler_left, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],

#["westar_34_twin", "Westar 34 Twin Blasters", [("westar_34",0),("westar_34_twin_inventory", ixmesh_inventory)],  itp_type_musket |itp_merchandise|itp_primary , 0, 
#870 , weight(1.5)|difficulty(0)|spd_rtng(38+38) | shoot_speed(220) | thrust_damage(24 ,pierce)|max_ammo(10)|accuracy(80),imodbits_none],
#["westar_34_twin_no_sound", "Westar 34 Twin Blasters no Sound", [("westar_34",0),("westar_34_twin_inventory", ixmesh_inventory)],  itp_type_musket |itp_merchandise|itp_primary ,0, 870 , weight(1.5)|difficulty(0)|spd_rtng(38+38) | shoot_speed(220) | thrust_damage(24 ,pierce)|max_ammo(10)|accuracy(80),imodbits_none],#_Sebastian_ used for extra shots
#["westar_34_twin_offhand","Westar 34 Twin Blasters", [("westar_34_offhandL",0)], itp_merchandise|itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],

#["dc17_twin", "DC 17 Twin Blaster", [("dc17",0)], itp_type_musket |itp_merchandise|itp_primary ,0, 900 , weight(1.5)|difficulty(0)|spd_rtng(38+38) | shoot_speed(220) | thrust_damage(24 ,pierce)|max_ammo(10)|accuracy(80),imodbits_none],
#["dc17_twin_offhand","DC 17 Twin Blasters", [("dc17_offhandL",0)], itp_merchandise|itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],

["shotgun", "DP-23 Shotgun", [("dp_23",0),("dp_23_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right,
 600, weight(6)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(65)|hit_points(550)|food_quality(5)|body_armor(23)|weapon_length(anim_rifle_aim_std)|abundance(snd_shotgun_sound),imodbits_none],

["shotgun1", "GH-19 Shotgun", [("shotgun1",0),("shotgun1_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back_right,
 600, weight(6)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(65)|hit_points(550)|food_quality(5)|body_armor(23)|weapon_length(anim_rifle_aim_std)|abundance(snd_shotgun_sound),imodbits_none],
 
["t_21_blaster", "T-21 Heavy Blaster", [("t_21",0),("t_21_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(40,pierce)|max_ammo(1)|accuracy(26)|hit_points(120)|food_quality(1)|body_armor(7)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_t21),imodbits_none],
["t_21_blaster_aim", "T-21 Heavy Blaster", [("t_21",0),("t_21_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(85,pierce)|max_ammo(1)|accuracy(16)|hit_points(650)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_t21),imodbits_none],

["sempire_heavyblaster", "Sith Heavy Blaster", [("sempire_heavyblaster",0),("sempire_heavyblaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(40,pierce)|max_ammo(1)|accuracy(26)|hit_points(120)|food_quality(1)|body_armor(7)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_t21),imodbits_none],
["sempire_heavyblaster_aim", "Sith Heavy Blaster", [("sempire_heavyblaster",0),("sempire_heavyblaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(85,pierce)|max_ammo(1)|accuracy(16)|hit_points(650)|food_quality(1)|body_armor(7)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_t21),imodbits_none],

["orepublic_heavyblaster", "Republic Heavy Blaster", [("orepublic_heavyblaster",0),("orepublic_heavyblaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(40,pierce)|max_ammo(1)|accuracy(26)|hit_points(120)|food_quality(1)|body_armor(7)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_t21),imodbits_none],
["orepublic_heavyblaster_aim", "Republic Heavy Blaster", [("orepublic_heavyblaster",0),("orepublic_heavyblaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(85,pierce)|max_ammo(1)|accuracy(16)|hit_points(650)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_t21),imodbits_none],
 
["rv10_blaster", "Rv10 Heavy Blaster", [("rv10mg",0),("rv10mg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back_right,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(40,pierce)|max_ammo(1)|accuracy(26)|hit_points(120)|food_quality(1)|body_armor(7)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_rv10),imodbits_none],
["rv10_blaster_aim", "Rv10 Heavy Blaster", [("rv10mg",0),("rv10mg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee,itcf_carry_quiver_back_right,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(80)|thrust_damage(85,pierce)|max_ammo(1)|accuracy(16)|hit_points(650)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_rv10),imodbits_none],

["bowcaster", "Bowcaster", [("bowcaster_wookie",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(8)|difficulty(0)|spd_rtng(0)|shoot_speed(120)|thrust_damage(44,pierce)|max_ammo(1)|accuracy(25)|hit_points(150)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_bowcaster),imodbits_none],
["bowcaster_aim", "Bowcaster", [("bowcaster_wookie",0)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 500, weight(8)|difficulty(0)|spd_rtng(0)|shoot_speed(70)|thrust_damage(53,pierce)|max_ammo(1)|accuracy(40)|hit_points(550)|food_quality(5)|body_armor(18)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_bowcaster),imodbits_none], 

["sonic_blaster", "Sonic Blaster", [("sonicblaster",0)],  itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(60)|thrust_damage(75,pierce)|max_ammo(1)|accuracy(30)|hit_points(600)|food_quality(1)|body_armor(18)|weapon_length(anim_rifle_aim_std)|abundance(snd_sonic_blaster),imodbits_none],
["sonic_blaster_aim", "Sonic Blaster", [("sonicblaster",0)],  itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 500, weight(5)|difficulty(0)|spd_rtng(0)|shoot_speed(60)|thrust_damage(75,pierce)|max_ammo(1)|accuracy(30)|hit_points(600)|food_quality(1)|body_armor(18)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_sonic_blaster),imodbits_none], 
 
["carbine3", "Stable Blaster Rifle", [("carbine3",0)],  itp_type_musket|itp_merchandise|itp_primary|itp_two_handed|itp_next_item_as_melee, itcf_carry_quiver_back,
 900, weight(9)|difficulty(0)|spd_rtng(0)|shoot_speed(95)|thrust_damage(42,pierce)|max_ammo(1)|accuracy(27)|hit_points(150)|food_quality(1)|body_armor(18)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["carbine3_aim", "Stable Blaster Rifle", [("carbine3",0)],  itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 900, weight(9)|difficulty(0)|spd_rtng(0)|shoot_speed(95)|thrust_damage(42,pierce)|max_ammo(1)|accuracy(12)|hit_points(170)|food_quality(1)|body_armor(3)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none], 

##Special Weapons##


["imp_heavy_blaster", "Heavy Blaster", [("imp_heavy_blaster",0)],  itp_type_musket |itp_merchandise|itp_primary|itp_two_handed ,0, 
812 , weight(3.5)|difficulty(0)|spd_rtng(160) | shoot_speed(220) | thrust_damage(35 ,pierce)|max_ammo(10)|accuracy(87),imodbits_none],


#27.10.2015, Illuminati Changes:

["e60r_launcher", "E-60R Grenade Launcher", [("e60r",0)],  itp_type_musket |itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed ,itcf_carry_quiver_back,  #Sherlock added extra difficulty to prevent usage in 0.35, revert later
 900 , weight(69)|difficulty(0)|spd_rtng(0)|shoot_speed(54)|thrust_damage(0,pierce)|max_ammo(35)|accuracy(30)|hit_points(3000)|food_quality(1)|body_armor(40)|weapon_length(anim_rifle_aim_launcher)|abundance(snd_rocket_launcher_shot),imodbits_none],
 
["wrist_launcher", "Wrist Launcher", [("nothing",0), ("icon_rocket", ixmesh_inventory)],  itp_type_musket |itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed ,itcf_carry_quiver_back,  #Sherlock added extra difficulty to prevent usage in 0.35, revert later
 900 , weight(69)|difficulty(0)|spd_rtng(0)|shoot_speed(54)|thrust_damage(0,pierce)|max_ammo(35)|accuracy(30)|hit_points(3000)|food_quality(1)|body_armor(40)|weapon_length(anim_rifle_aim_wrist)|abundance(snd_rocket_launcher_shot),imodbits_none], #Mark
 
 
 
 # AI Player style Weapons Begin
 
 
		##AI SMG##

["dc17m_blaster_newai", "DC-17m Blaster SMG", [("dc17m",0)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(29,pierce)|max_ammo(1)|accuracy(330)|hit_points(110)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc17),imodbits_none],
["dc17m_blaster_newai_aim", "DC-17m Blaster SMG", [("dc17m",0)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(29,pierce)|max_ammo(1)|accuracy(330)|hit_points(200)|food_quality(1)|body_armor(20)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc17),imodbits_none], 

["se_14_blaster_newai", "SE-14 Blaster SMG", [("se_14r",0)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(110)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_se14),imodbits_none],
["se_14_blaster_newai_aim", "SE-14 Blaster SMG", [("se_14r",0)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(200)|food_quality(1)|body_armor(20)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_se14),imodbits_none], 

["sg_4_blaster_newai", "SG-4 Blaster SMG", [("sg_4",0),("sg_4_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(110)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sg4),imodbits_none],
["sg_4_blaster_newai_aim", "SG-4 Blaster SMG", [("sg_4",0),("sg_4_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(200)|food_quality(1)|body_armor(20)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sg4),imodbits_none], 

["sfor_c_blaster_newai", "SFOR Blaster SMG", [("sfor_c",0),("sfor_c_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(110)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sforc),imodbits_none],
["sfor_c_blaster_newai_aim", "SFOR Carbine SMG", [("sfor_c",0),("sfor_c_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(200)|food_quality(1)|body_armor(20)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sforc),imodbits_none], 

["orepublic_smg_newai", "Republic SMG", [("orepublic_smg",0),("orepublic_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(110)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sforc),imodbits_none],
["orepublic_smg_newai_aim", "Republic SMG", [("orepublic_smg",0),("orepublic_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(200)|food_quality(1)|body_armor(20)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sforc),imodbits_none], 
 
["sempire_smg_newai", "Sith SMG", [("sempire_smg",0),("sempire_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(110)|food_quality(1)|body_armor(25)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_sforc),imodbits_none],
["sempire_smg_newai_aim", "Sith SMG", [("sempire_smg",0),("sempire_smg_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(13,pierce)|max_ammo(1)|accuracy(330)|hit_points(200)|food_quality(1)|body_armor(20)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_sforc),imodbits_none], 
 
		##AI Light Rifles##
 
["westar_m5_blaster_newai", "Westar M5 Light Rifle", [("westar_m5",0),("westar_m5_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_westarm5),imodbits_none],
["westar_m5_blaster_newai_aim", "Westar M5 Light Rifle", [("westar_m5",0),("westar_m5_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(260)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_westarm5),imodbits_none], 

["ee3_blaster_newai", "EE-3 Light Rifle", [("ee3",0),("ee3_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["ee3_blaster_newai_aim", "EE-3 Light Rifle", [("ee3",0),("ee3_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(260)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 

["dlt_20a_blaster_newai", "DLT-20AC Light Rifle", [("dlt_20ac",0),("dlt_20ac_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_westarm5),imodbits_none],
["dlt_20a_blaster_newai_aim", "DLT-20AC Light Rifle", [("dlt_20ac",0),("dlt_20ac_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(260)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_westarm5),imodbits_none], 

["tc22_blaster_newai", "TC-22 Light Rifle", [("tc22",0),("tc22_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["tc22_blaster_newai_aim", "TC-22 Light Rifle", [("tc22",0),("tc22_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(260)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 

["orepublic_lightrifle_newai", "Republic Light Rifle", [("orepublic_lightrifle",0),("orepublic_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["orepublic_lightrifle_newai_aim", "Republic Light Rifle", [("orepublic_lightrifle",0),("orepublic_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(260)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 
		
["sempire_lightrifle_newai", "Sith Light Rifle", [("sempire_lightrifle",0),("sempire_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(180)|food_quality(1)|body_armor(24)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ee3),imodbits_none],
["sempire_lightrifle_newai_aim", "Sith Light Rifle", [("sempire_lightrifle",0),("sempire_lightrifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 450, weight(5)|difficulty(0)|spd_rtng(6)|shoot_speed(1000)|thrust_damage(14,pierce)|max_ammo(1)|accuracy(220)|hit_points(260)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ee3),imodbits_none], 
 
		##AI Rifles##

["dc15_blaster_newai", "DC-15A Blaster Rifle", [("dc15_blaster",0),("dc15_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["dc15_blaster_newai_aim", "DC-15A Blaster Rifle", [("dc15_blaster",0)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none], 

["carbine2_newai", "DC-15A Blaster Rifle", [("dc15_blaster",0),("dc15_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["carbine2_newai_aim", "DC-15A Blaster Rifle", [("dc15_blaster",0)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none],  
 
["se_34_blaster_newai", "SE-34 Blaster Rifle", [("se_34",0),("se_34_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_se34),imodbits_none],
["se_34_blaster_newai_aim", "SE-34 Blaster Rifle", [("se_34",0),("se_34_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(41,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_se34),imodbits_none], 
 
["dnsr_blaster_newai", "Dawnsorrow Blaster Rifle", [("dawnsorrow_rifle",0),("dawnsorrow_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dnsr),imodbits_none],
["dnsr_blaster_newai_aim", "Dawnsorrow Blaster Rifle", [("dawnsorrow_rifle",0),("dawnsorrow_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dnsr),imodbits_none], 

["dh_17r_blaster_newai", "DH-17 Blaster Rifle", [("dh_17r",0),("dh_17r_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dh17r),imodbits_none],
["dh_17r_blaster_newai_aim", "DH-17 Blaster Rifle", [("dh_17r",0),("dh_17r_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dh17r),imodbits_none], 

["sempire_rifle_newai", "Sith Blaster Rifle", [("sempire_rifle",0),("sempire_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dh17r),imodbits_none],
["sempire_rifle_newai_aim", "Sith Blaster Rifle", [("sempire_rifle",0),("sempire_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dh17r),imodbits_none], 

["orepublic_rifle_newai", "Republic Blaster Rifle", [("orepublic_rifle",0),("orepublic_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(230)|food_quality(1)|body_armor(28)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dh17r),imodbits_none],
["orepublic_rifle_newai_aim", "Republic Blaster Rifle", [("orepublic_rifle",0),("orepublic_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 600, weight(5)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(16,pierce)|max_ammo(1)|accuracy(165)|hit_points(320)|food_quality(1)|body_armor(10)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dh17r),imodbits_none],
 
		##AI Carbines##

["dc15s_blaster_newai", "DC-15s Blaster Carbine", [("dc15s_blaster",0),("dc15s_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dc15s),imodbits_none],
["dc15s_blaster_newai_aim", "DC-15s Blaster Carbine", [("dc15s_blaster",0),("dc15s_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(150)|food_quality(1)|body_armor(17)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dc15s),imodbits_none], 

["e5_blaster_newai", "E-5 Blaster Carbine", [("e5_blaster",0),("e5_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_e5),imodbits_none],
["e5_blaster_newai_aim", "E-5 Blaster Carbine", [("e5_blaster",0),("e5_blaster_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(150)|food_quality(1)|body_armor(17)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_e5),imodbits_none], 

["e11_blaster_newai", "E-11 Blaster Carbine", [("e_11",0),("e_11_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_e11),imodbits_none],
["e11_blaster_newai_aim", "E-11 Blaster Carbine", [("e_11",0),("e_11_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(150)|food_quality(1)|body_armor(17)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_e11),imodbits_none], 
 
["a280_blaster_newai", "A280 Blaster Carbine", [("a280",0),("a280_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(130)|food_quality(1)|body_armor(14)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_a280),imodbits_none],
["a280_blaster_newai_aim", "A280 Blaster Carbine", [("a280",0),("a280_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back_right, 
 320, weight(3)|difficulty(0)|spd_rtng(8)|shoot_speed(1000)|thrust_damage(12,pierce)|max_ammo(1)|accuracy(275)|hit_points(150)|food_quality(1)|body_armor(17)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_a280),imodbits_none], 
 
 
 
		# AI Snipers
["dc15x_sniper_newai", "DC-15x Sniper Rifle", [("DC-15",0),("DC-15_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_snipersound),imodbits_none],
["dc15x_sniper_newai_aim", "DC-15x Sniper Rifle", [("DC-15",0),("DC-15_carry",ixmesh_carry)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1800)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_snipersound),imodbits_none], 

["e_5s_sniper_newai", "E-5s Sniper Rifle", [("e_5s",0),("e_5s_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_snipersound),imodbits_none],
["e_5s_sniper_newai_aim", "E-5s Sniper Rifle", [("e_5s",0),("e_5s_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(85,pierce)|max_ammo(1)|accuracy(110)|hit_points(1800)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_snipersound),imodbits_none], 

["dlt_19x_sniper_newai", "DLT-19X Sniper Rifle", [("dlt_19x",0),("dlt_19x_carry",ixmesh_carry)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_dlt19x),imodbits_none],
["dlt_19x_sniper_newai_aim", "DLT-19X  Sniper Rifle", [("dlt_19x",0),("dlt_19x_carry",ixmesh_carry)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1800)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_dlt19x),imodbits_none], 

["ld1_sniper_newai", "LD-1 Sniper Rifle", [("ld_1",0),("ld_1_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ld1),imodbits_none],
["ld1_sniper_newai_aim", "LD-1 Sniper Rifle", [("ld_1",0),("ld_1_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1800)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ld1),imodbits_none], 

["orepublic_sniper_newai", "Republic Sniper Rifle", [("orepublic_sniper",0),("orepublic_sniper_carry",ixmesh_carry)], itp_type_musket|itp_no_pick_up_from_ground|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ld1),imodbits_none],
["orepublic_sniper_newai_aim", "Republic Sniper Rifle", [("orepublic_sniper",0),("orepublic_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1800)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ld1),imodbits_none], 

["sempire_sniper_newai", "Sith Sniper Rifle", [("sempire_sniper",0),("sempire_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_no_pick_up_from_ground|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1200)|food_quality(1)|body_armor(30)|weapon_length(anim_rifle_aim_std)|abundance(snd_blaster_shot_ld1),imodbits_none],
["sempire_sniper_newai_aim", "Sith Sniper Rifle", [("sempire_sniper",0),("sempire_sniper_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 800, weight(13)|difficulty(0)|spd_rtng(20)|shoot_speed(1000)|thrust_damage(21,pierce)|max_ammo(1)|accuracy(110)|hit_points(1800)|food_quality(1)|body_armor(1)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_blaster_shot_ld1),imodbits_none], 
 
 
 
 # AI Cycler Rifles
["cyc_rifle_newai", "Cycler_Rifle", [("Cyc_rifle",0),("Cyc_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 700, weight(5)|difficulty(0)|spd_rtng(1)|shoot_speed(1000)|thrust_damage(33,pierce)|max_ammo(1)|accuracy(110)|hit_points(500)|food_quality(1)|body_armor(16)|weapon_length(anim_rifle_aim_std)|abundance(snd_Cycler_Rifle),imodbits_none],
["cyc_rifle_newai_aim", "Cycler Rifle", [("Cyc_rifle",0),("Cyc_rifle_carry",ixmesh_carry)], itp_type_musket|itp_merchandise|itp_primary|itp_two_handed, itcf_carry_quiver_back, 
 700, weight(5)|difficulty(0)|spd_rtng(1)|shoot_speed(1000)|thrust_damage(33,pierce)|max_ammo(1)|accuracy(110)|hit_points(1500)|food_quality(1)|body_armor(2)|weapon_length(anim_rifle_aim_aim2)|abundance(snd_Cycler_Rifle),imodbits_none], 
 
 
 
 
 
 
 
 
 
 
 ###############################################################
["player_guns_end", "0", [("invalid_item",0)], 0, 0, 0, 0, 0],#_Sebastian_ IMPORTANT !!!
###############################################################

#endregion

#region Backpack rockets
["backpack_rockets",         "Z-6 warhead missile", [("mandalorian_missile_head",0),("mandalorian_missile_head_flying", ixmesh_flying_ammo)], itp_type_thrown |itp_merchandise|itp_primary|itp_no_pick_up_from_ground ,itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
460 , weight(4)|difficulty(2)|spd_rtng(89) | shoot_speed(24) | thrust_damage(0 ,  pierce)|max_ammo(1)|weapon_length(65),imodbits_thrown,
 [(ti_on_missile_hit,[(multiplayer_is_server),
	  # (store_trigger_param_1, ":agent"),
   #    (call_script, "script_spawn_scene_prop", "spr_sw_mandalorian_grenade_tmp"),#_Sebastian_
   #    (scene_prop_set_slot, reg0, slot_prop_timer, 5),
   #    (scene_prop_set_slot, reg0, slot_prop_belongs_to_agent, ":agent"),
])]],

["blast_shield", "Blast Shield", [("blast_shield",0)], itp_merchandise|itp_wooden_parry|itp_type_shield, itcf_carry_round_shield,  500 , weight(130)|hit_points(2100)|body_armor(17)|spd_rtng(61)|shield_height(130)|shield_width(55),imodbits_shield ],	#Mark
["droid_shield", "Blast Shield", [("droid_shield",0)], itp_merchandise|itp_wooden_parry|itp_type_shield, itcf_carry_round_shield,  500 , weight(130)|hit_points(2100)|body_armor(17)|spd_rtng(61)|shield_height(130)|shield_width(55),imodbits_shield ],	#Mark
["empireshield", "Blast Shield", [("empireshield",0)], itp_merchandise|itp_wooden_parry|itp_type_shield, itcf_carry_round_shield,  500 , weight(130)|hit_points(2100)|body_armor(17)|spd_rtng(61)|shield_height(130)|shield_width(55),imodbits_shield ],	#Mark
["rebel_shield", "Blast Shield", [("rebel_shield",0)], itp_merchandise|itp_wooden_parry|itp_type_shield, itcf_carry_round_shield,  500 , weight(130)|hit_points(2100)|body_armor(17)|spd_rtng(61)|shield_height(130)|shield_width(55),imodbits_shield ],	#Mark
["shield2", "Blast Shield", [("shield2",0)], itp_merchandise|itp_wooden_parry|itp_type_shield, itcf_carry_round_shield,  500 , weight(130)|hit_points(2100)|body_armor(17)|spd_rtng(61)|shield_height(130)|shield_width(55),imodbits_shield ],
#["lightsaber_shield_blue", "Blue Lightsaber(Shield)", [("lightsaber_blue_shield",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  277 , weight(3.5)|hit_points(800)|body_armor(2)|spd_rtng(80)|shield_width(20),imodbits_shield ],
["clone_blast_shield_ai", "Blast Shield", [("shield2",0)], itp_merchandise|itp_wooden_parry|itp_type_shield, itcf_carry_round_shield,  500 , weight(130)|hit_points(600)|body_armor(17)|spd_rtng(61)|shield_width(40),imodbits_shield ],	#Mark
#endregion

#region Ammo
["power_pack","Power Pack", [("ammo_pack",0),("laser_bolt_red",ixmesh_inventory),("laser_bolt_red",ixmesh_flying_ammo)], itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield, 0, 100,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(220),imodbits_missile,[light_missile_hit]],#_Sebastian_
["sonic_pack","Sonic Ammunition", [("sonic_blast",0),("laser_bolt_red",ixmesh_inventory),("sonic_blast",ixmesh_flying_ammo)], itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield, 0, 100,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(40),imodbits_missile,[light_missile_hit]],#_Sebastian_
["power_pack_mg","Power Pack(MG)", [("ammo_pack",0),("laser_bolt_red",ixmesh_inventory),("laser_bolt_red",ixmesh_flying_ammo)], itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield, 0, 100,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(130),imodbits_missile,[light_missile_hit]],#_Sebastian_
["power_pack_sniper","Power Pack(Sniper)", [("ammo_pack",0),("laser_bolt_red",ixmesh_inventory),("laser_bolt_red",ixmesh_flying_ammo)], itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield, 0, 100,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(30),imodbits_missile,[light_missile_hit]],#_Sebastian_
["power_pack_red","Red Power Pack", [("nothing",0),("laser_bolt_red",ixmesh_flying_ammo),("nothing",ixmesh_inventory),("nothing",ixmesh_carry)], itp_default_ammo|itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield, 0, 0,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(220),imodbits_missile,[light_missile_hit]],#_Sebastian_
["power_pack_blue","Blue Power Pack", [("invalid_item",0),("laser_bolt_blue",ixmesh_flying_ammo)], itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 0,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(220),imodbits_missile,[light_missile_hit]],#_Sebastian_
["power_pack_green","Green Power Pack", [("nothing",0),("laser_bolt_green",ixmesh_flying_ammo),("nothing",ixmesh_inventory),("nothing",ixmesh_carry)], itp_ignore_gravity| itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 0,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(220),imodbits_missile,[light_missile_hit]],#_Sebastian_
["shotgun_ammo","Power Pack", [("nothing",0),("laser_bolt_red",ixmesh_flying_ammo)], itp_default_ammo|itp_ignore_gravity| itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, 0, 0,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(220),imodbits_missile,[light_missile_hit]],#_Sebastian_
["cycler_bullet","Cycler Bullet", [("cycler_ammo",0),("cycler_bullet",ixmesh_inventory),("cycler_bullet_head",ixmesh_flying_ammo)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 0,weight(4.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(130),imodbits_missile,[light_missile_hit]],#_Sebastian_
#endregion

#region Forces
#forces begin
["force_jump", "Force Jump", [("icon_jump",0)], 0, 0, 800, 0, 0],
["force_throw", "Force Throw", [("icon_defend1",0)], 0, 0, 400, 0, 0],
["force_lightning", "Sith Lightning", [("icon_lightning",0)], 0, 0, 400, 0, 0],
["force_defend1", "Force Defence", [("icon_defend",0)], 0, 0, 300, 0, 0],
["force_defend2", "Force Counter", [("icon_counter",0)], 0, 0, 400, 0, 0],
["force_healing", "Force Healing", [("icon_healing",0)], 0, 0, 400, 0, 0],
["force_travel", "Force Travel", [("icon_teleport",0)], 0, 0, 800, 0, 0],
["force_sprint", "Force Sprint", [("icon_teleport",0)], 0, 0, 400, 0, 0],
["force_push", "Force Push", [("icon_push",0)], 0, 0, 400, 0, 0],
["force_damage", "Force Offence", [("icon_offence",0)], 0, 0, 300, 0, 0],
["force_jetpack", "Jetpack", [("icon_jetpack",0)], 0, 0, 900, 0, 0],
["force_jetpack_geonosian", "Geonosian Wings", [("wings_inventory",0)], 0, 0, 900, 0, 0],
["force_jetpack_manda", "Z-6 Jetpack", [("icon_jetpack",0)], 0, 0, 900, 0, 0],
["force_shield", "Personal Shield", [("personal_shield_device",0)], 0, 0, 300, 0, 0],
["power_jump", "Power Jump", [("icon_jump",0)], 0, 0, 200, 0, 0],
["force_more_points_ludicrous", "Force Points +9001", [("icon_500",0)], 0, 0, 5000, 0, 0],
["force_more_points", "Force Points +500", [("icon_500",0)], 0, 0, 300, 0, 0],
["force_faster_points", "Force Regeneration +2/s", [("icon_2x",0)], 0, 0, 300, 0, 0],
#forces end



#endregion

#region Auras
#auras begin

["aura_regen", "Group Healing", [("icon_healing",0)], 0, 0, 100, 0, 0],
["aura_frenzy", "Group Frenzy", [("icon_lightning",0)], 0, 0, 100, 0, 0],

["auras_end", "0", [("invalid_item",0)], 0, 0, 0, 0, 0],
#auras end

#endregion

#region Shotgun ammo and items end
["shotgun_ammo_red","Power Pack", [("nothing",0),("laser_bolt_red",ixmesh_flying_ammo)], itp_default_ammo|itp_ignore_gravity| itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, 0, 0,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(100),imodbits_missile,[light_missile_hit]],#_Sebastian_
["shotgun_ammo_blue","Power Pack", [("nothing",0),("laser_bolt_blue",ixmesh_flying_ammo)], itp_default_ammo|itp_ignore_gravity| itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, 0, 0,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(0,pierce)|max_ammo(100),imodbits_missile,[light_missile_hit]],#_Sebastian_

["items_end", "Items End", [("shield_round_a",0)], 0, 0, 1, 0, 0],
]
#endregion

#endregion

#_Sebastian_ dummy guns generation
for i in range (itm_player_guns_begin, itm_player_guns_end):
	items += ["dummy_"+items[i][0], "0", [("invalid_item",0)], itp_type_musket, itcf_shoot_musket, 0, items[i][6], 0],
