from header_common import *
from header_operations import *
from module_constants import *
from header_parties import *
from header_skills import *
from header_mission_templates import *
from header_items import *
from header_triggers import *
from header_terrain_types import *
from header_music import *
from header_map_icons import *
from ID_animations import *
from ID_items import *


#from module_important_scripts import * 
#from module_shit_scripts import * 
#from module_adimi_scripts import * 
#from module_sw_scripts import * 






sw_scripts = [
#region Base scripts
 
#sw
("cf_jedi_projectile_defend_condition",
[
    (store_script_param, ":wounded", 1),
    #(store_script_param, ":attacker", 2),
    #(store_script_param, ":damage", 3),
    (store_script_param, ":position", 4),
    (store_script_param, ":item", 5),
	#carrying lightsaber?
	(assign, ":continue", 1),
	(agent_get_wielded_item, ":wounded_item", ":wounded", 0),
	(try_begin),
	  (call_script, "script_cf_is_lightsaber", ":wounded_item"),
	(else_try),
	  (call_script, "script_cf_is_thrown_lightsaber", ":wounded_item"),
	(else_try),
	  (assign, ":continue", 0),
	(try_end),
	(eq, ":continue", 1),
	#attacked by ranged weapon?
	(ge, ":item", 0),
	(item_get_type, ":type", ":item"),
	(this_or_next|eq, ":type", itp_type_pistol),
	(this_or_next|eq, ":type", itp_type_musket),
	(this_or_next|eq, ":type", itp_type_thrown),
	(this_or_next|eq, ":type", itp_type_bow),
	(eq, ":type", itp_type_crossbow),
	#is currently blocking?
	(agent_get_defend_action, ":action", ":wounded"),
	(this_or_next|eq, ":action", 1), #parrying
	(eq, ":action", 2), #blocking
	#blocking in the right direction?
	(copy_position, pos0, ":position"),
	(position_move_y, pos0, -1000),          
	(agent_get_look_position, pos1, ":wounded"),
	(position_transform_position_to_local, pos2, pos1, pos0),
	(position_get_y, ":y", pos2),
	(ge, ":y", 0),
]),
("cf_jedi_lightning_defend_condition",
[
    (store_script_param, ":wounded", 1),
    (store_script_param, ":attacker", 2),
	#carrying lightsaber?
	(agent_get_wielded_item, ":wounded_item", ":wounded", 0),
	(call_script, "script_cf_is_lightsaber", ":wounded_item"),
	#is currently blocking?
	(agent_get_defend_action, ":action", ":wounded"),
	(this_or_next|eq, ":action", 1), #parrying
	(eq, ":action", 2), #blocking
	#blocking in the right direction?
	(agent_get_position, pos0, ":attacker"),
	(position_move_z, pos0, 130),
	(agent_get_look_position, pos1, ":wounded"),
	(position_transform_position_to_local, pos2, pos1, pos0),
	(position_get_y, ":y", pos2),
	(ge, ":y", 0),
]),
("calculate_ranged_weapon_damage",
[
		(store_script_param, ":item", 1),
		(store_script_param, ":add_damage", 2),
#		(store_script_param, ":attacker", 3),
#		(store_script_param, ":defender", 4),
		#damage calculation
		(try_begin),
		  (ge, ":item", 0),
		  (item_get_slot, ":max_damage", ":item", slot_item_damage),
		  (store_div, ":half_damage", ":max_damage", 2),
		  (store_random_in_range, ":new_damage", ":half_damage", ":max_damage"),
		(else_try),
		  (assign, ":new_damage", 0),
		(try_end),

		#addition for critical hits:
		(store_mul, ":damagex3", ":add_damage", 3),
		(val_add, ":new_damage", ":damagex3"),

		(assign, reg0, ":new_damage"),
]),
("agent_deliver_nonlethal_damage",
[
#		(store_script_param, ":attacker", 1),
		(store_script_param, ":defender", 2),
		(store_script_param, ":damage", 3),
		(store_agent_hit_points, ":hp", ":defender", 1),
		(val_sub, ":hp", ":damage"),
		(val_max, ":hp", 0),
		(agent_set_hit_points, ":defender", ":hp", 1),
]),
("cf_is_ranged",
[
		(store_script_param, ":item", 1),
		(gt, ":item", -1),#_Sebastian_
		(item_get_type, ":type", ":item"),
		(this_or_next|eq, ":type", itp_type_musket),#_Sebastian_
		(eq, ":type", itp_type_crossbow),#_Sebastian_
]),
("manual_projectile_damage",
[
	(try_begin),
		(store_script_param, ":wounded", 1),
        (store_script_param, ":attacker", 2),
		(store_script_param, ":damage", 3),
		#(store_script_param, ":position", 4),
		(store_script_param, ":item", 5),

		#attacked by ranged weapon?
		(call_script, "script_cf_is_ranged", ":item"),

		#play death/hit sound
		(store_agent_hit_points, ":hp", ":wounded", 1),
		(assign, ":sound", -1),
		(try_begin),
		  (le, ":damage", ":hp"),
		  #hit
		  (assign, ":sound", "snd_man_stun"),
		(try_end),
		(agent_play_sound, ":wounded", ":sound"),#_Sebastian_

		#finally, do damage
		(call_script, "script_agent_deliver_nonlethal_damage", ":attacker", ":wounded", ":damage"),
	(try_end),
]),
("player_requested_force_usement",
[
	(store_script_param, ":player_no", 1),
	(store_script_param, ":force", 2),
	(store_script_param, ":value", 3),
	(try_begin),
		(player_is_active,":player_no"),
		(player_get_agent_id, ":agent_no", ":player_no"),
		(assign, ":valid", 0),
		
        (try_begin),
			(agent_is_active,":agent_no"),
			(agent_is_alive,":agent_no"),
			(agent_get_wielded_item, ":item", ":agent_no", 0),
			(try_begin),
				(call_script, "script_cf_player_has_force_power", ":player_no", ":force"),  #agent has force check
				(assign, ":valid", 1),
			(try_end),
		  #region not occupied
			(agent_get_animation, ":anim", ":agent_no", 0),
		  (try_begin),
		    (neq, ":anim", "anim_force_choke"),
		    (agent_slot_eq, ":agent_no", slot_agent_occupied_with, 0),
		  (else_try),
		    (assign, ":valid", 0),
		  (try_end),
          #endregion
		  #region cooldown check
		  (troop_get_slot, ":cooldown", "trp_force_cooldown_array", ":force"),
		  (try_begin),
		    (eq, ":valid", 1),
		    (gt, ":cooldown", 0),
			(call_script, "script_player_get_force_power_slot", ":player_no", ":force"),
			(store_add, ":force_cooldown_slot", player_force_power_cooldown_0, reg0),
			(player_get_slot, ":cooldown_until", ":player_no", ":force_cooldown_slot"),
			(store_mission_timer_a_msec, ":time"),
			(lt, ":time", ":cooldown_until"),
			(assign, ":valid", 0),
		  (try_end),
          #endregion
		(try_end),

		  #region force usement cost
		  (try_begin),
		    (eq, ":valid", 1),
		    (troop_get_slot, ":cost", "trp_force_use_cost_array", ":force"),
			(try_begin),#dont take money for stopping forces
			  (eq, ":value", 0),
			  (this_or_next|eq, ":force", force_jetpack),
			  (this_or_next|eq, ":force", force_jetpack_geonosian),
			  (this_or_next|eq, ":force", force_jump),
			  (eq, ":force", power_jump),
			  (assign, ":cost", 0),
			(try_end),
		    (player_get_slot, ":player_gold", ":player_no", slot_player_force_points),
			(ge, ":player_gold", ":cost"),
		  (else_try),
		    (assign, ":valid", 0),
		  (try_end),
          #endregion
		  
          
          
            (try_begin),
		    #region Old Forces
                (this_or_next|eq, ":force", force_jump),
		        (eq, ":force", power_jump),
			    (try_begin),
		          (eq, ":valid", 1),
			      (eq, ":value", 1),
			      (try_begin),
			      (eq, ":anim", "anim_jump"),
			        (agent_set_animation, ":agent_no", "anim_sw_force_jump2", 0),#_Sebastian_
			        (agent_set_slot, ":agent_no", slot_agent_using_force, ":force"),
				    (try_begin),
		              (eq, ":force", power_jump),
			          (agent_set_slot, ":agent_no", slot_agent_stop_force_in, 6),
				    (else_try),
			          (agent_set_slot, ":agent_no", slot_agent_stop_force_in, 10),
				    (try_end),
			      (else_try),
			        (assign, ":valid", 0),
			      (try_end),
			    (else_try),
			      (eq, ":value", 0),
			      (eq, ":anim", "anim_sw_force_jump2"),
			      (agent_set_animation, ":agent_no", "anim_sw_force_jump2_end", 0),#_Sebastian_
			    (try_end),
		  (else_try),
		    (eq, ":valid", 1),
			(eq, ":force", force_lightning),
			(agent_set_slot, ":agent_no", slot_agent_lightning_used, 0),
		    (agent_set_animation, ":agent_no", "anim_force_lightning", 0),#_Sebastian_
		  (else_try),
		    (eq, ":valid", 1),
		    (eq, ":force", force_damage),
			(agent_set_slot, ":agent_no", slot_agent_using_force, force_damage),
			(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent_no", slot_agent_using_force, force_damage),
			(agent_set_slot, ":agent_no", slot_agent_stop_force_in, 100),
		  (else_try),
		    (eq, ":valid", 1),
		    (eq, ":force", force_sprint),
			(agent_set_slot, ":agent_no", slot_agent_using_force, force_sprint),
			(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent_no", slot_agent_using_force, force_sprint),
			(agent_set_slot, ":agent_no", slot_agent_stop_force_in, 200),
		  (else_try),
		    (eq, ":valid", 1),
		    (eq, ":force", force_healing),
			(call_script, "script_server_force_healing", ":agent_no"),
		    (agent_is_active,":agent_no"),
            (agent_is_alive, ":agent_no"),
			(agent_set_hit_points, ":agent_no", 100, 0),
			(agent_set_slot, ":agent_no", slot_agent_stop_force_in, 0),
		  (else_try),
		        (eq, ":valid", 1),
		        (eq, ":force", force_throw),
			    (try_begin),
			      (lt, ":item", 0),
			      (call_script, "script_agent_force_pull_lightsaber", ":agent_no"),
			    (else_try),
		          (call_script, "script_cf_is_lightsaber", ":item"),
			      (agent_unequip_item, ":agent_no", ":item"),
			      (call_script, "script_lightsaber_get_thrown", ":item"),
			      (agent_equip_item, ":agent_no", "itm_hilt_1_red_thrown"),
			      (agent_set_wielded_item, ":agent_no", "itm_hilt_1_red_thrown"),
			    (else_try),
			      (is_between, ":item", lightsabers_thrown_begin, lightsabers_thrown_end),
			      (agent_unequip_item, ":agent_no", ":item"),
			      (call_script, "script_thrown_get_lightsaber", ":item"),
			      (agent_equip_item, ":agent_no", "itm_hilt1_red"),
			      (agent_set_wielded_item, ":agent_no", "itm_hilt1_red"),
			    (try_end),
		  (else_try),
		    (eq, ":force", force_push),
			(try_begin),
		      (eq, ":valid", 1),
			  (call_script, "script_get_force_push_victim", ":agent_no"),
			  (assign, ":victim", reg0),
			  (try_begin),
			    (ge, ":victim", 0),
				(agent_get_position, pos1, ":agent_no"),
				(agent_get_look_position, pos2, ":victim"),
				(position_transform_position_to_local, pos3, pos2, pos1),
				(position_get_y, ":y", pos3),
				(try_begin),
				  (lt, ":y", 0), #victim doesnt see attacker
		          (agent_set_animation, ":agent_no", "anim_sw_force_push_user", 0),#_Sebastian_
			      #(multiplayer_send_int_to_player, ":player_no", multiplayer_event_agent_choke_return, ":victim"),
			      (agent_set_slot, ":agent_no", slot_agent_using_force, force_push),
			      (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent_no", slot_agent_using_force, force_push),
			      (agent_set_slot, ":agent_no", slot_agent_force_victim, ":victim"),
				  (agent_set_slot, ":agent_no", slot_agent_stop_force_in, 50),
				(else_try),
				  (position_copy_rotation, pos2, pos1),
				  (position_rotate_z, pos2, 180),
				  (position_get_rotation_around_z, ":z_rot", pos2),
#				  (agent_set_position, ":victim", pos2),
#				  (get_max_players, ":max_players"),
#				  (multiplayer_get_my_player, ":my_player"),
#				  (try_for_range, ":cur_player", 0, ":max_players"),
#					(neq, ":cur_player", ":my_player"),
#					(player_is_active, ":cur_player"),
#				    (multiplayer_send_2_int_to_player, ":cur_player", multiplayer_agent_set_rotation_sync, ":victim", ":z_rot"),
#				  (try_end),
#				  (call_script, "script_agent_set_animation_sync", ":victim", "anim_sw_force_push", 0),
                  (call_script, "script_get_pushed_anim", ":victim", ":z_rot"),
	              (assign, ":animation", reg0),
				  (agent_set_team, ":victim", 2),
	              (agent_set_animation, ":victim", ":animation", 0),#_Sebastian_

				  (agent_set_animation, ":agent_no", "anim_sw_force_push_user_end", 0),#_Sebastian_
				(try_end),
			  (else_try),
		        (assign, ":valid", 0),
			  (try_end),
			(try_end),
		  (else_try),
		    (eq, ":force", force_jetpack),
			(try_begin),
		      (eq, ":valid", 1),
			  (eq, ":value", 1),
			  (try_begin),
			    (this_or_next|eq, ":anim", "anim_jump"),
	            (this_or_next|eq, ":anim", "anim_jump_loop"),
			    (eq, ":anim", "anim_sw_force_jump_end"),
#			    (agent_get_position, pos1, ":agent_no"),
#			    (position_get_z, ":z", pos1),
#			    (le, ":z", 7000),
			    (agent_set_animation, ":agent_no", "anim_sw_force_jump", 0),#_Sebastian_
			    (agent_set_slot, ":agent_no", slot_agent_stop_force_in, 0),
			    (agent_set_slot, ":agent_no", slot_allowed_to_be_in_kick_anim, 1),
			  (else_try),
			    (assign, ":valid", 0),
			  (try_end),
			(else_try),
			  (eq, ":value", 0),
			  (eq, ":anim", "anim_sw_force_jump"),
			  (agent_set_animation, ":agent_no", "anim_sw_force_jump_end", 0),#_Sebastian_
			(try_end),
		  (else_try),
		    (eq, ":force", force_jetpack_geonosian),
			(try_begin),
		      (eq, ":valid", 1),
			  (eq, ":value", 1),
			  (try_begin),
			    (this_or_next|eq, ":anim", "anim_jump"),
	            (this_or_next|eq, ":anim", "anim_jump_loop"),
			    (eq, ":anim", "anim_sw_force_jump_end"),
#			    (agent_get_position, pos1, ":agent_no"),
#			    (position_get_z, ":z", pos1),
#			    (le, ":z", 7000),
			    (agent_set_animation, ":agent_no", "anim_sw_force_jump", 0),#_Sebastian_
			    (agent_set_slot, ":agent_no", slot_agent_stop_force_in, 0),
			    (agent_set_slot, ":agent_no", slot_allowed_to_be_in_kick_anim, 1),
			  (else_try),
			    (assign, ":valid", 0),
			  (try_end),
			(else_try),
			  (eq, ":value", 0),
			  (eq, ":anim", "anim_sw_force_jump"),
			  (agent_set_animation, ":agent_no", "anim_sw_force_jump_end", 0),#_Sebastian_
			(try_end),	
		  (else_try),
		    (eq, ":force", force_shield),
			(call_script, "script_agent_equip_item_sync", ":agent_no", "itm_personal_shield"),
            #endregion
            #region New Forces
            (else_try),
                 (eq, ":force", 666),
                 #(call_script, "script_force", ":player_no", ":player_gold"),
            (else_try),
            #endregion
            (try_end),

		  #cost
		  (try_begin),
		    (eq, ":valid", 1),
			(val_sub, ":player_gold", ":cost"),
		    (call_script, "script_player_set_force_points", ":player_no", ":player_gold"),
		  #set cooldown
		  (try_begin),
		    (eq, ":valid", 1),
		    (gt, ":cooldown", 0),
			(store_add, ":cooldown_until", ":time", ":cooldown"),
			(player_set_slot, ":player_no", ":force_cooldown_slot", ":cooldown_until"),
		    (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":force_cooldown_slot", ":cooldown"),
		  (try_end),
    (try_end),
]),
("server_force_lightning",
[
          (store_script_param, ":agent", 1),
		  (agent_get_look_position, pos1, ":agent"),
		  (position_move_z,pos1, 140, 1),
		  (position_move_y,pos1, 100),
		  (particle_system_burst, "psys_sw_lightning", pos1, 50),
		  (particle_system_burst, "psys_sw_lightning_b", pos1, 8),
		  (agent_play_sound, ":agent", "snd_force_lightning"),#_Sebastian_
		  (try_for_agents, ":agent_b"),
			  (neq, ":agent_b", ":agent"),
			  (agent_get_position, pos2, ":agent_b"),
			  (get_distance_between_positions, ":dist", pos1, pos2),
			  (le, ":dist", 500),
			  (position_transform_position_to_local, pos3, pos1, pos2),
			  (position_get_y, ":y", pos3),
			  (gt, ":y", 0),
			  (try_begin),
			    (call_script, "script_cf_jedi_lightning_defend_condition", ":agent_b", ":agent"),
				(assign, ":blocking_cost", 50),
				(try_begin),
					(call_script, "script_cf_agent_has_force_power", ":agent_b", force_defend1),
					(assign, ":blocking_cost", 10), #Sherlock defend, value was 30.
				(try_end),
				(agent_get_player_id, ":player_no", ":agent_b"),
				(ge, ":player_no", 0),
				(player_get_slot, ":force_points", ":player_no", slot_player_force_points),
				(ge, ":force_points", ":blocking_cost"),
				(val_sub, ":force_points", ":blocking_cost"),
				(call_script, "script_player_set_force_points", ":player_no", ":force_points"),
			  (else_try),
			    (position_move_z, pos2, 100),
			    (particle_system_burst, "psys_sw_lightning_victim", pos2, 100),
			    (particle_system_burst, "psys_sw_lightning_b", pos2, 15),
                (try_begin),#Anti Duell Lightining Begin
                   (eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
                   (agent_get_slot, ":opponent_agent", ":agent", slot_agent_in_duel_with),
                   (eq,":opponent_agent",":agent_b"),
			       (agent_deliver_damage_to_agent, ":agent", ":agent_b", 30),
			       (agent_is_human, ":agent_b"),
			       (agent_get_horse, ":horse", ":agent_b"),
			        (try_begin),
			         (ge, ":horse", 0),
				     (agent_set_animation, ":agent_b", "anim_force_lightning_victim", 1),#_Sebastian_
			        (else_try),
				     (agent_set_animation, ":agent_b", "anim_force_lightning_victim", 0),#_Sebastian_
			        (try_end),
                (else_try),
                     (neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
                     (agent_deliver_damage_to_agent, ":agent", ":agent_b", 30),
			         (agent_is_human, ":agent_b"),
			         (agent_get_horse, ":horse", ":agent_b"),
			        (try_begin),
			         (ge, ":horse", 0),
			   	     (agent_set_animation, ":agent_b", "anim_force_lightning_victim", 1),#_Sebastian_
			        (else_try),
				     (agent_set_animation, ":agent_b", "anim_force_lightning_victim", 0),#_Sebastian_
			        (try_end),
			    (try_end),#Anti Duell Lightining End
			  (try_end),
		  (try_end),
]),

("bf_data_init", [
  (store_script_param, ":agent_no", 1),
  
  (agent_get_troop_id, ":trp_id", ":agent_no"),
	(try_begin),
    (eq, ":trp_id", "trp_bf_settings"),
  (agent_set_slot, ":agent_no", vyrn_ai_settings, 1),
	(try_end),
		
  (agent_get_troop_id, ":trp_id", ":agent_no"),
	(try_begin),
	(this_or_next|eq, ":trp_id", "trp_jedi_force_multiplayer_ai"),
	(this_or_next|eq, ":trp_id", "trp_sith_force_multiplayer_ai"),
	(this_or_next|eq, ":trp_id", "trp_jedi_multiplayer_ai"),
    (eq, ":trp_id", "trp_sith_multiplayer_ai"),
  (agent_set_slot, ":agent_no", slot_agent_has_vyrn_AI, 1),
	(try_end),
	
  (agent_get_troop_id, ":trp_id", ":agent_no"),
	(try_begin),
	(this_or_next|eq, ":trp_id", "trp_jedi_force_multiplayer_ai"),
	(this_or_next|eq, ":trp_id", "trp_sith_force_multiplayer_ai"),
	(this_or_next|eq, ":trp_id", "trp_jedi_multiplayer_ai"),
    (eq, ":trp_id", "trp_sith_multiplayer_ai"),
  (agent_set_slot, ":agent_no", slot_vyrn_AI_chance, 2),
	(try_end),
	
  (agent_get_troop_id, ":trp_id", ":agent_no"),
	(try_begin),
	(this_or_next|eq, ":trp_id", "trp_jedi_force_multiplayer_ai"),
	(this_or_next|eq, ":trp_id", "trp_sith_force_multiplayer_ai"),
	(this_or_next|eq, ":trp_id", "trp_jedi_multiplayer_ai"),
    (eq, ":trp_id", "trp_sith_multiplayer_ai"),
  (agent_set_slot, ":agent_no", cur_agent_fighting_style, -1),
	(try_end),
	   
]),

("multiplayer_calculate_cur_selected_forces_cost",
[
     (store_script_param, ":player_no", 1),
	 #force powers
	 (player_get_troop_id, ":player_troop", ":player_no"),
	 (assign, ":total_cost", 0),
	 (try_for_range, ":force_no", 0, 10),
	   (store_add, ":player_force_slot", ":force_no", player_force_power_0),
	   (store_add, ":troop_force_slot", ":force_no", troop_force_power_0),
	   (troop_get_slot, ":force", ":player_troop", ":troop_force_slot"),
	   (player_get_slot, ":selected", ":player_no", ":player_force_slot"),
	   (try_begin),
	     (ge, ":selected", 1),
		 (troop_get_slot, ":cost", "trp_force_cost_array", ":force"),
         (val_add, ":total_cost", ":cost"),
	   (try_end),
	 (try_end),
	 (assign, reg0, ":total_cost"),
]),
("player_get_force_power_slot",
[
     (store_script_param, ":player_no", 1),
     (store_script_param, ":force", 2),
	 (assign, ":force_slot", -1),
	 (try_for_range, ":force_no", 0, 10),
	   (store_add, ":force_used_slot", ":force_no", player_force_power_used_0),
	   (player_get_slot, ":cur_force", ":player_no", ":force_used_slot"),
	   (eq, ":cur_force", ":force"),
	   (assign, ":force_slot", ":force_no"),
	 (try_end),
	 (assign, reg0, ":force_slot"),
]),
("cf_player_has_force_power",
[
     (store_script_param, ":player_no", 1),
     (store_script_param, ":force", 2),
	 (call_script, "script_player_get_force_power_slot", ":player_no", ":force"),
	 (ge, reg0, 0),
]),
("cf_agent_has_force_power",
[
     (store_script_param, ":agent_id", 1),
     (store_script_param, ":force", 2),
     (agent_is_active,":agent_id"),
     (agent_is_alive,":agent_id"),
     (neg|agent_is_non_player,":agent_id"),
	 (agent_get_player_id, ":player_no", ":agent_id"),
	 (player_is_active, ":player_no"),
	 (call_script, "script_cf_player_has_force_power", ":player_no", ":force"),
]),
("get_next_useable_force_power",
[
	(store_script_param, ":cur_force", 1),
	(store_script_param, ":player", 2),
	(assign, ":end_cond", 11),
	(try_for_range, ":force_num", 0, ":end_cond"),

	    (try_begin),
		  (eq, ":force_num", 10),
		  (assign, ":cur_force", -1),
		(try_end),

		(lt, ":force_num", 10),
		(val_add, ":cur_force", 1),

		(try_begin),
		  (ge, ":cur_force", 10),
		  (assign, ":cur_force", 0),
		(try_end),

		(store_add, ":force_used_slot", ":cur_force", player_force_power_used_0),
		(player_get_slot, ":force", ":player", ":force_used_slot"),
		(ge, ":force", 1),

		#unuseable:
		(neq, ":force", force_defend1),
		(neq, ":force", force_defend2),
		(neq, ":force", force_ai_droid),
		(neq, ":force", force_ai_clone),
	    (neq, ":force", force_more_points_ludicrous), #Mark
		(neq, ":force", force_more_points),
		(neq, ":force", force_faster_points),
		(assign, ":end_cond", 0), #break

	(try_end),
	(assign, reg0, ":cur_force"),
]),

#force travel
  # script_shift_pos1_along_y_axis_to_ground
  # Input: arg1: increment (in centimeters) to shift on each tick
  #        arg2: max distance to shift before failing
  # Output: pos1 shifted to ground level along forward y-axis
  ("shift_pos1_along_y_axis_to_ground",
    [
      (store_script_param, ":increment", 1), #y value to shift at each tick
      (store_script_param, ":max_distance", 2),
      (assign, reg0, 1), #output value
      (assign, ":distance_so_far", 0),
      (call_script, "script_shift_pos1_along_y_axis_to_ground_aux", ":increment", ":max_distance", ":distance_so_far")
    ]),
  ("shift_pos1_along_y_axis_to_ground_aux",
    [
      (store_script_param, ":increment", 1), #y value to shift at each tick
      (store_script_param, ":max_distance", 2),
      (store_script_param, ":distance_so_far", 3), #y value to shift at each tick
      (get_scene_boundaries, pos10, pos11),
      (position_get_x, ":scene_min_x", pos10),
      (position_get_y, ":scene_min_y", pos10),
      (position_get_x, ":scene_max_x", pos11),
      (position_get_y, ":scene_max_y", pos11),
      (copy_position, pos2, pos1),
      (position_set_z_to_ground_level, pos2),
      (position_get_x, ":pos1_x", pos1),
      (position_get_y, ":pos1_y", pos1),
      (position_get_z, ":pos1_z", pos1),
      (position_get_z, ":ground_z", pos2),
      (try_begin),                                                  #IF:
        (this_or_next|ge, ":distance_so_far", ":max_distance"),     #distance so far > max_distance OR
        (this_or_next|le, ":pos1_x", ":scene_min_x"),               #pos1 x <= scene min x OR
        (this_or_next|le, ":pos1_y", ":scene_min_y"),               #pos1 y <= scene min y OR
        (this_or_next|ge, ":pos1_x", ":scene_max_x"),               #pos1 x >= scene max x OR
        (ge, ":pos1_y", ":scene_max_y"),                            #pos1 y >= scene max y THEN
        (assign, reg0, -1),                                         #return failed output
      (else_try),
        (gt, ":increment", 0),
        (le, ":ground_z", ":pos1_z"),
        (position_move_y, pos1, ":increment"),
        (val_add, ":distance_so_far", ":increment"),
        (call_script, "script_shift_pos1_along_y_axis_to_ground_aux", ":increment", ":max_distance", ":distance_so_far"),
      (else_try),
        (lt, ":increment", 0),
        (ge, ":ground_z", ":pos1_z"),
        (position_move_y, pos1, ":increment"),
        (val_sub, ":distance_so_far", ":increment"),
        (call_script, "script_shift_pos1_along_y_axis_to_ground_aux", ":increment", ":max_distance", ":distance_so_far"),
      (else_try),
        (position_set_z_to_ground_level, pos1),
      (try_end),
    ]),
#/force travel
("server_force_healing",
[
      (store_script_param, ":agent", 1),
	  (agent_get_position, pos1, ":agent"),
	  (agent_get_team, ":team_a", ":agent"),
	  (agent_play_sound, ":agent", "snd_force_healing"),#_Sebastian_
	  (particle_system_burst, "psys_sw_healing", pos1, 100),
	  (try_for_agents, ":agent_b"),
	    (agent_is_alive, ":agent_b"),
		(agent_get_team, ":team_b", ":agent_b"),
		(neg|teams_are_enemies, ":team_a", ":team_b"),
		(agent_get_position, pos2, ":agent_b"),
		(get_distance_between_positions, ":dist", pos1, pos2),
		(le, ":dist", 500),
		(store_agent_hit_points, ":hp", ":agent_b", 1),
		(store_random_in_range, ":add", 10, 20),
		(val_add, ":hp", ":add"),
		(agent_set_hit_points, ":agent_b", ":hp", 1),
		(position_move_z, pos2, 170),
		(eq, ":agent_b", 1),
	    (particle_system_burst, "psys_sw_healing", pos2, 100),
	  (try_end),
]),
("cf_is_lightsaber",
[
      (store_script_param, ":item", 1),
	  (this_or_next|eq, ":item", "itm_hilt1_red"),
	  (this_or_next|eq, ":item", "itm_hilt2_red"),
	  (this_or_next|eq, ":item", "itm_hilt3_red"),
	  (this_or_next|eq, ":item", "itm_hilt1_blue"),
	  #(this_or_next|eq, ":item", "itm_hilt1_blue_dual"),
	  #(this_or_next|eq, ":item", "itm_hilt1_blue_offhand"),
	  (this_or_next|eq, ":item", "itm_hilt2_blue"),
	  (this_or_next|eq, ":item", "itm_hilt3_blue"),
	  (this_or_next|eq, ":item", "itm_hilt4_red"),
      (this_or_next|eq, ":item", "itm_hilt1_purple"),
	  (this_or_next|eq, ":item", "itm_hilt3_purple"),
	  (this_or_next|eq, ":item", "itm_hilt1_green"),
	  (this_or_next|eq, ":item", "itm_lightsaber_2_ai"),
	  (this_or_next|eq, ":item", "itm_lightsaber_3_ai"),
	  (this_or_next|eq, ":item", "itm_hilt2_green"),
	  (this_or_next|eq, ":item", "itm_hilt1_yellow"),	  
	  (this_or_next|eq, ":item", "itm_fpike_red"),	  
	  (this_or_next|eq, ":item", "itm_fpike_blue"),	  
	  (this_or_next|eq, ":item", "itm_hilt1_orange"),	    	  
	  (eq, ":item", "itm_hilt3_green"),
]),
("cf_neg_is_lightsaber",
[
      (store_script_param, ":item", 1),
	  (assign, ":continue", 1),
	  (try_begin),
	    (call_script, "script_cf_is_lightsaber", ":item"),
		(assign, ":continue", 0),
	  (try_end),
	  (eq, ":continue", 1),
]),
("cf_is_thrown_lightsaber",
[
      (store_script_param, ":item", 1),
	  (this_or_next|eq, ":item", "itm_hilt_throw1_red_thrown"),
	  (this_or_next|eq, ":item", "itm_hilt_throw1_blue_thrown"),
	  (this_or_next|eq, ":item", "itm_hilt_throw1_purple_thrown"),
	  (eq, ":item", "itm_hilt_throw1_green_thrown"),
]),
("cf_neg_is_thrown_lightsaber",
[
      (store_script_param, ":item", 1),
	  (assign, ":continue", 1),
	  (try_begin),
	    (call_script, "script_cf_is_thrown_lightsaber", ":item"),
		(assign, ":continue", 0),
	  (try_end),
	  (eq, ":continue", 1),
]),
("lightsaber_get_blade",
[(store_script_param, ":wielded_saber_hilt", 1), #Has to be a valid hilt
 
(try_begin),
 
        #Red
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt1_red"),
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_red"),
		(this_or_next|eq, ":wielded_saber_hilt", "itm_lightsaber_3_ai"),
        (eq, ":wielded_saber_hilt", "itm_hilt3_red"),
 
        (assign, reg0, "itm_lightsaber_blade_red_1"), #Give out the blade color
 
 (else_try),
       
        #Blue
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt1_blue"),
		#(this_or_next|eq, ":wielded_saber_hilt", "itm_hilt1_blue_dual"),
		#(this_or_next|eq, ":wielded_saber_hilt", "itm_hilt1_blue_offhand"),
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_blue"),
        (eq, ":wielded_saber_hilt", "itm_hilt3_blue"),
 
        (assign, reg0, "itm_lightsaber_blade_blue_1"), #Give out the blade color
       
 (else_try),
       
        #Purple
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt1_purple"),
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_purple"),
        (eq, ":wielded_saber_hilt", "itm_hilt3_purple"),
 
        (assign, reg0, "itm_lightsaber_blade_purple_1"), #Give out the blade color
		
 (else_try),
       
        #Yellow
        (eq, ":wielded_saber_hilt", "itm_hilt1_yellow"),
       # (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_purple"),
       # (eq, ":wielded_saber_hilt", "itm_hilt3_purple"),
 
        (assign, reg0, "itm_lightsaber_blade_yellow_1"), #Give out the blade color	

 (else_try),
       
        #Yellow
        (eq, ":wielded_saber_hilt", "itm_hilt1_orange"),
       # (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_purple"),
       # (eq, ":wielded_saber_hilt", "itm_hilt3_purple"),
 
        (assign, reg0, "itm_lightsaber_blade_orange_1"), #Give out the blade color		
(else_try),
       
        #Red Pike
        (eq, ":wielded_saber_hilt", "itm_fpike_red"),
       # (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_purple"),
       # (eq, ":wielded_saber_hilt", "itm_hilt3_purple"),
 
        (assign, reg0, "itm_lightsaber_blade_red_3"), #Give out the blade color		
(else_try),
       
        #Blue Pike
        (eq, ":wielded_saber_hilt", "itm_fpike_blue"),
       # (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_purple"),
       # (eq, ":wielded_saber_hilt", "itm_hilt3_purple"),
 
        (assign, reg0, "itm_lightsaber_blade_blue_3"), #Give out the blade color		
(else_try),
       
        #Red Double Bladed
        (eq, ":wielded_saber_hilt", "itm_hilt4_red"),
       # (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_purple"),
       # (eq, ":wielded_saber_hilt", "itm_hilt3_purple"),
 
        (assign, reg0, "itm_lightsaber_blade_red_2"), #Give out the blade color			
       
 (else_try),
       
        #Green
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt1_green"),
        (this_or_next|eq, ":wielded_saber_hilt", "itm_hilt2_green"),
		(this_or_next|eq, ":wielded_saber_hilt", "itm_lightsaber_2_ai"),
        (eq, ":wielded_saber_hilt", "itm_hilt3_green"),
 
        (assign, reg0, "itm_lightsaber_blade_green_1"), #Give out the blade color
(try_end),
]),
("lightsaber_get_thrown",
[
      (store_script_param, ":lightsaber_hilt", 1),
	  
	  (try_begin),
		(eq, ":lightsaber_hilt", "itm_hilt1_red"),
		(assign, reg0, "itm_hilt_1_red_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt2_red"),
		(assign, reg0, "itm_hilt_2_red_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt3_red"),
		(assign, reg0, "itm_hilt_3_red_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt1_blue"),
		(assign, reg0, "itm_hilt_1_blue_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt2_blue"),
		(assign, reg0, "itm_hilt_2_blue_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt3_blue"),
		(assign, reg0, "itm_hilt_3_blue_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt1_green"),
		(assign, reg0, "itm_hilt_1_green_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt2_green"),
		(assign, reg0, "itm_hilt_2_green_thrown"),
	  (else_try),
	    (eq, ":lightsaber_hilt", "itm_hilt3_green"),
		(assign, reg0, "itm_hilt_3_green_thrown"),
	  (try_end),
]),
("thrown_get_lightsaber",
[
      (store_script_param, ":thrown_item", 1),
	  
	  (try_begin),
		(eq, ":thrown_item", "itm_hilt_1_red_thrown"),
		(assign, reg0, "itm_hilt1_red"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_2_red_thrown"),
		(assign, reg0, "itm_hilt2_red"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_3_red_thrown"),
		(assign, reg0, "itm_hilt3_red"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_1_blue_thrown"),
		(assign, reg0, "itm_hilt1_blue"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_2_blue_thrown"),
		(assign, reg0, "itm_hilt2_blue"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_3_blue_thrown"),
		(assign, reg0, "itm_hilt3_blue"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_1_green_thrown"),
		(assign, reg0, "itm_hilt1_green"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_2_green_thrown"),
		(assign, reg0, "itm_hilt2_green"),
	  (else_try),
		(eq, ":thrown_item", "itm_hilt_3_green_thrown"),
		(assign, reg0, "itm_hilt3_green"),
	  (try_end),
]),
("cf_troop_is_jedi",
[
      (store_script_param, ":troop", 1),
	  (this_or_next|eq, ":troop", "trp_sith_force_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_sith_lightsaber_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_sith_empire_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_jedi_force_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_jedi_force_multiplayer_ai"),
	  (this_or_next|eq, ":troop", "trp_sith_force_multiplayer_ai"),
	  (this_or_next|eq, ":troop", "trp_jedi_multiplayer_ai"),
	  (this_or_next|eq, ":troop", "trp_sith_multiplayer_ai"),
	  (this_or_next|eq, ":troop", "trp_jedi_rebels_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_sith_acolyte_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_jedi_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_jedi_padawan_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_jedi_padawan_multiplayer_1"),
	  (this_or_next|eq, ":troop", "trp_jedi_padawan_multiplayer_2"),
	  (this_or_next|eq, ":troop", "trp_sith_apprentice_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_sith_apprentice_multiplayer_1"),
	  (this_or_next|eq, ":troop", "trp_sith_apprentice_multiplayer_2"),
	  (this_or_next|eq, ":troop", "trp_christmas_jedi_multiplayer"),
	  (this_or_next|eq, ":troop", "trp_christmas_sith_multiplayer"),
	  (eq, ":troop", "trp_jedi_lightsaber_multiplayer"),
]),
("cf_is_jedi",
[
      (store_script_param, ":agent", 1),
	  (agent_get_troop_id, ":troop", ":agent"),
	  (call_script, "script_cf_troop_is_jedi", ":troop"),
]),
("agent_force_pull_lightsaber",
[
	  (store_script_param, ":agent_no", 1),
	  (set_fixed_point_multiplier, 100),
	  (agent_get_position, pos1, ":agent_no"),
	  (assign, ":lightsaber_found", -1),
	  (assign, ":lightsaber_item", -1),
	  (try_for_range, ":item_range", 0, 2),
	    (try_begin),
		  (eq, ":item_range", 0),
		  (assign, ":loop_begin", lightsabers_begin),
		  (assign, ":loop_end", lightsabers_end),
		(else_try),
		  (assign, ":loop_begin", lightsabers_thrown_begin),
		  (assign, ":loop_end", lightsabers_thrown_end),
		(try_end),
	    (try_for_range, ":item", ":loop_begin", ":loop_end"),
	      (scene_spawned_item_get_num_instances, ":num_instances", ":item"),
		  (try_for_range, ":instance_no", 0, ":num_instances"),
		    (lt, ":lightsaber_found", 0),
		    (scene_spawned_item_get_instance, ":instance", ":item", ":instance_no"),
		    (prop_instance_get_position, pos2, ":instance"),
		    (get_distance_between_positions, ":dist", pos1, pos2),
		    (le, ":dist", 1000),
		    (assign, ":lightsaber_found", ":instance"),
			(try_begin),
			  (is_between, ":item", lightsabers_thrown_begin, lightsabers_thrown_end),
			  (call_script, "script_thrown_get_lightsaber", ":item"),
			  (assign, ":item", reg0),
			(try_end),
	        (assign, ":lightsaber_item", ":item"),
		  (try_end),
	    (try_end),
	  (try_end),
	  (try_begin),
	    (ge, ":lightsaber_found", 0),
		(try_begin),
		  (call_script, "script_cf_is_lightsaber", ":lightsaber_item"),
		(else_try),
		  (val_sub, ":lightsaber_item", 1),
		(try_end),
		(init_position, pos1),
		(position_set_z, pos1, -10000),
		(prop_instance_set_position, ":lightsaber_found", pos1),
		(agent_equip_item, ":agent_no", ":lightsaber_item"),
		(agent_set_wielded_item, ":agent_no", ":lightsaber_item"),
		(agent_set_animation, ":agent_no", "anim_lightsaber_force_pull", 0),#_Sebastian_
	  (try_end),
]),
("agent_force_push",
[
	  (store_script_param, ":agent_no", 1),
	  (store_script_param, ":victim", 2),
#	  (store_script_param, ":damage", 3),
	  (set_fixed_point_multiplier, 100),
	  (agent_set_animation, ":agent_no", "anim_sw_force_push_user", 1),#_Sebastian_
	  (agent_get_look_position, pos1, ":agent_no"),
	  (position_move_z,pos1, 140, 1),
	  (try_for_agents, ":victim"),
		 (neq, ":victim", ":agent_no"),
		 (agent_is_human, ":victim"),
		 (agent_get_horse, ":horse", ":victim"),
		 (lt, ":horse", 0),
		 (agent_get_position, pos2, ":victim"),
		 (get_distance_between_positions, ":dist", pos1, pos2),
		 (le, ":dist", 2000),
		 (position_transform_position_to_local, pos3, pos1, pos2),
		 (position_get_y, ":y", pos3),
		 (gt, ":y", 0),
		 (position_get_x, ":x", pos3),
		 (is_between, ":x", -200, 200),
		 (call_script, "script_agent_force_push_victim", ":agent_no", ":victim", 20),
	  (try_end),
]),
("get_force_push_victim",
[
	  (store_script_param, ":agent_no", 1),
	  (agent_get_team, ":team_no", ":agent_no"),
	  (set_fixed_point_multiplier, 100),
	  (agent_get_look_position, pos1, ":agent_no"),
	  (position_move_z,pos1, 160, 1),
	  (assign, ":lowest_dist", 2000),
	  (assign, ":result_agent", -1),
	  (try_for_agents, ":victim"),
		 (neq, ":victim", ":agent_no"),
		 (agent_is_human, ":victim"),
		 (agent_is_alive, ":victim"),
		 (agent_get_horse, ":horse", ":victim"),
		 (lt, ":horse", 0),
	     (agent_get_team, ":team_no2", ":victim"),
		 (teams_are_enemies, ":team_no", ":team_no2"),
		 (agent_get_position, pos2, ":victim"),
		 (position_transform_position_to_local, pos3, pos1, pos2),
		 (position_get_y, ":y", pos3),
		 (gt, ":y", 0),
		 (position_get_x, ":x", pos3),
		 (is_between, ":x", -200, 200),
		 (get_distance_between_positions, ":dist", pos1, pos2),
		 (le, ":dist", ":lowest_dist"),
		 (assign, ":lowest_dist", ":dist"),
		 (assign, ":result_agent", ":victim"),
	  (try_end),
	  (assign, reg0, ":result_agent"),
]),
("agent_force_push_victim",
[
	  (store_script_param, ":agent_no", 1),
	  (store_script_param, ":victim", 2),
	  (store_script_param, ":damage", 3),
	  #(call_script, "script_agent_set_animation_sync", ":victim", "anim_sw_force_push", 0),
	  (position_get_rotation_around_z, ":z_rot", pos1),
	  (val_add, ":z_rot", 180),
	  (init_position, pos4),
	  (agent_get_position, pos2, ":victim"),
	  (position_rotate_z, pos4, ":z_rot"),
	  (position_copy_rotation, pos2, pos4),
	  (agent_set_position, ":victim", pos2),
	  (try_begin),
	    (ge, ":damage", 0),
	    (agent_deliver_damage_to_agent, ":agent_no", ":victim", ":damage"),
	  (try_end),
]),
("multiplayer_set_agent_hero_equipment",
[
	  (store_script_param, ":player_no", 1),
	  (store_script_param, ":hero", 2),
	  #resetting forces
	  (try_for_range, ":slot", player_force_power_used_0, player_force_powers_used_end),
		(player_set_slot, ":player_no", ":slot", 0),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":slot", 0),
	  (try_end),
	  (try_begin),
#	    (eq, ":hero", hero_sith1),#jango fett
#		(player_set_troop_id, ":player_no", "trp_clone_trooper_multiplayer"),
#		(player_add_spawn_item, ":player_no", ek_body, "itm_mandalorian_armor"),
#		(player_add_spawn_item, ":player_no", ek_head, "itm_mandalorian_helmet"),
#		(player_add_spawn_item, ":player_no", ek_foot, "itm_clone_boots"),
#		(player_add_spawn_item, ":player_no", ek_gloves, "itm_clone_gloves"),
#		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack"),
#		(player_set_slot, ":player_no", player_force_power_used_0, force_jetpack),
#		(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_set_player_slot, player_force_power_used_0, force_jetpack),
#		(try_for_range, ":slot", player_force_power_used_1, player_force_powers_used_end),
#		  (player_set_slot, ":player_no", ":slot", 0),
#		  (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":slot", 0),
#		(try_end),
#	  (else_try),
	    (eq, ":hero", hero_sith1),#?
		(player_set_troop_id, ":player_no", "trp_sith_force_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_armoured_robes"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_hilt3_red"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_lightning),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_lightning),
		(player_set_slot, ":player_no", player_force_power_used_1, force_defend1),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_1, force_defend1),
		(player_set_slot, ":player_no", player_force_power_used_2, force_jump),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_2, force_jump),
	  (else_try),
	    (eq, ":hero", hero_sith2),#anakin skywalker
		(player_set_troop_id, ":player_no", "trp_sith_lightsaber_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_armoured_robes"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_hilt1_red"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_jump),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_jump),
		(player_set_slot, ":player_no", player_force_power_used_1, force_defend1),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_1, force_defend1),
		(player_set_slot, ":player_no", player_force_power_used_2, force_defend2),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_2, force_defend2),
	  (else_try),
	    (eq, ":hero", hero_sith3),#anakin skywalker
		(player_set_troop_id, ":player_no", "trp_sith_force_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_armoured_robes"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_hilt2_red"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_lightning),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_lightning),
		(player_set_slot, ":player_no", player_force_power_used_1, force_defend1),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_1, force_defend1),
		(player_set_slot, ":player_no", player_force_power_used_2, force_jump),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_2, force_jump),
	  (else_try),
	    (eq, ":hero", trooper_sith1),#B1 Battle Droid
		(player_set_troop_id, ":player_no", "trp_b1_droid_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_droid_armor"),
		(player_add_spawn_item, ":player_no", ek_head, "itm_droid_helmet"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_gloves, "itm_droid_gloves"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_e5_blaster"),
		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack"),
		(store_random_in_range, ":rand", 0, 100),
		(try_begin),
		  (lt, ":rand", 20),
		  (player_set_slot, ":player_no", player_force_power_used_0, force_shield),
		  (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_damage),
		(try_end),
	  (else_try),
	    (eq, ":hero", trooper_sith2),#Rocket Battle Droid
		(player_set_troop_id, ":player_no", "trp_b1_droid_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_droid_armor_rocket"),
		(player_add_spawn_item, ":player_no", ek_head, "itm_droid_helmet"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_gloves, "itm_droid_gloves"),
		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_jetpack),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_jetpack),
	  (else_try),
	    (eq, ":hero", trooper_sith3),#BX Battle Droid
		(player_set_troop_id, ":player_no", "trp_b1_droid_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_bx_droid_armor"),
		(player_add_spawn_item, ":player_no", ek_head, "itm_bx_droid_helmet"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_gloves, "itm_droid_gloves"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_e5_blaster"),
		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack"),
		(player_add_spawn_item, ":player_no", ek_item_2, "itm_viber_sword"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_damage),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_damage),
	  (else_try),
	    (eq, ":hero", trooper_sith4),#Battle Droid Commander
		(player_set_troop_id, ":player_no", "trp_b1_droid_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_droid_armor"),
		(player_add_spawn_item, ":player_no", ek_head, "itm_droid_helmet"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_gloves, "itm_droid_gloves"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_se_34_blaster"),
	  (else_try),
	    (eq, ":hero", hero_jedi1),#?
		(player_set_troop_id, ":player_no", "trp_jedi_force_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_armoured_robes"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_hilt1_blue"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_jump),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_jump),
		(player_set_slot, ":player_no", player_force_power_used_1, force_travel),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_1, force_travel),
		(player_set_slot, ":player_no", player_force_power_used_2, force_defend1),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_2, force_defend1),
	  (else_try),
	    (eq, ":hero", hero_jedi2),#?
		(player_set_troop_id, ":player_no", "trp_sith_lightsaber_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_armoured_robes"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_hilt3_green"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_jump),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_jump),
		(player_set_slot, ":player_no", player_force_power_used_1, force_defend1),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_1, force_defend1),
		(player_set_slot, ":player_no", player_force_power_used_2, force_defend2),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_2, force_defend2),
	  (else_try),
	    (eq, ":hero", hero_jedi3),#?
		(player_set_troop_id, ":player_no", "trp_jedi_force_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_armoured_robes"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_hilt2_blue"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_jump),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_jump),
		(player_set_slot, ":player_no", player_force_power_used_1, force_defend1),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_1, force_defend1),
		(player_set_slot, ":player_no", player_force_power_used_2, force_healing),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_2, force_healing),
	  (else_try),
	    (eq, ":hero", trooper_jedi1),#Clone Trooper
		(player_set_troop_id, ":player_no", "trp_clone_trooper_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_clone_armor_plain"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_dc15s_blaster"),
		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack_blue"),
	  (else_try),
	    (eq, ":hero", trooper_jedi2),#Clone Trooper Jetpack
		(player_set_troop_id, ":player_no", "trp_clone_trooper_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_clone_armor_plain_jetpack"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack_blue"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_jetpack),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_jetpack),
	  (else_try),
	    (eq, ":hero", trooper_jedi3),#Clone Trooper Commander
		(player_set_troop_id, ":player_no", "trp_clone_trooper_multiplayer"),
		(player_add_spawn_item, ":player_no", ek_body, "itm_clone_armor"),
		(player_add_spawn_item, ":player_no", ek_head, "itm_clone_helmet"),
		(player_add_spawn_item, ":player_no", ek_foot, "itm_lightsaber_no_blade"),
		(player_add_spawn_item, ":player_no", ek_item_0, "itm_dc15_blaster"),
		(player_add_spawn_item, ":player_no", ek_item_1, "itm_power_pack_blue"),
		(player_set_slot, ":player_no", player_force_power_used_0, force_damage),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", player_force_power_used_0, force_damage),
	  (try_end),
]),
("multiplayer_set_random_agent_hero_equipment",
[
	  (store_script_param, ":player_no", 1),
	  (player_get_team_no, ":team", ":player_no"),
	  (team_get_faction, ":faction", ":team"),
	  (try_begin),
	    (eq, ":faction", "fac_kingdom_1"),#light side
	    (store_random_in_range, ":random_hero", hero_jedi1, heroes_jedi_end),
	  (else_try),#dark side
	    (store_random_in_range, ":random_hero", hero_sith1, heroes_sith_end),
	  (try_end),
	  (call_script, "script_multiplayer_set_agent_hero_equipment", ":player_no", ":random_hero"),
]),
#G: script_client_get_my_agent
("client_get_my_agent",
[
    (multiplayer_get_my_player, ":player_id"),
		(try_begin),#_Sebastian_ fix
			(player_is_active, ":player_id"),
			(player_get_agent_id, ":agent_id", ":player_id"),#_Sebastian_ fix
			(assign, reg0, ":agent_id"),
		(try_end),
]),
("agent_get_jetpack_blast",
[
	  (store_script_param, ":agent", 1),
	  (try_begin),
	    (this_or_next|agent_has_item_equipped, ":agent", "itm_droid_armor_commander_rocket"),
	    (this_or_next|agent_has_item_equipped, ":agent", "itm_droid_armor_bx_rocket"),
	    (agent_has_item_equipped, ":agent", "itm_droid_armor_rocket"),
		(assign, reg0, "itm_droid_jetpack_blast"),
	  (else_try),
	    #(this_or_next|agent_has_item_equipped, ":agent", "itm_clone_armor_jetpack"),
	    (this_or_next|agent_has_item_equipped, ":agent", "itm_rebel_armour_jetpack"),
	    (this_or_next|agent_has_item_equipped, ":agent", "itm_stormtrooper_armour_jetpack"),
	    (this_or_next|agent_has_item_equipped, ":agent", "itm_stormtrooper_armour_jetpack_camo"),
		(this_or_next|agent_has_item_equipped, ":agent", "itm_sith_jetpack"),
		(this_or_next|agent_has_item_equipped, ":agent", "itm_clone_armor_plain_jetpack_desert"),
		(this_or_next|agent_has_item_equipped, ":agent", "itm_clone_armor_plain_jetpack_camo"),
	    (agent_has_item_equipped, ":agent", "itm_clone_armor_plain_jetpack"),
		(assign, reg0, "itm_clone_jetpack_blast"),
	  (else_try), 
	    (this_or_next|agent_has_item_equipped, ":agent", "itm_rebel_armour_jetpack"),
		(this_or_next|agent_has_item_equipped, ":agent", "itm_havoc_armor_jet"),
		(this_or_next|agent_has_item_equipped, ":agent", "itm_havoc_armor_jet_camo"),
	    (agent_has_item_equipped, ":agent", "itm_sith_jetpack"),
		(assign, reg0, "itm_jetpack_blast"),	  
	  (else_try),
	    (agent_has_item_equipped, ":agent", "itm_mandalorian_armor"),
	    (assign, reg0, "itm_manda_jetpack_blast"),
	  (else_try),
	    (assign, reg0, "itm_lightsaber_no_blade"),
	  (try_end),
]),
("get_rnd_bt_troop",
[
#	  (store_script_param, ":player_no", 1),
	  (store_script_param, ":team_no", 2),
	  (team_get_faction, ":faction", ":team_no"),
	  (try_begin),
	    (eq, ":faction", "fac_kingdom_1"),#light side
	    (assign, ":low_end", hero_jedi1),
	    (assign, ":high_end", troopers_jedi_end),
	  (else_try),#dark side
	    (assign, ":low_end", hero_sith1),
	    (assign, ":high_end", troopers_sith_end),
	  (try_end),
	  (try_for_range, ":slot", hero_sith1, troopers_jedi_end),
	    (troop_set_slot, "trp_temp_array_a", ":slot", 0),
		(troop_set_slot, "trp_temp_array_b", ":slot", 0),
	  (try_end),
	  (assign, ":total_number", 0),

	  (try_begin),
	    (eq, ":team_no", 1),
		(assign, ":bot_number","$g_multiplayer_num_bots_team_1"),
	  (else_try),
		(assign, ":bot_number","$g_multiplayer_num_bots_team_2"),
	  (try_end),
	  (try_begin),
	    (eq, ":faction", "fac_kingdom_1"),#light side
		(troop_set_slot, "trp_temp_array_a", trooper_jedi1, ":bot_number"),
      (else_try),#dark side
		(troop_set_slot, "trp_temp_array_a", trooper_sith1, ":bot_number"),
      (try_end),

	  (val_add, ":total_number", ":bot_number"),
	  (try_for_agents, ":agent_no"),
	    (agent_get_player_id, ":cur_player", ":agent_no"),
		(ge, ":cur_player", 0),
	    (player_get_slot, ":class", ":cur_player", slot_player_class),
		(gt, ":class", 0),
		(troop_get_slot, ":number", "trp_temp_array_a", ":class"),
		(val_add, ":number", 1),
		(troop_set_slot, "trp_temp_array_a", ":class", ":number"),
		(val_add, ":total_number", 1),
	  (try_end),
	  (assign, ":rand_high", 0),
	  (val_max, ":total_number", 1),
	  (try_for_range, ":class", ":low_end", ":high_end"),
		(troop_get_slot, ":number", "trp_temp_array_a", ":class"),
		(troop_get_slot, ":aim_number", "trp_bt_rnd_troop_percentage", ":class"),
		(val_mul, ":number", 100),
		(val_div, ":number", ":total_number"),
		(gt, ":aim_number", ":number"),
		(val_sub, ":aim_number", ":number"),
		(troop_set_slot, "trp_temp_array_b", ":class", ":aim_number"),
		(val_add, ":rand_high", ":aim_number"),
	  (try_end),
	  (store_random_in_range, ":result_no", 0, ":rand_high"),
	  (assign, ":rand_high", 0),
	  (assign, ":result_class", -1),
	  (assign, ":end_loop", ":high_end"),
	  (try_for_range, ":class", ":low_end", ":end_loop"),
		(troop_get_slot, ":number", "trp_temp_array_a", ":class"),
		(troop_get_slot, ":percent", "trp_temp_array_b", ":class"),
		(troop_get_slot, ":aim_number", "trp_bt_rnd_troop_percentage", ":class"),
		(val_mul, ":number", 100),
		(val_div, ":number", ":total_number"),
		(gt, ":aim_number", ":number"),
		(try_begin),
		  (le, ":result_no", ":percent"),
		  (assign, ":end_loop", 0),
		  (assign, ":result_class", ":class"),
		(else_try),
		  (val_sub, ":result_no", ":percent"),
		(try_end),
	  (try_end),
	  (try_begin),
	    (lt, ":result_class", 0),
		(display_message, "@error: no class found", 0xFF0000),
		(try_begin),
		  (eq, ":faction", "fac_kingdom_1"),#light side
	      (assign, ":result_class", trooper_jedi1),
	    (else_try),#dark side
	      (assign, ":result_class", trooper_sith1),
	    (try_end),
	  (try_end),
	  (assign, reg0, ":result_class"),
]),
("position_face_position",
[
    (store_script_param, ":position", 1),
    (store_script_param, ":position_b", 2),
	(assign,":min_dist",9999999),
    (assign,":z_rotation",0),
	(try_for_range,":rotation",1,361),
	  (position_rotate_z,":position",1),
	  (copy_position,pos0,":position"),
	  (position_move_y,pos0,1000),
	  (get_distance_between_positions,":dist",pos0,":position_b"),
	  (lt,":dist",":min_dist"),
	  (assign,":min_dist",":dist"),
	  (assign,":z_rotation",":rotation"),
	(try_end),
	(position_rotate_z,":position",":z_rotation"),
]),
("agent_equip_item_sync",
[
	(store_script_param, ":agent", 1),
	(store_script_param, ":item", 2),
	(agent_equip_item, ":agent", ":item"),
	(multiplayer_get_my_player, ":my_player"),
	(get_max_players, ":max_players"),
	(try_for_range, ":player_no", 0, ":max_players"),
	  (player_is_active, ":player_no"),
	  (neq, ":player_no", ":my_player"),
	  (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_agent_equip_item, ":agent", ":item"),
	(try_end),
]),
("agent_stop_sound",
[
	(store_script_param, ":agent", 1),
	(try_begin),
	  (agent_slot_eq, ":agent", slot_agent_playing_sound, 1),
	  (agent_set_slot, ":agent", slot_agent_playing_sound, 0),
	  (agent_stop_sound, ":agent"),
	  (display_message, "@stopping sound2"),
	(try_end),
]),
#explosion script?
("effect_at_position",
  [
    (store_script_param, ":item", 1),
    (store_script_param, ":agent", 2),
    (try_begin),
      (neg|multiplayer_is_dedicated_server), # If a client and not a dedicated server that calls then play locally.
      (try_begin),
        (eq, ":item", itm_smoke_grenade),
        (particle_system_burst_no_sync, "psys_explosion_smoke", pos1, 100),
	  (else_try),
	    (eq, ":item", itm_signal_nade_red),
        (particle_system_burst_no_sync, "psys_explosion_smoke_red", pos1, 100),
	  (else_try),
	    (eq, ":item", itm_signal_nade_green),
        (particle_system_burst_no_sync, "psys_explosion_smoke_green", pos1, 100),	
	  (else_try),
	    (eq, ":item", itm_signal_nade_blue),
        (particle_system_burst_no_sync, "psys_explosion_smoke_blue", pos1, 100),	
      (else_try),
        # (this_or_next|eq, ":item", itm_frag_grenade),
        # (eq, ":item", itm_det_pack),
        (particle_system_burst_no_sync, "psys_explosion_fire", pos1, 4),
        (particle_system_burst_no_sync, "psys_explosion_dust", pos1, 20),
        (particle_system_burst_no_sync, "psys_explosion_debris", pos1, 100),
        (play_sound_at_position, "snd_explode1", pos1),
      (try_end),
    (try_end),

    (try_begin),
      (multiplayer_is_server), # If this is a server broadcast the sound to all players 
      (set_fixed_point_multiplier, 100),
      (position_get_x, ":pos_x", pos1),
      (position_get_y, ":pos_y", pos1),
      (position_get_z, ":pos_z", pos1),
      (try_for_players, ":cur_player", 1),
        (multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_return_effect_at_pos, ":item", ":pos_x",":pos_y",":pos_z"),
      (try_end),

      (agent_is_active, ":agent"),
      (item_get_swing_damage, ":max_damage", ":item"),
      (val_mul, ":max_damage", 1000),
      (item_get_hit_points, ":max_distance", ":item"),
      (store_div, ":factor", ":max_damage", ":max_distance"),

      (position_move_z, pos1, -90, 1),#adjust for center of body
      (try_for_agents, ":cur_agent", pos1, ":max_distance"),
        (agent_is_alive, ":cur_agent"),
        (agent_get_position, pos2, ":cur_agent"),
        (get_distance_between_positions, ":distance", pos1, pos2),
        (store_sub, ":damage", ":max_distance", ":distance"),
        (val_mul, ":damage", ":factor"),
        (val_div, ":damage", 1000),
        (agent_deliver_damage_to_agent_advanced, ":unused", ":agent", ":cur_agent", ":damage", ":item"),
        # (assign, reg1, ":distance"),
        # (assign, reg2, ":damage"),
        # (display_message, "@dist:{reg1} dmg:{reg2}"),

        (agent_is_alive,":cur_agent"), # still alive?
        (try_begin),
          (agent_is_human, ":cur_agent"),
          #(agent_get_horse, ":horse", ":cur_agent"), #Marko
          #(try_begin),
            #(neg|agent_is_active, ":horse"), # No horse so play the fall animation
            #(agent_set_animation, ":cur_agent", "anim_strike_fall_back_rise_upper"),
          #(try_end),
        (else_try),
          (agent_set_animation, ":cur_agent", "anim_horse_rear"),
        (try_end),
      (try_end),
    (try_end),
  ]),
("cf_no_presentation",
[
    (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
    (neg|is_presentation_active, "prsnt_multiplayer_team_select"),
    (neg|is_presentation_active, "prsnt_multiplayer_troop_select"),
    (neg|is_presentation_active, "prsnt_multiplayer_item_select"),
    (neg|is_presentation_active, "prsnt_multiplayer_spawn_select"),
    (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
    (neg|is_presentation_active, "prsnt_adimi_poll_panel"),
    (neg|is_presentation_active, "prsnt_adimi_chat"),
    (neg|is_presentation_active, "prsnt_adimi_admin_chat"),
    (neg|is_presentation_active, "prsnt_adimi_features"),
    (neg|is_presentation_active, "prsnt_adimi_tool_spawn_items"),
    (neg|is_presentation_active, "prsnt_admin_level_pin"),
]),
#_Sebastian_ end
("player_buy_force",
[
	 (store_script_param, ":player_no", 1),
	 (store_script_param, ":force", 2),
	 (assign, ":endloop", 10),
	 (try_for_range, ":force_no", 0, ":endloop"),
	   (store_add, ":player_force_used_slot", ":force_no", player_force_power_used_0),
	   (player_slot_eq, ":player_no", ":player_force_used_slot", 0),
	   (player_set_slot, ":player_no", ":player_force_used_slot", ":force"),
	   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":player_force_used_slot", ":force"),
	   (assign, ":endloop", -1),
	 (try_end),
]),
("player_set_force_points",
[
	 (store_script_param, ":player_no", 1),
	 (store_script_param, ":points", 2),
	 (multiplayer_get_my_player, ":my_player"),
    (try_begin),
	 (player_is_active,":player_no"),
	 (player_set_slot, ":player_no", slot_player_force_points, ":points"),
	 (try_begin),
	   (neq, ":player_no", ":my_player"),
	   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", slot_player_force_points, ":points"),
	 (try_end),
	(try_end),
]),
("get_pushed_anim",
[
	 (store_script_param, ":agent_no", 1),
	 (store_script_param, ":z_rot", 2),
	 (agent_get_position, pos0, ":agent_no"),
	 (position_get_rotation_around_z, ":z", pos0),
	 (val_sub, ":z", ":z_rot"),
	 (val_mod, ":z", 360),
	 (try_begin),
	   (is_between, ":z", 45, 135),
	   (assign, reg0, "anim_force_pushpx"),
	 (else_try),
	   (is_between, ":z", 135, 225),
	   (assign, reg0, "anim_force_pushpy"),
	 (else_try),
	   (is_between, ":z", 225, 315),
	   (assign, reg0, "anim_force_pushmx"),
	 (else_try),
	   (assign, reg0, "anim_force_pushmy"),
	 (try_end),
]),
("lightsaber_throw_trigger",
[
	 (store_script_param, ":item_no", 1),
	 (store_script_param, ":agent", 2),
	 (store_script_param, ":scene_prop", 3),
	 (try_begin),
	   (ge, ":agent", 0),
	   (agent_get_player_id, ":player", ":agent"),
	   (ge, ":player", 0),
       (player_get_slot, ":player_gold", ":player", slot_player_force_points),
	   (ge, ":player_gold", 200),
	   (val_sub, ":player_gold", 200),
	   (call_script, "script_player_set_force_points", ":player", ":player_gold"),
     (call_script, "script_spawn_scene_prop", ":scene_prop"),#_Sebastian_
	   (scene_prop_set_slot, reg0, slot_prop_lightsaber_thrown_agent, ":agent"),
	   (scene_prop_set_slot, reg0, slot_prop_lightsaber_thrown_item, ":item_no"),
	 (try_end),
]),
("gun_calculate_dispersion",
[
    (store_script_param, ":agent", 1),
    (store_script_param, ":item", 2),

    (item_get_accuracy, ":dispersion", ":item"),

    (agent_get_slot, ":shots", ":agent", slot_agent_effective_shots),
    (val_sub, ":shots", 1),
    (val_max, ":shots", 0),

    (item_get_body_armor, reg1, ":item"),
    (val_mul, reg1, ":shots"),

    (val_add, ":dispersion", reg1),
    (val_mul, ":dispersion", 3),

    #(set_fixed_point_multiplier, 100),
    #(agent_get_speed, pos2, ":agent"),
    #(position_normalize_origin, reg1, pos2),
    #(val_add, ":dispersion", reg1), 
	
    (agent_get_animation, ":anim", ":agent", 0),
    (try_begin),
        (eq, ":anim", "anim_crouch"),
        (val_div, ":dispersion", 2),
    (else_try),
        (eq, ":anim", "anim_crouch_walk"),
        (val_add, ":dispersion", 50),
	(else_try),
        (eq, ":anim", "anim_crouch_walk"),
        (val_add, ":dispersion", 125),	
    (else_try),
        (this_or_next|eq, ":anim", "anim_sw_strike_victim"),
		(this_or_next|eq, ":anim", "anim_jump"),
        (this_or_next|eq, ":anim", "anim_jump_end"),
		(this_or_next|eq, ":anim", "anim_jump_loop"),
        (eq, ":anim", "anim_jump_end_hard"),
        (val_add, ":dispersion", 500),
	(else_try),
		(is_between, ":anim", "anim_strike_head_left", "anim_strike_fall_back_rise_upper"),
        (val_add, ":dispersion", 600),	
	(else_try),
		(this_or_next|eq, ":anim", "anim_sw_force_jump"),
        (this_or_next|eq, ":anim", "anim_sw_force_jump_end"),
		(this_or_next|eq, ":anim", "anim_force_lightning_victim"),
		(this_or_next|eq, ":anim", "anim_equip_sword"),
		(this_or_next|eq, ":anim", "anim_unequip_sword"),
		(this_or_next|eq, ":anim", "anim_equip_greatsword"),
		(this_or_next|eq, ":anim", "anim_unequip_greatsword"),
		(this_or_next|eq, ":anim", "anim_equip_axe_left_hip"),
		(this_or_next|eq, ":anim", "anim_unequip_axe_left_hip"),
		(this_or_next|eq, ":anim", "anim_equip_axe_back"),
		(this_or_next|eq, ":anim", "anim_unequip_axe_back"),
		(this_or_next|eq, ":anim", "anim_equip_bayonet"),
		(this_or_next|eq, ":anim", "anim_unequip_bayonet"),
		(this_or_next|eq, ":anim", "anim_equip_default"),
		(this_or_next|eq, ":anim", "anim_unequip_default"),
		(this_or_next|eq, ":anim", "anim_equip_crossbow"),
		(this_or_next|eq, ":anim", "anim_unequip_crossbow"),
		(this_or_next|eq, ":anim", "anim_equip_bow_back"),
		(this_or_next|eq, ":anim", "anim_unequip_bow_back"),
		(this_or_next|eq, ":anim", "anim_equip_spear"),
		(this_or_next|eq, ":anim", "anim_unequip_spear"),
		(this_or_next|eq, ":anim", "anim_equip_dagger_front_left"),
		(this_or_next|eq, ":anim", "anim_unequip_dagger_front_left"),
		(this_or_next|eq, ":anim", "anim_equip_dagger_front_right"),
		(this_or_next|eq, ":anim", "anim_unequip_dagger_front_right"),
		(this_or_next|eq, ":anim", "anim_equip_revolver_right"),
		(this_or_next|eq, ":anim", "anim_unequip_revolver_right"),
		(this_or_next|eq, ":anim", "anim_equip_shield"),
		(this_or_next|eq, ":anim", "anim_unequip_shield"),
		(this_or_next|eq, ":anim", "anim_equip_bow_left_hip"),
		(this_or_next|eq, ":anim", "anim_unequip_bow_left_hip"),
		(this_or_next|eq, ":anim", "anim_equip_pistol_front_left"),
		(this_or_next|eq, ":anim", "anim_unequip_pistol_front_left"),
		(this_or_next|eq, ":anim", "anim_equip_katana"),
		(this_or_next|eq, ":anim", "anim_unequip_katana"),
		(this_or_next|eq, ":anim", "anim_equip_wakizashi"),
		(this_or_next|eq, ":anim", "anim_unequip_wakizashi"),
        (this_or_next|eq, ":anim", "anim_stand_to_crouch_new"),
        (eq, ":anim", "anim_crouch_to_stand_new"),
        (val_add, ":dispersion", 250),	
	(else_try),
		(this_or_next|eq, ":anim", "anim_walk_backward"),
		(this_or_next|eq, ":anim", "anim_walk_backward_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_backward_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_backward_left"),
		(this_or_next|eq, ":anim", "anim_walk_backward_left_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_backward_left_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_backward_right"),
		(this_or_next|eq, ":anim", "anim_walk_backward_right_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_backward_right_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_forward_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_forward_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_right"),
		(this_or_next|eq, ":anim", "anim_walk_right_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_right_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_left"),
		(this_or_next|eq, ":anim", "anim_walk_left_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_left_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_forward_right"),
		(this_or_next|eq, ":anim", "anim_walk_forward_right_hips_right"),
		(this_or_next|eq, ":anim", "anim_walk_forward_right_hips_left"),
		(this_or_next|eq, ":anim", "anim_walk_forward_left"),
		(this_or_next|eq, ":anim", "anim_walk_forward_left_hips_right"),
		(eq, ":anim", "anim_walk_forward_left_hips_left"),
		(val_add, ":dispersion", 12),
	(else_try),
		(this_or_next|eq, ":anim", "anim_turn_right"),
        (this_or_next|eq, ":anim", "anim_turn_left"),
        (this_or_next|eq, ":anim", "anim_turn_right_single"),
        (this_or_next|eq, ":anim", "anim_turn_left_single"),
        (this_or_next|eq, ":anim", "anim_run_forward"),
        (this_or_next|eq, ":anim", "anim_run_forward_hips_right"),
        (this_or_next|eq, ":anim", "anim_run_forward_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_forward_right"),
		(this_or_next|eq, ":anim", "anim_run_forward_right_hips_right"),
		(this_or_next|eq, ":anim", "anim_run_forward_right_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_forward_left"),
		(this_or_next|eq, ":anim", "anim_run_forward_left_hips_right"),
		(this_or_next|eq, ":anim", "anim_run_forward_left_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_backward"),
		(this_or_next|eq, ":anim", "anim_run_backward_hips_right"),
		(this_or_next|eq, ":anim", "anim_run_backward_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_backward_right"),
		(this_or_next|eq, ":anim", "anim_run_backward_right_hips_right"),
		(this_or_next|eq, ":anim", "anim_run_backward_right_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_backward_left"),
		(this_or_next|eq, ":anim", "anim_run_backward_left_hips_right"),
		(this_or_next|eq, ":anim", "anim_run_backward_left_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_right"),
		(this_or_next|eq, ":anim", "anim_run_right_hips_right"),
		(this_or_next|eq, ":anim", "anim_run_right_hips_left"),
		(this_or_next|eq, ":anim", "anim_run_left"),
		(this_or_next|eq, ":anim", "anim_run_left_hips_right"),
        (eq, ":anim", "anim_run_left_hips_left"),
        (val_add, ":dispersion", 24),
    (try_end),
    (assign, reg0, ":dispersion"),
]),




("gun_calculate_dispersion_ai",
[
    (store_script_param, ":agent", 1),
    (store_script_param, ":item", 2),
	
	(try_begin), #AI SMGs
		(this_or_next|eq, ":item", itm_dc17m_blaster_newai),
		(this_or_next|eq, ":item", itm_se_14_blaster_newai),
		(this_or_next|eq, ":item", itm_sg_4_blaster_newai),
		(this_or_next|eq, ":item", itm_sfor_c_blaster_newai),
		(this_or_next|eq, ":item", itm_orepublic_smg_newai),
		(eq, ":item", itm_sempire_smg_newai),
		(assign, ":dispersion", 33),
	(else_try), #AI SMGs aim
		(this_or_next|eq, ":item", itm_dc17m_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_se_14_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_sg_4_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_sfor_c_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_orepublic_smg_newai_aim),
		(eq, ":item", itm_sempire_smg_newai_aim),
		(assign, ":dispersion", 25),
	(else_try), #AI Light-Rifles
		(this_or_next|eq, ":item", itm_westar_m5_blaster_newai),
		(this_or_next|eq, ":item", itm_ee3_blaster_newai),
		(this_or_next|eq, ":item", itm_dlt_20a_blaster_newai),
		(this_or_next|eq, ":item", itm_tc22_blaster_newai),
		(this_or_next|eq, ":item", itm_orepublic_lightrifle_newai),
		(eq, ":item", itm_sempire_lightrifle_newai),
		(assign, ":dispersion", 43),
	(else_try), #AI Light-Riflse aim
		(this_or_next|eq, ":item", itm_westar_m5_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_ee3_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_dlt_20a_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_tc22_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_orepublic_lightrifle_newai_aim),
		(eq, ":item", itm_sempire_lightrifle_newai_aim),
		(assign, ":dispersion", 22),
	(else_try), #AI Rifles
		(this_or_next|eq, ":item", itm_dc15_blaster_newai),
		(this_or_next|eq, ":item", itm_carbine2_newai),
		(this_or_next|eq, ":item", itm_se_34_blaster_newai),
		(this_or_next|eq, ":item", itm_dnsr_blaster_newai),
		(this_or_next|eq, ":item", itm_dh_17r_blaster_newai),
		(this_or_next|eq, ":item", itm_sempire_rifle_newai),
		(eq, ":item", itm_orepublic_rifle_newai),
		(assign, ":dispersion", 48),
	(else_try), #AI Rifles aim
		(this_or_next|eq, ":item", itm_dc15_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_carbine2_newai_aim),
		(this_or_next|eq, ":item", itm_se_34_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_dnsr_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_dh_17r_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_sempire_rifle_newai_aim),
		(eq, ":item", itm_orepublic_rifle_newai_aim),
		(assign, ":dispersion", 20),
	(else_try), #AI Carbines
		(this_or_next|eq, ":item", itm_dc15s_blaster_newai),
		(this_or_next|eq, ":item", itm_e5_blaster_newai),
		(this_or_next|eq, ":item", itm_e11_blaster_newai),
		(eq, ":item", itm_a280_blaster_newai),
		(assign, ":dispersion", 37),
	(else_try), #AI Carbines aim
		(this_or_next|eq, ":item", itm_dc15s_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_e5_blaster_newai_aim),
		(this_or_next|eq, ":item", itm_e11_blaster_newai_aim),
		(eq, ":item", itm_a280_blaster_newai_aim),
		(assign, ":dispersion", 27),
	(else_try), #AI Sniper-Rifles
		(this_or_next|eq, ":item", itm_dc15x_sniper_newai),
		(this_or_next|eq, ":item", itm_e_5s_sniper_newai),
		(this_or_next|eq, ":item", itm_dlt_19x_sniper_newai),
		(this_or_next|eq, ":item", itm_ld1_sniper_newai),
		(this_or_next|eq, ":item", itm_orepublic_sniper_newai),
		(eq, ":item", itm_sempire_sniper_newai),
		(assign, ":dispersion", 60),
	(else_try), #AI Sniper-Rifles aim
		(this_or_next|eq, ":item", itm_dc15x_sniper_newai_aim),
		(this_or_next|eq, ":item", itm_e_5s_sniper_newai_aim),
		(this_or_next|eq, ":item", itm_dlt_19x_sniper_newai_aim),
		(this_or_next|eq, ":item", itm_ld1_sniper_newai_aim),
		(this_or_next|eq, ":item", itm_orepublic_sniper_newai_aim),
	(eq, ":item", itm_sempire_sniper_newai_aim),
		(assign, ":dispersion", 2),
	(else_try), #AI Cycler Rifle
		(this_or_next|eq, ":item", itm_dc17m_blaster_newai),
		(assign, ":dispersion", 38),
	(else_try), #AI Cycler Rifle aim
		(eq, ":item", itm_dc17m_blaster_newai),
		(assign, ":dispersion", 4),
	(else_try),
		(assign, ":dispersion", 3),
	(try_end),
	
    (agent_get_slot, ":shots", ":agent", slot_agent_effective_shots),
    (val_sub, ":shots", 1),
    (val_max, ":shots", 0),

    (item_get_body_armor, reg1, ":item"),
    (val_mul, reg1, ":shots"),

    (val_add, ":dispersion", reg1),
    (val_mul, ":dispersion", 5),

    (set_fixed_point_multiplier, 100),
    (agent_get_speed, pos2, ":agent"),
    (position_normalize_origin, reg1, pos2),
    (val_add, ":dispersion", reg1),

    (agent_get_animation, ":anim", ":agent", 0),
    (try_begin),
        (eq, ":anim", "anim_crouch"),
        (val_div, ":dispersion", 2),
    (else_try),
        (eq, ":anim", "anim_crouch_walk"),
        (val_add, ":dispersion", 50),
    (else_try),
        (this_or_next|eq, ":anim", "anim_sw_force_jump"),
        (this_or_next|eq, ":anim", "anim_sw_force_jump_end"),
        (this_or_next|eq, ":anim", "anim_sw_force_jump2"),
        (this_or_next|eq, ":anim", "anim_sw_force_jump2_end"),
        (this_or_next|eq, ":anim", "anim_jump"),
        (this_or_next|eq, ":anim", "anim_jump_end"),
        (this_or_next|eq, ":anim", "anim_jump_end_hard"),
        (eq, ":anim", "anim_jump_loop"),
        (val_add, ":dispersion", 100),
    (try_end),
    (assign, reg0, ":dispersion"),
]),




#_Sebastian_ begin
("gun_get_ammo",
[
	(store_script_param, ":agent", 1),
	(store_script_param, ":gun", 2),
    (try_begin),
         (is_between, ":gun", itm_e60r_launcher, itm_wrist_launcher+1),
        (assign, reg0, itm_missile_launcher_ammo),
    (else_try),
		(eq, ":gun", itm_wrist_launcher),
        (assign, reg0, itm_missile_launcher_ammo),
    (else_try),
        (is_between, ":gun", itm_sonic_blaster, itm_sonic_blaster_aim+1),
        (assign, reg0, itm_sonic_pack),
	(else_try),
        (is_between, ":gun", itm_cyc_rifle, itm_cyc_rifle_aim+1),
        (assign, reg0, itm_cycler_bullet),	
	(else_try),
        (agent_get_team, ":team", ":agent"),
        (team_get_faction, ":faction", ":team"),
        (try_begin),
            (eq, ":gun", itm_shotgun),
            (try_begin),
                (eq, ":faction", "fac_kingdom_1"),
                (assign, reg0, itm_shotgun_ammo_blue),
            (else_try),
                (assign, reg0, itm_shotgun_ammo_red),
            (try_end),
        (else_try),
            (try_begin),
                (eq, ":faction", "fac_kingdom_1"),
                (assign, reg0, itm_power_pack_blue),	
            (else_try),
			    (eq, ":faction", "fac_kingdom_3"), 
                (assign, reg0, itm_power_pack_green),
            (else_try),
			    (eq, ":faction", "fac_kingdom_8"), 
                (assign, reg0, itm_power_pack_blue),				
            (else_try),
                (assign, reg0, itm_power_pack_red),
            (try_end),
        (try_end),
    (try_end),
]),
#_Sebastian_ end
("force_defend2_calculate_accuracy",
[
		(try_begin),
	      (store_script_param, ":agent", 1),
	      (store_script_param, ":position", 2),
		  (copy_position, pos1, ":position"),
	      (agent_get_speed, pos2, ":agent"),
	      (position_get_x, ":speed_penalty", pos2),
	      (position_get_y, ":y", pos2),
	      (val_mul, ":speed_penalty", ":speed_penalty"),
	      (val_mul, ":y", ":y"),
	      (val_add, ":speed_penalty", ":y"),
		  (convert_to_fixed_point, ":speed_penalty"),
		  (store_sqrt, ":speed_penalty", ":speed_penalty"),
		  (convert_from_fixed_point, ":speed_penalty"),
		  (agent_get_animation, ":anim", ":agent", 0),
		  (try_begin),
		    (eq, ":anim", "anim_crouch_walk"),
			(assign, ":speed_penalty", 50),
		  (else_try),
		    (this_or_next|eq, ":anim", "anim_sw_force_jump"),
		    (this_or_next|eq, ":anim", "anim_sw_force_jump_end"),
		    (this_or_next|eq, ":anim", "anim_sw_force_jump2"),
		    (this_or_next|eq, ":anim", "anim_sw_force_jump2_end"),
		    (this_or_next|eq, ":anim", "anim_jump"),
			(this_or_next|eq, ":anim", "anim_jump_end"),
			(this_or_next|eq, ":anim", "anim_jump_end_hard"),
		    (eq, ":anim", "anim_jump_loop"),
			(val_add, ":speed_penalty", 250),
		  (try_end),
		  (try_begin),
		    (call_script, "script_cf_agent_has_force_power", ":agent", force_defend2),
			(assign, ":force_penalty", 0),
		  (else_try),
			(assign, ":force_penalty", 300),
		  (try_end),
		  (agent_get_look_position, pos0, ":agent"),
		  (position_move_z, pos0, 15, 1),
		  (position_move_y, pos0, 10),
		  (get_distance_between_positions, ":look_penalty", pos0, pos1),
		  (val_mul, ":look_penalty", 1),
		  (store_add, ":accuracy", ":speed_penalty", ":force_penalty"),
		  (val_add, ":accuracy", ":look_penalty"),
		  (val_add, ":accuracy", 150),
		  (assign, reg1, ":accuracy"),
		  (assign, reg2, ":speed_penalty"),
		  (assign, reg3, ":force_penalty"),
		  (assign, reg4, ":look_penalty"),
		  (assign, reg0, ":accuracy"),
		(try_end),
]),
("HSL_to_RGB",
[
	    (store_script_param, ":hue", 1),
	    (store_script_param, ":saturation", 2),
	    (store_script_param, ":lightness", 3),
		(val_mul, ":hue", 100),
		(val_mul, ":saturation", 100),
		(val_mul, ":lightness", 100),
		(store_mul, ":C", ":lightness", 2),
		(val_sub, ":C", 1),
		(try_begin),
		  (lt, ":C", 0),
		  (val_mul, ":C", -1),
		(try_end),
		(store_sub, ":C", 1, ":C"),
		(val_mul, ":C", ":saturation"),

		(store_div, ":H-", ":hue", 60),
		(store_mod, ":X", ":H-", 2),
		(val_sub, ":X", 1),
		(try_begin),
		  (lt, ":X", 0),
		  (val_mul, ":X", -1),
		(try_end),
		(store_sub, ":X", 1, ":X"),
		(val_mul, ":X", ":C"),

		(store_div, ":m", ":C", 2),
		(store_sub, ":m", ":lightness", ":m"),

		(try_begin),
		  (is_between, ":H-", 0, 100),
		  (store_add, ":R", ":C", ":m"),
		  (store_add, ":G", ":X", ":m"),
		  (store_add, ":B", 0, ":m"),
		(else_try),
		  (is_between, ":H-", 100, 200),
		  (store_add, ":R", ":X", ":m"),
		  (store_add, ":G", ":C", ":m"),
		  (store_add, ":B", 0, ":m"),
		(else_try),
		  (is_between, ":H-", 200, 300),
		  (store_add, ":R", 0, ":m"),
		  (store_add, ":G", ":C", ":m"),
		  (store_add, ":B", ":X", ":m"),
		(else_try),
		  (is_between, ":H-", 300, 400),
		  (store_add, ":R",0, ":m"),
		  (store_add, ":G", ":X", ":m"),
		  (store_add, ":B", ":C", ":m"),
		(else_try),
		  (is_between, ":H-", 400, 500),
		  (store_add, ":R", ":X", ":m"),
		  (store_add, ":G", 0, ":m"),
		  (store_add, ":B", ":C", ":m"),
		(else_try),
		  (is_between, ":H-", 500, 600),
		  (store_add, ":R", ":C", ":m"),
		  (store_add, ":G", 0, ":m"),
		  (store_add, ":B", ":X", ":m"),
		(try_end),
		(val_div, ":R", 100),
		(val_div, ":G", 100),
		(val_div, ":B", 100),
		# (val_shl, ":R", 16),
		# (val_shl, ":G", 8),
		(store_or, reg0, ":R", 0xFF000000),
		(val_or, reg0, ":G"),
		(val_or, reg0, ":B"),
]),
("color_get_component",
[
	    (store_script_param, ":color", 1),
	    (store_script_param, ":part", 2),
	    (store_script_param, ":component", 3),
		(val_mul, ":part", 9),
		(try_for_range, ":unused", 0, ":part"),
		  (store_mod, ":mod", ":color", 2),
		  (try_begin),
		    (lt, ":mod", 0),
			(assign, ":mod", 1),
		  (try_end),
		  (val_div, ":color", 2),
		  (val_sub, ":color", ":mod"),
		(try_end),
#		(val_shr, ":color", ":part"),
		(val_mul, ":component", 3),
		(try_for_range, ":unused", 0, ":component"),
		  (store_mod, ":mod", ":color", 2),
		  (try_begin),
		    (lt, ":mod", 0),
			(assign, ":mod", 1),
		  (try_end),
		  (val_div, ":color", 2),
		  (val_sub, ":color", ":mod"),
		(try_end),
#		(val_shr, ":color", ":component"),
		(store_and, reg0, ":color", 7),
]),
("color_set_component",
[
	    (store_script_param, ":color", 1),
	    (store_script_param, ":part", 2),
	    (store_script_param, ":component", 3),
	    (store_script_param, ":value", 4),
		(assign, ":mask", 7),
		(val_min, ":value", 7),
		(val_max, ":value", 0),
		(val_mul, ":part", 9),
		(try_for_range, ":unused", 0, ":part"),
		  (val_mul, ":value", 2),
		  (val_mul, ":mask", 2),
		(try_end),
		#(val_shl, ":value", ":part"),
		#(val_shl, ":mask", ":part"),
		(val_mul, ":component", 3),
		(try_for_range, ":unused", 0, ":component"),
		  (val_mul, ":value", 2),
		  (val_mul, ":mask", 2),
		(try_end),
#		(val_shl, ":value", ":component"),
#		(val_shl, ":mask", ":component"),
#		(val_not, ":mask"),
		(val_mul, ":mask", -1),
		(val_sub, ":mask", 1),
		(val_and, ":color", ":mask"),
		(store_or, reg0, ":color", ":value"),
]),
("color_get_part",
[
	    (store_script_param, ":color", 1),
	    (store_script_param, ":part", 2),
		(call_script, "script_color_get_component", ":color", ":part", 2),
		(assign, ":red", reg0),
		(val_mul, ":red", 255),
		(val_div, ":red", 10),
		(val_add, ":red", 5),
		(call_script, "script_color_get_component", ":color", ":part", 1),
		(assign, ":green", reg0),
		(val_mul, ":green", 255),
		(val_div, ":green", 10),
		(val_add, ":green", 5),
		(call_script, "script_color_get_component", ":color", ":part", 0),
		(assign, ":blue", reg0),
		(val_mul, ":blue", 255),
		(val_div, ":blue", 10),
		(val_add, ":blue", 5),
		(store_add, ":m", ":red", ":blue"),
		(val_add, ":m", ":green"),
		(val_div, ":m", 3),
		(val_mul, ":red", 2),
		(val_add, ":red", ":m"),
		(val_div, ":red", 3),
		(val_mul, ":green", 2),
		(val_add, ":green", ":m"),
		(val_div, ":green", 3),
		(val_mul, ":blue", 2),
		(val_add, ":blue", ":m"),
		(val_div, ":blue", 3),
		(val_mul, ":red", 65536),
		(val_mul, ":green", 256),
		#(val_shl, ":red", 16),
		#(val_shl, ":green", 8),
		(store_or, reg0, ":red", ":green"),
		(val_or, reg0, ":blue"),
		(val_or, reg0, 0xFF000000),
		#(call_script, "script_HSL_to_RGB", ":hue", ":saturation", ":lightness"),

]),
("display_bits",
[
	    (store_script_param, ":value", 1),
		(str_store_string, s1, "str_empty_string"),
		(try_for_range, ":unused", 0, 32),
		  (store_mod, reg1, ":value", 2),
		  (try_begin),
		    (lt, reg1, 0),
			(assign, reg1, 1),
		  (try_end),
		  (str_store_string, s1, "@{reg1}{s1}"),
		  # (val_shr, ":value", 1),
		(try_end),
		(display_message, s1),
]),
("colorize_armor",
[
	(store_script_param, ":tableau", 1),
	(store_script_param, ":agent_no", 2),

	(try_begin),
		(agent_is_active, ":agent_no"),
		(agent_get_player_id, ":player_no", ":agent_no"),
		(try_begin),
			(player_is_active, ":player_no"),
#_Sebastian_ begin
			(player_get_banner_id, ":color", ":player_no"),
			(val_sub, ":color", 1024),
			# (assign, reg52, ":player_no"),
			# (assign, reg53, ":color"),
			# (display_message, "@[DEBUG] player {reg52} armor color {reg53}"),
#_Sebastian_end
		(else_try),#default (AIAI) color
			(try_begin),
				(this_or_next|eq, ":tableau", "tableau_droid_armor_hardened"),
                (eq, ":tableau", "tableau_jedicloth"),
				(assign, ":color", -376),
			(else_try),
				(this_or_next|eq, ":tableau", "tableau_mandalorian_head"),
				(eq, ":tableau", "tableau_mandalorian_body"),
				(assign, ":color", -62466479),
			(else_try),
				(eq, ":tableau", "tableau_battledroid"),
				(assign, ":color", -24379393),	
			(else_try),
				(this_or_next|eq, ":tableau", "tableau_arctrooper_helm"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper_helm_desert"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper_helm_camo"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper_camo"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper_desert"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper2_desert"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper2_camo"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper2"),
				(this_or_next|eq, ":tableau", "tableau_arctrooper2_helm"),
				(eq, ":tableau", "tableau_arctrooper"),
				(assign, ":color", -24379393),
			(try_end),
		(try_end),
	(else_try),
		(profile_get_banner_id, ":color"),
	(try_end),
	(cur_item_set_tableau_material,":tableau", ":color"),
]),
("spawn_scene_prop",
[
	(store_script_param, ":scene_prop", 1),
	(try_begin),
		(is_between, ":scene_prop", "spr_sw_lightsaber_blade_red", "spr_sw_det_pack"),
		(assign, ":slot", slot_prop_lightsaber_thrown_agent),
	(else_try),
		(assign, ":slot", slot_prop_belongs_to_agent),
	(try_end),
	(scene_prop_get_num_instances, ":num_instances", ":scene_prop"),
	(try_for_range, ":instance_no", 0, ":num_instances"),
		(scene_prop_get_instance, reg0, ":scene_prop", ":instance_no"),
		(scene_prop_slot_eq, reg0, ":slot", -1),#is free
		(prop_instance_set_position, reg0, pos1),
		(assign, ":num_instances", -1),
	(try_end),
	(try_begin),
		(gt, ":num_instances", -1),
		(set_spawn_position, pos1),
		(spawn_scene_prop, ":scene_prop"),
	(try_end),
]),
("remove_scene_prop",
[
    (store_script_param, ":scene_prop", 1),
    (scene_prop_set_slot, ":scene_prop", slot_prop_belongs_to_agent, -1),
    (scene_prop_set_slot, ":scene_prop", slot_prop_lightsaber_thrown_agent, -1),
    (set_fixed_point_multiplier, 100),
    (position_set_z, pos1, -10000),
    (prop_instance_set_position, ":scene_prop", pos1),
]),
("autofire",
[
    (store_script_param, ":agent", 1),
    (store_script_param, ":item", 2),
    (store_script_param, ":time", 3),
    (store_script_param, ":is_server", 4),

    (set_fixed_point_multiplier, 100),
    (try_begin),
        (agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
        (item_get_weapon_length, ":item_anim", ":item"),
        (agent_get_animation, ":anim", ":agent", 1),
        (neq, ":anim", ":item_anim"),
        (agent_set_animation, ":agent", ":item_anim", 1),
    (try_end),

    (try_begin),
        (agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 1),
        (item_get_food_quality, ":num_missiles", ":item"),
        (item_get_max_ammo, ":consumption", ":item"),
        (val_mul, ":consumption", ":num_missiles"),

        (try_begin),
            (is_between, ":item", itm_sonic_blaster, itm_sonic_blaster_aim+1),
            (assign, ":ammo_type", itm_sonic_pack),
		(else_try),
		    (is_between, ":item", itm_cyc_rifle, itm_cyc_rifle_aim+1),
            (assign, ":ammo_type", itm_cycler_bullet),	
        (else_try),
            (assign, ":ammo_type", itm_power_pack),
        (try_end),
        
        (assign, ":ammo", 0),
        (try_for_range, ":slot", ek_item_0, ek_head),
            (agent_get_item_slot, ":slot_item", ":agent", ":slot"),
            (eq, ":slot_item", ":ammo_type"),
            (agent_get_ammo_for_slot, ":slot_ammo", ":agent", ":slot"),
            (val_add, ":ammo", ":slot_ammo"),
        (try_end),
        
        (try_begin),
            (ge, ":ammo", ":consumption"),
            (agent_get_slot, ":last_time", ":agent", slot_agent_last_time_shot),
            (item_get_hit_points, ":frequency", ":item"),
            (store_add, ":next_time", ":last_time", ":frequency"),
            (try_begin),
                (ge, ":time", ":next_time"),
                (agent_set_slot, ":agent", slot_agent_last_time_shot, ":time"),

                (agent_get_slot, ":effective_shots", ":agent", slot_agent_effective_shots),
                (val_add, ":effective_shots", 1),
                (agent_set_slot, ":agent", slot_agent_effective_shots, ":effective_shots"),

                (eq, ":is_server", 1),
                (val_sub, ":ammo", ":consumption"),
                (agent_set_ammo,":agent", ":ammo_type", ":ammo"),

                (agent_get_look_position, pos0, ":agent"),
                (agent_get_animation, ":anim", ":agent", 0),

                (try_begin),
                    (this_or_next|eq, ":anim", "anim_crouch"),
                    (eq, ":anim", "anim_crouch_walk"),
                    (position_move_z, pos0, 110, 1),
                (else_try),
                    (position_move_z, pos0, 160, 1),
                (try_end),

                (item_get_missile_speed, ":speed", ":item"),
                (val_mul, ":speed", 100),
                (call_script, "script_gun_calculate_dispersion", ":agent", ":item"),
                (assign, ":dispersion", reg0),
                (call_script, "script_gun_get_ammo", ":agent", ":item"),
                (assign, ":missile", reg0),

                (val_sub, ":item", itm_player_guns_begin),
                (val_add, ":item", itm_dummy_player_guns_begin),

                (try_for_range, ":unused", 0, ":num_missiles"),
                    (copy_position, pos1, pos0),
                    (store_random_in_range, ":rand", 0, 36000),
                    (position_rotate_y_floating, pos1, ":rand"),
                    (store_random_in_range, ":rand", 0, ":dispersion"),
                    (position_rotate_x_floating, pos1, ":rand"),
                    (add_missile, ":agent", pos1, ":speed", ":item", 0, ":missile", 0),
                (try_end),
            (try_end),
        (else_try),
            (agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),
        (try_end),

    (else_try),#spread cool down
        (agent_get_slot, ":last_time", ":agent", slot_agent_last_time_shot_recover),
        (item_get_hit_points, ":frequency", ":item"),
        (val_div, ":frequency", 2),
        (store_add, ":next_time", ":last_time", ":frequency"),
        (ge, ":time", ":next_time"),
        (agent_set_slot, ":agent", slot_agent_last_time_shot_recover, ":time"),
        (agent_get_slot, ":effective_shots", ":agent", slot_agent_effective_shots),
        (gt, ":effective_shots", 0),
        (val_sub, ":effective_shots", 1),
        (agent_set_slot, ":agent", slot_agent_effective_shots, ":effective_shots"),
    (try_end),
]),


("autofire_ai",
[
    (store_script_param, ":agent", 1),
    (store_script_param, ":item", 2),
    (store_script_param, ":time", 3),
    (store_script_param, ":is_server", 4),

    (set_fixed_point_multiplier, 100),
    (try_begin),
        (agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
        (item_get_weapon_length, ":item_anim", ":item"),
        (agent_get_animation, ":anim", ":agent", 1),
        (neq, ":anim", ":item_anim"),
        (agent_set_animation, ":agent", ":item_anim", 1),
    (try_end),

    (try_begin),
        (agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 1),
        (item_get_food_quality, ":num_missiles", ":item"),
        (item_get_max_ammo, ":consumption", ":item"),
        (val_mul, ":consumption", ":num_missiles"),

        (try_begin),
            (is_between, ":item", itm_sonic_blaster, itm_sonic_blaster_aim+1),
            (assign, ":ammo_type", itm_sonic_pack),
		(else_try),
		    (is_between, ":item", itm_cyc_rifle, itm_cyc_rifle_aim+1),
            (assign, ":ammo_type", itm_cycler_bullet),	
        (else_try),
            (assign, ":ammo_type", itm_power_pack),
        (try_end),
        
        (assign, ":ammo", 0),
        (try_for_range, ":slot", ek_item_0, ek_head),
            (agent_get_item_slot, ":slot_item", ":agent", ":slot"),
            (eq, ":slot_item", ":ammo_type"),
            (agent_get_ammo_for_slot, ":slot_ammo", ":agent", ":slot"),
            (val_add, ":ammo", ":slot_ammo"),
        (try_end),
        
        (try_begin),
            (ge, ":ammo", ":consumption"),
            (agent_get_slot, ":last_time", ":agent", slot_agent_last_time_shot),
            (item_get_hit_points, ":frequency", ":item"),
            (store_add, ":next_time", ":last_time", ":frequency"),
            (try_begin),
                (ge, ":time", ":next_time"),
                (agent_set_slot, ":agent", slot_agent_last_time_shot, ":time"),

                (agent_get_slot, ":effective_shots", ":agent", slot_agent_effective_shots),
                (val_add, ":effective_shots", 1),
                (agent_set_slot, ":agent", slot_agent_effective_shots, ":effective_shots"),

                (eq, ":is_server", 1),
                (val_sub, ":ammo", ":consumption"),
                (agent_set_ammo,":agent", ":ammo_type", ":ammo"),

                (agent_get_look_position, pos0, ":agent"),
                (agent_get_animation, ":anim", ":agent", 0),

                (try_begin),
                    (this_or_next|eq, ":anim", "anim_crouch"),
                    (eq, ":anim", "anim_crouch_walk"),
                    (position_move_z, pos0, 110, 1),
                (else_try),
                    (position_move_z, pos0, 160, 1),
                (try_end),

                
				(try_begin),
					(is_between, ":item", itm_dc17m_blaster_newai, itm_westar_m5_blaster_newai), #SMG
					(assign, ":speed", 100),
				(else_try),
					(is_between, ":item", itm_westar_m5_blaster_newai, itm_dc15_blaster_newai), #Light Rifles
					(assign, ":speed", 120),
				(else_try),
					(is_between, ":item", itm_dc15_blaster_newai, itm_dc15s_blaster_newai), #Rifles
					(assign, ":speed", 140),
				(else_try),
					(is_between, ":item", itm_dc15s_blaster_newai, itm_dc15x_sniper_newai),
					(assign, ":speed", 100),
				(else_try),
					(is_between, ":item", itm_dc15x_sniper_newai, itm_cyc_rifle_newai),
					(assign, ":speed", 200),
				(else_try),
					(is_between, ":item", itm_cyc_rifle_newai, itm_player_guns_end),
					(assign, ":speed", 150),
				(else_try),
					(item_get_missile_speed, ":speed", ":item"),
				(try_end),
                (val_mul, ":speed", 100),
                (call_script, "script_gun_calculate_dispersion_ai", ":agent", ":item"),
                (assign, ":dispersion", reg0),
                (call_script, "script_gun_get_ammo", ":agent", ":item"),
                (assign, ":missile", reg0),

                (val_sub, ":item", itm_player_guns_begin),
                (val_add, ":item", itm_dummy_player_guns_begin),

                (try_for_range, ":unused", 0, ":num_missiles"),
                    (copy_position, pos1, pos0),
                    (store_random_in_range, ":rand", 0, 36000),
                    (position_rotate_y_floating, pos1, ":rand"),
                    (store_random_in_range, ":rand", 0, ":dispersion"),
                    (position_rotate_x_floating, pos1, ":rand"),
                    (add_missile, ":agent", pos1, ":speed", ":item", 0, ":missile", 0),
                (try_end),
            (try_end),
        (else_try),
            (agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),
        (try_end),

    (else_try),#spread cool down
        (agent_get_slot, ":last_time", ":agent", slot_agent_last_time_shot_recover),
        (item_get_hit_points, ":frequency", ":item"),
        (val_div, ":frequency", 2),
        (store_add, ":next_time", ":last_time", ":frequency"),
        (ge, ":time", ":next_time"),
        (agent_set_slot, ":agent", slot_agent_last_time_shot_recover, ":time"),
        (agent_get_slot, ":effective_shots", ":agent", slot_agent_effective_shots),
        (gt, ":effective_shots", 0),
        (val_sub, ":effective_shots", 1),
        (agent_set_slot, ":agent", slot_agent_effective_shots, ":effective_shots"),
    (try_end),
]),


("multiplayer_broadcast_message",
[
    (store_script_param_1, ":log"),
    (store_script_param_2, ":color"),
    
    (try_begin),
        (eq, ":log", 1),
        (server_add_message_to_log, s0),
    (try_end),
    (try_begin),
        (eq, ":color", color_server_message),
        (str_store_string, s0, "str_server_s0"),
    (try_end),
         
    (try_for_players, ":cur_player", 0),
        (call_script, "script_multiplayer_send_message_to_player", ":cur_player", ":color"),
    (try_end),
    (str_clear, s0),#clear string
]),
("multiplayer_send_message_to_player",
[
    (store_script_param_1, ":player"),
    (store_script_param_2, ":color"),

    (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, ":color"),
	
	(multiplayer_send_4_int_to_player, ":player", sw_multiplayer_events, sw_custom_multiplayer_event_client_set_string_event, sw_custom_multiplayer_string_event_client_show_message, 0, 0),
    (multiplayer_send_string_to_player, ":player", sw_multiplayer_string_events, s0),
]),
("cf_player_check_is_force_usable",
[
     (store_script_param, ":player_no", 1),
     (store_script_param, ":troop", 2),
  (try_begin),
     (player_is_active,":player_no"),
	(try_for_range, ":force_no", 0, 10),
	   (store_add, ":current_slot", ":force_no", player_force_power_used_0),
	   (player_get_slot, ":cur_force", ":player_no", ":current_slot"),
      (try_begin),
	   (this_or_next|eq, ":cur_force", force_jump), #JEDI or SITH
	   (this_or_next|eq, ":cur_force", force_defend1),
	   
	   (this_or_next|eq, ":cur_force", force_aura_frenzy), #AURA
       (this_or_next|eq, ":cur_force", force_aura_healing), #AURA
	   
	   (this_or_next|eq, ":cur_force", force_defend2),
	   (this_or_next|eq, ":cur_force", force_healing),
	   (this_or_next|eq, ":cur_force", force_travel),
	   (this_or_next|eq, ":cur_force", force_push),
	   (this_or_next|eq, ":cur_force", force_choke),
	   (this_or_next|eq, ":cur_force", force_more_points_ludicrous), #Mark
	   (this_or_next|eq, ":cur_force", force_more_points),
	   (this_or_next|eq, ":cur_force", force_faster_points),
	   (eq, ":cur_force", force_lightning),
       (neq,":troop","trp_jedi_force_multiplayer"),
	   (neq,":troop","trp_jedi_force_multiplayer_ai"),
	   (neq,":troop","trp_sith_force_multiplayer_ai"),
	   (neq,":troop","trp_jedi_multiplayer_ai"), 
	   (neq,":troop","trp_sith_multiplayer_ai"),
	   (neq,":troop","trp_jedi_padawan_multiplayer"),
	   (neq,":troop","trp_jedi_padawan_multiplayer_1"),
	   (neq,":troop","trp_jedi_padawan_multiplayer_2"),
	   (neq,":troop","trp_sith_apprentice_multiplayer"),
	   (neq,":troop","trp_sith_apprentice_multiplayer_1"),
	   (neq,":troop","trp_sith_apprentice_multiplayer_2"),
	   (neq,":troop","trp_sith_acolyte_multiplayer"),
	   (neq,":troop","trp_jedi_multiplayer"),
       (neq,":troop","trp_jedi_lightsaber_multiplayer"),
       (neq,":troop","trp_jedi_rebels_multiplayer"),
       (neq,":troop","trp_sith_empire_multiplayer"),
       (neq,":troop","trp_sith_force_multiplayer"),
       (neq,":troop","trp_sith_lightsaber_multiplayer"),
	   (neq,":troop","trp_christmas_jedi_multiplayer"),
	   (neq,":troop","trp_christmas_sith_multiplayer"),
       (player_set_slot, ":player_no", ":current_slot", 0),
    #   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":current_slot", 0),
       (neg|multiplayer_is_dedicated_server),
       (assign,"$restart_force_presentation",1),
    #   (call_script,"script_cf_upgrade_force_icon"),
       (try_end),
      (try_begin),
       (eq, ":cur_force", force_damage), # JEDI SITH OR MAGNA GUARD
       (neq,":troop","trp_jedi_force_multiplayer"),
	   (neq,":troop","trp_jedi_force_multiplayer_ai"),
	   (neq,":troop","trp_sith_force_multiplayer_ai"),
	   (neq,":troop","trp_jedi_multiplayer_ai"),
	   (neq,":troop","trp_sith_multiplayer_ai"),
	   (neq,":troop","trp_sith_acolyte_multiplayer"),
	   (neq,":troop","trp_jedi_multiplayer"),
       (neq,":troop","trp_jedi_rebels_multiplayer"),
       (neq,":troop","trp_jedi_lightsaber_multiplayer"),
       (neq,":troop","trp_sith_force_multiplayer"),
       (neq,":troop","trp_sith_empire_multiplayer"),
       (neq,":troop","trp_sith_lightsaber_multiplayer"),
       (player_set_slot, ":player_no", ":current_slot", 0),
    #   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":current_slot", 0),
       (neg|multiplayer_is_dedicated_server),
       (assign,"$restart_force_presentation",1),
    #   (call_script,"script_cf_upgrade_force_icon"),
      (try_end),
      (try_begin),
       (eq, ":cur_force", power_jump), #MAGNA GUARD OR B2 DROID
	   (neq,":troop","trp_jedi_padawan_multiplayer"),
	   (neq,":troop","trp_jedi_padawan_multiplayer_1"),
	   (neq,":troop","trp_jedi_padawan_multiplayer_2"),
	   (neq,":troop","trp_sith_apprentice_multiplayer"),
	   (neq,":troop","trp_sith_apprentice_multiplayer_1"),
	   (neq,":troop","trp_sith_apprentice_multiplayer_2"),
       (player_set_slot, ":player_no", ":current_slot", 0),
    #   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":current_slot", 0),
       (neg|multiplayer_is_dedicated_server),
       (assign,"$restart_force_presentation",1),
    #   (call_script,"script_cf_upgrade_force_icon"),
      (try_end),
      (try_begin),
       (eq, ":cur_force", force_jetpack),# JETPACK CLONE OR BOUNTY HUNTER
       (neq,":troop","trp_clone_jet_trooper_multiplayer"),
       (neq,":troop","trp_bounty_hunter_multiplayer"),
       (neq,":troop","trp_rebel_trooper_jet_multiplayer"),
       (neq,":troop","trp_stormtrooper_jet_multiplayer"),
	   (neq,":troop","trp_sith_jet_multiplayer"),
	   (neq,":troop","trp_republic_jet_multiplayer"),
       (player_set_slot, ":player_no", ":current_slot", 0),
    #   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":current_slot", 0),
       (neg|multiplayer_is_dedicated_server),
       (assign,"$restart_force_presentation",1),
    #   (call_script,"script_cf_upgrade_force_icon"),
	  (try_end),
	  (try_begin),
       (eq, ":cur_force", force_jetpack_geonosian),# GEONOSIAN
       (neq,":troop","trp_geonosian_multiplayer"),
       (player_set_slot, ":player_no", ":current_slot", 0),
    #   (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_player_slot, ":player_no", ":current_slot", 0),
       (neg|multiplayer_is_dedicated_server),
       (assign,"$restart_force_presentation",1),
    #   (call_script,"script_cf_upgrade_force_icon"),
	  (try_end), 
    (try_end),
  (try_end),
]),
("cf_upgrade_force_icon",
[
(try_begin),
   (multiplayer_get_my_player,":my_player"),
   (player_is_active,":my_player"),
   (call_script, "script_get_next_useable_force_power", "$g_cur_force", ":my_player"),
   (assign, "$g_cur_force", reg0),
   (ge, "$g_cur_force", 0),
   (presentation_set_duration, 0),
   (start_presentation, "prsnt_multiplayer_force_display"),
(try_end),
]),

#endregion

#region Commented stuff

#_Sebastian_ not needed any more
# ("agent_play_sound_sync",
# [
		# (try_begin),
          # (store_script_param, ":agent", 1),
          # (store_script_param, ":sound", 2),
          # (store_script_param, ":long", 3),
		  # (ge, ":sound", 0),
		  # (call_script, "script_agent_play_sound", ":agent", ":sound", ":long"),
		  # (get_max_players, ":max_players"),
		  # (multiplayer_get_my_player, ":my_player"),
		  # (try_for_range, ":player_no", 0, ":max_players"),
            # (player_is_active, ":player_no"),
		    # (neq, ":player_no", ":my_player"),
		    # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_agent_play_sound, ":agent", ":sound", ":long"),
		  # (try_end),
		# (try_end),
# ]),
# ("agent_set_animation_sync",
# [
          # (store_script_param, ":agent", 1),
          # (store_script_param, ":anim", 2),
          # (store_script_param, ":option", 3),
		  # (agent_set_animation, ":agent", ":anim", ":option"),
		  # (get_max_players, ":max_players"),
		  # (multiplayer_get_my_player, ":my_player"),
		  # (try_for_range, ":player_no", 0, ":max_players"),
            # (player_is_active, ":player_no"),
		    # (neq, ":player_no", ":my_player"),
		    # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_agent_set_animation, ":agent", ":anim", ":option"),
		  # (try_end),
# ]),

#27.10.2015, Illuminati Change:
#("lightsaber_get_blade",
#[
#      (store_script_param, ":item", 1),
#	  (val_sub, ":item", "itm_hilt1_red"),
#	  (val_div, ":item", 8),
#	  (val_add, ":item", "itm_lightsaber_blade_red_1"),
#	  (assign, reg0, ":item"),
#]),


#new lightsaber_get_blade
#made by: Illuminati
#Explanation: If the player wields a hilt1_red from the lightsaber then it will give out to reg0 that the red lightsaber sword is needed. Reg0 is the cached value which get's used by the script which calls lightsaber_get_blade

#27.10.2015, Illuminati Change:
#("lightsaber_get_thrown",
#[
#     (store_script_param, ":item", 1),
#	  (val_sub, ":item", "itm_hilt1_red"),
#	  (val_div, ":item", 6),
#	  (val_add, ":item", "itm_hilt_1_red_thrown"),
#	  (assign, reg0, ":item"),
#]),

#27.10.2015, Illuminati Change:
#("thrown_get_lightsaber",
#[
#      (store_script_param, ":item", 1),
#	  (val_sub, ":item", "itm_hilt_1_red_thrown"),
#	  (val_mul, ":item", 6),
#	  (val_add, ":item", "itm_hilt1_red"),
#	  (assign, reg0, ":item"),
#]),


#_Sebastian_ disable
# ("emp_at_pos1",
# [
#       (set_fixed_point_multiplier, 100),
# 	  (try_for_agents, ":agent"),
# 	    (agent_get_troop_id, ":troop", ":agent"),
# 		(agent_is_alive, ":agent"),
# 		(agent_get_position, pos2, ":agent"),
# 		(get_distance_between_positions, ":dist", pos1, pos2),
# 		(le, ":dist", 4000),
# 		(agent_get_player_id, ":player_no", ":agent"),
# 		(try_begin),
# 		  (ge, ":player_no", 0),
# 		  #(multiplayer_send_int_to_player, ":player_no", multiplayer_event_stop_all_sound, 0),
# 		(try_end),
# 		(le, ":dist", 1200),
# 		(troop_get_type, ":skin", ":troop"),
# 		(eq, ":skin", tf_droid),
# 		(agent_set_slot, ":agent", slot_agent_occupied_with, occupied_emp),
# 		(agent_set_slot, ":agent", slot_agent_occupied_until, 100),
# 	  (try_end),
# 	  (call_script, "script_play_sound_at_position_sync", "snd_explode1", pos1),
# 	  (init_position, pos2),
# 	  (position_copy_rotation, pos1, pos2),
# 	  (position_move_z, pos1, 200),
# 	  (particle_system_burst, "psys_emp_explosion_1", pos1, 3),
# 	  (particle_system_burst, "psys_emp_explosion_2", pos1, 20),
# 	  (particle_system_burst, "psys_sw_lightning_victim", pos1, 100),
# ]),
# ("explosion_at_pos1",
# [
#       (set_fixed_point_multiplier, 100),
# 	  (store_script_param, ":deliverer", 1),
# 	  (try_for_agents, ":agent"),
# 		(agent_is_alive, ":agent"),
# 		(agent_get_position, pos2, ":agent"),
# 		(get_distance_between_positions, ":dist", pos1, pos2),
# 		(le, ":dist", 4000),
# 		(agent_get_player_id, ":player_no", ":agent"),
# 		(try_begin),
# 		  (ge, ":player_no", 0),
# 		  #(multiplayer_send_int_to_player, ":player_no", multiplayer_event_stop_all_sound, 0),
# 		(try_end),
# 		(try_begin),
# 		  (le, ":dist", 500),
# 		  (agent_deliver_damage_to_agent, ":deliverer", ":agent", 100),
# 		(else_try),
# 		  (le, ":dist", 800),
# 		  (agent_deliver_damage_to_agent, ":deliverer", ":agent", 30),
# 		(else_try),
# 		  (le, ":dist", 1500),
# 		  (agent_deliver_damage_to_agent, ":deliverer", ":agent", 10),
# 		(try_end),
# 	  (try_end),
# 	  (call_script, "script_play_sound_at_position_sync", "snd_explode1", pos1),
#       (particle_system_burst, "psys_explosion_fire", pos1, 10),
# 	  (particle_system_burst, "psys_explosion_debris", pos1, 100),
# 	  (particle_system_burst, "psys_explosion_dirt", pos1, 100),
# ]),

#_Sebastian_not needed anymore
# ("agent_play_sound",
# [
	# (store_script_param, ":agent", 1),
	# (store_script_param, ":sound", 2),
	# (store_script_param, ":long", 3),
	# (try_begin),
	  # (agent_play_sound, ":agent", ":sound"),
	# (else_try),
	  # (eq, ":long", 0),
	  # (agent_get_position, pos1, ":agent"),
	  # (play_sound_at_position, ":sound", pos1),
	# (else_try),
	  # (neg|agent_slot_eq, ":agent", slot_agent_playing_sound, 1),
	  # (agent_set_slot, ":agent", slot_agent_playing_sound, 1),
	  # (agent_play_sound, ":agent", ":sound"),
	  # (display_message, "@playing sound"),
	# (try_end),
# ]),


#_Sebastian_ disable
# ("play_sound_at_position_sync",
# [
#     (set_fixed_point_multiplier, 1),
# 	(store_script_param, ":sound", 1),
# 	(store_script_param, ":position", 2),
# 	(position_get_x, ":x", ":position"),
# 	(position_get_y, ":y", ":position"),
# 	(position_get_z, ":z", ":position"),
# 	(assign, ":coded", ":x"),
# 	(val_mul, ":y", 1000),
# 	(val_add, ":coded", ":y"),
# 	(val_mul, ":z", 1000000),
# 	(val_add, ":coded", ":z"),
# 	(call_script, "script_play_sound_at_coded_position", ":sound", ":coded"),
# 	(multiplayer_get_my_player, ":my_player"),
# 	(get_max_players, ":max_players"),
# 	(try_for_range, ":player_no", 0, ":max_players"),
# 	  (player_is_active, ":player_no"),
# 	  (neq, ":player_no", ":my_player"),
# 	  (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_play_sound_at_position, ":sound", ":coded"),
# 	(try_end),
# ]),
# ("play_sound_at_coded_position",
# [
#     (set_fixed_point_multiplier, 1),
# 	(store_script_param, ":sound", 1),
# 	(store_script_param, ":coded", 2),
# 	(init_position, pos1),
# 	(store_mod, ":x", ":coded", 1000),
# 	(val_div, ":coded", 1000),
# 	(store_mod, ":y", ":coded", 1000),
# 	(val_div, ":coded", 1000),
# 	(store_mod, ":z", ":coded", 1000),
# 	(position_set_x, pos1, ":x"),
# 	(position_set_y, pos1, ":y"),
# 	(position_set_z, pos1, ":z"),
# 	(play_sound_at_position, ":sound", pos1),
# ]),

#_Sebastian_ begin


#_Sebastian_ disable
# ("detonate_det_packs",
# [
# 	 (store_script_param, ":agent_no", 1),
# 	 (init_position, pos2),
# 	 (position_set_z, pos2, -10000),
# 	 (scene_prop_get_num_instances, ":num_instances", "spr_sw_det_pack"),
# 	 (try_for_range, ":instance_no", 0, ":num_instances"),
# 	   (scene_prop_get_instance, ":instance", "spr_sw_det_pack", ":instance_no"),
# 	   (scene_prop_slot_eq, ":instance", slot_prop_belongs_to_agent, ":agent_no"),
# 	   (prop_instance_get_position, pos1, ":instance"),
# 	   (call_script, "script_explosion_at_pos1", ":agent_no"),
# 	   (scene_prop_set_slot, ":instance", slot_prop_belongs_to_agent, -1),
# 	   (prop_instance_set_position, ":instance", pos2),
# 	 (try_end),
# ]),
# ("remove_det_packs",
# [
# 	 (store_script_param, ":agent_no", 1),
# 	 (init_position, pos2),
# 	 (position_set_z, pos2, -10000),
# 	 (scene_prop_get_num_instances, ":num_instances", "spr_sw_det_pack"),
# 	 (try_for_range, ":instance_no", 0, ":num_instances"),
# 	   (scene_prop_get_instance, ":instance", "spr_sw_det_pack", ":instance_no"),
# 	   (scene_prop_slot_eq, ":instance", slot_prop_belongs_to_agent, ":agent_no"),
# 	   (prop_instance_get_position, pos1, ":instance"),
# 	   (scene_prop_set_slot, ":instance", slot_prop_belongs_to_agent, -1),
# 	   (prop_instance_set_position, ":instance", pos2),
# 	 (try_end),
# ]),


#_Sebastian_ not needed anymore, merged into script_spawn_scene_prop
# ("spawn_det_pack",
# [
	 # (store_script_param, ":position", 1),
	 # (assign, ":return", -1),
	 # (scene_prop_get_num_instances, ":num_instances", "spr_sw_det_pack"),
	 # (try_for_range, ":instance_no", 0, ":num_instances"),
	   # (scene_prop_get_instance, ":instance", "spr_sw_det_pack", ":instance_no"),
	   # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
	   # (lt, ":agent", 0),
	   # (assign, ":return", ":instance"),
	   # (assign, ":num_instances", -1),
	 # (try_end),
	 # (try_begin),
	   # (ge, ":return", 0),
	   # (prop_instance_set_position, ":return", ":position"),
	   # (assign, reg0, ":return"),
	 # (else_try),
	   # (set_spawn_position, ":position"),
	   # (spawn_scene_prop, "spr_sw_det_pack"),
	 # (try_end),
# ]),
# ("spawn_thrown_lightsaber",
# [
	 # (store_script_param, ":scene_prop", 1),
	 # (store_script_param, ":position", 2),
	 # (assign, ":return", -1),
	 # (scene_prop_get_num_instances, ":num_instances", ":scene_prop"),
	 # (try_for_range, ":instance_no", 0, ":num_instances"),
	   # (scene_prop_get_instance, ":instance", ":scene_prop", ":instance_no"),
	   # (scene_prop_get_slot, ":agent", ":instance", slot_prop_lightsaber_thrown_agent),
	   # (lt, ":agent", 0),
	   # (assign, ":return", ":instance"),
	   # (assign, ":num_instances", -1),
	 # (try_end),
	 # (try_begin),
	   # (ge, ":return", 0),
	   # (prop_instance_set_position, ":return", ":position"),
	   # (assign, reg0, ":return"),
	 # (else_try),
	   # (set_spawn_position, ":position"),
	   # (spawn_scene_prop, ":scene_prop"),
	 # (try_end),
# ]),


#_Sebastian_ disable
# ("cf_agent_can_aim",
# [
# 	    (store_script_param, ":agent", 1),
# 		(agent_get_wielded_item, ":item_no", ":agent"),
# 		(ge, ":item_no", 0),
# 		(call_script, "script_cf_is_ranged", ":item_no"),#_Sebastian_
# 	    (agent_get_animation, ":upper_anim", ":agent", 1),
# 		(this_or_next|eq,":upper_anim", "anim_sw_force_jump"),
# 		(this_or_next|eq,":upper_anim", "anim_sw_force_jump_end"),
# 		(this_or_next|eq,":upper_anim", "anim_sw_force_jump2"),
# 		(this_or_next|eq,":upper_anim", "anim_sw_force_jump2_end"),
# 		(this_or_next|eq,":upper_anim", "anim_stand_b2"),
# 		(lt, ":upper_anim", "anim_mount_horse"),

# 		(neg|is_between,":upper_anim", "anim_prepare_kick_0", "anim_kick_left_leg"),
# 		(neq, ":upper_anim", "anim_jump_end"),
# 		(neq, ":upper_anim", "anim_jump_end_hard"),
# ]),

#_Sebastian_ begin

#_Sebastian_ end

#_Sebastian_ disable
# ("shoot_gun",
# [
# 	(store_script_param, ":agent", 1),
# 	(store_script_param, ":item", 2),
# 	(store_script_param, ":accuracy", 3),
# 	(store_script_param, ":speed", 4),
# 	# (store_script_param, ":sound", 5),#_Sebastian_ not needed any more
# 	(store_mission_timer_a_msec, ":time"),
# 	(agent_get_look_position, pos1, ":agent"),
# 	(agent_get_animation, ":anim", ":agent", 0),
# 	(call_script, "script_gun_get_ammo", ":agent", ":item"),
# 	(assign, ":missile", reg0),
# 	(try_begin),
# 	  (this_or_next|eq, ":anim", "anim_crouch"),
# 	  (eq, ":anim", "anim_crouch_walk"),
# 	  (position_move_z, pos1, 110, 1),
# 	(else_try),
# 	  (position_move_z, pos1, 160, 1),
# 	(try_end),
# 	(store_random_in_range, ":rand", 0, 360),
# 	(position_rotate_y, pos1, ":rand"),
# 	(store_random_in_range, ":rand", 0, ":accuracy"),
# 	(position_rotate_x_floating, pos1, ":rand"),
# 	(item_get_slot, ":damage_ref", ":item", slot_item_damage_reference),
# 	(add_missile, ":agent", pos1, ":speed", ":damage_ref", 0, ":missile", 0),
# 	(item_get_slot, ":add_shots", ":item", slot_item_additional_shots),
# 	(try_begin),
# 	  (gt, ":add_shots", 0),
# 	  (item_get_slot, ":spread", ":item", slot_item_spread),
# 	  # (val_add, ":add_shots", 1),#_Sebastian_
# 	  (store_div, ":each_angle", 360, ":add_shots"),
# 		(val_add, ":damage_ref", 1),#_Sebastian_ use the no sound weapon for additional shots
# 	  (try_for_range, ":angle", 0, ":add_shots"),#_Sebastian_
# 		(copy_position, pos2, pos1),
# 		(val_mul, ":angle", ":each_angle"),
# 		(position_rotate_y, pos2, ":angle"),
# 	    (position_rotate_x_floating, pos2, ":spread"),
# 	    (add_missile, ":agent", pos2, ":speed", ":damage_ref", 0, ":missile", 0),
# 	  (try_end),
# 	(try_end),
# 	(try_begin),
# 	  (eq, ":item", "itm_backpack_rockets"),
# 	  (agent_unequip_item, ":agent", "itm_backpack_rockets"),
# 	(try_end),
# #_Sebastian_ sound effects are fired via script_game_missile_launch
# 	#G:    Test
#     #(try_for_range,":unit_test",1,200),
#     #  (call_script, "script_agent_play_sound_sync", ":agent", ":sound", 0),
#     #(try_end),
# 	(agent_set_slot, ":agent", slot_agent_last_time_shot, ":time"),
# ]),

# ("twin_blaster_shoot_gun",#Gotha addition
# [
# 	(store_script_param, ":agent", 1),
# 	(store_script_param, ":item", 2),
# 	(store_script_param, ":accuracy", 3),
# 	(store_script_param, ":speed", 4),
# 	# (store_script_param, ":sound", 5),#_Sebastian_ not needed any more
# 	(store_mission_timer_a_msec, ":time"),
# 	(agent_get_look_position, pos1, ":agent"),
# 	(agent_get_animation, ":anim", ":agent", 0),
# 	(call_script, "script_gun_get_ammo", ":agent", ":item"),
# 	(assign, ":missile", reg0),
# 	(try_begin),
# 	  (this_or_next|eq, ":anim", "anim_crouch"),
# 	  (eq, ":anim", "anim_crouch_walk"),
# 	  (position_move_z, pos1, 110, 1),
# 	(else_try),
# 	  (position_move_z, pos1, 160, 1),
# 	(try_end),
#     (copy_position, pos2, pos1),#double pistol
# 	(store_random_in_range, ":rand", 0, 360),
# 	(store_random_in_range, ":rand_2", 0, 360),
# 	(position_rotate_y, pos1, ":rand"),
# 	(position_rotate_y, pos2, ":rand"),
# 	(store_random_in_range, ":rand", 0, ":accuracy"),
# 	(store_random_in_range, ":rand_2", 0, ":accuracy"),
# 	(position_rotate_x_floating, pos1, ":rand"),
# 	(position_rotate_x_floating, pos2, ":rand_2"),
# 	#(position_move_x,pos1,-5),
# 	(position_move_x,pos2,15),
# 	(item_get_slot, ":damage_ref", ":item", slot_item_damage_reference),
# 	(add_missile, ":agent", pos1, ":speed", ":damage_ref", 0, ":missile", 0),
# 	(val_add, ":damage_ref", 1),#_Sebastian_ use the no sound weapon for additional shots
#     (add_missile, ":agent", pos2, ":speed", ":damage_ref", 0, ":missile", 0),
# 	(try_begin),
# 	  (eq, ":item", "itm_backpack_rockets"),
# 	  (agent_unequip_item, ":agent", "itm_backpack_rockets"),
# 	(try_end),
# #_Sebastian_ sound effects are fired via script_game_missile_launch
# 	#G:    Test    (call_script, "script_agent_play_sound_sync", ":agent", ":sound", 0),
# 	(agent_set_slot, ":agent", slot_agent_last_time_shot, ":time"),
# ]),


#/sw
#WSE
	#script_wse_game_saved
	# Called each time after game is saved successfully
#	("wse_game_saved", [
#	]),

	#script_wse_chat_message_received
	# Called each time a chat message is received (both for servers and clients)
	# INPUT
	# script param 1 = sender player id
	# script param 2 = chat type (0 = global, 1 = team)
	# s0 = message
	# OUTPUT
	# trigger result = anything non-zero suppresses default chat behavior. Server will not even broadcast messages to clients.
	# result string = changes message text for default chat behavior (if not suppressed).
#	("wse_chat_message_received", [
		#(store_script_param, ":player_no", 1),
		#(store_script_param, ":chat_type", 2),
#	]),

	#script_wse_console_command_received
	# Called each time a command is typed on the dedicated server console (after parsing standard commands)
	# INPUT
	# s0 = text
	# OUTPUT
	# trigger result = anything non-zero if the command succeeded
	# result string = message to display on success (if empty, default message will be used)
#	("wse_console_command_received", [
#	]),

	#script_wse_multiplayer_message_received
# Called each time a composite multiplayer message is received
# INPUT
# script param 1 = sender player no
# script param 2 = event no
#("wse_multiplayer_message_received", [
#	(store_script_param, ":player_no", 1),
#	(store_script_param, ":event_no", 2),
#]),

#script_wse_get_agent_scale
# Called each time an agent is created
# INPUT
# script param 1 = troop no
# script param 2 = horse item no
# script param 3 = horse item modifier
# script param 4 = player no
# OUTPUT
# trigger result = agent scale (fixed point)
#("wse_get_agent_scale", [
#	(store_script_param, ":troop_no", 1),
#	(store_script_param, ":horse_item_no", 2),
#	(store_script_param, ":horse_item_modifier", 3),
#	(store_script_param, ":player_no", 4),
#]),

#script_wse_window_opened
# Called each time a window (party/inventory/character) is opened
# INPUT
# script param 1 = window no
# script param 2 = window param 1
# script param 3 = window param 2
# OUTPUT
# trigger result = presentation that replaces the window (if not set or negative, window will open normally)
#("wse_window_opened", [
#	(store_script_param, ":window_no", 1),
#	(store_script_param, ":window_param_1", 2),
#	(store_script_param, ":window_param_2", 3),
#]),

#script_game_missile_dives_into_water
# Called each time a missile dives into water
# INPUT
# script param 1 = missile item no
# script param 2 = missile item modifier
# script param 3 = launcher item no
# script param 4 = launcher item modifier
# script param 5 = shooter agent no
# pos1 = water impact position and rotation
#("game_missile_dives_into_water", [
#	(store_script_param, ":missile_item_no", 1),
#	(store_script_param, ":missile_item_modifier", 2),
#	(store_script_param, ":launcher_item_no", 3),
#	(store_script_param, ":launcher_item_modifier", 4),
#	(store_script_param, ":shooter_agent_no", 5),
#]),

#_Sebastian_ begin


#_Sebastian_ end

#_Sebastian_ not needed anymore, merged into optimized version above
# ("spawn_grenade",
# [G
	 # (store_script_param, ":position", 1),
	 # (assign, ":return", -1),
	 # (scene_prop_get_num_instances, ":num_instances", "spr_sw_grenade_tmp"),
	 # (try_for_range, ":instance_no", 0, ":num_instances"),
	   # (scene_prop_get_instance, ":instance", "spr_sw_grenade_tmp", ":instance_no"),
	   # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
	   # (lt, ":agent", 0),
	   # (assign, ":return", ":instance"),
	   # (assign, ":num_instances", -1),
	 # (try_end),
	 # (try_begin),
	   # (ge, ":return", 0),
	   # (prop_instance_set_position, ":return", ":position"),
	   # (assign, reg0, ":return"),
	 # (else_try),
	   # (set_spawn_position, ":position"),
	   # (spawn_scene_prop, "spr_sw_grenade_tmp"),
	 # (try_end),
# ]),

# ("spawn_emp_grenade",
# [
	 # (store_script_param, ":position", 1),
	 # (assign, ":return", -1),
	 # (scene_prop_get_num_instances, ":num_instances", "spr_sw_emp_grenade_tmp"),
	 # (try_for_range, ":instance_no", 0, ":num_instances"),
	   # (scene_prop_get_instance, ":instance", "spr_sw_emp_grenade_tmp", ":instance_no"),
	   # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
	   # (lt, ":agent", 0),
	   # (assign, ":return", ":instance"),
	   # (assign, ":num_instances", -1),
	 # (try_end),
	 # (try_begin),
	   # (ge, ":return", 0),
	   # (prop_instance_set_position, ":return", ":position"),
	   # (assign, reg0, ":return"),
	 # (else_try),
	   # (set_spawn_position, ":position"),
	   # (spawn_scene_prop, "spr_sw_emp_grenade_tmp"),
	 # (try_end),
# ]),

# ("spawn_sw_mandalorian_grenade",
# [
	 # (store_script_param, ":position", 1),
	 # (assign, ":return", -1),
	 # (scene_prop_get_num_instances, ":num_instances", "spr_sw_mandalorian_grenade_tmp"),
	 # (try_for_range, ":instance_no", 0, ":num_instances"),
	   # (scene_prop_get_instance, ":instance", "spr_sw_mandalorian_grenade_tmp", ":instance_no"),
	   # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
	   # (lt, ":agent", 0),
	   # (assign, ":return", ":instance"),
	   # (assign, ":num_instances", -1),
	 # (try_end),
	 # (try_begin),
	   # (ge, ":return", 0),
	   # (prop_instance_set_position, ":return", ":position"),
	   # (assign, reg0, ":return"),
	 # (else_try),
	   # (set_spawn_position, ":position"),
	   # (spawn_scene_prop, "spr_sw_mandalorian_grenade_tmp"),
	 # (try_end),
# ]),
#endregion

#region New scripts


("sw_process_auras", [
    
    #(call_script, "script_array_print", 1, "trp_array_active_aura_sources"), #DEBUG
    #(call_script, "script_array_print", 2, "trp_array_active_aura_recipients"), #DEBUG

    #reads active auras sources table and applies bonuses to agents in proximity.
    #clears invalid sources or invalid recipient agents if needed.
    
    (troop_get_slot, ":sources_index", "trp_array_active_aura_sources", 0),
    (troop_get_slot, ":recipients_index", "trp_array_active_aura_recipients", 0),
 

    #(call_script, "script_array_print", 2, "trp_array_active_aura_recipients"), #DEBUG
#================================================================================
    (val_add, ":recipients_index", 1),

    #(call_script, "script_debug", 5, ":recipients_index"), #DEBUG

    (try_for_range, ":current_recipient", 1, ":recipients_index"),
        (troop_get_slot, ":recipient", "trp_array_active_aura_recipients", ":current_recipient"),
        (call_script, "script_cf_validate_agent", ":recipient"),
        (agent_get_slot, ":aura", ":recipient", slot_agent_aura_effect),

        #(call_script, "script_debug", 6, ":aura"), #DEBUG

        (try_begin),
            (eq, ":aura", force_aura_frenzy),
            (agent_set_damage_modifier, ":recipient", 100),

            #(call_script, "script_debug", 7, ":recipient"), #DEBUG

            (agent_set_slot, ":recipient", slot_agent_aura_effect, 0),
        (else_try),
            (eq, ":aura", force_aura_accuracy),
        (try_end),

    (try_end),

    (call_script, "script_array_clear", "trp_array_active_aura_recipients"),

    #(call_script, "script_array_print", 3, "trp_array_active_aura_recipients"), #DEBUG
    #(call_script, "script_debug", 4, ":sources_index"), #DEBUG
#=================================================================================   
    #(call_script, "script_array_print", 1, "trp_array_active_aura_sources"), #DEBUG

    (val_add, ":sources_index", 1),
    (try_for_range, ":source", 1, ":sources_index"),
        (troop_get_slot, ":source_agent", "trp_array_active_aura_sources", ":source"),

        (call_script, "script_cf_validate_agent", ":source_agent"),
        (agent_get_player_id, ":source_player", ":source_agent"),
        (player_is_active, ":source_player"),
        (player_get_slot, ":source_player_force_points", ":source_player", slot_player_force_points),
        (ge, ":source_player_force_points",sw_aura_tick_cost),
        #(call_script, "script_debug", 2, ":source_agent"), #DEBUG

        (try_begin),
            (call_script, "script_cf_validate_agent", ":source_agent"),
        (else_try),
            (call_script, "script_array_remove", "trp_array_active_aura_sources", ":source", 0),
        (try_end),

        (call_script, "script_cf_validate_agent", ":source_agent"),
        (agent_get_slot, ":aura", ":source_agent", slot_agent_aura_source_type),
#-----------------------------
        (try_begin),
            (eq, ":aura", force_aura_frenzy),

            (agent_get_position, pos2, ":source_agent"),
            (try_for_agents, ":recipient", pos2, sw_aura_radius),
                (call_script, "script_cf_validate_agent", ":recipient"),
                (call_script, "script_array_insert", "trp_array_active_aura_recipients", ":recipient"),

                (agent_set_damage_modifier, ":recipient", force_aura_frenzy_modifier),
                (agent_set_slot, ":recipient", slot_agent_aura_effect, force_aura_frenzy),
            (try_end),
#-----------------------------------
        (else_try),
            (eq, ":aura", force_aura_healing),

            (agent_get_position, pos2, ":source_agent"),
            (try_for_agents, ":recipient", pos2, sw_aura_radius),
                (call_script, "script_cf_validate_agent", ":recipient"),
                
                (store_agent_hit_points, ":hp", ":recipient", 1),
                (val_add, ":hp", force_aura_healing_modifier),
                (agent_set_hit_points, ":recipient", ":hp", 1),
            (try_end),
#-----------------------------------
        (else_try),
            (eq, ":aura", force_aura_accuracy),
        (try_end),
    
        
        (val_sub, ":source_player_force_points", sw_aura_tick_cost),
        (call_script, "script_player_set_force_points", ":source_player", ":source_player_force_points"),
    (try_end), 

]),


("cf_validate_agent", [
    
    (store_script_param, ":agent", 1),    
    
    (agent_is_active, ":agent"),
    #(neg|agent_is_non_player, ":agent"),   #DEBUG
    (agent_is_alive, ":agent"),
    (agent_is_human, ":agent"),
    (gt, ":agent", 0),
    
]),

("show_message", [
    
    (store_script_param, ":player", 1),
    #s1 holds message
    #if executed on server and player value is 0 script will send message to all players connected.
    #if executed on client script behaves just like display_message.

    (try_begin),
        (multiplayer_is_dedicated_server),
        (try_begin),
            (eq, ":player", 0),
        
            (try_for_players, ":curr_player", 1),
                (player_is_active, ":curr_player"),
                (multiplayer_send_4_int_to_player, ":curr_player", sw_multiplayer_events, sw_custom_multiplayer_event_client_set_string_event, sw_custom_multiplayer_string_event_client_show_message, 0, 0),
                (multiplayer_send_string_to_player, ":curr_player", sw_multiplayer_string_events, s1),
            (try_end),

        (else_try),
            (neq, ":player", 0),
            (player_is_active, ":player"),
            (multiplayer_send_4_int_to_player, ":player", sw_multiplayer_events, sw_custom_multiplayer_event_client_set_string_event, sw_custom_multiplayer_string_event_client_show_message, 0, 0),
            (multiplayer_send_string_to_player, ":player", sw_multiplayer_string_events, s1),
        (try_end),
    (else_try),
        (display_message, s1),
    (try_end),
    
]),

("debug", [
    
    (store_script_param, ":order", 1),
    (store_script_param, ":value", 2),
    
    #inserts value into first free slot. Free slot is defined as slot with value of 0.
    #slot with id of 0 is an array index and points at last occupied slot id.

    (assign, reg1, ":order"),
    (assign, reg2, ":value"),
    (display_message, "@============================================================"),
    (display_message, "@OUTPUT {reg1} = {reg2}"),

]),

("cf_send_custom_event", [
    
    (store_script_param, ":player", 1),
    (store_script_param, ":event", 2),
    (store_script_param, ":param1", 3),
    (store_script_param, ":param2", 4),
    (store_script_param, ":param3", 5),
    
    #sends custom event to player/server.
    #if executed on server and player value is 0 script will send event to all players connected.
    #if executed on client script will just send event to server.

    (gt, ":event", 0),

    (try_begin),
        (multiplayer_is_dedicated_server),
        (try_begin),
            (eq, ":player", 0),
            
            (try_for_players, ":curr_player", 1),
                (player_is_active, ":curr_player"),
                (multiplayer_send_4_int_to_player, ":curr_player", sw_multiplayer_events, ":event", ":param1", ":param2", ":param3"),
            (try_end),

        (else_try),
            (neq, ":player", 0),
            (player_is_active, ":player"),
            (multiplayer_send_4_int_to_player, ":player", sw_multiplayer_events, ":event", ":param1", ":param2", ":param3"),
        (try_end),
    (else_try),
        (multiplayer_send_4_int_to_server, sw_multiplayer_events, ":event", ":param1", ":param2", ":param3"),
    (try_end),
    
]),

("cf_send_custom_string_event", [
    
    (store_script_param, ":player", 1),
    (store_script_param, ":event", 2),
    
    #sends custom string event to player/server.
    #s1 holds string value
    #if executed on server and player value is 0 script will send event to all players connected.
    #if executed on client script will just send event to server.

    (gt, ":event", 0),

    (try_begin),
        (multiplayer_is_dedicated_server),
        (try_begin),
            (eq, ":player", 0),
            
            (try_for_players, ":curr_player", 1),
                (player_is_active, ":curr_player"),
                (multiplayer_send_4_int_to_player, ":curr_player", sw_multiplayer_events, sw_custom_multiplayer_event_client_set_string_event, ":event", 0, 0),
                (multiplayer_send_string_to_player, ":curr_player", sw_multiplayer_string_events, s1),
            (try_end),

        (else_try),
            (neq, ":player", 0),
            (player_is_active, ":player"),
            (multiplayer_send_4_int_to_player, ":player", sw_multiplayer_events, sw_custom_multiplayer_event_client_set_string_event, ":event", 0, 0),
            (multiplayer_send_string_to_player, ":player", sw_multiplayer_string_events, s1),
        (try_end),
    (else_try),
        (multiplayer_send_4_int_to_server, sw_multiplayer_events, sw_custom_multiplayer_event_server_set_string_event, ":event", 0, 0),
        (multiplayer_send_string_to_server, sw_multiplayer_string_events, s1),
    (try_end),
    
]),
#endregion

#scene_type_plain 

#scene_type_desert 

#scene_type_snow 

#scene_type_kamino 
#scene_type_mygeeto 

#scene_type_night 

#scene_type_space 

#scene_type_indoors

  ("multiplayer_server_set_weather_time",
    [
        (try_begin),
            (eq, "$g_multiplayer_cloud_amount", -1),
            (try_begin),
                (neq, "$g_cur_scene_type", scene_type_desert),
                (neq, "$g_cur_scene_type", scene_type_cantina), #
                #(neq, "$g_cur_scene_type", scene_type_kamino),
                (neq, "$g_cur_scene_type", scene_type_indoors),
                (neq, "$g_cur_scene_type", scene_type_night),
                (neq, "$g_cur_scene_type", scene_type_space),
                (store_random_in_range, "$g_cloud_amount", 0, 101),
            (else_try),
                (eq, "$g_cur_scene_type", scene_type_mygeeto),
                (store_random_in_range, "$g_cloud_amount", 40, 101),
            (else_try),
                (this_or_next|eq, "$g_cur_scene_type", scene_type_cantina), #
                (eq, "$g_cur_scene_type", scene_type_desert),
                (store_random_in_range, "$g_cloud_amount", 0, 15),
        (else_try),
            (assign, "$g_cloud_amount", "$g_multiplayer_cloud_amount"),
        (try_end),
       
        (try_begin),
            (eq, "$g_multiplayer_precipitation_strength", -1),
            (try_begin),
                (this_or_next|eq, "$g_cur_scene_type", scene_type_mygeeto), #If Kamino or Mygeeto guarantee precipitations
                (eq, "$g_cur_scene_type", scene_type_kamino),
                (store_random_in_range, "$g_precipitation_strength", 30, 90),
            (else_try),
                (this_or_next|ge, "$g_cloud_amount", 75),
                (eq, "$g_cur_scene_type", scene_type_snow),
                (store_random_in_range, "$g_precipitation_strength", 0, 201),
                (try_begin),
                    (gt, "$g_precipitation_strength", 100),
                    (assign, "$g_precipitation_strength", 0),
                (try_end),
            (else_try),
                (assign, "$g_precipitation_strength", 0),
            (try_end),         
        (else_try),
            (assign, "$g_precipitation_strength", "$g_multiplayer_precipitation_strength"),
        (try_end),
       
        (try_begin),
            (eq, "$g_multiplayer_fog_distance", -1),                           
       
            (try_begin),
                (gt, "$g_precipitation_strength", 0),
                (store_mul, ":distance_decrease", "$g_precipitation_strength", 4),
                (store_sub, ":max_fog_distance", 501, ":distance_decrease"),
            (else_try),
                (assign, ":max_fog_distance", 10001),
            (try_end),
                       
            (store_random_in_range, "$g_fog_distance", 0, ":max_fog_distance"),
            (try_begin),
                (eq, "$g_cur_scene_type", scene_type_mygeeto), #Mygeeto hardcoded fog distance
                (assign, "$g_fog_distance", 30),           
            (else_try),
                (eq, "$g_cur_scene_type", scene_type_kamino), #Kamino hardcoded fog distance
                (assign, "$g_fog_distance", 110),  
            (else_try),
                (gt, "$g_fog_distance", 1300),
                (assign, "$g_fog_distance", 10000),
            (try_end),         
        (else_try),
            (assign, "$g_fog_distance", "$g_multiplayer_fog_distance"),
        (try_end),
       
        (try_begin),
            (eq, "$g_multiplayer_thunderstorm", -1),
            (try_begin),
                (gt, "$g_precipitation_strength", 0),
                (ge, "$g_cloud_amount", 75),
                (store_random_in_range, "$g_thunderstorm", 0, 2),
            (else_try),
                (assign, "$g_thunderstorm", 0),
            (try_end),
        (else_try),
            (assign, "$g_thunderstorm", "$g_multiplayer_thunderstorm"),
        (try_end),
       
        (try_begin),
            (eq, "$g_multiplayer_wind_strength", -1),
            (store_random_in_range, "$g_wind_strength", 1, 5), #Maximum is 11
        (else_try),
            (assign, "$g_wind_strength", "$g_multiplayer_wind_strength"),
        (try_end),
       
        (try_begin),
            (eq, "$g_multiplayer_wind_direction", -1),
            (store_random_in_range, "$g_wind_direction", 0, 360),
        (else_try),
            (assign, "$g_wind_direction", "$g_multiplayer_wind_direction"),
        (try_end),
 
        (try_begin),
            (eq, "$g_multiplayer_day_time", -1),
            (try_begin),
                (this_or_next|eq, "$g_cur_scene_type", scene_type_kamino),
                (this_or_next|eq, "$g_cur_scene_type", scene_type_night),
                (eq, "$g_cur_scene_type", scene_type_space),
                (store_random_in_range, "$g_day_time", 0, 3),
            (else_try),
                (eq, "$g_cur_scene_type", scene_type_cantina),
                (assign, "$g_day_time", 0),
            (else_try),
                (eq, "$g_cur_scene_type", scene_type_snow), #Avoid time from 10-14 due to sun bloom.
                (store_random_in_range, ":day_time", 6, 9),
                (store_random_in_range, ":day_time_2", 16, 20),
                (store_random_in_range, ":chance", 0, 2),
                (try_begin),
                    (eq, ":chance", 0),
                    (assign, "$g_day_time", ":day_time"),
                (else_try),
                    (assign, "$g_day_time", ":day_time_2"),
                (try_end),
            (else_try),            
                (this_or_next|eq, "$g_cur_scene_type", scene_type_indoors),
                (eq, "$g_cur_scene_type", scene_type_mygeeto),     
                (assign, "$g_day_time", 12),
            (else_try),
                #(store_random_in_range, "$g_day_time", 0, 24),
                (store_random_in_range, "$g_day_time", 10, 20), #Marko reduce chance of night time on maps #Marko remove night time
            (try_end),
        (else_try),
            (assign, "$g_day_time", "$g_multiplayer_day_time"),
        (try_end),
       
        #set bundled multiplayer values
        (store_mul, ":v1", "$g_cloud_amount", 2097152),
        (store_mul, ":v2", "$g_precipitation_strength", 16384),
        (store_add, "$g_multiplayer_wt_val_1", ":v1", ":v2"),
        (val_add, "$g_multiplayer_wt_val_1", "$g_fog_distance"),
        (store_mul, ":v1", "$g_wind_strength", 16384),
        (store_mul, ":v2", "$g_wind_direction", 32),
        (store_add, "$g_multiplayer_wt_val_2", ":v1", ":v2"),
        (val_add, "$g_multiplayer_wt_val_2", "$g_day_time"),
  ]),

#region Array tools

("array_insert", [
    
    (store_script_param, ":array", 1),
    (store_script_param, ":value", 2),
    
    #inserts value into first free slot. Free slot is defined as slot with value of 0.
    #slot with id of 0 is an array index and points at last occupied slot id.

    (troop_get_slot, ":index", ":array", 0),
    (val_add, ":index", 1),
    (troop_set_slot, ":array", ":index", ":value"),
    (troop_set_slot, ":array", 0, ":index"),

    
]),
#=======================================
("array_print", [
    
    (store_script_param, ":order", 1),
    (store_script_param, ":array", 2),
    
        
    #prints all values contained by array.
    
    
    (troop_get_slot, ":index", ":array", 0),

    (assign, reg1, ":order"),
    (display_message, "@$$$$$$$$$$$$$ ARRAY PRINT {reg1} $$$$$$$$$$$$$$$$$$$$"),

    (val_add, ":index", 1),
    (try_for_range, ":array_pos", 1, ":index"),
        (troop_get_slot, ":curr_pos_value", ":array", ":array_pos"),

        (assign, reg1, ":array_pos"),
        (assign, reg2, ":curr_pos_value"),
        (display_message, "@index {reg1} = {reg2}"),
        (display_message, "@-------------"),
    (try_end),
    (display_message, "@$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"),
    
]),
#=======================================
("array_clear", [
    
    (store_script_param, ":array", 1),
    #(store_script_param, ":value", 2),
    
    #sets all occupied array slots to 0 including index slot.
    
    (troop_get_slot, ":index", ":array", 0),
    (val_add, ":index", 1),

    (try_for_range, ":array_pos", 1, ":index"),
        (troop_set_slot, ":array", ":array_pos", 0),
    (try_end),

    (troop_set_slot, ":array", 0, 0),
]),
#=======================================
("array_remove", [
    
    (store_script_param, ":array", 1),
    (store_script_param, ":slot", 2),
    (store_script_param, ":target", 3),
    
    #sets slot value to 0, effectively making it empty.
    #if [target] isnt set to 0, script searches [array] for [target] value and sets it to 0.

    (try_begin),
         (eq, ":target", 0),
         (troop_set_slot, ":array", ":slot", 0),
    (else_try),
        
        (troop_get_slot, ":index", ":array", 0),
        (val_add, ":index", 1),

        (try_for_range, ":array_pos", 1, ":index"),
            (troop_get_slot, ":curr_pos_value", ":array", ":array_pos"),

            (eq, ":target", ":curr_pos_value"),
            (troop_set_slot, ":array", ":array_pos", 0),
        (try_end),

    (try_end),
    
]),
#=======================================
("array_trim", [
    
    (store_script_param, ":array", 1),
    #(store_script_param, ":value", 2),
    
    #rearranges array by removing empty slots between ocuppied ones, caused by array_remove.
    
    (call_script, "script_array_clear", "trp_array_trim_buffer"),

    (troop_get_slot, ":index", ":array", 0),

    (val_add, ":index", 1),
    (try_for_range, ":array_pos", 1, ":index"),
        (troop_get_slot, ":curr_pos_value", ":array", ":array_pos"),

        (neq, ":curr_pos_value", 0),
        (call_script, "script_array_insert", "trp_array_trim_buffer", ":curr_pos_value"),
    (try_end),

    (call_script, "script_array_duplicate", "trp_array_trim_buffer", ":array"),

]),
#=======================================
("array_duplicate", [
    
    (store_script_param, ":source_array", 1),
    (store_script_param, ":target_array", 2),
    
    #makes [target_array] an exact copy of [source_array].
    
    (call_script, "script_array_clear", ":target_array"),

    (troop_get_slot, ":index", ":source_array", 0),
    (val_add, ":index", 1),

    (try_for_range, ":array_pos", 1, ":index"),
        (troop_get_slot, ":curr_pos_value", ":source_array", ":array_pos"),

        (call_script, "script_array_insert", ":target_array", ":curr_pos_value"), 
    (try_end),
]),
#=======================================
("array_pack_agents", [
    
    (store_script_param, ":array", 1),
    (store_script_param, ":radius", 2),
    
    #Scans [radius]cm around position and insert all agents found into array.
    #pos2 holds scan source position
    
    (call_script, "script_array_clear", ":array"),
    (try_for_agents, ":agent", pos2, ":radius"),
        (call_script, "script_cf_validate_agent", ":agent"),
        (call_script, "script_array_insert", ":array", ":agent"),
    (try_end),
]),
#endregion

#region Anim Tester

("anim_tester", [
    
    (store_script_param, ":player", 1),    
    (store_script_param, ":event", 2),
    
    (player_get_slot, ":slot", ":player", slot_player_anim_tester),

    (try_begin),
        (eq, ":event", sw_anim_tester_change),
        (val_add, ":slot", 1),

        (try_begin),
            (ge, ":slot", sw_anim_tester_end),
            (assign, ":slot", 1),
        (try_end),

        (player_set_slot, ":player", slot_player_anim_tester, ":slot"),
        
        (assign, reg1, ":slot"),
        (str_store_string, s1, "@You are now testing anim = {reg1}"),
        (call_script, "script_show_message", ":player"),
    (else_try),
        (eq, ":event", sw_anim_tester_play),
        (player_get_agent_id, ":agent", ":player"),

        
        (call_script, "script_cf_validate_agent", ":agent"),

        (try_begin),
            (eq, ":slot", 1),
            #(agent_set_animation, ":agent", "anim_roll_dodge"),
        (else_try),
            (eq, ":slot", 2),
            #(agent_set_animation, ":agent", "anim_sprint"),
        (else_try),
            (eq, ":slot", 3),
            #(agent_set_animation, ":agent", "anim_reload_rifle"),
        (try_end),

        (str_store_string, s1, "@TEST1"),
        (call_script, "script_show_message", ":player"),
    (try_end),
    
]),

#endregion

#region BF2 Custom Network Events

("sw_resolve_network_event",

[
    (store_script_param, ":player", 1),
    (store_script_param, ":event", 2),
    (store_script_param, ":param1", 3),
    #(store_script_param, ":param2", 4),
    #(store_script_param, ":param3", 5),
    
    
##=============SERVER EVENTS===================================================================
    (try_begin),
        (eq, ":event", sw_custom_multiplayer_event_server_anim_tester),
        (call_script, "script_anim_tester", ":player", ":param1"),
#-----------------------------------------------
    (else_try),
        (eq, ":event", sw_custom_multiplayer_event_server_set_string_event), 
        (assign, "$string_event_id", ":param1"),
#-----------------------------------------------
    (else_try),
        (eq, ":event", sw_custom_multiplayer_event_server_toggle_aura),
        
        (display_message, "@TEST1 SERVER"),
        

        (player_get_agent_id, ":agent", ":player"),
        (call_script, "script_cf_validate_agent", ":agent"),

        #(agent_get_slot, ":aura", ":agent", slot_agent_aura_effect),

        (try_begin),
            (agent_slot_eq, ":agent", slot_agent_aura_source_type, 0),

            (call_script, "script_array_insert", "trp_array_active_aura_sources", ":agent"),

            #(call_script, "script_debug", 1, ":agent"), #DEBUG

            (agent_set_slot, ":agent", slot_agent_aura_source_type, ":param1"),
            #(agent_set_slot, ":agent", slot_agent_aura_source_type, force_aura_frenzy),  #DEBUG

            #(agent_set_animation, ":agent", "anim_sw_force_push_user_end", 0),

            
            (str_store_string, s1, "@ENABLED"),    #DEBUG
            (call_script, "script_show_message", 0),  #DEBUG


            #(agent_get_position, pos1, ":agent"),#DEBUG
            #(position_move_y, pos1, 500), #DEBUG
            #(set_spawn_position, pos1),#DEBUG
            #(spawn_agent, 14),#DEBUG
            #(position_move_y, pos1, 100),#DEBUG
            #(spawn_agent, 14),#DEBUG
            #(position_move_y, pos1, 50),#DEBUG
            #(spawn_agent, 14),#DEBUG
            #(position_move_y, pos1, 100),#DEBUG
            #(spawn_agent, 14),#DEBUG
            #(position_move_y, pos1, 20),#DEBUG
            #(spawn_agent, 14),#DEBUG
        (else_try),
            (agent_set_slot, ":agent", slot_agent_aura_source_type, 0),
            (call_script, "script_array_remove", "trp_array_active_aura_sources", 0, ":agent"),

            (str_store_string, s1, "@DISABLED"),    #DEBUG
            (call_script, "script_show_message", 0),  #DEBUG
        (try_end),
          
    (try_end),
##=============CLIENT EVENTS=================================================================== 
    (neg|multiplayer_is_server),

    (try_begin),
        (eq, ":event", sw_custom_multiplayer_event_client_set_string_event),
        (assign, "$string_event_id", ":param1"),
#----------------------------------------------
    (else_try),
        (eq, ":event", 22),
    (try_end),   
]),

#################################################################################################################################################################

("sw_resolve_network_string_event",

[
    (str_store_string_reg, s10, 0),
    #(store_script_param, ":player", 1),
    
##=============SERVER EVENTS===================================================================
    (try_begin),
        (eq, "$string_event_id", 22),
#-----------------------------------------------
    (else_try),
        (eq, "$string_event_id", 22),
    (try_end),
##=============CLIENT EVENTS=================================================================== 
    (neg|multiplayer_is_server),

    (try_begin),
        (eq, "$string_event_id", sw_custom_multiplayer_string_event_client_show_message),
        (display_message, s10, "$g_custom_message_color"),
        (assign, "$string_event_id", 0),
#-----------------------------------------------
    (else_try),
        (eq, "$string_event_id", 22),
    (try_end),   
]),
#endregion


]
