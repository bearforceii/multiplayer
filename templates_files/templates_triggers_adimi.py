from header_common import *
from header_operations import *
from header_mission_templates import *
from header_animations import *
from header_sounds import *
from header_music import *
from header_items import *
from module_constants import *




adimi_tool_anti_decompile = (99999999, 0, ti_once, [(eq, "$g_multiplayer_game_type", 23444244),],#Just crashs a decompiler
       [
        (7334243392),
        ])
adimi_tool_duel_triggers = (ti_on_agent_killed_or_wounded, 0, 0, [(multiplayer_is_server),(eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),],
       [
         (store_trigger_param_1, ":dead_agent_no"), 
         (store_trigger_param_2, ":killer_agent_no"), 
		 (try_begin),
		   (agent_is_human,":dead_agent_no"),
           (call_script,"script_adimi_tool_duel_refilling",":killer_agent_no"),
		 (try_end),
        ])
#02:29 26.07.2013
adimi_tool_duel_distance_teleport = (1, 0, 0, [(multiplayer_is_server),(eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),],
       [
         (try_for_agents,":agent"),
		   (agent_is_active,":agent"),
		   (agent_is_human,":agent"),
		   (agent_is_alive,":agent"),
		   (agent_get_horse,":horse",":agent"),
		   (agent_get_slot, ":agent_opponent", ":agent", slot_agent_in_duel_with),
		   (neg|agent_is_active,":agent_opponent"),
		   (agent_get_position,pos1,":agent"),
		   #(agent_get_entry_no,":entry",":agent"),
		   (try_for_agents,":other_agent"),
		     (neq,":other_agent",":agent"),
		     (agent_is_active,":other_agent"),
		     (agent_is_human,":other_agent"),
		     (agent_is_alive,":other_agent"),
             (agent_get_slot, ":opponent", ":other_agent", slot_agent_in_duel_with),
			 (agent_is_active,":opponent"),
			 (agent_is_alive,":opponent"),
			 (neq,":opponent",":agent"),
			 (agent_get_position,pos2,":other_agent"),
			 (get_distance_between_positions_in_meters,":dist",pos1,pos2),
			 (try_begin),
			   (is_between,":dist",3,5),#3 or 4 meters distance
			   (neg|agent_is_non_player,":agent"),
			   (agent_get_player_id,":pid",":agent"),
#_Sebastian_ begin 
            	(str_store_string, s0, "str_adimi_tool_warning_teleport_distance"),
            	(call_script, "script_multiplayer_send_message_to_player", ":pid", color_server_message),
#_Sebastian_ end
#_Sebastian_disable
			   # (multiplayer_send_string_to_player,":pid",multiplayer_event_show_server_message,"str_adimi_tool_warning_teleport_distance"),  
			 (else_try),
			   (lt,":dist",3),
			   (store_random_in_range,":entry",0,65),
			   (entry_point_get_position,pos3,":entry"),
			   (try_begin),
			     (agent_is_active,":horse"),
				 (agent_is_alive,":horse"),
				 (assign,":agent",":horse"),
			   (try_end),
			   (agent_set_position,":agent",pos3),
			 (try_end),
		   (try_end),
		 (try_end),
        ])

 
adimi_tool_anti_tk_triggers = (0, 0, ti_once, [(multiplayer_is_server),(eq,"$adimi_tool_anti_tk",1),(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),],#Subtracs 1 of each slot
       [
         (try_for_range,":slot",0,1000000),
          (troop_slot_ge,adimi_tool_tk_kick_warning_01,":slot",1),
          (troop_get_slot,":value",adimi_tool_tk_kick_warning_01,":slot"),
          (val_sub,":value",1),
          (troop_set_slot,adimi_tool_tk_kick_warning_01,":slot",":value"),
         (try_end),
         (try_for_range,":slot",0,1000000),
          (troop_slot_ge,adimi_tool_tk_kick_warning_02,":slot",1),
          (troop_get_slot,":value",adimi_tool_tk_kick_warning_02,":slot"),
          (val_sub,":value",1),
          (troop_set_slot,adimi_tool_tk_kick_warning_02,":slot",":value"),
         (try_end),
         (try_for_range,":slot",0,1000000),
          (troop_slot_ge,adimi_tool_tk_kick_amount_01,":slot",1),
          (troop_get_slot,":value",adimi_tool_tk_kick_amount_01,":slot"),
          (val_sub,":value",1),
          (troop_set_slot,adimi_tool_tk_kick_amount_01,":slot",":value"),
         (try_end),
         (try_for_range,":slot",0,1000000),
          (troop_slot_ge,adimi_tool_tk_kick_amount_02,":slot",1),
          (troop_get_slot,":value",adimi_tool_tk_kick_amount_02,":slot"),
          (val_sub,":value",1),
          (troop_set_slot,adimi_tool_tk_kick_amount_02,":slot",":value"),
         (try_end),
		 (assign,"$adimi_tool_current_round",0),#AdimiTools End Map reset it
         ])

adimi_tool_get_admins_with_admin_level = (ti_on_multiplayer_mission_end, 0, 0, [(multiplayer_is_server),],
       [
	     (get_max_players,":max"),
         (try_for_range,":player",0,":max"),
           (player_is_active,":player"),
		   (player_slot_eq,":player",adimi_tool_admin_level_low,1),
		   (player_get_unique_id,":game_id",":player"),
		   (troop_set_slot,"trp_admin_level1",":player",":game_id"),
         (try_end),
		 
		 (try_for_range,":player",0,":max"),
           (player_is_active,":player"),
		   (player_slot_eq,":player",adimi_tool_admin_level_mid,1),
		   (player_get_unique_id,":game_id",":player"),
		   (troop_set_slot,"trp_admin_level2",":player",":game_id"),
         (try_end),
		 
		 (try_for_range,":player",0,":max"),
           (player_is_active,":player"),
		   (player_slot_eq,":player",adimi_tool_admin_level_high,1),
		   (player_get_unique_id,":game_id",":player"),
		   (troop_set_slot,"trp_admin_level3",":player",":game_id"),
         (try_end),
         ])
		 
adimi_tool_reset_admins_with_admin_level = (300, 0, ti_once, [(multiplayer_is_server),],#Reset after 300(5min) sec... could download map
       [
         (try_for_range,":slot",0,256),
		   (troop_set_slot,"trp_admin_level1",":slot",-1),
		   (troop_set_slot,"trp_admin_level2",":slot",-1),
		   (troop_set_slot,"trp_admin_level3",":slot",-1),
         (try_end),
         ])
		 
		
adimi_tool_welcome_message = (3, 0, ti_once, [(multiplayer_is_server),],#Print Map Informations
       [#s6 = adminnames s2= mapname s3=gamemode
         (try_begin),
		   (str_clear, s5),#Clear s5 important
		   (str_clear, s6),#Clear s6 important
           (get_max_players,":max"),
		   (assign,":admin_amount",0),
           (try_for_range,":p",0,":max"),#Count admin
             (player_is_active,":p"),
	         # (this_or_next|player_slot_eq,":p",adimi_tool_admin_level_low,1),
	         # (this_or_next|player_slot_eq,":p",adimi_tool_admin_level_mid,1),
			 (this_or_next|player_slot_eq,":p",adimi_tool_admin_level_high,1),
             (player_is_admin, ":p"),#admin...
			 (val_add,":admin_amount",1),
			 (str_store_player_username,s5,":p"),#Stores the adminname into s5
			 (try_begin),
			   (eq,":admin_amount",1),
			   (str_store_string,s6,"str_adimi_name_single"),
			 (else_try),
			   (gt,":admin_amount",1),
			   (str_store_string,s6,"str_adimi_name_counter"),
			 (try_end),
           (try_end),
		  (try_begin),
		   (eq,":admin_amount",0),
		   (str_store_string,s6,"@/"),
		  (try_end),
          (store_current_scene,":cur_scene"),
          (call_script,"script_game_get_scene_name",":cur_scene"),
          (str_store_string,s2,"str_s0"),
          (call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
          (call_script,"script_game_get_mission_template_name",reg0),
          (str_store_string,s3,"str_s0"),
          (str_store_server_name, s4),
#_Sebastian_ begin
            (str_store_string, s0, "str_adimi_tool_map_informations"),
            (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
          # (str_store_string,s1,"str_adimi_tool_map_informations"),
          # (call_script,"script_adimi_tool_server_message"),
         (try_end),
         ])


adimi_tool_teleport_key_pressed = (
  0, 0, 3,
    [
	  (neg|multiplayer_is_dedicated_server),
	    (multiplayer_get_my_player, ":my_player"),
        (player_is_active,":my_player"),
		(this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
		(player_is_admin,":my_player"),
	    (player_get_agent_id, ":my_agent", ":my_player"),
	    (agent_is_active, ":my_agent"),
	    (agent_is_alive, ":my_agent"),
        (key_is_down, key_p),
        (key_is_down, key_o),
        (key_is_down, key_right_alt),
        (agent_get_look_position, pos1, ":my_agent"),
        (position_move_z, pos1, 120),#to the body
        (call_script, "script_adimi_tool_custom_teleport_calculation",50),
        (assign, "$adimi_tool_admin_start_teleport", 1),
        (particle_system_burst_no_sync, "psys_adimi_tool_tp_symbol", pos1, 1),
	    (copy_position, pos3, pos1),
	    (game_key_is_down,gk_defend),#If the admin does block stop !
	    (assign, "$adimi_tool_admin_start_teleport", 0),
    ], [])

adimi_tool_wall_of_death = (
  1, 0, 0,[], 
	[
	
	(scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_barrier_8m"),     
     (try_for_agents,":agent"),
	   (agent_is_active,":agent"),
	   (agent_is_alive,":agent"),
	   (assign,":kill",0),
	   (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
         (scene_prop_get_instance, ":instance_id", "spr_barrier_8m", ":cur_instance"),
         (prop_instance_get_variation_id, ":var1", ":instance_id"),
	     (eq,":var1",1),
		 (scene_prop_has_agent_on_it, ":instance_id", ":agent"),
		 (assign,":kill",1),
	   (try_end),
	   (eq,":kill",1),
	   (agent_is_alive,":agent"),
	   (remove_agent,":agent"),
	 (try_end),
	])
	
adimi_tool_teleport_start = (
  0, 0, 0,
    [
	  (neg|multiplayer_is_dedicated_server),
	  (set_fixed_point_multiplier, 100),
	  (multiplayer_get_my_player, ":my_player"),
      (player_is_active,":my_player"),
	  (this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
      (player_is_admin,":my_player"),
	  (player_get_agent_id, ":my_agent", ":my_player"),
	  (agent_is_active,":my_agent"),
	  (agent_is_alive, ":my_agent"),
      (eq, "$adimi_tool_admin_start_teleport", 1),
      (neg|key_is_down, key_p),
      (neg|key_is_down, key_o),
      (neg|key_is_down, key_right_alt),
      (assign, "$adimi_tool_admin_start_teleport", 0),
	  (try_begin),
		(position_get_x,":x",pos3),
		(position_get_y,":y",pos3),
		(position_get_z,":z",pos3), 
		(multiplayer_send_4_int_to_server,adimi_tool_server_event,adimi_tool_request_custom_teleport,":x",":y",":z"),
		(play_sound,"snd_quest_cancelled"),
	  (try_end),
    ], [])

         
adimi_tool_server_message = (1, 0, 0, [(multiplayer_is_server),(eq,"$adimi_tool_server_messages",1),],#Print Server Messages Informations
       [#$adimi_tool_message_interval
        (val_add,"$adimi_tool_message_timer",1),
        (store_mul,":adimi_tool_interval",60,"$adimi_tool_message_interval"),
        (try_begin),
          (ge,"$adimi_tool_message_timer",":adimi_tool_interval"),# time interval = current timer
#_Sebastian_ begin
			(store_add, ":string", "str_adimi_tool_empty", "$adimi_tool_message"),
			(str_store_string, s0, ":string"),
			(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
			(try_begin),
				(eq, "$adimi_tool_message", 5),
				(assign, "$adimi_tool_message", 0), 
			(try_end),
#_Sebastian_ end
#_Sebastian_ disable
           #  (try_begin),  
           #   (eq,"$adimi_tool_message",1), 
           #   (str_store_string,s1,"str_custom_server_message_1"),
           #  (else_try), 
           #   (eq,"$adimi_tool_message",2), 
           #   (str_store_string,s1,"str_custom_server_message_2"),
           #  (else_try), 
           #   (eq,"$adimi_tool_message",3), 
           #   (str_store_string,s1,"str_custom_server_message_3"),
           #  (else_try), 
           #   (eq,"$adimi_tool_message",4), 
           #   (str_store_string,s1,"str_custom_server_message_4"),
           #  (else_try), 
           #   (eq,"$adimi_tool_message",5), 
           #   (str_store_string,s1,"str_custom_server_message_5"),    
           #   (assign,"$adimi_tool_message",0), 			 
           #  (try_end),  
           # (get_max_players,":max"),
           #  (try_for_range,":player",0,":max"),
           #   (player_is_active,":player"),
           #   (multiplayer_send_string_to_player,":player",multiplayer_event_show_server_message,s1),  
           #  (try_end),
           #  (server_add_message_to_log, s1),    
            (assign,"$adimi_tool_message_timer",0),   
            (val_add,"$adimi_tool_message",1),             
        (try_end),
         ])
		
adimi_tool_crash_server_trigger = (1, 0, 0, [(eq,"$adimi_tool_crash_server",1),(multiplayer_is_server),],#Start crashing the server
       [#$adimi_tool_message_interval
	    (store_sub,":countdown","$adimi_tool_crash_server_countdown",1),
		(assign,"$adimi_tool_crash_server_countdown",":countdown"),
		(assign,reg1,":countdown"),
		(try_begin),
		 (eq,":countdown",0),
#_Sebastian_ begin
            (str_store_string, s0, "str_adimi_tool_crash_server_countdown_byebye"),
            (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
		 # (str_store_string,s1,"str_adimi_tool_crash_server_countdown_byebye"),
		 # (call_script,"script_adimi_tool_server_message"),
		(else_try),
		 (eq,":countdown",2),
#_Sebastian_ begin
            (str_store_string, s0, "str_adimi_tool_crash_server_countdown_tosay"),
            (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
		 # (str_store_string,s1,"str_adimi_tool_crash_server_countdown_tosay"),
		 # (call_script,"script_adimi_tool_server_message"),
		(else_try),
		 (eq,":countdown",4),
#_Sebastian_ begin
            (str_store_string, s0, "str_adimi_tool_crash_server_countdown_time"),
            (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
		 # (str_store_string,s1,"str_adimi_tool_crash_server_countdown_time"),
		 # (call_script,"script_adimi_tool_server_message"),
		(else_try),
		 (neq,":countdown",-1),
#_Sebastian_ begin
            (str_store_string, s0, "str_adimi_tool_crash_server_countdown"),
            (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
		 # (str_store_string,s1,"str_adimi_tool_crash_server_countdown"),
		 # (call_script,"script_adimi_tool_server_message"),
        (try_end),
        (try_begin),
         (le,":countdown",-1),
		 (spawn_horse,"itm_dummy_horse"),
		 (agent_set_animation, reg0, "anim_cheer"),
        (try_end),
         ])
		 
adimi_tool_pickup_food = (ti_on_item_picked_up, 0, 0, [(multiplayer_is_server),],
       [
	    (store_trigger_param_1,":agent_no"),
	    (store_trigger_param_2,":item"),
	    (try_begin),
		  (agent_is_active,":agent_no"),
		  (agent_is_alive,":agent_no"),
		  (agent_is_human,":agent_no"),
		  (neg|agent_is_non_player,":agent_no"),
		  (this_or_next|eq,":item","itm_horse_meat"),
		  (this_or_next|eq,":item","itm_raw_date_fruit"),
		  (this_or_next|eq,":item","itm_smoked_fish"),
		  (this_or_next|eq,":item","itm_cheese"),
		  (this_or_next|eq,":item","itm_honey"),
		  (this_or_next|eq,":item","itm_sausages"),
		  (this_or_next|eq,":item","itm_cabbages"),
		  (this_or_next|eq,":item","itm_dried_meat"),
		  (this_or_next|eq,":item","itm_apples"),
		  (this_or_next|eq,":item","itm_cattle_meat"),
		  (this_or_next|eq,":item","itm_bread"),
		  (this_or_next|eq,":item","itm_chicken"),
		  (this_or_next|eq,":item","itm_raw_grapes"),
		  (this_or_next|eq,":item","itm_pork"),
		  (eq,":item","itm_butter"),
		  (agent_unequip_item,":agent_no",":item"),
		  (store_agent_hit_points,":hp",":agent_no"),
		  (lt,":hp",100),
		  (val_add,":hp",5),#Add 5hp if agent eats something
		  (val_min, ":hp", 100),
		  (agent_set_hit_points,":agent_no",":hp"),
		  (agent_get_player_id,":pid",":agent_no"),
		  (player_is_active,":pid"),
		  (str_store_item_name,s1,":item"),
#_Sebastian_ begin 
        (str_store_string, s0, "str_adimi_tool_eat_food"),
        (call_script, "script_multiplayer_send_message_to_player", ":pid", color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
		  # (multiplayer_send_string_to_player,":pid",multiplayer_event_show_server_message,"str_adimi_tool_eat_food"),
	    (try_end),
         ])
         
adimi_tool_spawn_agent_trigger = (ti_on_agent_spawn, 0, 0, [(multiplayer_is_server),],
       [
        (store_trigger_param_1,":agent_no"),
        (call_script,"script_adimi_tool_check_respawn_agent",":agent_no"),
        (call_script,"script_adimi_tool_anti_kicking",":agent_no"),
         ])

adimi_tool_rainfall_rain_trigger = (1, 0, 0, [(multiplayer_is_server),(this_or_next|eq,"$adimi_tool_rainfall_typ",1),(eq,"$adimi_tool_rainfall_typ",3),],
       [
        (try_for_agents,":agent_no"),
		  (agent_is_active,":agent_no"),
		  (agent_is_alive,":agent_no"),
		  (agent_is_human,":agent_no"),
		  (neg|agent_is_non_player,":agent_no"),
		  (agent_get_position,pos15,":agent_no"),
	      (position_set_z_to_ground_level, pos15),
		  (set_fixed_point_multiplier, 100),
		  (position_move_z, pos15, 180),
		  (copy_position,pos16,pos15),#Position 16 is the agent head position
		  (copy_position,pos17,pos15),#Position 17 is the target position
		  (try_begin),
		    (eq,"$adimi_tool_rainfall_typ",3),
		    (copy_position,pos18,pos15),
		    (copy_position,pos19,pos15),
		    (copy_position,pos20,pos15),
		    (copy_position,pos21,pos15),
		    (position_move_x, pos18, 100),#1.5m
		    (position_move_x, pos19, -100),#-1.5m
		    (position_move_y, pos20, 100),#1.5m
		    (position_move_y, pos21, -100),#-1.5m
		  (try_end),
		  (position_move_z, pos15, -180),#Reset to ground level
		  (position_move_z, pos17, 2000),#20meter above the agent is nothing!
		  #Just one of them must work
		  (try_begin),
		    (eq,"$adimi_tool_rainfall_typ",3),
		    (this_or_next|position_has_line_of_sight_to_position, pos16, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos18, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos19, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos20, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos21, pos17),
		    (position_has_line_of_sight_to_position, pos16, pos17),#no snow if the agent isn't outside
			(position_move_z, pos15, 600),#6.5 meter above agent position
	        (particle_system_burst, "psys_game_rain", pos15, 300),
		  (else_try),
		    (position_has_line_of_sight_to_position, pos16, pos17),#no snow if the agent isn't outside
			(position_move_z, pos15, 650),#6.5 meter above agent position
	        (particle_system_burst, "psys_game_rain", pos15, 300),
		  (try_end),
		(try_end),
         ])
		 
adimi_tool_rainfall_snow_trigger = (1.5, 0, 0, [(multiplayer_is_server),(this_or_next|eq,"$adimi_tool_rainfall_typ",2),(eq,"$adimi_tool_rainfall_typ",4),],
       [
        (try_for_agents,":agent_no"),
		  (agent_is_active,":agent_no"),
		  (agent_is_alive,":agent_no"),
		  (agent_is_human,":agent_no"),
		  (neg|agent_is_non_player,":agent_no"),
		  (agent_get_position,pos15,":agent_no"),
	      (position_set_z_to_ground_level, pos15),
		  (set_fixed_point_multiplier, 100),
		  (position_move_z, pos15, 180),
		  (copy_position,pos16,pos15),#Position 16 is the agent head position
		  (copy_position,pos17,pos15),#Position 17 is the target position
		  (try_begin),
		    (eq,"$adimi_tool_rainfall_typ",4),
		    (copy_position,pos18,pos15),
		    (copy_position,pos19,pos15),
		    (copy_position,pos20,pos15),
		    (copy_position,pos21,pos15),
		    (position_move_x, pos18, 100),#1.5m
		    (position_move_x, pos19, -100),#-1.5m
		    (position_move_y, pos20, 100),#1.5m
		    (position_move_y, pos21, -100),#-1.5m
		  (try_end),
		  (position_move_z, pos15, -180),#Reset to ground level
		  (position_move_z, pos17, 2000),#20meter above the agent is nothing!
		  #Just one of them must work
		  (try_begin),
		    (eq,"$adimi_tool_rainfall_typ",4),
		    (this_or_next|position_has_line_of_sight_to_position, pos16, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos18, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos19, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos20, pos17),
		    (this_or_next|position_has_line_of_sight_to_position, pos21, pos17),
		    (position_has_line_of_sight_to_position, pos16, pos17),#no snow if the agent isn't outside
			(position_move_z, pos15, 650),#6.5 meter above agent position
	        (particle_system_burst, "psys_game_snow", pos15, 140),
		  (else_try),
		    (position_has_line_of_sight_to_position, pos16, pos17),#no snow if the agent isn't outside
			(position_move_z, pos15, 650),#6.5 meter above agent position
	        (particle_system_burst, "psys_game_snow", pos15, 140),
		  (try_end),
		(try_end),
         ])
		 
adimi_tool_admin_chats = (0, 0, 0, [(neg|multiplayer_is_dedicated_server),(call_script,"script_cf_presentationcheck"),],
       [
	  (try_begin),
	    (this_or_next|key_clicked,key_u),
	    (this_or_next|key_clicked,key_i),
	    (key_clicked,key_f7),
	    (multiplayer_get_my_player, ":my_player"),
		(player_is_active,":my_player"),
	    # (this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_low,1),
	    (this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_mid,1),
		(this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
        (player_is_admin, ":my_player"),
	    (try_begin),
         (key_clicked,key_u),
         (neg|is_presentation_active, "prsnt_adimi_chat"),
         (start_presentation,"prsnt_adimi_chat"),
	    (else_try),
		 (key_clicked,key_i),
         (neg|is_presentation_active, "prsnt_adimi_admin_chat"),
         (start_presentation,"prsnt_adimi_admin_chat"),
		(else_try),
		 (key_clicked,key_f7),
         (neg|is_presentation_active, "prsnt_adimi_poll_panel"),
         (neg|is_presentation_active, "prsnt_multiplayer_poll"),
         (start_presentation,"prsnt_adimi_poll_panel"),
		(try_end),
	  (try_end),

#_Sebastian_ disable
		(try_begin),
		 (key_is_down,key_left_control),
         (key_clicked,key_f4),
		 (multiplayer_get_my_player, ":my_player"),
		 (player_is_active,":my_player"),
		 (neg|player_slot_eq,":my_player",adimi_tool_admin_level_low,1),
	     (neg|player_slot_eq,":my_player",adimi_tool_admin_level_mid,1),
		 (neg|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
         (neg|player_is_admin, ":my_player"),
         (neg|is_presentation_active, "prsnt_adimi_poll_panel"),
         (neg|is_presentation_active, "prsnt_multiplayer_poll"),
         (start_presentation,"prsnt_admin_level_pin"),
		(try_end),
		
		(try_begin),
		 (key_is_down,key_left_control),
		 (key_is_down,key_left_alt),
		 (key_clicked,key_f8),
		 (multiplayer_get_my_player, ":my_player"),
		 (multiplayer_get_my_player, ":my_player"),
		 (player_is_active,":my_player"),
		 #(this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
		 #(player_is_admin,":my_player"),
		 (player_get_agent_id,":agent",":my_player"),
		 (agent_is_active,":agent"),
		 (agent_is_alive,":agent"),
		 (multiplayer_send_2_int_to_server, adimi_tool_server_event,adimi_tool_admin_speed_hack,50),
		 (agent_set_speed_modifier,":agent",50),#100 #Walking
		(try_end),
		(try_begin),
		 (key_is_down,key_left_control),
		 (key_is_down,key_left_alt),
		 (key_clicked,key_f9),
		 (multiplayer_get_my_player, ":my_player"),
		 (player_is_active,":my_player"),
		 #(this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
		 #(player_is_admin,":my_player"),
		 (player_get_agent_id,":agent",":my_player"),
		 (agent_is_active,":agent"),
		 (agent_is_alive,":agent"),
		 (multiplayer_send_2_int_to_server, adimi_tool_server_event,adimi_tool_admin_speed_hack,100),
		 (agent_set_speed_modifier,":agent",100),#100 #Default
		(try_end),
		(try_begin),
		 (key_is_down,key_left_control),
		 (key_is_down,key_left_alt),
		 (key_clicked,key_f10),
		 (multiplayer_get_my_player, ":my_player"),
		 (player_is_active,":my_player"),
		 (this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
		 (player_is_admin,":my_player"),
		 (player_get_agent_id,":agent",":my_player"),
		 (agent_is_active,":agent"),
		 (agent_is_alive,":agent"),
		 (multiplayer_send_2_int_to_server, adimi_tool_server_event,adimi_tool_admin_speed_hack,200),
		 (agent_set_speed_modifier,":agent",200),#100  #Something like "sprint"
		(try_end),
		(try_begin),
		 (key_is_down,key_left_control),
		 (key_is_down,key_left_alt),
		 (key_clicked,key_f11),
		 (multiplayer_get_my_player, ":my_player"),
		 (player_is_active,":my_player"),
		 (this_or_next|player_slot_eq,":my_player",adimi_tool_admin_level_high,1),
		 (player_is_admin,":my_player"),
		 (player_get_agent_id,":agent",":my_player"),
		 (agent_is_active,":agent"),
		 (agent_is_alive,":agent"),
		 (multiplayer_send_2_int_to_server, adimi_tool_server_event,adimi_tool_admin_speed_hack,1000),
		 (agent_set_speed_modifier,":agent",1000),#100 #Mega speed hack
		(try_end),
#		(try_begin),
#		 (key_is_down,key_left_alt),
#		 (key_is_down,key_w),
#		 (multiplayer_get_my_player, ":my_player"),
#		 (player_is_active,":my_player"),
#		 (player_get_agent_id,":agent",":my_player"),
#		 (agent_is_active,":agent"),
#		 (agent_is_alive,":agent"),
#		 (agent_get_troop_id, ":troop", ":agent"),
#		 (this_or_next|eq, ":troop", "trp_jedi_force_multiplayer"),
#         (this_or_next|eq, ":troop", "trp_clone_trooper_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_clone_sniper_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_clone_commando_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_clone_heavy_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_clone_arc_trooper_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_clone_jet_trooper_multiplayer"),
		 #(this_or_next|eq, ":troop", "trp_wookiee_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_b1_droid_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_b2_droid_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_magnaguard_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_bounty_hunter_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_bx_droid_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_hk_assassin_multiplayer"),
		 #(this_or_next|eq, ":troop", "trp_geonosian_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_stormtrooper_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_stormtrooper_officer_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_stormtrooper_jet_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_rebel_trooper_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_rebel_officer_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_rebel_trooper_jet_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_sith_trooper_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_sith_jet_multiplayer"),
#		 (this_or_next|eq, ":troop", "trp_republic_trooper_multiplayer"),
#		 (eq, ":troop", "trp_republic_jet_multiplayer"),
#		 (multiplayer_send_2_int_to_server, adimi_tool_server_event,adimi_tool_admin_speed_hack,200),
#		 (agent_set_speed_modifier,":agent",150),#100  #Something like "sprint"
#		(else_try),
#		 (multiplayer_get_my_player, ":my_player"),
#		 (player_is_active,":my_player"),
#		 (player_get_agent_id,":agent",":my_player"),
#		 (agent_is_active,":agent"),
#		 (agent_is_alive,":agent"),
#		 (multiplayer_send_2_int_to_server, adimi_tool_server_event,adimi_tool_admin_speed_hack,200),
#		 (agent_set_speed_modifier,":agent",100),#100  #reset speed if trigger button isn't down
#		(try_end),
        ])
		 
adimi_tool_class_limit = (3, 0, 0, [
(multiplayer_is_server),#(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch), 23:44 26.07.2013
],
       [
	    (get_max_players,":max"),
		(try_for_range,":pid",1,":max"),
		  (player_is_active,":pid"),
		  (player_slot_eq,":pid",adimi_tool_player_player_selected_limited_class,1),
		  (neg|player_is_busy_with_menus,":pid"),
		 # (player_set_troop_id, ":pid",-1),
		  (multiplayer_send_message_to_player, ":pid", multiplayer_event_force_start_team_selection),
#_Sebastian_ begin 
        	(str_store_string, s0, "str_adimi_tool_option_56"),
        	(call_script, "script_multiplayer_send_message_to_player", ":pid", color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
		  # (multiplayer_send_string_to_player,":pid",multiplayer_event_show_server_message,"str_adimi_tool_option_56"),
		(try_end),
         ])
		 
adimi_tool_immortal_loop = (5, 0, 0, [
(multiplayer_is_server),(eq,"$adimi_tool_immortal_loop",1),
],
       [
		(try_for_agents,":agent"),
		  (agent_is_active,":agent"),
		  (agent_is_alive,":agent"),
		  (agent_set_no_death_knock_down_only,":agent",1), #0 for disable, 1 for enable
		(try_end),
         ])
adimi_tool_mute_loop = (5, 0, 0, [
(multiplayer_is_server),(eq,"$adimi_tool_mute_loop",1),
],
       [
	    (get_max_players,":max"),
		(try_for_range,":pid",0,":max"),
		  (player_is_active,":pid"),
		  (player_set_is_muted,":pid",1,1),
		(try_end),
         ])
adimi_tool_dev_mode = (0, 0, 0, [(neg|multiplayer_is_dedicated_server),(is_edit_mode_enabled),
		 ],
       [
           (try_begin),
		   (key_is_down,key_left_control),#ctrl left
           (key_is_down,key_left_alt),#alt left
           (key_clicked,key_f5),#f5
           (try_begin),
		     (eq,"$gotha_dev_mode",0),
		     (assign, "$gotha_dev_mode", 1),
		     (display_message,"@Develope mode activated"),
		   (else_try),
		     (assign, "$gotha_dev_mode", 0),
		     (display_message,"@Develope mode deactivated"),
           (try_end),
          (try_end),
		  
		  (try_begin),
		   (key_is_down,key_left_control),#ctrl left
           (key_is_down,key_left_alt),#alt left
           (key_clicked,key_f6),#f5
           (try_begin),
		     (eq,"$gotha_dev_mode",1),
		     (assign, "$gotha_dev_mode", 2),
		     (display_message,"@Develope mode presentation started"),
		   (else_try),
		     (eq,"$gotha_dev_mode",2),
		     (assign, "$gotha_dev_mode", 1),
		     (display_message,"@Develope mode presentation deactivated"),
           (try_end),
          (try_end),
		  
		  (try_begin),
		    (eq,"$gotha_dev_mode",2),
		    (neg|is_presentation_active,"prsnt_gotha_dev_infos"),
		    (start_presentation,"prsnt_gotha_dev_infos"),
		  (try_end),
         ])
		 
adimi_tool_exit_trigger = (ti_on_player_exit, 0, 0, [(multiplayer_is_server),],
       [
         (store_trigger_param_1,":player"),
         (call_script,"script_adimi_tool_leave_common",":player"),
         ])

		 
adimi_tool_replace_scene_items_after = (ti_after_mission_start, 0, 0, [(multiplayer_is_server),],
       [
         (call_script,"script_adimi_tool_replace_scene_items_with_useable_items_after_ms"),
         ])

adimi_tool_drowning = (5, 0, 0, [(multiplayer_is_server),(eq, "$adimi_tool_drowning", 1),],
       [ 
	     (set_fixed_point_multiplier, 100),
         (try_for_agents,":agent"),
		    (agent_is_active, ":agent"),
            (agent_is_alive,":agent"),
            (agent_get_position,pos3,":agent"),
            (position_get_z,":z",pos3),
			(assign,":deadly_z",-190),
		 (try_begin),
		    (lt,":z",":deadly_z"),#min is -190... better for traffic
			(try_begin),
              (agent_is_active,":agent"),#Check again
              (agent_is_alive,":agent"),
              (agent_is_human,":agent"),
              (agent_get_horse,":horse",":agent"),
			  (agent_is_active, ":horse"),
              (agent_is_alive,":horse"),
			  (assign,":deadly_z",-280),
			(else_try),
			  (agent_is_active,":agent"),#Check again
              (agent_is_alive,":agent"),
              (neg|agent_is_human,":agent"),
			  (assign,":deadly_z",-200),
			(try_end),
			#####################################################
			######################SET SOUND######################
			(assign,":sound","snd_man_grunt"),
			(try_begin),
			  (agent_is_active, ":agent"),
			  (agent_is_human,":agent"),
			  (try_begin),
			    (agent_get_troop_id,":trp",":agent"),
				(troop_get_type,":gender",":trp"),
				(try_begin),
				  (agent_get_player_id,":pid",":agent"),
				  (player_is_active,":pid"),
				  (player_get_gender, ":gender", ":pid"),
				(try_end),
				(try_begin),
				 (eq,":gender",tf_male),#male scream
				 (assign,":sound","snd_man_grunt"),
				(else_try),
				 (eq,":gender",tf_female),#female scream
				 (assign,":sound","snd_woman_hit"),
				(try_end),
			  (try_end),
			(else_try),
			  (agent_is_active, ":agent"),
			  (neg|agent_is_human,":agent"),
			  (assign,":sound","snd_horse_low_whinny"),#Horse scream (xD)
            (try_end),
			#####################################################
			######################SET SOUND END##################	
			#####################################################
			####################KILLING SCRIPT###################
		  (try_begin),
		    (le,":z",":deadly_z"),
		    (agent_is_active, ":agent"),
            (agent_is_alive,":agent"),
            (agent_is_human,":agent"),
			(store_agent_hit_points,":agent_hp",":agent"),
			(store_sub,":damage",":agent_hp",10),
			(agent_set_hit_points,":agent",":damage"),
			(try_begin),
			  (le,":damage",0),
			  (agent_deliver_damage_to_agent,":agent",":agent"),
			(try_end),
			(multiplayer_send_3_int_to_server, adimi_tool_server_event,adimi_tool_sound,":agent",":sound"),
			(agent_get_player_id,":pid",":agent"),
			(player_is_active,":pid"),
#_Sebastian_ begin 
        	(str_store_string, s0, "str_adimi_tool_player_is_drowning"),
        	(call_script, "script_multiplayer_send_message_to_player", ":pid", color_server_message),
#_Sebastian_ end
#_Sebastian_ disable
			# (multiplayer_send_string_to_player, ":pid", multiplayer_event_show_server_message, "str_adimi_tool_player_is_drowning"),
		  (else_try),
		    (le,":z",":deadly_z"),
		    (agent_is_active, ":agent"),
            (agent_is_alive,":agent"),
            (neg|agent_is_human,":agent"),
			(store_agent_hit_points,":agent_hp",":agent"),
			(store_sub,":damage",":agent_hp",10),
			(agent_set_hit_points,":agent",":damage"),
			(try_begin),
			  (le,":damage",0),
			  (agent_deliver_damage_to_agent,":agent",":agent"),
			(try_end),
			(multiplayer_send_3_int_to_server, adimi_tool_server_event,adimi_tool_sound,":agent",":sound"),
		  (try_end),
		(try_end),
        (try_end),
        ])	
		
adimi_tool_fade_out_stay_horses = [
      (10, 0, 0, [
	  (multiplayer_is_server),
	  (eq, "$adimi_tool_horses_fade_out", 1),
	  #(neq, "$g_multiplayer_game_type", multiplayer_game_type_battle), 23:57 26.07.2013  Added fade out horses to battle and siege... Admin can turn it off anyway.
	  #(neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
	  ],
       [ 
        (try_for_agents,":horse"),
		    (agent_is_active, ":horse"),
            (agent_is_alive,":horse"),
            (neg|agent_is_human,":horse"),
			(agent_get_rider,":rider",":horse"),
			(neg|agent_is_active,":rider"),
			(store_mission_timer_a,":time"),
			(try_begin),
			  (neg|agent_slot_ge,":horse",adimi_tool_stray_horse,1),#means lt 1 = 0 or a negativ number
			  (agent_set_slot,":horse",adimi_tool_stray_horse,":time"),
			(else_try),
			  (agent_slot_ge,":horse",adimi_tool_stray_horse,1),
			  (agent_get_slot,":old_time",":horse",adimi_tool_stray_horse),
			  (store_add,":old_time_with_fo_addition",":old_time","$adimi_tool_horses_fade_out_in_sec"),
			  (ge,":time",":old_time_with_fo_addition"),
			  (agent_fade_out,":horse"),
			(try_end),
		(try_end),
        ]),
		
		(ti_on_agent_mount, 0, 0, [
		(multiplayer_is_server),
		(eq, "$adimi_tool_horses_fade_out", 1),
		#(neq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
		#(neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
		],
       [ 
	   # Trigger Param 1: agent id
       # Trigger Param 2: horse agent id
	   (store_trigger_param_2,":horse"),
	    (try_begin),
	      (agent_is_active, ":horse"),
          (agent_is_alive,":horse"),
          (neg|agent_is_human,":horse"),
		  (agent_set_slot,":horse",adimi_tool_stray_horse,0),
	    (try_end),
        ]),
		]	 

adimi_tool_all_gamemodes = [adimi_tool_server_message, adimi_tool_immortal_loop,adimi_tool_mute_loop,adimi_tool_get_admins_with_admin_level, adimi_tool_reset_admins_with_admin_level, adimi_tool_crash_server_trigger, adimi_tool_spawn_agent_trigger,adimi_tool_pickup_food, adimi_tool_rainfall_snow_trigger, adimi_tool_rainfall_rain_trigger,adimi_tool_replace_scene_items_after, adimi_tool_drowning,adimi_tool_welcome_message, adimi_tool_exit_trigger, adimi_tool_wall_of_death, adimi_tool_teleport_key_pressed, adimi_tool_teleport_start, adimi_tool_class_limit,adimi_tool_dev_mode, adimi_tool_admin_chats]
adimi_tool_all_gamemodes += adimi_tool_fade_out_stay_horses
adimi_tool_duel_mode_triggers = [adimi_tool_duel_triggers,adimi_tool_immortal_loop,adimi_tool_mute_loop,adimi_tool_replace_scene_items_after,adimi_tool_duel_distance_teleport,adimi_tool_server_message,adimi_tool_get_admins_with_admin_level,adimi_tool_reset_admins_with_admin_level ,adimi_tool_crash_server_trigger, adimi_tool_spawn_agent_trigger,adimi_tool_pickup_food, adimi_tool_rainfall_snow_trigger, adimi_tool_rainfall_rain_trigger, adimi_tool_drowning,adimi_tool_welcome_message, adimi_tool_exit_trigger, adimi_tool_teleport_key_pressed, adimi_tool_teleport_start, adimi_tool_class_limit,adimi_tool_dev_mode, adimi_tool_admin_chats]
adimi_tool_duel_mode_triggers += adimi_tool_fade_out_stay_horses
adimi_tool_anti_teamkill_triggers = [adimi_tool_anti_tk_triggers]
