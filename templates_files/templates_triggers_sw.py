from header_common import *
from header_operations import *
from header_mission_templates import *
from header_animations import *
from header_sounds import *
from header_music import *
from header_items import *
from module_constants import *





#region Common sw triggers
sw_on_agent_hit = (ti_on_agent_hit, 0, 0, [],
	[
	#(multiplayer_is_server),#_Sebastian_ only triggered server side anyway

	(set_fixed_point_multiplier, 100),
	(store_trigger_param_1, ":wounded_agent"),
	(store_trigger_param_2, ":attacker_agent"),
	(store_trigger_param_3, ":damage"),
	#(store_trigger_param, ":bone", 4),
	#(store_trigger_param, ":missile_item", 5),
	(assign, ":item", reg0),
	
	#27.10.2015, Illuminati Change:
	#Medic system
	(try_begin),
		(eq, ":item", itm_medic_pack),
		(set_trigger_result, 0),#_Sebastian_

		#(agent_get_troop_id, ":trp_id", ":attacker_agent"), #Marko disable agent loop
		#(try_begin),#_sebastian_#Marko disable try loop
			#(this_or_next|eq, ":trp_id", "trp_b1_droid_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_rebel_trooper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_stormtrooper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_republic_trooper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_sith_trooper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_clone_sniper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_clone_commando_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_clone_heavy_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_clone_arc_trooper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_clone_jet_trooper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_wookiee_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_b2_droid_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_magnaguard_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_bounty_hunter_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_bx_droid_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_hk_assassin_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_geonosian_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_stormtrooper_sniper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_stormtrooper_officer_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_stormtrooper_heavy_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_stormtrooper_jet_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_rebel_sniper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_rebel_heavy_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_rebel_officer_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_rebel_trooper_jet_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_sith_heavy_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_sith_sniper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_sith_jet_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_republic_jet_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_republic_sniper_multiplayer"),
			#(this_or_next|eq, ":trp_id", "trp_republic_heavy_multiplayer"),
			#(eq, ":trp_id", "trp_clone_trooper_multiplayer"),
			
			(agent_get_slot, ":medic_packs", ":attacker_agent", slot_agent_medic_packs),
#_Sebastian_ disable		
			# (assign, reg5, ":medic_packs"),
			# (str_store_string, s3, "@You have {reg5} medic packs"),
			
			(gt, ":medic_packs", 0),
			
			(store_agent_hit_points, ":agents_current_hp_in_percent", ":wounded_agent", 0),
			(neq, ":agents_current_hp_in_percent", 100),#_Sebastian_

			(store_random_in_range, ":random_hp_heal_percent", 30, 40), #Get random integer between 1 and 20 (bcs high range - 1)
			(val_add, ":agents_current_hp_in_percent", ":random_hp_heal_percent"), #add to the wounded agents current hp in percent the random integer and set it to his new hp's
			#example: agent has 20% hp. Random integer is 15. agent has now 35% hp.
			(agent_set_hit_points, ":wounded_agent", ":agents_current_hp_in_percent", 0),

			(val_sub, ":medic_packs", 1),
			(agent_set_slot, ":attacker_agent", slot_agent_medic_packs, ":medic_packs"),
#_Sebastian_ disable	
			# (assign, reg6, ":medic_packs"),
			# (str_store_string, s3, "@You have now {reg6} medic packs"),
			
			(agent_get_position, pos5, ":attacker_agent"), #playing at pos so every1 hears it
			(play_sound_at_position, "snd_force_healing", pos5),		
			
			(agent_get_player_id, ":player_id", ":attacker_agent"),#_Sebastian_
			(try_begin),
				(player_is_active, ":player_id"),
#_Sebastian_ begin
				(assign, reg6, ":medic_packs"),#_Sebastian_
				(str_store_string, s0, "@You have now {reg6} medic packs"),
				(call_script, "script_multiplayer_send_message_to_player", ":player_id", color_medic_pack),
#_Sebastian_ end
				(player_get_score, ":score", ":player_id"), #apply 1 assist for healing some1
				(val_add, ":score", 1),
				(player_set_score, ":player_id", ":score"),
			(try_end),
			
			(le, ":medic_packs", 0), #if no medic packs anymore, disarm all medic pack items
			(try_for_range, ":agent_equip_slot", ek_item_0, ek_item_3 + 1),
				(agent_get_item_slot, ":item_id", ":attacker_agent", ":agent_equip_slot"),
				(eq, ":item_id", "itm_medic_pack"),
				(agent_unequip_item, ":attacker_agent", "itm_medic_pack", ":agent_equip_slot"),
			(try_end),
		#(try_end),#_Sebastian_ #Marko disable try loop
	(else_try), #Not doctor/healing attack so do normal stuff	
        (eq, ":item", itm_medic_pack), #Mark begin
		(set_trigger_result, 0),
        
            (agent_get_slot, ":medic_packs", ":attacker_agent", slot_agent_medic_packs),
        
            (gt, ":medic_packs", 0),
        
            #(store_agent_hit_points, ":agents_current_hp_in_percent", ":wounded_agent", 0),
			(agent_get_slot, ":shield", ":wounded_agent", slot_agent_shield),
            (gt, ":shield", 0),
            (lt, ":shield", 100),#_Sebastian_
            
            (store_random_in_range, ":healing", 30, 40), #Get random integer between 1 and 20 (bcs high range - 1)
            #(val_add, ":shield", ":healing"),
            (store_add, ":new_shield", ":shield", ":healing"),
            (try_begin),
                (gt, ":new_shield", 100),
                (assign, ":new_shield", 99),
            (try_end),
            #(agent_set_hit_points, ":wounded_agent", ":agents_current_hp_in_percent", 0),
            (agent_set_slot, ":wounded_agent", slot_agent_shield, ":new_shield"),
            (try_begin),
                (neg|agent_is_non_player, ":wounded_agent"),
                (agent_get_player_id, ":player_no", ":wounded_agent"),
                (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":wounded_agent", slot_agent_shield, ":new_shield"),
            (try_end),
            
            (val_sub, ":medic_packs", 1),
			(agent_set_slot, ":attacker_agent", slot_agent_medic_packs, ":medic_packs"),
            
            (agent_get_position, pos5, ":attacker_agent"), #playing at pos so every1 hears it
			(play_sound_at_position, "snd_force_healing", pos5),		
			
			(agent_get_player_id, ":player_id", ":attacker_agent"),#_Sebastian_
			(try_begin),
				(player_is_active, ":player_id"),
#_Sebastian_ begin
				(assign, reg6, ":medic_packs"),#_Sebastian_
				(str_store_string, s0, "@You have now {reg6} medic packs"),
				(call_script, "script_multiplayer_send_message_to_player", ":player_id", color_medic_pack),
#_Sebastian_ end
			(try_end),
			
			(le, ":medic_packs", 0), #if no medic packs anymore, disarm all medic pack items
			(try_for_range, ":agent_equip_slot", ek_item_0, ek_item_3 + 1),
				(agent_get_item_slot, ":item_id", ":attacker_agent", ":agent_equip_slot"),
				(eq, ":item_id", "itm_medic_pack"),
				(agent_unequip_item, ":attacker_agent", "itm_medic_pack", ":agent_equip_slot"),
			(try_end),
         #Mark End
    (else_try),

#_Sebastian_ disable
	#Blood camera splash system
	# (try_begin),
	# 	(agent_get_player_id, ":player_id", ":wounded_agent"),
	# 	(player_is_active, ":player_id"),
		#(multiplayer_send_int_to_player, ":player_id", multiplayer_event_splash_blood),
	# (try_end),
	
	
		# (try_begin),#det pack
		#   (eq, ":item", "itm_det_pack"),#go through people
		#   (set_trigger_result, 0),
		# (try_end),

		(agent_get_animation, ":anim", ":wounded_agent", 0),#_Sebastian_
		(try_begin),#falling damage
		  (this_or_next|eq, ":anim", "anim_jump"),
		  (this_or_next|eq, ":anim", "anim_jump_loop"),
		  (this_or_next|eq, ":anim", "anim_jump_loop"),
		  (this_or_next|eq, ":anim", "anim_jump_end"),
		  (eq, ":anim", "anim_jump_end_hard"),
		  (eq, ":attacker_agent", ":wounded_agent"),
		  (ge, ":damage", 200),
		  (try_begin),
			(call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_jump),
			(set_trigger_result, 0),
		  (else_try),
			(call_script, "script_cf_agent_has_force_power", ":wounded_agent", power_jump),
			(set_trigger_result, 0),
		  (else_try),
			(call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_jetpack_geonosian),
			(set_trigger_result, 0),	
		  (else_try),
			(call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_jetpack),
			(set_trigger_result, 0),
		  (else_try),
			#(agent_deliver_damage_to_agent, ":wounded_agent", ":wounded_agent", 40),
			(val_div, ":damage", 100),
			(options_set_damage_to_player, 2),
			(convert_to_fixed_point, ":damage"),
			(set_trigger_result, ":damage"),
		  (try_end),
		(try_end),

		(copy_position, pos3, pos0),#_Sebastian_
		(try_begin),
		  #checking if event = projectile blocked by jedi
		  (assign, ":blocked", 0),
		  (try_begin),
			(this_or_next|agent_has_item_equipped, ":wounded_agent", "itm_personal_shield"),
			(call_script, "script_cf_jedi_projectile_defend_condition", ":wounded_agent", ":attacker_agent", ":damage", pos3, ":item"),
			(assign, ":blocking_cost", 80), #Sherlock, was 50, 0.4 30
			(try_begin),
			  (call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_defend1),
			  (assign, ":blocking_cost", 60), #Sherlock, was 30, 0.4 10
			(try_end),
			(try_begin),
			  (call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_defend2),
			  (assign, ":blocking_cost", 100), #Sherlock, was 70, 0.4 50
			(try_end),
			(try_begin),
			  (call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_defend1),
			  (call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_defend2),
			  (assign, ":blocking_cost", 80), #Sherlock, was 50, 0.4 20
			(try_end),
			(agent_get_player_id, ":player_no", ":wounded_agent"),
			(ge, ":player_no", 0),
			(player_get_slot, ":force_points", ":player_no", slot_player_force_points),
			(ge, ":force_points", ":blocking_cost"),
			(val_sub, ":force_points", ":blocking_cost"),
			(call_script, "script_player_set_force_points", ":player_no", ":force_points"),
			(assign, ":blocked", 1),
		  (try_end),
		  (eq, ":blocked", 1),
		  (set_trigger_result, 0),
		  (agent_play_sound, ":wounded_agent", "snd_lightsaber_block_laser"),#_Sebastian_
		  (try_begin),#dont play animation for personal shields
			(neg|agent_has_item_equipped, ":wounded_agent", "itm_personal_shield"),
			(agent_set_animation, ":wounded_agent", "anim_sw_parry_ranged_lightsaber",1),#_Sebastian_
		  (try_end),
		  (call_script, "script_cf_agent_has_force_power", ":wounded_agent", force_defend2),
		  (copy_position, pos4, pos3),
		  (position_rotate_z, pos4, 180),
		  (position_move_y, pos4, 100),
		  (call_script, "script_force_defend2_calculate_accuracy", ":wounded_agent", pos4),
		  (assign, ":accuracy", reg0),
		  (store_random_in_range, ":rand", 0, 360),
		  (position_rotate_y, pos4, ":rand"),
		  (store_random_in_range, ":rand", 0, ":accuracy"),
		  (position_rotate_x_floating, pos4, ":rand"),
		  (agent_get_team, ":team", ":attacker_agent"),
		  (team_get_faction, ":faction", ":team"),
		  (try_begin),
			(eq, ":faction", "fac_kingdom_1"),
			(assign, ":missile_item", "itm_power_pack_blue"),
		  (else_try),
			(eq, ":faction", "fac_kingdom_3"),		  
			(assign, ":missile_item", "itm_power_pack_green"),			
		  (else_try),
			(assign, ":missile_item", "itm_power_pack_red"),
		  (try_end),
		  (agent_is_active, ":wounded_agent"),
		  (neg|eq, ":item", "itm_medic_pack"),
          # (neg|eq, ":item", "itm_shield_pack"),
		  (multiplayer_send_4_int_to_server,multiplayer_missile_invalidforce_sync,1,":wounded_agent",":item",":missile_item"),
		 # (add_missile, ":wounded_agent", pos4, 10000, ":item", 0, ":missile_item", 0),
		 #(add_missile, <agent_id>, <starting_position>, <starting_speed_fixed_point>, <weapon_item_id>, <weapon_item_modifier>, <missile_item_id>, <missile_item_modifier>)
		(try_end),
		#droids losing head
		#(try_begin),
		#(this_or_next|agent_has_item_equipped, ":wounded_agent", "itm_droid_helmet"),
		#(agent_has_item_equipped, ":wounded_agent", "itm_bx_droid_helmet"),
		  #(ge, ":damage", 30),
		  #(eq, ":bone", 9),
		  #(eq, 0, 1),#disabled
		  #(call_script, "script_agent_equip_item_sync", ":wounded_agent", "itm_droid_no_head"),
		#(try_end),
		#blood
		(try_begin),
		  (ge, ":damage", 15),
	#	  (agent_get_troop_id, ":troop_id", ":wounded_agent"),
	#	  (troop_get_type, ":skin", ":troop_id"),
		  (try_begin),#always sparks
	#	    (eq, ":skin", tf_droid),
			(particle_system_burst, "psys_droid_blood", pos3, 50),
		  (else_try),
			(particle_system_burst, "psys_human_blood", pos3, 20),
			(particle_system_burst, "psys_human_blood_2", pos3, 20),
		  (try_end),
		(try_end),
		(try_begin),#sonic blaster
		  (eq, ":item", "itm_sonic_blaster"),
		  (particle_system_burst, "psys_sonic_explosion", pos1, 50),
		  (copy_position, pos1, pos3),
		  (position_rotate_z, pos1, 180),
	#	  (position_get_rotation_around_z, ":z_rot", pos1),

	#	  (agent_get_position, pos2, ":wounded_agent"),
	#	  (init_position, pos1),
	#	  (position_rotate_z, pos1, ":z_rot"),
	#	  (position_copy_rotation, pos2, pos1),
	#	  (agent_set_position, ":wounded_agent", pos2),
	#	  (get_max_players, ":max_players"),
	#	  (multiplayer_get_my_player, ":my_player"),
	#	  (try_for_range, ":cur_player", 0, ":max_players"),
	#	    (neq, ":cur_player", ":my_player"),
	#	    (player_is_active, ":cur_player"),
	#	    (multiplayer_send_2_int_to_player, ":cur_player", multiplayer_agent_set_rotation_sync, ":wounded_agent", ":z_rot"),
	#	  (try_end),
	#	  (call_script, "script_agent_set_animation_sync", ":wounded_agent", "anim_sw_force_push", 0),
	#      (call_script, "script_get_pushed_anim", ":wounded_agent", ":z_rot"),
	#	  (assign, ":animation", reg0),
	#	  (call_script, "script_agent_set_animation_sync", ":wounded_agent", ":animation", 0),

	#	  (call_script, "script_agent_deliver_nonlethal_damage", ":attacker_agent", ":wounded_agent", 10),
	#	  (val_add, ":full_damage", 10),
		(try_end),
		#force damage (damage x2)
		(try_begin),
		(agent_slot_eq,":attacker_agent", slot_agent_using_force, force_damage),
		 (agent_play_sound, ":attacker_agent", "snd_force_offence"),#_Sebastian_
		(call_script, "script_agent_deliver_nonlethal_damage", ":attacker_agent", ":wounded_agent", ":damage"),
		(try_end),
		#electrostaff effect
		(try_begin),
		  (is_between, ":item", itm_electrostaff, itm_electrostaff_alt +1),#_Sebastian_
		  (particle_system_burst, "psys_sw_lightning_victim", pos3, 100),
		  (particle_system_burst, "psys_sw_lightning_b", pos3, 15),
		(try_end),
		
	(try_end),
])

sw_force_requests = (0, 0, 0, [(multiplayer_get_my_player, ":my_player"),(neg|player_is_busy_with_menus, ":my_player"),],
[
	(multiplayer_get_my_player, ":my_player"),
	(player_is_active, ":my_player"),
	(player_get_agent_id, ":my_agent", ":my_player"),
	(agent_is_active, ":my_agent"),
	(agent_is_alive, ":my_agent"),
	(store_add, ":force_used_slot", "$g_cur_force", player_force_power_used_0),
	(player_get_slot, ":force", ":my_player", ":force_used_slot"),
	(agent_get_animation, ":anim", ":my_agent", 0),
	#(agent_get_wielded_item, ":item", ":my_agent", 0),
	
    #(assign, ":force", "itm_aura_regen"), #DEBUG
    
    #(call_script, "script_debug", 500, ":force"), #DEBUG

    (try_begin),
	  (is_between, ":force", force_auras_begin, force_auras_end),

      (game_key_clicked, gk_use_force),

      #(str_store_string, s1, "@OPEN2"),    #DEBUG
      #(call_script, "script_show_message", 0),  #DEBUG

      (call_script, "script_debug", 666, ":force"), #DEBUG

	  (call_script, "script_cf_send_custom_event", 0, sw_custom_multiplayer_event_server_toggle_aura, ":force", 0, 0),  #DEBUG
	(try_end),
    
    
    (try_begin),
	  (this_or_next|eq, ":force", force_jump),
	  (eq, ":force", power_jump),
	  (game_key_is_down, gk_use_force),
	  (eq, ":anim", "anim_jump"),
	  (multiplayer_send_2_int_to_server, multiplayer_event_agent_request_force_usement, ":force", 1),
	(else_try),
	  (neg|game_key_is_down, gk_use_force),
	  (eq, ":anim", "anim_sw_force_jump2"),
	  (multiplayer_send_2_int_to_server, multiplayer_event_agent_request_force_usement, force_jump, 0),
    (try_end),
	(try_begin),
	  (eq, ":force", force_lightning),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_lightning),
	(try_end),
	(try_begin),
	  (eq, ":force", force_sprint),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_sprint),
	(try_end),
	(try_begin),
	  (eq, ":force", force_damage),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_damage),
	(try_end),
	(try_begin),
	  (eq, ":force", force_healing),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_healing),
	(try_end),
	(try_begin),
	  (eq, ":force", force_throw),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_throw),
	(try_end),
	(try_begin),
	  (eq, ":force", force_push),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_push),
	(try_end),
	(try_begin),
	  (eq, ":force", force_jetpack),
	  (game_key_is_down, gk_use_force),
	  (this_or_next|eq, ":anim", "anim_jump"),
	  (this_or_next|eq, ":anim", "anim_jump_loop"),
	  (eq, ":anim", "anim_sw_force_jump_end"),
#	  (agent_get_position, pos1, ":my_agent"),
#	  (position_get_z, ":z", pos1),
#	  (le, ":z", 8000),
	  (multiplayer_send_2_int_to_server, multiplayer_event_agent_request_force_usement, force_jetpack, 1),
	(else_try),
	  (neg|game_key_is_down, gk_use_force),
	  (agent_get_animation, ":anim", ":my_agent", 0),
	  (eq, ":anim", "anim_sw_force_jump"),
	  (multiplayer_send_2_int_to_server, multiplayer_event_agent_request_force_usement, force_jetpack, 0),
	(try_end),
	(try_begin),
	  (eq, ":force", force_jetpack_geonosian),
	  (game_key_is_down, gk_use_force),
	  (this_or_next|eq, ":anim", "anim_jump"),
	  (this_or_next|eq, ":anim", "anim_jump_loop"),
	  (eq, ":anim", "anim_sw_force_jump_end"),
#	  (agent_get_position, pos1, ":my_agent"),
#	  (position_get_z, ":z", pos1),
#	  (le, ":z", 8000),
	  (multiplayer_send_2_int_to_server, multiplayer_event_agent_request_force_usement, force_jetpack_geonosian, 1),
	(else_try),
	  (neg|game_key_is_down, gk_use_force),
	  (agent_get_animation, ":anim", ":my_agent", 0),
	  (eq, ":anim", "anim_sw_force_jump"),
	  (multiplayer_send_2_int_to_server, multiplayer_event_agent_request_force_usement, force_jetpack_geonosian, 0),
	(try_end),
	(try_begin),
	  (eq, ":force", force_shield),
	  (game_key_clicked, gk_use_force),
	  (multiplayer_send_int_to_server, multiplayer_event_agent_request_force_usement, force_shield),
	(try_end),

])
sw_kick_replace = (0, 0, 0, [(game_key_clicked, gk_kick)],
[
#	  (multiplayer_send_int_to_server, multiplayer_player_crouch_request, "anim_jedi_stance_1"),
])
sw_occupy = (0.1, 0, 0, [(multiplayer_is_server)],
[
    (set_fixed_point_multiplier, 100),
	(try_for_agents, ":agent"),
	  (agent_is_active, ":agent"),
	  (agent_is_alive, ":agent"),
	  (agent_get_slot, ":occupied", ":agent", slot_agent_occupied_with),
	  (gt, ":occupied", 0),
	  #stopping?
	  (try_begin),
	    (agent_get_slot, ":stop_in", ":agent", slot_agent_occupied_until),
		(val_sub, ":stop_in", 1),
		(agent_set_slot, ":agent", slot_agent_occupied_until, ":stop_in"),
		(le, ":stop_in"),
		(agent_set_slot, ":agent", slot_agent_occupied_with, 0),
		(try_begin),
	      (eq, ":occupied", occupied_emp),
		  (agent_set_no_dynamics, ":agent", 0),
		(try_end),
		(assign, ":occupied", 0),
	  (try_end),
	  (try_begin),
	    (eq, ":occupied", occupied_emp),
		(agent_set_no_dynamics, ":agent", 1),
		(agent_set_animation, ":agent", "anim_emp_victim", 0),#_Sebastian_
		(try_for_range, ":unused", 0, 3),
		  (agent_get_position, pos1, ":agent"),
		  (store_random_in_range, ":x_offset", -20, 20),
		  (store_random_in_range, ":y_offset", -20, 20),
		  (store_random_in_range, ":z_offset", 0, 170),
		  (position_move_x, pos1, ":x_offset"),
		  (position_move_y, pos1, ":y_offset"),
		  (position_move_z, pos1, ":z_offset"),
	      (particle_system_burst, "psys_droid_blood_2", pos1, 10),
		(try_end),
	  (try_end),
	(try_end),
])
sw_forces = (0.1, 0, 0, [(multiplayer_is_server)],
[
    (set_fixed_point_multiplier, 100),
	(try_for_agents, ":agent"),
      (agent_is_active,":agent"),
	  (agent_is_alive, ":agent"),
	  (neg|agent_is_non_player, ":agent"),
	  (agent_get_player_id, ":player_no", ":agent"),
	  #personal shield
	  #(try_begin),
	   # (agent_has_item_equipped, ":agent", "itm_personal_shield"),
	#	(call_script, "script_player_get_force_power_slot", ":player_no", force_shield),
	#	(store_add, ":force_cooldown_slot", player_force_power_cooldown_0, reg0),
	#	(player_get_slot, ":cooldown_until", ":player_no", ":force_cooldown_slot"),
	#	(store_mission_timer_a_msec, ":time"),
	#	(ge, ":time", ":cooldown_until"),
	#	(call_script, "script_agent_equip_item_sync", ":agent", "itm_lightsaber_no_blade"),
	 # (try_end),

	  (agent_get_slot, ":force", ":agent", slot_agent_using_force),
	  (agent_get_animation, ":anim", ":agent"),
	  #jetpack
	  (try_begin),
	    (eq, ":anim", "anim_sw_force_jump"),
	    (troop_get_slot, ":cost", "trp_force_use_cost2_array", force_jetpack),
	    (player_get_slot, ":player_gold", ":player_no", slot_player_force_points),
		(try_begin),
		  (ge, ":player_gold", ":cost"),
		  (val_sub, ":player_gold", ":cost"),
	      (call_script, "script_player_set_force_points", ":player_no", ":player_gold"),
#		  (agent_get_position, pos1, ":agent"),
#		  (position_get_z, ":z", pos1),
#		  (try_begin),
#		    (gt, ":z", 9000),
#		    (call_script, "script_agent_set_animation_sync", ":agent", "anim_sw_force_jump_end", 0),
#		  (try_end),
		(else_try),
		  (agent_set_animation, ":agent", "anim_sw_force_jump_end", 0),#_Sebastian_
		(try_end),
	  (try_end),
	  (try_begin),
	    (eq, ":anim", "anim_sw_force_jump"),
	    (troop_get_slot, ":cost", "trp_force_use_cost2_array", force_jetpack_geonosian),
	    (player_get_slot, ":player_gold", ":player_no", slot_player_force_points),
		(try_begin),
		  (ge, ":player_gold", ":cost"),
		  (val_sub, ":player_gold", ":cost"),
	      (call_script, "script_player_set_force_points", ":player_no", ":player_gold"),
#		  (agent_get_position, pos1, ":agent"),
#		  (position_get_z, ":z", pos1),
#		  (try_begin),
#		    (gt, ":z", 9000),
#		    (call_script, "script_agent_set_animation_sync", ":agent", "anim_sw_force_jump_end", 0),
#		  (try_end),
		(else_try),
		  (agent_set_animation, ":agent", "anim_sw_force_jump_end", 0),#_Sebastian_
		(try_end),
	  (try_end),

	  #force jump
	  (try_begin),
	    (eq, ":anim", "anim_sw_force_jump2"),
		(neq, ":force", force_jump),
		(neq, ":force", power_jump),
		(agent_set_animation, ":agent", "anim_sw_force_jump2_end", 0),#_Sebastian_
	  (try_end),

	  #force lightning
	  (try_begin),
	    (eq, ":anim", "anim_force_lightning_end"),
		(neg|agent_slot_eq, ":agent", slot_agent_lightning_used, 1),
		(agent_set_slot, ":agent", slot_agent_lightning_used, 1),
		(call_script, "script_server_force_lightning", ":agent"),
	  (try_end),

	  (gt, ":force", 0),
	  (assign, ":valid", 1),
	  #stopping force?
	  (try_begin),
	    (agent_get_slot, ":stop_in", ":agent", slot_agent_stop_force_in),
		(val_sub, ":stop_in", 1),
	    (agent_set_slot, ":agent", slot_agent_stop_force_in, ":stop_in"),
		(le, ":stop_in", 0),
	    (agent_set_slot, ":agent", slot_agent_using_force, 0),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent", slot_agent_using_force, 0),
		(assign, ":valid", 0),
	  (try_end),
	  #force usement cost
	  (try_begin),
	    (eq, ":valid", 1),
	    (troop_get_slot, ":cost", "trp_force_use_cost2_array", ":force"),
	    (player_get_slot, ":player_gold", ":player_no", slot_player_force_points),
		(ge, ":player_gold", ":cost"),
		(val_sub, ":player_gold", ":cost"),
	    (call_script, "script_player_set_force_points", ":player_no", ":player_gold"),
		(assign, ":valid", 1),
	  (else_try),
	    (eq, ":valid", 1),
		(agent_set_slot, ":agent", slot_agent_stop_force_in, 0),
		(agent_set_slot, ":agent", slot_agent_using_force, 0),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent", slot_agent_using_force, 0),
		(assign, ":valid", 0),
	  (try_end),

	  (try_begin),
	    (eq, ":valid", 1),
	    (eq, ":force", force_lightning),#unused now
		(try_begin),
	      (agent_get_wielded_item, ":item", ":agent"),
	      (lt, ":item", 0),
		  (call_script, "script_server_force_lightning", ":agent"),
		(else_try),
		  (agent_set_slot, ":agent", slot_agent_using_force, 0),
		  (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent", slot_agent_using_force, 0),
		(try_end),
	  (else_try),
	    (eq, ":valid", 1),
	    (eq, ":force", force_push),
		(agent_get_slot, ":victim", ":agent", slot_agent_force_victim),
		(try_begin),
		  (this_or_next|neg|agent_is_alive, ":agent"),
		  (neg|agent_is_alive, ":victim"),
		  (assign, ":valid", 0),
		(else_try),
		  (agent_get_animation, ":victim_anim", ":victim", 0),
		  (try_begin),
		    (neq, ":victim_anim", "anim_force_choke"),
		    (agent_set_animation, ":victim", "anim_force_choke", 0),#_Sebastian_
		  (try_end),
		  (try_begin),
		    (neq, ":anim", "anim_sw_force_push_user"),
		    (agent_set_animation, ":agent", "anim_sw_force_push_user", 0),#_Sebastian_
		  (try_end),
		  (try_begin),
		    (store_random_in_range, ":damage", 0, 2),
			(store_agent_hit_points, ":hp", ":victim", 1),
			(try_begin),
			  (le, ":hp", 0),
			  (agent_deliver_damage_to_agent, ":agent", ":victim", 10),
			(else_try),
			  (gt, ":damage", 0),
			  (call_script, "script_agent_deliver_nonlethal_damage", ":agent", ":victim", 1),
			(try_end),
		  (try_end),
		(try_end),
	  (try_end),
	  (try_begin),
	    (eq, ":valid", 0),
	    (eq, ":force", force_push),
		(agent_get_slot, ":victim", ":agent", slot_agent_force_victim),
		(agent_set_slot, ":agent", slot_agent_using_force, 0),
		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent", slot_agent_using_force, 0),
		(agent_set_animation, ":agent", "anim_sw_force_push_user_end", 0),#_Sebastian_
		(agent_set_animation, ":victim", "anim_end_any_animation", 0),#_Sebastian_
	  (try_end),
	(try_end),

	#lightsaber thrown
	(try_for_range, ":prop", "spr_sw_lightsaber_blade_red", "spr_sw_det_pack"),
	  (scene_prop_get_num_instances, ":num_instances", ":prop"),
	  (try_for_range, ":instance_no", 0, ":num_instances"),
		(scene_prop_get_instance, ":instance", ":prop", ":instance_no"),
		(scene_prop_get_slot, ":agent", ":instance", slot_prop_lightsaber_thrown_agent),
		(ge, ":agent", 0),
		(assign, ":remove", 0),
	    (try_begin),
		  (neg|agent_is_alive, ":agent"),
		  (assign, ":remove", 1),
		(else_try),
		  (agent_get_position, pos1, ":agent"),
		  (prop_instance_get_position, pos2, ":instance"),
		  (position_move_z, pos1, 120),
		  (get_distance_between_positions, ":dist", pos1, pos2),
		  (try_begin),
		    (le, ":dist", 100),
		    (assign, ":remove", 1),
		    (scene_prop_get_slot, ":item", ":instance", slot_prop_lightsaber_thrown_item),
		    (try_begin),
			  (neg|agent_has_item_equipped, ":agent", ":item"),
			  (agent_equip_item, ":agent", ":item"),
			(else_try),
			  (try_begin),
			    (agent_get_wielded_item, ":wielded_item", ":agent", 0),
				(eq, ":wielded_item", ":item"),
			    (agent_get_ammo, ":ammo", ":agent", 1),
				(val_add, ":ammo", 1),
			    (agent_set_ammo, ":agent", ":item", ":ammo"),
			  (else_try),
			    (agent_set_ammo, ":agent", ":item", 1),
			  (try_end),
			(try_end),
		    (agent_set_wielded_item, ":agent", ":item"),
	      (else_try),
			(copy_position, pos3, pos1),
			(copy_position, pos4, pos2),
			(position_set_z, pos3, 0),
			(position_set_z, pos4, 0),
			(get_distance_between_positions, ":xydist", pos3, pos4),
			(val_div, ":xydist", 2),
			(position_move_z, pos1, ":xydist"),
		    (store_div, ":time", ":dist", 10),
		    (prop_instance_rotate_to_position, ":instance", pos1, ":time", ":time"),
		  (try_end),
		(try_end),
		(try_begin),
		  (eq, ":remove", 1),
		  (init_position, pos1),
		  (position_set_z, pos1, -1000),
		  (prop_instance_stop_animating, ":instance"),
		  (prop_instance_set_position, ":instance", pos1),
		  (scene_prop_set_slot, ":instance", slot_prop_lightsaber_thrown_agent, -1),
		(try_end),
	  (try_end),
	(try_end),
])
sw_on_item_wielded = (0, 0, 0, [],
[
	(try_for_agents, ":agent"),
      (agent_is_active,":agent"),
	  (agent_is_alive, ":agent"),
	  (agent_get_animation, ":anim", ":agent", 0),
	  (assign, ":has_boots", -1),
	  (call_script, "script_agent_get_jetpack_blast", ":agent"),
	  (assign, ":jetpack_blast", reg0),
	  (try_begin),#jetpack effect
	    (eq, ":anim", "anim_sw_force_jump"),
	    (assign, ":has_boots", 1),
	    (neq, ":jetpack_blast", "itm_lightsaber_no_blade"),
		(neg|agent_has_item_equipped, ":agent", ":jetpack_blast"),

		(agent_equip_item, ":agent", ":jetpack_blast"),
		(agent_play_sound, ":agent", "snd_jetpack_start"),#_Sebastian_
		#(call_script, "script_agent_play_sound", ":agent", "snd_jetpack_loop", 1),

	  (else_try),
	    (neq, ":jetpack_blast", "itm_lightsaber_no_blade"),
	    (neg|eq, ":anim", "anim_sw_force_jump"),
		(agent_has_item_equipped, ":agent", ":jetpack_blast"),

		#(call_script, "script_agent_stop_sound", ":agent"),
		(agent_play_sound, ":agent", "snd_jetpack_end"),#_Sebastian_
	    (val_max, ":has_boots", 0),
	  (try_end),

	  (agent_get_wielded_item, ":wielded_item", ":agent", 0),
	  (agent_get_slot, ":unwielded_item", ":agent", slot_agent_wielded_item),

	  (try_begin),
	    (neq, ":wielded_item", ":unwielded_item"),
#	  (neq, ":anim", "anim_lightsaber_force_pull"),
	  #double pistols/sword and pistol
	    (try_begin),
	      (eq, ":wielded_item", "itm_westar_34_twin"),
		  (agent_equip_item, ":agent", "itm_westar_34_twin_offhand"),
		(else_try),
		  (eq, ":wielded_item", "itm_dc17_twin"),
		  (agent_equip_item, ":agent", "itm_dc17_twin_offhand"),
		#(else_try),
		  #(eq, ":wielded_item", "itm_hilt1_blue_dual"),
		  #(agent_equip_item, ":agent", "itm_hilt1_blue_offhand"),
		(else_try),
	      (this_or_next|eq, ":wielded_item", "itm_vibro_sword_blaster_combo_melee"),
          (eq, ":wielded_item", "itm_vibro_sword_blaster_combo"),
		  (agent_equip_item, ":agent", "itm_kyd21_offhand"),
        (else_try),
		  (this_or_next|eq, ":unwielded_item", "itm_westar_34_twin"),
		  (this_or_next|eq, ":unwielded_item", "itm_dc17_twin"),
	      (this_or_next|eq, ":unwielded_item", "itm_vibro_sword_blaster_combo_melee"),
          (eq, ":unwielded_item", "itm_vibro_sword_blaster_combo"),
		  (agent_equip_item, ":agent", "itm_def_gloves"),
		(try_end),
	  #lightsabers
	    (try_begin),
	      (call_script, "script_cf_is_lightsaber", ":unwielded_item"),
		  (call_script, "script_cf_neg_is_thrown_lightsaber", ":wielded_item"),

	      (agent_play_sound, ":agent", "snd_lightsaber_putaway"),#_Sebastian_
		#(call_script, "script_agent_stop_sound", ":agent"),
	      (val_max, ":has_boots", 0),

	    (try_end),
	    (try_begin),
		  (call_script, "script_cf_is_thrown_lightsaber", ":unwielded_item"),
		  (call_script, "script_cf_neg_is_lightsaber", ":wielded_item"),

#	      (call_script, "script_agent_play_sound", ":agent", "snd_lightsaber_putaway", 0),
		#(call_script, "script_agent_stop_sound", ":agent"),
	      (val_max, ":has_boots", 0),

	    (try_end),
	    (try_begin),
	      (call_script, "script_cf_is_lightsaber", ":wielded_item"),
		  (call_script, "script_cf_neg_is_thrown_lightsaber", ":unwielded_item"),
		  (try_begin),
		    (call_script, "script_cf_is_jedi", ":agent"),
	        (agent_play_sound, ":agent", "snd_lightsaber_takeout", 0),#_Sebastian_
	        #(call_script, "script_agent_play_sound", ":agent", "snd_lightsaber_idleloop", 1),
		    (call_script, "script_lightsaber_get_blade", ":wielded_item"),
	        (agent_equip_item, ":agent", reg0),
	        (assign, ":has_boots", 1),
		  (else_try),
		    (agent_unequip_item, ":agent", ":wielded_item"),
		    (val_add, ":wielded_item", 1),
		    (agent_equip_item, ":agent", ":wielded_item"),
	        (assign, ":has_boots", 1),
		    (agent_set_wielded_item, ":agent", ":wielded_item"),
		    (agent_set_animation, ":agent", "anim_lightsaber_fail", 1),
		  (try_end),
	    (try_end),
	    (try_begin),
		  (call_script, "script_cf_is_thrown_lightsaber", ":wielded_item"),
		  (call_script, "script_cf_neg_is_lightsaber", ":unwielded_item"),
		  (try_begin),
		    (call_script, "script_cf_is_jedi", ":agent"),
#	        (call_script, "script_agent_play_sound", ":agent", "snd_lightsaber_takeout", 0),
		    (call_script, "script_lightsaber_get_blade", ":wielded_item"),
	        (agent_equip_item, ":agent", reg0),
	        (assign, ":has_boots", 1),
		  (else_try),
		    (agent_unequip_item, ":agent", ":wielded_item"),
		    (val_add, ":wielded_item", 1),
		    (agent_equip_item, ":agent", ":wielded_item"),
	        (assign, ":has_boots", 1),
		    (agent_set_wielded_item, ":agent", ":wielded_item"),
		    (agent_set_animation, ":agent", "anim_lightsaber_fail", 1),
		  (try_end),
	    (try_end),
	    (agent_set_slot, ":agent", slot_agent_wielded_item, ":wielded_item"),
	  (try_end),
	  (try_begin),
	    (eq, ":has_boots", 0),
		(agent_equip_item, ":agent", "itm_lightsaber_no_blade"),
	  (try_end),
	(try_end),
])
sw_on_picked_up = (ti_on_item_picked_up, 0 ,0 ,[],
[#Illuminati
	(store_trigger_param_1, ":agent"),
	(store_trigger_param_2, ":item"),
	(store_trigger_param_3, ":scene_prop_id"),
	
	#Get medic pack count from the medic pack scene prop and add it to the players medic packs, unequip the medic pack if one is already equipped
	(try_begin),
		# (this_or_next|eq, ":item", "itm_shield_pack"),
        (eq, ":item", "itm_medic_pack"),
		(scene_prop_get_slot, ":medic_packs", ":scene_prop_id", slot_scene_prop_dropped_medic_packs_count),
		(agent_get_slot, ":medic_packs_old", ":agent", slot_agent_medic_packs),
		(val_add, ":medic_packs_old", ":medic_packs"),
		(agent_set_slot, ":agent", slot_agent_medic_packs, ":medic_packs_old"),
		
		(assign, ":medic_pack_itemamounts", 0),
		(agent_get_player_id, ":player_id", ":agent"),
#_Sebastian_ begin
		(assign, reg6, ":medic_packs_old"),#_Sebastian_
		(str_store_string, s0, "@You have now {reg6} medic packs"),
		(call_script, "script_multiplayer_send_message_to_player", ":player_id", color_medic_pack),
#_Sebastian_ end
		
		(try_begin),
			(try_for_range, ":agent_equip_slot", ek_item_0, ek_item_3 + 1),
				(agent_get_item_slot, ":item_id", ":agent", ":agent_equip_slot"),
				(try_begin),
                    (eq, ":item_id", "itm_medic_pack"),
                    (val_add, ":medic_pack_itemamounts", 1),
				
                    (gt, ":medic_pack_itemamounts", 1),
                    (agent_unequip_item, ":agent", "itm_medic_pack", ":agent_equip_slot"),
                # (else_try),
                    # (eq, ":item_id", "itm_shield_pack"),
                    # (val_add, ":medic_pack_itemamounts", 1),
				
                    # (gt, ":medic_pack_itemamounts", 1),
                    # (agent_unequip_item, ":agent", "itm_shield_pack", ":agent_equip_slot"),
                (try_end),
			(try_end),
		(try_end),
		
	(else_try),
		(store_sub, ":prev_item", ":item", 1),
		(call_script, "script_cf_is_lightsaber", ":prev_item"),
		(agent_unequip_item, ":agent", ":item"),
		(agent_equip_item, ":agent", ":prev_item"),
		(agent_set_wielded_item, ":agent", ":prev_item"),
	(try_end),
])
sw_presentations = (0, 0, 0, [],
[
	(multiplayer_get_my_player, ":my_player"),
    (player_is_active,":my_player"),
	(try_begin),
	  (neg|is_presentation_active, "prsnt_multiplayer_force_display"),
	  (player_get_agent_id, ":my_agent", ":my_player"),
	  (agent_is_active, ":my_agent"),
	  (agent_is_alive, ":my_agent"),
	  (call_script, "script_get_next_useable_force_power", -1, ":my_player"),
	  (assign, "$g_cur_force", reg0),
	  (start_presentation, "prsnt_multiplayer_force_display"),
	(try_end),
])
#force travel
sw_force_travel_key_pressed = (
  0.05, 0, 3,
    [
	  (multiplayer_get_my_player, ":my_player"),
      (player_is_active,":my_player"),
	  (player_get_agent_id, ":my_agent", ":my_player"),
	  (agent_is_active, ":my_agent"),
	  (agent_is_alive, ":my_agent"),
	  (store_add, ":force_used_slot", "$g_cur_force", player_force_power_used_0),
	  (player_get_slot, ":force", ":my_player", ":force_used_slot"),
	  (eq, ":force", force_travel),

      (game_key_is_down, gk_use_force),

      (agent_get_look_position, pos1, ":my_agent"),
      (position_move_z, pos1, 120),
      (call_script, "script_shift_pos1_along_y_axis_to_ground", 100, 30000),
      (assign, "$force_travel_start", 1),
      (particle_system_burst, "psys_fat_arrow", pos1, 1),
	  (copy_position, pos43, pos1),

	  (game_key_is_down,gk_defend),
	  (assign, "$force_travel_start", 0),
    ], [])
sw_force_travel_start = (
  0, 0, 0,
    [
	  (set_fixed_point_multiplier, 100),
	  (multiplayer_get_my_player, ":my_player"),
      (player_is_active,":my_player"),
	  (player_get_agent_id, ":my_agent", ":my_player"),
	  (agent_is_active,":my_agent"),
	  (agent_is_alive, ":my_agent"),
	  (store_add, ":force_used_slot", "$g_cur_force", player_force_power_used_0),
	  (player_get_slot, ":force", ":my_player", ":force_used_slot"),
	  (eq, ":force", force_travel),

      (eq, "$force_travel_start", 1),
      (neg|game_key_is_down, gk_use_force),
      (assign, "$force_travel_start", 0),
      (agent_get_position, pos1, ":my_agent"),
	  (get_distance_between_positions, ":distance", pos1, pos43),
	  (try_begin),
	    (le,":distance",3000),
		(position_get_x,":x",pos43),
		(position_get_y,":y",pos43),
		(position_get_z,":z",pos43),
		(multiplayer_send_3_int_to_server,multiplayer_event_agent_request_force_travel,":x",":y",":z"),
	  (else_try),
	    (display_message,"@Target is too far from you"),
	  (try_end),
    ], [])
#/force travel
sw_animation_triggers = (0, 0, 0.4, [(neg|multiplayer_is_dedicated_server)],#_Sebastian_
	[
		(multiplayer_get_my_player, ":my_player"),
		(player_get_agent_id, ":agent", ":my_player"),
		(ge, ":agent", 0),
		(agent_get_animation, ":cur_anim", ":agent", 0),
		#crouching
		(try_begin),
			(game_key_is_down, gk_crouch_mode),
			(agent_get_horse, ":horse", ":agent"),
			(lt, ":horse", 0),
			(try_begin),
				(neg|is_between, ":cur_anim", "anim_stand_to_crouch_new", "anim_crouch_end"),
				#(neg|is_presentation_active, "prsnt_multiplayer_get_admin_message"), #Sherlock not needed anymore
				(agent_get_position, pos1, ":agent"),
				(copy_position, pos2, pos1),
				(position_set_z_to_ground_level, pos2),
				(get_distance_between_positions, ":distance", pos1, pos2),
				(le, ":distance", 0),
				(multiplayer_send_int_to_server,multiplayer_player_crouch_request, "anim_stand_to_crouch_new"),
			(else_try),
				(eq, ":cur_anim", "anim_crouch"),
				(game_key_is_down, gk_move_forward),
				(multiplayer_send_int_to_server,multiplayer_player_crouch_request, "anim_crouch_walk"),
			(else_try),
				(eq, ":cur_anim", "anim_crouch_walk"),
				(neg|game_key_is_down, gk_move_forward),
				(multiplayer_send_int_to_server,multiplayer_player_crouch_request, "anim_crouch"),
			(try_end),
    (else_try),
			# (player_get_agent_id, ":agent", ":my_player"),#_Sebastian_ disable
			# (ge, ":agent", 0),
			(is_between, ":cur_anim", "anim_stand_to_crouch_new", "anim_crouch_end"),
			(neq, ":cur_anim", "anim_crouch_to_stand_new"),
			(multiplayer_send_int_to_server,multiplayer_player_crouch_request, "anim_crouch_to_stand_new"),
    (try_end),
#/crouching
#sprinting
		(try_begin),
			(agent_slot_eq, ":agent", slot_agent_using_force, force_sprint),
			(neg|is_between, ":cur_anim", "anim_stand_to_crouch_new", "anim_crouch_end"),
			(store_add, ":stances_end", "anim_jedi_stance_2", 1),
			(neg|is_between, ":cur_anim", "anim_jedi_stance_1", ":stances_end"),
			(try_begin),
				(game_key_is_down, gk_move_forward),
				(agent_get_position, pos1, ":agent"),
				(copy_position, pos2, pos1),
				(position_set_z_to_ground_level, pos2),
				(get_distance_between_positions, ":distance", pos1, pos2),
				(le, ":distance", 0),
        		(game_key_is_down, gk_move_forward),
				(multiplayer_send_int_to_server,multiplayer_player_crouch_request, "anim_sprint"),
			(else_try),
				(neg|game_key_is_down, gk_move_forward),
				(eq, ":cur_anim", "anim_jedi_stance_1"),
				(multiplayer_send_int_to_server,multiplayer_player_crouch_request, "anim_end_animation"),
			(try_end),
    (try_end),
	])
	
	#Illuminati
sw_on_item_drop = (ti_on_item_dropped, 0, 0, [],
  [(store_trigger_param_1, ":agent_id"),
  (store_trigger_param_2, ":item_id"),
  (store_trigger_param_3, ":scene_prop_id"),
  
	(try_begin), #assign the medic pack amount on the medic pack scene prop and set agents medic packs to 0
		# (this_or_next|eq, ":item_id", "itm_shield_pack"),
        (eq, ":item_id", "itm_medic_pack"),
		(agent_get_slot, ":medic_packs", ":agent_id", slot_agent_medic_packs),
		(scene_prop_set_slot, ":scene_prop_id", slot_scene_prop_dropped_medic_packs_count, ":medic_packs"),
		(agent_set_slot, ":agent_id", slot_agent_medic_packs, 0),
    (try_end),
#_Sebastian_ begin
	(call_script, "script_cf_is_ranged", ":item_id"),
  	(agent_slot_eq, ":agent_id", slot_agent_gun_aim_on, 1),
	(agent_set_slot, ":agent_id", slot_agent_gun_aim_on, 0),
	(agent_set_slot, ":agent_id", slot_agent_gun_automatic_on, 0),
	#(item_has_property, ":item_id", itp_unbalanced), marko remove unbalanced
	(agent_set_speed_modifier, ":agent_id", 100),

	(agent_get_player_id, ":player_id", ":agent_id"),
	(player_is_active, ":player_id"),
	(multiplayer_send_3_int_to_player, ":player_id", multiplayer_event_set_agent_slot, ":agent_id", slot_agent_gun_aim_on, 0),
	(multiplayer_send_3_int_to_player, ":player_id", multiplayer_event_set_agent_slot, ":agent_id", slot_agent_gun_automatic_on, 0),
#_Sebastian_ end 
  ])
sw_force_points = (0.5, 0, 0, [],
  [
	(get_max_players, ":max_players"),
	(try_for_range, ":player_no", 0, ":max_players"),
	  (player_is_active, ":player_no"),
	  (player_get_agent_id, ":agent_no", ":player_no"),
	  (agent_is_active, ":agent_no"),
	  (agent_is_alive, ":agent_no"),
	  (player_get_slot, ":force_points", ":player_no", slot_player_force_points),
      (try_begin),
	  (call_script, "script_cf_player_has_force_power", ":player_no", force_more_points_ludicrous), #Mark
      (assign,":force_point_limit",9001),
	  (else_try),
      (call_script, "script_cf_player_has_force_power", ":player_no", force_more_points),
      (assign,":force_point_limit",1500),
      (else_try),
      (assign,":force_point_limit",1000),
      (try_end),
	  (lt, ":force_points", ":force_point_limit"),
	  (try_begin),
	    (call_script, "script_cf_player_has_force_power", ":player_no", force_damage),
	    (val_add, ":force_points", 10),
	  (else_try),
        (call_script, "script_cf_player_has_force_power", ":player_no", force_faster_points),
	    (val_add, ":force_points", 30),	
	  (else_try),
        (call_script, "script_cf_player_has_force_power", ":player_no", force_jetpack),
	    (val_add, ":force_points", 10),		
	  (else_try),
	    (val_add, ":force_points", 15),
	  (try_end),
	  (try_begin),
        (call_script, "script_cf_player_has_force_power", ":player_no", force_more_points_ludicrous),
	    (val_min, ":force_points", 9001),	
	  (else_try),
	    (call_script, "script_cf_player_has_force_power", ":player_no", force_more_points),
	    (val_min, ":force_points", 1500),	
	  (else_try),
	    (val_min, ":force_points", 1000),	  
	  (try_end),
	  (call_script, "script_player_set_force_points", ":player_no", ":force_points"),
	(try_end),
  ])
sw_before_mission_start = (ti_before_mission_start, 0, 0, [],
[
    (team_set_relation, 0, 2, 1),
    (team_set_relation, 1, 2, 1),
	(assign, "$guns_not_load_until", -1),#_Sebastian_
	(assign, "$guns_cant_fire_until", -1),#_Sebastian_
])

#_Sebastian_ begin
sw_warcry = (0, 0, 5,  #was 15
[
	(neg|multiplayer_is_dedicated_server),
	(game_key_clicked, gk_party_window),
	(call_script, "script_cf_no_presentation"),

	(multiplayer_get_my_player, ":player"),
    (player_is_active,":player"),
    (neg|player_is_busy_with_menus, ":player"),

    (player_get_agent_id, ":agent", ":player"),
	(agent_is_active,":agent"),
	(agent_is_alive, ":agent"),

	(agent_get_troop_id, ":troop", ":agent"),
	(troop_slot_ge, ":troop", slot_troop_warcry, 1),
],
[
	(multiplayer_send_int_to_server, multiplayer_event_warcry_binoculars, 0),
])

sw_guns = (0, 0, 0, [],
[
	(store_mission_timer_a_msec, ":time"),

	(try_begin),#client side
		(neg|multiplayer_is_dedicated_server),

		(call_script, "script_cf_no_presentation"),

		(multiplayer_get_my_player, ":player"),
    	(player_is_active,":player"),
    	(neg|player_is_busy_with_menus, ":player"),

    	(try_begin),
    		(key_clicked, key_m),
    		(store_current_scene, ":scene"),
        	(scene_get_slot, reg0, ":scene", slot_scene_map_mesh),
        	(gt, reg0, 0),
    		(start_presentation,"prsnt_scene_map"),
    	(try_end),

		(player_get_agent_id, ":agent", ":player"),
		(agent_is_active,":agent"),
		(agent_is_alive, ":agent"),
		
		(set_fixed_point_multiplier, 100),
		(store_zoom_amount, ":zoom"),
		(try_begin),
			(game_key_is_down, gk_zoom),
			(try_begin),
				(is_presentation_active, "prsnt_danyele_sniper_code"),
				(agent_get_wielded_item,":item_id",":agent", 0),
				(try_begin), #marko
					(eq,":item_id","itm_binoculars"),
					(neq, ":zoom", 170),
					(set_zoom_amount, 170),
					#(display_message, "@Binoc zoom 170"),
				(else_try), 
					(neq,":item_id","itm_binoculars"),
					(neq, ":zoom", 150),
					(set_zoom_amount, 150),
					#(display_message, "@zoom 150"),
				(try_end),											
			(else_try),
				(neq, ":zoom", 100),
				(set_zoom_amount, 100),
				#(display_message, "@Zoom 100"),
		    (try_end),
			
		(else_try),
			(try_begin),
				(is_presentation_active, "prsnt_danyele_sniper_code"),
				(try_begin),
					(neq, ":zoom", 100),
					(set_zoom_amount, 100),
				(try_end),
			(else_try),
				(neq, ":zoom", 0),
				(set_zoom_amount, 0),
			(try_end),
		#MARKO	manage zoom when player is dead
		#(else_try),
			#(neq|agent_is_alive, ":agent"),
			#(try_begin),
				#(game_key_is_down, gk_zoom),
				#(try_begin),					
					#(neq, ":zoom", 100),
					#(set_zoom_amount, 100),
				#(try_end),
			#(else_try),
				#(neq, ":zoom", 0),
				#(set_zoom_amount, 0),		
			#(try_end),
		#MARKO	
		(try_end),
		
		#MARKO	manage zoom when player is dead
		#(else_try),
		#	(neq|agent_is_alive, ":agent"),
		#	(display_message, "@{!}DEBUG : Agent is dead"),
		#	(try_begin),
		#		(game_key_is_down, gk_zoom),
		#		(display_message, "@{!}DEBUG :Zoom is down"),
		#		(try_begin),					
		#			(neq, ":zoom", 100),
		#			(set_zoom_amount, 100),
		#			(display_message, "@{!}DEBUG : Set zoom 100"),
		#		(try_end),
		#	(else_try),
		#		(display_message, "@{!}DEBUG : Zoom isn't down"),
		#		(neq, ":zoom", 0),
		#		(set_zoom_amount, 0),		
		#		(display_message, "@{!}DEBUG : Set zoom 0"),
		#	(try_end),
		#MARKO	
		
		#(set_fixed_point_multiplier, 100),
		#(store_zoom_amount, ":zoom"),
		#(try_begin),
		#	(game_key_is_down, gk_zoom),
		#	(try_begin),
		#		(is_presentation_active, "prsnt_danyele_sniper_code"),
		#		(try_begin),
		#			(neq, ":zoom", 150),
		#			(set_zoom_amount, 150),
		#		(try_end),
		#	(else_try),
		#		(neq, ":zoom", 100),
		#		(set_zoom_amount, 100),
		#	(try_end),
		#(else_try),
		#	(try_begin),
		#		(is_presentation_active, "prsnt_danyele_sniper_code"),
		#		(try_begin),
		#			(neq, ":zoom", 100),
		#			(set_zoom_amount, 100),
		#		(try_end),
		#	(else_try),
		#		(neq, ":zoom", 0),
		#		(set_zoom_amount, 0),
		#	#MARKO
		#	(else_try),
		#		#(try_for_agents, ":agent"),
		#		(player_get_agent_id, ":agent", ":player"),
		#		(neq|agent_is_alive, ":agent"),
		#		(set_zoom_amount, 0),
		#	#MARKO END
		#	(try_end),
		#(try_end),

		(agent_get_wielded_item, ":item", ":agent", 0),
		(try_begin),
			(call_script, "script_cf_is_ranged", ":item"),
			(try_begin),
				(game_key_clicked, gk_defend),
				(neg|game_key_is_down, gk_attack),
				(multiplayer_send_int_to_server, multiplayer_event_guns, guns_aim_toggle),
				(neq, ":player", 0),#dont call if this is the server itself
				(try_begin),
	                (agent_slot_eq, ":agent", slot_agent_gun_aim_on, 0),
	                (agent_set_slot, ":agent", slot_agent_gun_aim_on, 1),
				(else_try),
	            	(agent_set_slot, ":agent", slot_agent_gun_aim_on, 0),
	                (agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),
	            (try_end),
		  	(try_end),

		  	(try_begin),
				(game_key_clicked, gk_attack),
				(agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
				(try_begin),
					(ge, ":time", "$guns_not_load_until"),
					(store_add, "$guns_not_load_until", ":time", 1000),
					(item_get_speed_rating, ":load_time", ":item"),
					(try_begin),
						(gt, ":load_time", 1),#bot workaround
						(val_mul, ":load_time", 100),
						(store_add, "$guns_cant_fire_until", ":time", ":load_time"),
					(else_try),
						(assign, "$guns_cant_fire_until", ":time"),
					(try_end),	
				(else_try),
						(assign, "$guns_cant_fire_until", -1),
				(try_end),
			(try_end),

			(try_begin),
				(game_key_is_down, gk_attack),
				(try_begin),
					(agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
					(agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 0),
					(neq, "$guns_cant_fire_until", -1),
					(ge, ":time", "$guns_cant_fire_until"),
					(item_get_food_quality, ":num_missiles", ":item"),
					(item_get_max_ammo, ":consumption", ":item"),
					(val_mul, ":consumption", ":num_missiles"),

					(try_begin),
			            (is_between, ":item", itm_sonic_blaster, itm_sonic_blaster_aim+1),
			            (assign, ":ammo_type", itm_sonic_pack),
					(else_try),
						(is_between, ":item", itm_cyc_rifle, itm_cyc_rifle_aim+1),
						(assign, ":ammo_type", itm_cycler_bullet),	
			        (else_try),
			            (assign, ":ammo_type", itm_power_pack),
			        (try_end),
			        
			        (assign, ":ammo", 0),
			        (try_for_range, ":slot", ek_item_0, ek_head),
			            (agent_get_item_slot, ":slot_item", ":agent", ":slot"),
			            (eq, ":slot_item", ":ammo_type"),
			            (agent_get_ammo_for_slot, ":slot_ammo", ":agent", ":slot"),
			            (val_add, ":ammo", ":slot_ammo"),
			        (try_end),

					(ge, ":ammo", ":consumption"),
					(agent_set_slot, ":agent", slot_agent_gun_automatic_on, 1),
					(neq, ":player", 0),#dont call if this is the server itself
					(multiplayer_send_int_to_server, multiplayer_event_guns, guns_fire),
				(try_end),
			(else_try),
				(agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 1),
				(agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),
				(neq, ":player", 0),#dont call if this is the server itself
				(multiplayer_send_int_to_server, multiplayer_event_guns, guns_fire),
			(try_end),
			(neq, ":player", 0),#dont call if this is the server itself
			(call_script, "script_autofire", ":agent", ":item", ":time", 0),
		
		(else_try),#binoculars 
			(eq, ":item", itm_binoculars),
			(game_key_clicked, gk_defend),			
			(multiplayer_send_int_to_server, multiplayer_event_warcry_binoculars, 1), #marko  
			#(multiplayer_send_3_int_to_player, ":agent", multiplayer_event_binocular, 0, 0, 0),
			#(agent_get_animation, ":anim", ":agent", 1),
			#(neq, ":anim", "anim_binocular"),
			#(start_presentation,"prsnt_danyele_sniper_code"),
			#(start_presentation,"prsnt_binoculars_presentation"),
		(try_end),
	(try_end),

	(try_begin),#server side
		(multiplayer_is_server),
		(try_for_agents, ":agent"),
			(agent_is_alive, ":agent"),
			(agent_get_wielded_item, ":item", ":agent", 0),
			(call_script, "script_cf_is_ranged", ":item"),
			(neg|agent_is_non_player, ":agent"),
			(call_script, "script_autofire", ":agent", ":item", ":time", 1),
		(try_end),
	(try_end),
])

sw_on_item_unwield = (ti_on_item_unwielded, 0, 0, [],
[
	(store_trigger_param_1, ":agent"),
	(store_trigger_param_2, ":item"),

  	(call_script, "script_cf_is_ranged", ":item"),
  	(agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
	(agent_set_slot, ":agent", slot_agent_gun_aim_on, 0),
	(agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),

	(multiplayer_is_server),
	(item_has_property, ":item", itp_unbalanced),
	(agent_set_speed_modifier, ":agent", 100),
])

#_Sebastian_ end

#_Sebastian_ disable
# sw_guns = (0, 0, 0, [],
# [
# 	(multiplayer_get_my_player, ":my_player"),
#     (player_is_active,":my_player"),
#     (neg|player_is_busy_with_menus, ":my_player"),
# 	(player_get_agent_id, ":my_agent", ":my_player"),
# 	(agent_is_active,":my_agent"),
# 	(agent_is_alive, ":my_agent"),
# 	(store_mission_timer_a_msec, ":time"),
#     (try_begin),
# 	  (game_key_clicked, gk_defend),
# 	  (agent_get_wielded_item, ":item", ":my_agent", 0),
# 	  (ge, ":item", 0),
# 	  (item_slot_ge, ":item", slot_item_ranged_type, 1),
# 	  (multiplayer_send_int_to_server, multiplayer_event_guns, guns_aim_toggle),
# 	(try_end),
# 	(try_begin),
# 	  (game_key_is_down, gk_attack),
# 	  (ge, ":time", "$guns_not_load_until"),
# 	  (agent_slot_eq, ":my_agent", slot_agent_gun_automatic_on, 0),
# 	  (agent_get_ammo, ":ammo", ":my_agent", 1),
# 	  (gt, ":ammo", 0),
# 	  (agent_get_wielded_item, ":item", ":my_agent", 0),
# 	  (ge, ":item", 0),
# 	  (item_slot_ge, ":item", slot_item_ranged_type, 1),
# 	  (agent_get_animation, ":anim", ":my_agent", 1),
# 	  (item_slot_eq, ":item", slot_item_animation, ":anim"),
# #	  (agent_get_animation_progress, ":progress", ":my_agent", 1),
# #	  (ge, ":progress", 100),
# 	  #G:	test
# 	  #(try_for_range,":unit_test",1,200),
# 	    (multiplayer_send_2_int_to_server, multiplayer_event_guns, guns_fire, 1),
# 	  #(try_end),
# 	  (store_add, "$guns_not_load_until", ":time", 250),
# 	(try_end),
# 	(try_begin),
# 	  (neg|game_key_is_down, gk_attack),
# 	  (ge, ":time", "$guns_not_stop_fire_until"),
# 	  (agent_slot_eq, ":my_agent", slot_agent_gun_automatic_on, 1),
# 	  (multiplayer_send_2_int_to_server, multiplayer_event_guns, guns_fire, 0),
# 	  (store_add, "$guns_not_stop_fire_until", ":time", 250),
# 	(try_end),
# ])
# sw_guns_server = (0.05, 0, 0, [(multiplayer_is_server)],
# [
# 	(set_fixed_point_multiplier, 100),
# 	(store_mission_timer_a_msec, ":time"),
# 	(try_for_agents, ":agent"),
#       (agent_is_active,":agent"),
# 	  (agent_get_slot, ":last_time", ":agent", slot_agent_last_time_shot),
# 	  (agent_get_wielded_item, ":wielded_item", ":agent", 0),
# 	  (assign, ":frequenzy", 0),
# 	  (try_begin),
# 	    (ge, ":wielded_item", 0),
# 		(item_get_slot, ":frequenzy", ":wielded_item", slot_item_frequenzy),
# 	  (try_end),
# 	  (store_add, ":next_time", ":last_time", ":frequenzy"),

# 	  (agent_get_animation, ":upper_anim", ":agent", 1),
# 	  (try_begin),
# 	    (call_script, "script_cf_agent_can_aim", ":agent"),
# 	    (try_begin),
# 		  (agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
# 		  (ge, ":wielded_item", 0),
# 		  (item_get_slot, ":item_anim", ":wielded_item", slot_item_animation),
# 		  (neq, ":upper_anim", ":item_anim"),
# 		  (agent_set_animation, ":agent", ":item_anim", 1),#_Sebastian_
# 	    (else_try),
# 		  (agent_get_troop_id, ":troop", ":agent"),
# 		  (eq, ":troop", "trp_b2_droid_multiplayer"),
# 	      (this_or_next|lt, ":wielded_item", 0),
# 		  (agent_slot_eq, ":agent", slot_agent_gun_aim_on, 0),
# 		  (neq, ":upper_anim", "anim_stand_b2"),
# 		  (agent_set_animation, ":agent", "anim_stand_b2", 1),#_Sebastian_
# 		(try_end),
# 	  (try_end),

# 	  (ge, ":wielded_item", 0),
# 	  (agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 1),
# 	  (ge, ":time", ":next_time"),
# 	  (item_slot_eq, ":wielded_item", slot_item_animation, ":upper_anim"),
# 	  #(agent_get_animation_progress, ":progress", ":agent", 1),
# 	  #(ge, ":progress", 100),
# 	  (item_get_slot, ":ammo_needed", ":wielded_item", slot_item_energy_per_shot),
# 	  (agent_get_ammo, ":ammo", ":agent", 1),
# 	  (try_begin),
# 	    (ge, ":ammo", ":ammo_needed"),
# 		(agent_is_alive, ":agent"),
# 		(ge, ":wielded_item", 0),
# 	  (else_try),
# 	    (agent_get_player_id, ":player_no", ":agent"),
# 		(ge, ":player_no", 0),
# 	    (agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),
# 		(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_set_agent_slot, ":agent", slot_agent_gun_automatic_on, 0),
# 	  (try_end),
# 	  (agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 1),
# 	  (val_sub, ":ammo", ":ammo_needed"),
# 	  (try_begin),
# 		(eq, ":wielded_item", "itm_backpack_rockets"),
# 	    (agent_set_ammo, ":agent", "itm_backpack_rockets", ":ammo"),
# 	  (else_try),
# 		(eq, ":wielded_item", "itm_sonic_blaster"), #Sonic blaster Sherlock
# 		(agent_set_ammo,":agent", "itm_sonic_pack", ":ammo"),
# 	  (else_try),
# 	    (agent_set_ammo, ":agent", "itm_power_pack", ":ammo"),
# 	  (try_end),
# 	  (call_script, "script_gun_calculate_accuracy", ":agent", 1),
# 	  (assign, ":accuracy", reg0),
#       (assign,":gun_sound","snd_blaster_shot"),
#       (try_begin),
#       (item_get_slot,":sound",":wielded_item",sw_gun_shot_sound),
#       (is_between,":sound","snd_gun_sounds_begin","snd_gun_sounds_end"),
#       (assign,":gun_sound",":sound"),
#       (try_end),
#       (item_get_slot,":slot",":wielded_item",slot_item_is_double_gun),
#       (try_begin),
#         (eq,":slot",1),
# 	    (call_script, "script_twin_blaster_shoot_gun", ":agent", ":wielded_item", ":accuracy", 10000, ":gun_sound"),
#       (else_try),
#         (neq,":slot",1),
# 	    (call_script, "script_shoot_gun", ":agent", ":wielded_item", ":accuracy", 10000, ":gun_sound"),
#       (try_end),
# 	(try_end),
# ])

sw_prop_movement_trigger = (10, 0, 0, [(multiplayer_is_server)], [
	(store_random_in_range, ":rand", 0, 100),
	(try_begin),
	  (ge, ":rand", 90),
	  (assign, "$g_z95_move", 1),
	(try_end),
])
sw_prop_movement = (0.1, 0, 0, [(multiplayer_is_server)],
[
	(eq, "$g_z95_move", 1),
	(scene_prop_get_num_instances, ":num_instances", "spr_sw_z95_headhunter"),
	(gt, ":num_instances", 0),
	(scene_prop_get_instance, ":instance", "spr_sw_z95_headhunter", 0),
	(scene_prop_get_slot, ":target", ":instance", slot_movement_prop_target),
	(prop_instance_get_position, pos1, ":instance"),
	(assign, ":continue", 0),
	(try_begin),
	  (eq, ":target", 0),
	  (assign, ":target", -1),
	  (assign, ":continue", 1),
	(else_try),
	  (prop_instance_get_position, pos3, ":target"),
	  (get_distance_between_positions, ":dist", pos1, pos3),
	  (le, ":dist", 800),
	  (prop_instance_get_variation_id, ":target", ":target"),
	  (assign, ":continue", 1),
	(try_end),
	(eq, ":continue", 1),
	(scene_prop_get_num_instances, ":num_instances", "spr_sw_z95_headhunter_waypoint"),
	(val_add, ":target", 1),
	(val_mod, ":target", ":num_instances"),
	(assign, ":start", -1),
	(assign, ":result_instance", -1),
	(try_for_range, ":target_instance_no", 0, ":num_instances"),
	  (scene_prop_get_instance, ":target_instance", "spr_sw_z95_headhunter_waypoint", ":target_instance_no"),
	  (prop_instance_get_variation_id, ":variation", ":target_instance"),
	  (try_begin),
	    (eq, ":variation", 0),
		(assign, ":start", ":target_instance"),
	  (else_try),
	    (eq, ":variation", ":target"),
	    (assign, ":result_instance", ":target_instance"),
	  (try_end),
	(try_end),
	(try_begin),
	  (ge, ":result_instance", 0),
	  (prop_instance_get_position, pos2, ":result_instance"),
	  (get_distance_between_positions, ":time", pos1, pos2),
	  (val_div, ":time", 70),
	  (prop_instance_animate_to_position, ":instance", pos2, ":time"),
	  (scene_prop_set_slot, ":instance", slot_movement_prop_target, ":result_instance"),
	(else_try),
	  (ge, ":start", 0),
	  (prop_instance_get_position, pos2, ":start"),
	  (prop_instance_animate_to_position, ":instance", pos2, 0),
	  (scene_prop_set_slot, ":instance", slot_movement_prop_target, ":start"),
	  (assign, "$g_z95_move", 0),
	(try_end),
])
wall_of_death_and_jetpack = (1, 0, 0, [(multiplayer_is_server)],#_Sebastian_ fix
[
	(scene_prop_get_num_instances, ":num_instances", "spr_sw_wall_of_death"),
	(try_for_range, ":instance_no", 0, ":num_instances"),
		(scene_prop_get_instance, ":instance", "spr_sw_wall_of_death", ":instance_no"),
		(prop_instance_get_position, pos1, ":instance"),
		(try_for_agents, ":agent_no"),
            (agent_is_active,":agent_no"),
			(agent_is_alive, ":agent_no"),
			(agent_get_position, pos2, ":agent_no"),
			(position_is_behind_position, pos2, pos1),
			(agent_set_hit_points, ":agent_no", 0),
			(agent_deliver_damage_to_agent, ":agent_no", ":agent_no", 1),#_Sebastian_ fix
		(try_end),
	(try_end),
	(scene_prop_get_num_instances, ":num_instances", "spr_sw_wall_of_no_jetpack"),
	(try_for_range, ":instance_no", 0, ":num_instances"),
		(scene_prop_get_instance, ":instance", "spr_sw_wall_of_no_jetpack", ":instance_no"),
		(prop_instance_get_position, pos1, ":instance"),
		(try_for_agents, ":agent_no"),
            (agent_is_active,":agent_no"),
			(agent_is_alive, ":agent_no"),
			(agent_get_position, pos2, ":agent_no"),
			(position_is_behind_position, pos2, pos1),
			(agent_get_animation, ":anim", ":agent_no"),
			(eq, ":anim", "anim_sw_force_jump"),
			(agent_set_animation, ":agent_no", "anim_sw_force_jump_end", 0),#_Sebastian_
		(try_end),
	(try_end),
    # grenades
#_Sebastian_ begin
	(try_for_range, ":cur_prop", "spr_sw_grenade_tmp", "spr_sw_day_time"),
		(try_for_prop_instances, ":instance", ":cur_prop", somt_temporary_object),
			(scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
			(agent_is_active, ":agent"),
			(prop_instance_get_position, pos1, ":instance"),
			# (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
			# (ge, ":agent", 0),
			(scene_prop_get_slot, ":time_remaining", ":instance", slot_prop_timer),
			(try_begin),
				(gt, ":time_remaining", 0),
				(val_sub, ":time_remaining", 1),
				(particle_system_burst, "psys_grenade_timer", pos1, 50),
				(scene_prop_set_slot, ":instance", slot_prop_timer, ":time_remaining"),
			(else_try),
				(try_begin),
					(eq, ":cur_prop", "spr_sw_grenade_tmp"),
					(call_script, "script_effect_at_position", itm_frag_grenade, ":agent"),
				(else_try),
					(eq, ":cur_prop", "spr_sw_grenade_tmp"),
					(call_script, "script_effect_at_position", itm_missile_launcher_ammo, ":agent"),
				(else_try),
					(eq, ":cur_prop", "spr_sw_smoke_grenade_tmp"),
					(call_script, "script_effect_at_position", itm_smoke_grenade, -1),
				(else_try),
					(eq, ":cur_prop", "spr_sw_signal_nade_red_tmp"),
					(call_script, "script_effect_at_position", itm_signal_nade_red, -1),
				(else_try),
					(eq, ":cur_prop", "spr_sw_signal_nade_green_tmp"),
					(call_script, "script_effect_at_position", itm_signal_nade_green, -1),
				(else_try),
					(eq, ":cur_prop", "spr_sw_signal_nade_blue_tmp"),
					(call_script, "script_effect_at_position", itm_signal_nade_blue, -1),
				(try_end),
				(call_script, "script_remove_scene_prop", ":instance"),
			(try_end),
		(try_end),
	(try_end),
#_Sebastian_ end	
	
#_Sebastian_ not needed anymore, merged into optimized version above	
	# (scene_prop_get_num_instances, ":num_instances", "spr_sw_grenade_tmp"),
	# (try_for_range, ":instance_no", 0, ":num_instances"),
		# (scene_prop_get_instance, ":instance", "spr_sw_grenade_tmp", ":instance_no"),
		# (prop_instance_get_position, pos1, ":instance"),
        # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
        # (ge, ":agent", 0),
        # (scene_prop_get_slot, ":time_remaining", ":instance", slot_prop_timer),
        # (val_sub, ":time_remaining", 1),
        # (try_begin),
            # (le, ":time_remaining", 0),
            # (scene_prop_set_slot, ":instance", slot_prop_belongs_to_agent, -1),
            # (call_script, "script_explosion_at_pos1", ":agent"),
        # (else_try),
            # (particle_system_burst, "psys_grenade_timer", pos1, 50),
            # (call_script, "script_play_sound_at_position_sync", "snd_beep", pos1),
            # (scene_prop_set_slot, ":instance", slot_prop_timer, ":time_remaining"),
        # (try_end),
	# (try_end),
	# (scene_prop_get_num_instances, ":num_instances", "spr_sw_emp_grenade_tmp"),
	# (try_for_range, ":instance_no", 0, ":num_instances"),
		# (scene_prop_get_instance, ":instance", "spr_sw_emp_grenade_tmp", ":instance_no"),
		# (prop_instance_get_position, pos1, ":instance"),
        # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
        # (ge, ":agent", 0),
        # (scene_prop_get_slot, ":time_remaining", ":instance", slot_prop_timer),
        # (val_sub, ":time_remaining", 1),
        # (try_begin),
            # (le, ":time_remaining", 0),
            # (scene_prop_set_slot, ":instance", slot_prop_belongs_to_agent, -1),
            # (call_script, "script_emp_at_pos1", ":agent"),
        # (else_try),
            # (particle_system_burst, "psys_grenade_timer", pos1, 50),
            # (call_script, "script_play_sound_at_position_sync", "snd_beep", pos1),
            # (scene_prop_set_slot, ":instance", slot_prop_timer, ":time_remaining"),
        # (try_end),
	# (try_end),
	# (scene_prop_get_num_instances, ":num_instances", "spr_sw_mandalorian_grenade_tmp"),
	# (try_for_range, ":instance_no", 0, ":num_instances"),
		# (scene_prop_get_instance, ":instance", "spr_sw_mandalorian_grenade_tmp", ":instance_no"),
		# (prop_instance_get_position, pos1, ":instance"),
        # (scene_prop_get_slot, ":agent", ":instance", slot_prop_belongs_to_agent),
        # (ge, ":agent", 0),
        # (scene_prop_get_slot, ":time_remaining", ":instance", slot_prop_timer),
        # (val_sub, ":time_remaining", 1),
        # (try_begin),
            # (le, ":time_remaining", 0),
            # (scene_prop_set_slot, ":instance", slot_prop_belongs_to_agent, -1),
            # (call_script, "script_explosion_at_pos1", ":agent"),
        # (else_try),
            # (particle_system_burst, "psys_grenade_timer", pos1, 50),
            # (call_script, "script_play_sound_at_position_sync", "snd_beep", pos1),
            # (scene_prop_set_slot, ":instance", slot_prop_timer, ":time_remaining"),
        # (try_end),
	# (try_end),
])
 #pavise
pavise = (0, 0, 0, [],
       [
        (try_for_agents, ":cur_agent"),
        
            (agent_is_human, ":cur_agent"),         
            (agent_is_alive, ":cur_agent"),  
            (agent_get_position, pos2, ":cur_agent"),
            #==click g && has pavise
            (try_begin),

                (agent_get_wielded_item, ":shield_item", ":cur_agent", 1),
                
				#(this_or_next|eq, ":shield_item", "itm_bo_rebel_shield1"), #BALANCE FIX
				#(this_or_next|eq, ":shield_item", "itm_bo_rebel_shield2"),
				#(this_or_next|eq, ":shield_item", "itm_bo_rebel_shield3"),
				(eq, ":shield_item", "itm_droid_shield"),
                
				(key_clicked, key_g),
                (clear_omitted_keys),
            
                (scene_prop_get_num_instances, ":num_props", "spr_pavise"),
                (position_move_y, pos2, 50),
                (set_spawn_position, pos2),

                (assign, ":pav_found", 0),#pf
                #FOR # props if prop at LATENT spot
                (try_for_range, ":prop_no", 0, ":num_props"),
                    (eq, ":pav_found", 0),#pf
                    (scene_prop_get_instance, ":scene_prop_id", "spr_pavise", ":prop_no"),
                    (prop_instance_get_position, pos3, ":scene_prop_id"), #pos3 holds position of current prop
                    (position_get_z, ":z_coor", pos3),
                    (eq, ":z_coor", -100*100),#-100 LATENT spot

                    (prop_instance_set_position, ":scene_prop_id", pos2),#pavise move
                    (agent_unequip_item, ":cur_agent", ":shield_item"),  #BALANCE FIX
                    (assign, ":pav_found", 1),#pf
                (try_end),
            (try_end),#click g
        (try_end),#for agents
        ])#0,0,0 pavise
sw_player_check_force_is_usable = (ti_on_agent_spawn, 0, 0, [(neg|multiplayer_is_dedicated_server)],
[
    (store_trigger_param_1, ":agent_no"),
    (multiplayer_get_my_player, ":my_player"),
    (player_is_active,":my_player"),
    (player_get_agent_id,":agent",":my_player"),
    (agent_is_active,":agent"),
    (agent_is_alive,":agent"),
    (eq,":agent",":agent_no"),
    (assign,"$restart_force_presentation",1),
	(try_begin),
	  (neg|is_presentation_active, "prsnt_multiplayer_force_display"),
	  (player_get_agent_id, ":my_agent", ":my_player"),
	  (agent_is_active, ":my_agent"),
	  (agent_is_alive, ":my_agent"),
	  (call_script, "script_get_next_useable_force_power", -1, ":my_player"),
	  (assign, "$g_cur_force", reg0),
	  (start_presentation, "prsnt_multiplayer_force_display"),
	(try_end),
  #  (store_trigger_param_1, ":agent_no"),
  #    (try_begin),
  #     (agent_is_active,":agent_no"),
  #     (agent_is_alive,":agent_no"),
  #     (neg|agent_is_non_player,":agent_no"),
  #     (agent_get_troop_id,":troop", ":agent_no"),
  #     (agent_get_player_id,":player",":agent_no"),
  #     (player_is_active,":player"),
  #     (call_script,"script_cf_player_check_is_force_usable",":player",":troop"),
  #    (try_end),
  ])
  
#Module_Mission_templates
 
#Client-side trigger that plays the sniper presentation when aiming #Sherlock Snipers, DanyEle
#script_client_get_my_agent is just:
#(multiplayer_get_my_player, ":player_id"),
#(player_is_active, ":player_id"),
#(player_get_agent_id", ":agent_id"),
#(assign, reg0, ":agent_id"),
common_asist_system_1 =      (ti_on_agent_hit, 0, 0, [],
     [
     (store_trigger_param_1, ":agent_damaged"),
     (store_trigger_param_2, ":dealer_agent"),
     (store_trigger_param_3, ":damage"),
     
     (agent_is_human, ":agent_damaged"),
     (agent_is_human, ":dealer_agent"),
     (agent_is_active, ":agent_damaged"),
     (agent_is_active, ":dealer_agent"),
     
     
     (assign, ":begin", "trp_find_item_cheat"),
     
     (val_add, ":agent_damaged", ":begin"),
     (troop_get_inventory_slot, ":damage_ex", ":agent_damaged", ":dealer_agent"),
     (ge, ":damage", 0),
     (val_add, ":damage", ":damage_ex"),
     (troop_set_inventory_slot, ":agent_damaged", ":dealer_agent", ":damage"), #This saves, agent_damaged into a troop id. 
     ])
common_asist_system_2 = (ti_before_mission_start, 0,0, [],
[
       (assign, ":limit", 150),
       (val_add, ":limit", 1),
       (assign, ":begin", "trp_find_item_cheat"),
       (val_add, ":limit", ":begin"),
       (try_for_range, ":trp", ":begin", ":limit"),
       # (troop_get_inventory_capacity, ":capacity", ":trp"),
       (assign, ":capacity", 150),
       (val_add, ":capacity", 1),
       (try_for_range, ":inv", 0,  ":capacity"),
       (troop_set_inventory_slot, ":trp", ":inv",  0),
       (try_end),
       (try_end),
       ])
common_asist_system_3 =       (ti_on_agent_killed_or_wounded, 0, 0, [],
       [
      (this_or_next|multiplayer_is_server),
      (multiplayer_is_dedicated_server),
      (store_trigger_param_1, ":dead_agent_no"),
      (store_trigger_param_2, ":killer_agent_no"),
       
       (agent_is_human, ":dead_agent_no"),
       (agent_is_human, ":killer_agent_no"),
       (gt, ":dead_agent_no", 0),
       (gt, ":killer_agent_no", 0),
     
       (assign, ":begin", "trp_find_item_cheat"),
       
       (store_add, ":cur_troop", ":dead_agent_no", ":begin"),
       (store_free_inventory_capacity, ":capacity", ":cur_troop"),
       (assign, ":capacity", 150),
       (val_add, ":capacity", 1),
       
       (assign, reg1, ":capacity"),
       (assign, reg2, ":killer_agent_no"),
       (assign, reg3, ":dead_agent_no"),
       
       # (display_message, "@ Capacity: {reg1}, Killer Agent No: {reg2}, Dead Agent No: {reg3}"),
       
       (try_for_range, ":agent", 0, ":capacity"),
       (troop_get_inventory_slot, ":damage", ":cur_troop", ":agent"),
       
       (try_begin),
          (ge, ":damage", 20),
          (neq, ":killer_agent_no", ":agent"),
          (troop_set_inventory_slot, ":cur_troop", ":agent", 0),
		  (agent_get_player_id, ":player_id", ":agent"),
		  (player_is_active, ":player_id"),
          (player_get_score, ":score", ":player_id"),
          (val_add, ":score", 1),
          (player_set_score, ":player_id", ":score"),
       (try_end),
       (else_try),
          (troop_set_inventory_slot, ":cur_troop", ":agent", 0),
       (try_end),
	   
	   #Marko unzoom on death
	   (try_begin),
	   (neg|multiplayer_is_server),
			(set_zoom_amount, 0),		
		(try_end),
	   
	   #(try_begin),
	   #(neg|multiplayer_is_server),
	   #    (try_begin),
	   #		(game_key_is_down, gk_zoom),
	   #		(try_begin),					
		#			(neq, ":zoom", 100),
		#			(set_zoom_amount, 100),
		#		(try_end),
		#	(else_try),
		#		(neq, ":zoom", 0),
		#		(set_zoom_amount, 0),		
		#	(try_end),
		#(try_end),
	   #Marko end
         ])


danyele_bf2_sniper_code = (0, 0, 1,
 [
  (neg|multiplayer_is_dedicated_server),
  (game_key_clicked,gk_zoom),
  ],
  [
    (neg|is_presentation_active,"prsnt_danyele_sniper_code"),
    (call_script,"script_client_get_my_agent"),
    (assign,":agent_id",reg0),
    (agent_is_active,":agent_id"),
    (agent_is_alive,":agent_id"),

    (agent_get_wielded_item,":item_id",":agent_id", 0),
    (this_or_next|eq,":item_id","itm_binoculars"), #marko
    (this_or_next|eq,":item_id","itm_dc15x_sniper_aim"),
	(this_or_next|eq,":item_id","itm_cyc_rifle_aim"),
    (this_or_next|eq,":item_id","itm_dlt_19x_sniper_aim"),
    (this_or_next|eq,":item_id","itm_ld1_sniper_aim"),
	(this_or_next|eq,":item_id","itm_orepublic_sniper_aim"),
	(this_or_next|eq,":item_id","itm_sempire_sniper_aim"),
    (eq,":item_id","itm_e_5s_sniper_aim"),
    (agent_get_animation, ":anim_playing", ":agent_id", 1),
 
	#if aiming animation, play scope presentation
	(this_or_next|eq,":anim_playing","anim_binocular"), #marko
	(eq,":anim_playing","anim_rifle_aim_aim2"),
	(start_presentation,"prsnt_danyele_sniper_code"),
	
	#(eq,":anim_playing","anim_binocular"),
	#(start_presentation,"prsnt_binoculars_presentation"),
 ])  
#/sw
#endregion
#region New sw triggers
#region Auras triggers
sw_process_auras_trigger = (4, 0, 0, 

    [(multiplayer_is_server),],
	[
        #(str_store_string, s1, "@PROCESS"),    #DEBUG
        #(call_script, "script_show_message", 0),  #DEBUG

        (call_script, "script_sw_process_auras"),
    ],
)

sw_trim_aura_sources_table = (12, 0, 0, 

    [(multiplayer_is_server),],
	[
        #(str_store_string, s1, "@TRIM"),    #DEBUG
        #(call_script, "script_show_message", 0),  #DEBUG

        (call_script, "script_array_trim", "trp_array_active_aura_sources"),
    ],
)

sw_aura_spawn_workaround = (ti_on_agent_spawn, 0, 0, 

    [(multiplayer_is_server),],
	[
        (store_trigger_param_1, ":agent"),
        (agent_get_player_id, ":player", ":agent"),
        (player_set_slot, ":player", player_force_power_0, force_aura_healing),
    ],
)
#endregion
#region Anim tester triggers

sw_anim_tester_buttons = (1, 0, 0, 

    [],
	[
        (try_begin),
            (key_is_down, key_f1),
            (multiplayer_send_4_int_to_server, sw_multiplayer_events, sw_custom_multiplayer_event_server_anim_tester, sw_anim_tester_change, 0, 0),
            (multiplayer_get_my_player, ":player"),
            (call_script, "script_anim_tester", ":player", sw_anim_tester_change),
        (else_try),
            (key_is_down, key_f4),
            (multiplayer_send_4_int_to_server, sw_multiplayer_events, sw_custom_multiplayer_event_server_anim_tester, sw_anim_tester_play, 0, 0),
            (multiplayer_get_my_player, ":player"),
            (call_script, "script_anim_tester", ":player", sw_anim_tester_play),
        (try_end),
    ],
)


bot_ready_to_shoot = (0.1, 0.2, 0, [(multiplayer_is_server)],
[
	(try_for_agents, ":agent"), #Getting all Agents
	
		(ge, ":agent", 0),
		(agent_is_active, ":agent"),
		(agent_is_alive, ":agent"),
		(agent_is_non_player, ":agent"),
		(agent_is_human, ":agent"), #Do we have a living human bot
		(agent_get_wielded_item, ":item_no", ":agent", 0),
		(ge, ":item_no", 0),
		(agent_get_combat_state, ":combat_state", ":agent"),
		
		(try_begin),
			(agent_get_slot, ":shots", ":agent", slot_agent_effective_shots),
			(agent_get_slot, ":started_firing", ":agent", slot_agent_started_firing),
			
			(try_begin),
				(eq, ":started_firing", 1),
				(eq, ":shots", 0),
				(agent_set_slot, ":agent", slot_agent_started_firing, 0),
			(try_end),
			
			(call_script, "script_cf_is_ranged", ":item_no"),
			(this_or_next|eq, ":combat_state", 1),
			(eq, ":combat_state", 3),
			
			(try_begin),
				(gt, ":shots", 4),
				(agent_set_slot, ":agent", slot_agent_started_firing, 1),
			(try_end),
			
			(neq, ":started_firing", 1),
			(agent_set_slot, ":agent", slot_agent_is_ready, 1),
			(agent_set_speed_modifier, ":agent", 0),
			
		(else_try),
			(agent_set_slot, ":agent", slot_agent_is_ready, 0),
			(agent_set_slot, ":agent", slot_agent_gun_automatic_on, 0),
			(agent_set_speed_modifier, ":agent", 100),
		(try_end),
	(try_end),
])


sw_ai = (0.02, 0, 0, [(multiplayer_is_server),], #add check if there is any AI at all
[
	
	
	(try_for_agents, ":agent"),
		(ge, ":agent", 0),
		(agent_is_active,":agent"),
		(agent_is_alive, ":agent"),
		(agent_is_non_player, ":agent"), #we don't want players to shoot on their own
		(agent_is_human, ":agent"), #horses also shouldn't
		(store_mission_timer_a_msec, ":time"),
		(agent_get_wielded_item, ":item", ":agent", 0),
		(ge, ":item", 0),
		
		(try_begin),
			(call_script, "script_cf_is_ranged", ":item"),
			(agent_get_slot, ":started_firing", ":agent", slot_agent_started_firing),
			(try_begin),
	                (agent_slot_eq, ":agent", slot_agent_gun_aim_on, 0),
					(agent_set_slot, ":agent", slot_agent_gun_aim_on, 1),
					(item_get_weapon_length, ":item_anim", ":item"),
					(agent_set_animation, ":agent", ":item_anim", 1),
		  	(try_end),
			
			(try_begin),
			(agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
			(item_get_weapon_length, ":item_anim", ":item"),
			(agent_get_animation, ":anim", ":agent", 1),
			(neq, ":anim", ":item_anim"),
			(agent_set_animation, ":agent", ":item_anim", 1),
			(try_end),
			

			(try_begin),
				(agent_slot_eq, ":agent", slot_agent_is_ready, 1),
				(agent_slot_eq, ":agent", slot_agent_gun_aim_on, 1),
				(agent_slot_eq, ":agent", slot_agent_gun_automatic_on, 0),
				(lt, ":started_firing", 1),
				(item_get_food_quality, ":num_missiles", ":item"),
				(item_get_max_ammo, ":consumption", ":item"),
				(val_mul, ":consumption", ":num_missiles"),
				(try_begin),
			        (is_between, ":item", itm_sonic_blaster, itm_sonic_blaster_aim+1),
			        (assign, ":ammo_type", itm_sonic_pack),
				(else_try),
					(is_between, ":item", itm_cyc_rifle, itm_cyc_rifle_aim+1),
					(assign, ":ammo_type", itm_cycler_bullet),	
			    (else_try),
			        (assign, ":ammo_type", itm_power_pack),
			    (try_end),
			    (assign, ":ammo", 0),
			    (try_for_range, ":slot", ek_item_0, ek_head),
			        (agent_get_item_slot, ":slot_item", ":agent", ":slot"),
			        (eq, ":slot_item", ":ammo_type"),
			        (agent_get_ammo_for_slot, ":slot_ammo", ":agent", ":slot"),
			        (val_add, ":ammo", ":slot_ammo"),
			    (try_end),
				(ge, ":ammo", ":consumption"),
				(agent_set_slot, ":agent", slot_agent_gun_automatic_on, 1),
				
			(try_end),
			(call_script, "script_autofire_ai", ":agent", ":item", ":time", 1),
		(try_end),
		
	(try_end),
	],
)
#endregion
#endregion
#region Game modes injectors
##################################################################
sw_dm_triggers = [

	sw_on_agent_hit,
	sw_force_requests,
	sw_kick_replace,
	sw_occupy,
	sw_forces,
	sw_on_item_wielded,
	sw_on_picked_up,
	sw_presentations, sw_on_item_drop,
	sw_force_travel_key_pressed,
	pavise,
	danyele_bf2_sniper_code,
	sw_force_travel_start,
	sw_animation_triggers, 
	sw_force_points, 
	sw_before_mission_start,
	sw_warcry, sw_guns,
	sw_on_item_unwield, 
	sw_player_check_force_is_usable, 
	sw_prop_movement_trigger, 
	wall_of_death_and_jetpack,
	sw_prop_movement,
	wall_of_death_and_jetpack,
    #sw_anim_tester_buttons,
	#sw_aura_spawn_workaround,
	sw_process_auras_trigger,
	sw_trim_aura_sources_table,
	bot_ready_to_shoot,
	sw_ai,

]
#########################################
sw_tdm_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,

      sw_process_auras_trigger,
      sw_trim_aura_sources_table,
	  
	  bot_ready_to_shoot,
	  sw_ai,

]
##############################################
sw_hq_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##############################################
sw_cf_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##################################################
sw_sg_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  danyele_bf2_sniper_code,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##################################################
sw_bt_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  sw_force_travel_key_pressed,
	  pavise,
	  danyele_bf2_sniper_code,
	  sw_force_travel_start,
	  sw_animation_triggers, 
	  sw_force_points, 
	  sw_before_mission_start,
	  sw_warcry, 
	  sw_guns,
	  sw_on_item_unwield, 
	  sw_player_check_force_is_usable, 
	  sw_prop_movement_trigger, 
	  wall_of_death_and_jetpack, 
	  sw_prop_movement, 
	  wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,
	  common_asist_system_1,
	  common_asist_system_2,
	  common_asist_system_3,




]
##################################################
sw_fd_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##################################################
sw_iv_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##################################################
sw_duel_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##################################################
sw_hero_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  danyele_bf2_sniper_code,
	  sw_presentations,
	  sw_on_item_drop,
	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
##################################################
sw_bt_rnd_triggers = [

	sw_on_agent_hit,
	  sw_force_requests,
	  sw_kick_replace,
	  sw_occupy,
	  sw_forces,
	  sw_on_item_wielded,
	  sw_on_picked_up,
	  sw_presentations, sw_on_item_drop,
	  danyele_bf2_sniper_code,

	  sw_force_travel_key_pressed,
	  pavise,
	  sw_force_travel_start,
	  sw_animation_triggers, sw_force_points, sw_before_mission_start,sw_warcry, sw_guns, sw_on_item_unwield, sw_player_check_force_is_usable, sw_prop_movement_trigger, wall_of_death_and_jetpack, sw_prop_movement, wall_of_death_and_jetpack,
	  
	  bot_ready_to_shoot,
	  sw_ai,




]
#############################################
#endregion



