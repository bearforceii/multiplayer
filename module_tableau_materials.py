from header_common import *
from ID_animations import *
from header_mission_templates import *
from header_tableau_materials import *
from header_items import *
from module_constants import *

####################################################################################################################
#  Each tableau material contains the following fields:
#  1) Tableau id (string): used for referencing tableaux in other files. The prefix tab_ is automatically added before each tableau-id.
#  2) Tableau flags (int). See header_tableau_materials.py for a list of available flags
#  3) Tableau sample material name (string).
#  4) Tableau width (int).
#  5) Tableau height (int).
#  6) Tableau mesh min x (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  7) Tableau mesh min y (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  8) Tableau mesh max x (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  9) Tableau mesh max y (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  10) Operations block (list): A list of operations. See header_operations.py for reference.
#     The operations block is executed when the tableau is activated.
# 
####################################################################################################################

#banner height = 200, width = 85 with wood, 75 without wood

tableaus = [
  ("game_character_sheet", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 266, 532,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_character_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_character_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       ]),

  ("game_inventory_window", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 180, 270,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_inventory_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_inventory_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       ]),

  ("game_profile_window", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 320, 480, [
    (store_script_param, ":profile_no", 1),
    (assign, ":gender", ":profile_no"),
    (val_mod, ":gender", 2),
    (try_begin),
	  (gt, "$g_presentation_profile_troop", 0),
      (assign, ":troop_no", "$g_presentation_profile_troop"),
	(else_try),
      (eq, ":gender", 0),
      (assign, ":troop_no", "trp_multiplayer_profile_troop_jedi"),
    (else_try),
      (assign, ":troop_no", "trp_multiplayer_profile_troop_female"),
    (try_end),
    (troop_set_face_key_from_current_profile, ":troop_no"),
    (cur_tableau_set_background_color, 0xFF888888),
    (cur_tableau_set_ambient_light, 10,11,15),
    (set_fixed_point_multiplier, 100),
    (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

    (init_position, pos1),
    (position_set_z, pos1, 100),
    (position_set_x, pos1, -20),
    (position_set_y, pos1, -20),
    (cur_tableau_add_tableau_mesh, "tableau_troop_profile_color", ":troop_no", pos1, 0, 0),
    (position_set_z, pos1, 200),
    (cur_tableau_add_tableau_mesh, "tableau_troop_profile_alpha_mask", ":troop_no", pos1, 0, 0),
    ]),

  ("game_party_window", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 300, 300,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_party_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_party_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       ]),

  ("game_troop_label_banner", 0, "tableau_with_transparency", 256, 256, -128, 0, 128, 256,
   [
       (store_script_param, ":banner_mesh", 1),

       (cur_tableau_set_background_color, 0xFF888888),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 100, 100, 0, 100000),

       (init_position, pos1),
       (position_set_y, pos1, 120),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 120, 0),
	   
       #(init_position, pos1),
       #(position_set_z, pos1, 10),
       #(cur_tableau_add_mesh, "mesh_troop_label_banner", pos1, 112, 0),
       ]),
	   
   ("troop_note_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau", ":troop_no"),
       ]),

  ("troop_note_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFC6BB94),
       (cur_tableau_set_ambient_light, 10,11,15),
       (call_script, "script_add_troop_to_cur_tableau", ":troop_no"),
       ]),

  ("troop_character_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_character", ":troop_no"),
       ]),

  ("troop_character_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFE0CFB1),
       (cur_tableau_set_ambient_light, 10,11,15),
       (call_script, "script_add_troop_to_cur_tableau_for_character", ":troop_no"),
       ]),
  
  ("troop_inventory_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_inventory", ":troop_no"),
       ]),

  ("troop_inventory_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF6A583A),
       (cur_tableau_set_ambient_light, 10,11,15),
       (call_script, "script_add_troop_to_cur_tableau_for_inventory", ":troop_no"),
       ]),

  ("troop_profile_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_profile", ":troop_no"),
       ]),

  ("troop_profile_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFF9E7A8),
       (cur_tableau_set_ambient_light, 10,11,15),
       (call_script, "script_add_troop_to_cur_tableau_for_profile", ":troop_no"),
       ]),


  ("troop_party_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_party", ":troop_no"),
       ]),

  ("troop_party_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFBE9C72),
       (cur_tableau_set_ambient_light, 10,11,15),
       (call_script, "script_add_troop_to_cur_tableau_for_party", ":troop_no"),
       ]),

  ("troop_note_mesh", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 350, 350,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_note_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_note_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       (cur_tableau_add_mesh, "mesh_portrait_blend_out", pos1, 0, 0),
       ]),

  ("center_note_mesh", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 200, 200,
   [
       (store_script_param, ":center_no", 1),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       
       (init_position, pos8),
       (position_set_x, pos8, -210),
       (position_set_y, pos8, 200),
       (position_set_z, pos8, 300),
       (cur_tableau_add_point_light, pos8, 550,500,450),

##       (party_get_slot, ":troop_no", ":center_no", slot_town_lord),
##       (try_begin),
##         (ge, ":troop_no", 0),
##         (troop_get_slot, ":banner_spr", ":troop_no", slot_troop_banner_scene_prop),
##         (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
##         (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
##         (val_sub, ":banner_spr", banner_scene_props_begin),
##         (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
##       (try_end),
##
##       (init_position, pos1),
##       (position_set_x, pos1, -60),
##       (position_set_y, pos1, -100),
##       (position_set_z, pos1, 230),
##       (position_rotate_x, pos1, 90),
##       (assign, ":banner_scale", 105),

       (cur_tableau_set_camera_parameters, 1, 10, 10, 10, 10000),

##       (position_set_x, pos1, -100),
       (init_position, pos1),
       (position_set_z, pos1, 0),
       (position_set_z, pos1, -500),


       (init_position, pos1),
       (position_set_y, pos1, -100),
       (position_set_x, pos1, -100),
       (position_set_z, pos1, 100),
       (position_rotate_z, pos1, 200),

##       (cur_tableau_add_mesh, ":banner_mesh", pos1, ":banner_scale", 0),
       (party_get_icon, ":map_icon", ":center_no"),
       (try_begin),
         (ge, ":map_icon", 0),
         (cur_tableau_add_map_icon, ":map_icon", pos1, 0),
       (try_end),

       (init_position, pos5),
       (position_set_x, pos5, -90),
       (position_set_z, pos5, 500),
       (position_set_y, pos5, 480),
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),
       (position_rotate_x, pos5, -35),
       (cur_tableau_set_camera_position, pos5),
       ]),

  ("faction_note_mesh_for_menu", 0, "pic_arms_swadian", 1024, 512, 0, 0, 450, 225,
   [
     (store_script_param, ":faction_no", 1),
     (cur_tableau_set_background_color, 0xFFFFFFFF),
     (set_fixed_point_multiplier, 100),
     (try_begin),
       (is_between, ":faction_no", "fac_kingdom_1", kingdoms_end), #Excluding player kingdom
       (store_add, ":banner_mesh", "mesh_pic_arms_swadian", ":faction_no"),
       (val_sub, ":banner_mesh", "fac_kingdom_1"),
       (init_position, pos1),
       (position_set_y, pos1, -5),
       (position_set_x, pos1, -45),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 160, 80, 0, 100000),
     (try_end),
     ]),

  ("faction_note_mesh", 0, "pic_arms_swadian", 1024, 512, 0, 0, 500, 250,
   [
     (store_script_param, ":faction_no", 1),
     (cur_tableau_set_background_color, 0xFFFFFFFF),
     (set_fixed_point_multiplier, 100),
     (try_begin),
       (is_between, ":faction_no", "fac_kingdom_1", kingdoms_end), #Excluding player kingdom
       (store_add, ":banner_mesh", "mesh_pic_arms_swadian", ":faction_no"),
       (val_sub, ":banner_mesh", "fac_kingdom_1"),
       (init_position, pos1),
       (position_set_y, pos1, -5),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 100, 50, 0, 100000),
     (try_end),
     ]),

  ("faction_note_mesh_banner", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 200, 200,
   [
     (store_script_param, ":faction_no", 1),
     (set_fixed_point_multiplier, 100),
     (try_begin),
       (faction_get_slot, ":leader_troop", ":faction_no", slot_faction_leader),
       (ge, ":leader_troop", 0),
       (troop_get_slot, ":banner_spr", ":leader_troop", slot_troop_banner_scene_prop),
       (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
       (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
       (val_sub, ":banner_spr", banner_scene_props_begin),
       (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
       (init_position, pos1),
       (position_set_y, pos1, 100),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 210, 210, 0, 100000),
     (try_end),
     ]),
  
  ("2_factions_mesh", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 200, 200,
   [
     (store_script_param, ":faction_no", 1),
     (store_mod, ":faction_no_2", ":faction_no", 128),
     (val_div, ":faction_no", 128),
     (val_add, ":faction_no", kingdoms_begin),
     (val_add, ":faction_no_2", kingdoms_begin),
     (set_fixed_point_multiplier, 100),
     (try_begin),
       (faction_get_slot, ":leader_troop", ":faction_no", slot_faction_leader),
       (ge, ":leader_troop", 0),
       (troop_get_slot, ":banner_spr", ":leader_troop", slot_troop_banner_scene_prop),
       (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
       (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
       (val_sub, ":banner_spr", banner_scene_props_begin),
       (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
       (init_position, pos1),
       (position_set_x, pos1, -50),
       (position_set_y, pos1, 100),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 0, 0),
     (try_end),
     (try_begin),
       (faction_get_slot, ":leader_troop", ":faction_no_2", slot_faction_leader),
       (ge, ":leader_troop", 0),
       (troop_get_slot, ":banner_spr", ":leader_troop", slot_troop_banner_scene_prop),
       (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
       (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
       (val_sub, ":banner_spr", banner_scene_props_begin),
       (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
       (init_position, pos1),
       (position_set_x, pos1, 50),
       (position_set_y, pos1, 100),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 0, 0),
     (try_end),
     (cur_tableau_set_camera_parameters, 0, 210, 210, 0, 100000),
     ]),

  ("color_picker", 0, "missiles", 32, 32, 0, 0, 0, 0,
   [
     (store_script_param, ":color", 1),
     (set_fixed_point_multiplier, 100),
     (init_position, pos1),
     (cur_tableau_add_mesh, "mesh_color_picker", pos1, 0, 0),
     (position_move_z, pos1, 1),
     (position_move_x, pos1, -2),
     (position_move_y, pos1, -2),
     (cur_tableau_add_mesh_with_vertex_color, "mesh_white_plane", pos1, 200, 0, ":color"),
     (cur_tableau_set_camera_parameters, 0, 20, 20, 0, 100000),
     ]),

  ("custom_banner_square_no_mesh", 0, "missiles", 512, 512, 0, 0, 300, 300,
   [
     (store_script_param, ":troop_no", 1),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", ":troop_no", 0, 0, 10000, 10000, 9800, 9800, 10000, 10000, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 100, 0, 100000),
     ]),

  ("custom_banner_default", 0, "missiles", 512, 256, 0, 0, 0, 0,
   [
     (store_script_param, ":troop_no", 1),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", ":troop_no", -9, -2, 7450, 19400, 7200, 18000, 9000, 10000, 0),
     (init_position, pos1),
     (position_set_z, pos1, 10),
     (cur_tableau_add_mesh, "mesh_tableau_mesh_custom_banner", pos1, 0, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 200, 0, 100000),
     ]),

  ("custom_banner_tall", 0, "missiles", 512, 256, 0, 0, 0, 0,
   [
     (store_script_param, ":troop_no", 1),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", ":troop_no", -9, 12, 8250, 18000, 8000, 21000, 10000, 10000, 0),
     (init_position, pos1),
     (position_set_z, pos1, 10),
     (cur_tableau_add_mesh, "mesh_tableau_mesh_custom_banner", pos1, 0, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 200, 0, 100000),
     ]),

  ("custom_banner_square", 0, "missiles", 256, 256, 0, 0, 0, 0,
   [
     (store_script_param, ":troop_no", 1),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", ":troop_no", -11, 10, 7700, 7700, 7500, 7500, 8300, 10000, 0),
     (init_position, pos1),
     (position_set_z, pos1, 10),
     (cur_tableau_add_mesh, "mesh_tableau_mesh_custom_banner_square", pos1, 0, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 100, 0, 100000),
     ]),

  ("custom_banner_short", 0, "missiles", 256, 512, 0, 0, 0, 0,
   [
     (store_script_param, ":troop_no", 1),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", ":troop_no", -10, 0, 8050, 5000, 4200, 4800, 6600, 10000, 0),
     (init_position, pos1),
     (position_set_z, pos1, 10),
     (cur_tableau_add_mesh, "mesh_tableau_mesh_custom_banner_short", pos1, 0, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 50, 0, 100000),
     ]),

##  ("custom_banner", 0, "missiles", 256, 512, 0, 0, 0, 0,
##   [
##     (store_script_param, ":troop_no", 1),
##
##     (set_fixed_point_multiplier, 100),
##     (troop_get_slot, ":bg_type", ":troop_no", slot_troop_custom_banner_bg_type),
##     (val_add, ":bg_type", custom_banner_backgrounds_begin),
##     (troop_get_slot, ":bg_color_1", ":troop_no", slot_troop_custom_banner_bg_color_1),
##     (troop_get_slot, ":bg_color_2", ":troop_no", slot_troop_custom_banner_bg_color_2),
##     (troop_get_slot, ":num_charges", ":troop_no", slot_troop_custom_banner_num_charges),
##     (troop_get_slot, ":positioning", ":troop_no", slot_troop_custom_banner_positioning),
##     (call_script, "script_get_troop_custom_banner_num_positionings", ":troop_no"),
##     (assign, ":num_positionings", reg0),
##     (val_mod, ":positioning", ":num_positionings"),
##
##     (init_position, pos1),
##
##     (position_move_z, pos1, -10),
##     (cur_tableau_add_mesh_with_vertex_color, ":bg_type", pos1, 0, 0, ":bg_color_1"),
##     (position_move_z, pos1, -10),
##     (cur_tableau_add_mesh_with_vertex_color, "mesh_custom_banner_bg", pos1, 0, 0, ":bg_color_2"),
##     (init_position, pos1),
##     (position_move_z, pos1, -50),
##     (cur_tableau_add_mesh, "mesh_tableau_mesh_custom_banner", pos1, 0, 0),
##
##     (call_script, "script_get_custom_banner_charge_type_position_scale_color", "trp_player", ":positioning"),
##     (try_begin),
##       (ge, ":num_charges", 1),
##       (cur_tableau_add_mesh_with_vertex_color, reg0, pos0, reg1, 0, reg2),
##     (try_end),
##     (try_begin),
##       (ge, ":num_charges", 2),
##       (cur_tableau_add_mesh_with_vertex_color, reg3, pos1, reg4, 0, reg5),
##     (try_end),
##     (try_begin),
##       (ge, ":num_charges", 3),
##       (cur_tableau_add_mesh_with_vertex_color, reg6, pos2, reg7, 0, reg8),
##     (try_end),
##     (try_begin),
##       (ge, ":num_charges", 4),
##       (cur_tableau_add_mesh_with_vertex_color, reg9, pos3, reg10, 0, reg11),
##     (try_end),
##
##     (cur_tableau_set_camera_parameters, 0, 100, 200, 0, 100000),
##     ]),

  ("background_selection", 0, "missiles", 512, 512, 0, 0, 100, 100,
   [
     (store_script_param, ":banner_bg", 1),
     (troop_get_slot, ":old_bg", "trp_player", slot_troop_custom_banner_bg_type),
     (troop_set_slot, "trp_player", slot_troop_custom_banner_bg_type, ":banner_bg"),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", "trp_player", 0, 0, 10000, 10000, 9800, 9800, 10000, 10000, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 100, 0, 100000),
     (troop_set_slot, "trp_player", slot_troop_custom_banner_bg_type, ":old_bg"),
     ]),

  ("positioning_selection", 0, "missiles", 512, 512, 0, 0, 100, 100,
   [
     (store_script_param, ":positioning", 1),
     (troop_get_slot, ":old_positioning", "trp_player", slot_troop_custom_banner_positioning),
     (troop_set_slot, "trp_player", slot_troop_custom_banner_positioning, ":positioning"),
     (set_fixed_point_multiplier, 100),
     (call_script, "script_draw_banner_to_region", "trp_player", 0, 0, 10000, 10000, 9800, 9800, 10000, 10000, 0),
     (cur_tableau_set_camera_parameters, 0, 100, 100, 0, 100000),
     (troop_set_slot, "trp_player", slot_troop_custom_banner_positioning, ":old_positioning"),
     ]),

  
  ("retired_troop_alpha_mask", 0, "mat_troop_portrait_mask", 2048, 2048, 0, 0, 600, 600,
   [
       (store_script_param, ":type", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 10,11,15),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_retirement", ":type"),
       ]),

  ("retired_troop_color", 0, "mat_troop_portrait_color", 2048, 2048, 0, 0, 600, 600,
   [
       (store_script_param, ":type", 1),
       (cur_tableau_set_background_color, 0xFFe7d399),
       (cur_tableau_set_ambient_light, 10,11,15),
       (call_script, "script_add_troop_to_cur_tableau_for_retirement", ":type"),
       ]),

  ("retirement_troop", 0, "tableau_with_transparency", 2048, 2048, 0, 0, 600, 600,
   [
     (store_script_param, ":type", 1),
     (cur_tableau_set_background_color, 0xFF888888),
     (cur_tableau_set_ambient_light, 10,11,15),
     (set_fixed_point_multiplier, 100),
     (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

     (init_position, pos1),
     (position_set_z, pos1, 100),
     (position_set_x, pos1, -20),
     (position_set_y, pos1, -20),
     (cur_tableau_add_tableau_mesh, "tableau_retired_troop_color", ":type", pos1, 0, 0),
     (position_set_z, pos1, 200),
     (cur_tableau_add_tableau_mesh, "tableau_retired_troop_alpha_mask", ":type", pos1, 0, 0),
     ]),
	 
#sw
  ("jedicloth", 0, "cloth_1_sample", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),
	   (call_script, "script_color_get_part", ":color", 0),
	   (assign, ":color1", reg0),
#	   (try_begin),
#	     (eq, ":faction", "fac_kingdom_2"),#Seperatists
#		 (assign, ":color1", 0xFF222222),
#	   (try_end),
	   (call_script, "script_color_get_part", ":color", 1),
	   (assign, ":color2", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh_with_vertex_color, "mesh_tableau_cloth_1_color2", pos1, 0, 0, ":color2"),
       (position_set_z, pos1, 8),
       (cur_tableau_add_mesh, "mesh_tableau_cloth_1_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -7),
	   #(position_set_y, pos1, -10),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1700, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("mandalorian_head", 0, "mandalorian_head_sample", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
	   (call_script, "script_color_get_part", ":color", 2),
	   (assign, ":color2", reg0),
       (cur_tableau_set_background_color, ":color2"),
	   (init_position, pos1),
       (cur_tableau_add_mesh_with_vertex_color, "mesh_tableau_mandalorian_head_color2", pos1, 0, 0, ":color1"),
       (position_set_z, pos1, 8),
       (cur_tableau_add_mesh, "mesh_tableau_mandalorian_head_diffuse", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("mandalorian_body", 0, "mandalorian_body_sample", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
	   
	   #(call_script, "script_color_get_part", ":color", 2),
	   #(assign, ":color2", reg0),
       #(cur_tableau_set_background_color, ":color2"),
	   
       (cur_tableau_set_background_color, ":color1"), #new
	   
	   (init_position, pos1),
       #(cur_tableau_add_mesh_with_vertex_color, "mesh_tableau_mandalorian_body_color2", pos1, 0, 0, ":color1"),
       (position_set_z, pos1, 8),
       (cur_tableau_add_mesh, "mesh_tableau_mandalorian_body_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, 74),
	   #(position_set_y, pos1, 92),
	   #(position_rotate_z, pos1, 270),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("battledroid", 0, "battledroid", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_battledroid_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, 64),
	   #(position_set_y, pos1, -50),
	   #(position_rotate_z, pos1, 180),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1400, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("droid_armor_hardened", 0, "battledroid_hardened", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_battledroid_hardened_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, 64),
	   #(position_set_y, pos1, -50),
	   #(position_rotate_z, pos1, 180),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1400, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),     
  ("armoured_robes", 0, "jedi_armour_new", 2048, 2048, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_armoured_robes", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),		 	   
  ("padawan_robes", 0, "padawan_armour_new", 2048, 2048, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_padawan_robes", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),		 	   
  ("stormarmour", 0, "stormtrooper_armour", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_stormarmor", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	
  ("stormarmour_camo", 0, "stormtrooper_armour_camo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_stormarmor_camo", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),		 	   
  ("rebel_armour", 0, "rebel_troop", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_rebel_troop", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	   
  ("rebel_officer_armour", 0, "second_rebel_troop", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_rebel_troop_officer", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	   	   
  ("rebel_helmet", 0, "rebel_troop_helmet", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_rebel_helmet", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  
  ("rebel_helmet_officer", 0, "second_rebel_troop_helmet", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_rebel_helmet_officer", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 
  ("arf_helmet", 0, "arftrooper", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arf_helmet", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 	   
  ("arf_helmet_camo", 0, "arftrooper_camo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arf_helmet_camo", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 	   
  ("arf_helmet_desert", 0, "arftrooper_desert", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arf_helmet_desert", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 	   
  ("rebel_heavy_armour", 0, "republic_trooper_armor_vest", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_rebel_heavy_armour", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	   	   	   
  ("havoc_armor", 0, "Armor_Diffuse", 2048, 2048, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_havoc_armor", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	
  ("havoc_helmet", 0, "Helmet_Diffuse", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_havoc_helmet", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 
  ("havoc_armor_camo", 0, "Armor_Diffuse_Camo", 2048, 2048, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_havoc_armor_camo", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	
  ("havoc_helmet_camo", 0, "Helmet_Diffuse_Camo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_havoc_helmet_camo", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   
  ("commandoarmour", 0, "clonecommando_armor", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_commandoarmour_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  
  ("commandohelmet", 0, "clonecommando_helm", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_commandohelmet_diffuse", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("Sith_trooper_body", 0, "imp_troop", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_imp_troop", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 
  ("Sith_trooper_body.1", 0, "imp_helmet", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
	   (cur_tableau_add_mesh, "mesh_tableau_imp_helmet", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	   
  ("sith_acolyte", 0, "sith_eradicator_new3", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_sith_eradicator_new3", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("jedi_armor", 0, "OR_jedi_armor", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_OR_jedi_armor", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  
  ("CloneSnowAssaultTrooperSuit", 0, "clone_cold_armour", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_clone_cold_armour", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	  	   
  ("CloneSnowAssaultTrooperHelmet_pos", 0, "clone_cold_helmet", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_clone_cold_helmet", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),  	   
  ("sith_trooper_helmet", 0, "imp_helmet", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_imp_helmet", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	 	   
  ("arctrooper", 0, "ArcTrooperWhite", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_ArcTrooper_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("arctrooper_camo", 0, "ArcTrooperCamo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_ArcTrooperCamo_diffuse", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   
  ("arctrooper_desert", 0, "ArcTrooperDesert", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_ArcTrooper_desert", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   	   
  ("arctrooper_helm", 0, "ArcTrooperHelmWhite", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_ArcTrooperHelm_diffuse", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("arctrooper_helm_camo", 0, "ArcTrooperHelmCamo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_ArcTrooperHelmCamo_diffuse", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),   
  ("arctrooper_helm_desert", 0, "ArcTrooperHelmDesert", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_ArcTrooperHelm_desert", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),   
  ("paratrooper_helm", 0, "h_rep_sniper_blue", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_paratrooper_helm", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   		 
  ("paratrooper_helm_camo", 0, "h_rep_sniper_blue_camo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_paratrooper_helm_camo", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   	
  ("paratrooper_helm_desert", 0, "h_rep_sniper_blue_desert", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_paratrooper_helm_desert", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   	   
("arctrooper2", 0, "arc_clone_armor", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arctrooper2", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),
  ("arctrooper2_camo", 0, "arc_clone_armor_camo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arctrooper2_camo", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   
  ("arctrooper2_desert", 0, "arc_clone_armor_desert", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arctrooper2_desert", pos1, 0, 0),
       #(position_set_z, pos1, 10),
	   #(position_set_x, pos1, -94),
	   #(position_set_y, pos1, -59),
	   #(position_rotate_z, pos1, -15),
	   #(call_script, "script_color_get_component", ":color", 4, 0),
	   #(store_add, ":decal", reg0, "mesh_decal0"),
       #(cur_tableau_add_mesh, ":decal", pos1, 1000, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),	   	   	   
	("arctrooper2_helm", 0, "arc_clone_helmet", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arc_clone_helmet", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),   
	("arctrooper2_helm_desert", 0, "arc_clone_helmet_desert", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arc_clone_helmet_desert", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),   
	("arctrooper2_helm_camo", 0, "arc_clone_helmet_camo", 1024, 1024, 0, 0, 0, 0,
   [
       (store_script_param, ":color", 1),

       (set_fixed_point_multiplier, 100),

	   (call_script, "script_color_get_part", ":color", 0),#_Sebastian_ fix
	   (assign, ":color1", reg0),
       (cur_tableau_set_background_color, ":color1"),
	   (init_position, pos1),
       (cur_tableau_add_mesh, "mesh_tableau_arc_clone_helmet_camo", pos1, 0, 0),
       (cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
       ]),   	   
]
